<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>FT PROYECTO</title>
        <script type="text/javascript" src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>
        <script>
            function subst() {
                    var lastpage = 0;
                    var currentpage = 0;
                var vars = {};
                var query_strings_from_url = document.location.search.substring(1).split('&');
                for (var query_string in query_strings_from_url) {
                    if (query_strings_from_url.hasOwnProperty(query_string)) {
                        var temp_var = query_strings_from_url[query_string].split('=', 2);
                        vars[temp_var[0]] = decodeURI(temp_var[1]);
                        if (temp_var[0] == 'topage') {
                            lastpage = decodeURI(temp_var[1]);
                        }
                        if (temp_var[0] == 'page') {
                            currentpage = decodeURI(temp_var[1]);
                        }
                    }
                }
                var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages','tipoImpresion'];
                for (var css_class in css_selector_classes) {
                    if (css_selector_classes.hasOwnProperty(css_class)) {
                        var element = document.getElementsByClassName(css_selector_classes[css_class]);
                        for (var j = 0; j < element.length; ++j) {
                            element[j].textContent = vars[css_selector_classes[css_class]];
                        }
                    }
                }
            
                    $('#txtTotal').hide();
                    $('#txtPasa').show();
                    if (currentpage == lastpage) {
                        $('#txtTotal').show();
                        $('#txtPasa').hide();
                    }
            }
        </script>
        <style>
            body
            {
                margin:     0;
                padding:    0;
                color:black
            }
            table
            {
                table-layout: fixed;
            }
            thead { display: table-header-group }
            tfoot { display: table-row-group }
            tr { page-break-inside: avoid }
            .bordes {
                border: thin solid rgba(0,0,0,1);
            }
            .bordesLeftRightDown {
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-width: thin;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesDet2 {
                border-left-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-left-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-left-width: thin;
            }
            .bordesDet1 {
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-right-style: solid;
                border-left-width: thin;
                border-right-width: thin;
            }
            .bordesDet {
                border-left-color: rgba(255,255,255,1);
                border-right-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-width: thin;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesLeftRightUpDown {
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-top-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-right-style: solid;
                border-top-style: solid;
                border-bottom-style: solid;
                border-left-width: thin;
                border-right-width: thin;
                border-top-width: thin;
                border-bottom-width: thin;
            }
            .bordesLeftDown {
                border-left-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-bottom-style: solid;
                border-left-width: thin;
                border-bottom-width: thin;
            }
            .bordesRightDown {
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-right-style: solid;
                border-bottom-style: solid;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesDer1 {
                border-right-color: rgba(255,255,255,1);
                border-right-color: rgba(0,0,0,1);
                border-right-style: solid;
                border-right-width: thin;
            }
            .bordesDer {
                border-right-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-right-style: solid;
                border-bottom-style: solid;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesDetHead1 {
                border-top-color: rgba(255,255,255,1);
                border-left-color: rgba(255,255,255,1);
                border-right-color: rgba(255,255,255,1);
                border-top-color: rgba(0,0,0,1);
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-top-style: solid;
                border-left-style: solid;
                border-right-style: solid;
                border-top-width: thin;
                border-left-width: thin;
                border-right-width: thin;
            }
            .bordesUpDown {
                border-top-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-top-style: solid;
                border-bottom-style: solid;
                border-top-width: thin;
                border-bottom-width: thin;
            }
            .bordesDown {
                border-bottom-color: rgba(0,0,0,1);
                border-bottom-style: solid;
                border-bottom-width: thin;
            }
            .bordesDetHead {
                border-top-color: rgba(255,255,255,1);
                border-left-color: rgba(255,255,255,1);
                border-right-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-top-color: rgba(0,0,0,1);
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-top-style: solid;
                border-left-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-top-width: thin;
                border-left-width: thin;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesDerHead {
                border-top-color: rgba(255,255,255,1);
                border-right-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-top-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-top-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-top-width: thin;
                border-right-width: thin;
                border-bottom-width: thin;
            }
        </style>
    </head>

    @foreach ($proyecto as $item)
        <?php

            $poadescripcion = $item['proydesc'];
            $programapenia =  $item['proinvpendes'];
            $lineapenia =  $item['lininvpendes'];
            $programapei =  $item['proinvpeides'];
            $lineapei =  $item['lininvpeides'];
            $accionpicia =  $item['aretemnom'];
            $ciunom =  $item['ciunom'];
            $antecedentes = $item['proyant'];
            $justificacionimportancia = $item['proyjusimp'];
            $justificacionpertenencia = $item['proyjusper'];
            $justificacionimpacto = $item['proyjusipa'];
            $objetivogeneral = $item['proyobjgen'];
            $objetivosespecificos = $item['proyobjesp'];
            $actores = $item['proyact'];
            $beneficiarios = $item['proyben'];

            $tiempo = $item['convtieeje'];
            $fuentefinanciacion = $item['convfuefin'];  
            $valortotal = $item['convvaltot'];    
            $valorcofinanciado = $item['convvalcof'];
            $totaldinero = $item['convvaldin'];
            $totalespecie = $item['convtotesp'];
            $coordinador = $item['nombrecoordinador'];    
            $perscaf = $item['perscaf'];
            $dircaf = $item['dircaf'];
            $telcaf = $item['telcaf'];
            
            $persaff = $item['persaff'];
            $diraff = $item['diraff'];
            $telaff = $item['telaff'];

            $persttf = $item['persttf'];  
            $dirttf = $item['dirttf'];
            $telttf = $item['telttf'];        
        ?>
    @endforeach

    <body>

        <table style="font-family: Arial, Georgia, Serif; font-size: 10pt; color: #000000;" cellpadding="0" cellspacing="0" width="100%" border="1">
            <tr>
                <td style="width:40%">
                    FECHA
                </td>
                <td style="width:60%">
                    {{ date('d-M-Y') }}
                </td>    
            </tr>
            <tr>
                <td>
                    PROYECTO/ACCIÓN INSTITUCIONAL 
                </td>
                <td>
                    {{ $poadescripcion }}
                </td>    
            </tr> 
            <tr>
                <td>
                    PROGRAMA PENIA
                </td>
                <td>
                    {{ $programapenia }}
                </td>    
            </tr>
            <tr>
                <td>
                    LÍNEA PENIA
                </td>
                <td>
                    {{ $lineapenia }}
                </td>    
            </tr>
            <tr>
                <td>
                    PROGRAMA PEI
                </td>
                <td>
                    <table class="split_this_table" style="font-family: Arial, Georgia, Serif;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" border="0">
                        
                        <tbody>
                            
                            @foreach ($programaspei as $item)
                                <tr>
                                    <td align="left" style="padding-left: 1mm;padding-bottom: 0.5mm;" >{{ $item['proinvpeides'] }}</td>                                    
                                </tr>
                            @endforeach 
                            
                            <tr>
                                <td align="center" style="height: 1mm;font-size:7pt;"></td>                                                                                   
                            </tr>
                        </tbody>
                    </table>
                </td>    
            </tr>
            <tr>
                <td>
                    LÍNEA PEI
                </td>
                <td>
                    <table class="split_this_table" style="font-family: Arial, Georgia, Serif;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" border="0">
                        
                        <tbody>
                            
                            @foreach ($lininvpei as $item)
                                <tr>
                                    <td align="left" style="padding-left: 1mm;padding-bottom: 0.5mm;" >{{ $item['lininvpeides'] }}</td>                                    
                                </tr>
                            @endforeach 
                            
                            <tr>
                                <td align="center" style="height: 1mm;font-size:7pt;"></td>                                                                                   
                            </tr>
                        </tbody>
                    </table>
                </td>    
            </tr>
            <tr>
                <td>
                    ACCIÓN PICIA
                </td>
                <td>
                    <table class="split_this_table" style="font-family: Arial, Georgia, Serif;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" border="0">
                        
                        <tbody>
                            
                            @foreach ($accionespicia as $item)
                                <tr>
                                    <td align="left" style="padding-left: 1mm;padding-bottom: 0.5mm;" >{{ $item['accpicdes'] }}</td>                                    
                                </tr>
                            @endforeach 
                            
                            <tr>
                                <td align="center" style="height: 1mm;font-size:7pt;"></td>                                                                                   
                            </tr>
                        </tbody>
                    </table>
                </td>    
            </tr>   
        </table>
        
        <div>
            <p><b>Localización Geográfica:</b><br> {{ $ciunom }}</p>

            <p><b>Antecedentes:</b><br> {{ $antecedentes }}</p>

            <p><b>Justificación:
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Importancia:</b> {{ $justificacionimportancia }}
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Pertinencia:</b> {{ $justificacionpertenencia }}
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Impacto:</b> {{ $justificacionimpacto }}
            </p>

            <p><b>Objetivo General:</b><br> {{ $objetivogeneral }}</p>

            <p><b>Objetivos Específicos:</b><br> 
                <table class="split_this_table" style="font-family: Arial, Georgia, Serif;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" border="0">
                    
                    <thead>
                        <tr>
                            <td align="center" class="bordesDetHead" style="font-size:7pt;width: 100%;"><strong>DESCRIPCION</strong></td>                                                 
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($objetivos as $item)
                            <tr>
                                <td align="left" class="bordesDet1" style="padding-left: 1mm;padding-bottom: 0.5mm;font-size:9pt;" >{{ $item['proyobjdes'] }}</td>                                
                            </tr>
                        @endforeach 
                        
                        <tr>
                            <td align="center" class="bordesLeftRightDown" style="height: 1mm;font-size:7pt;"></td>                            
                            <td align="center" class="bordesDer" style="height: 1mm;font-size:7pt;"></td>                           
                        </tr>
                    </tbody>
                </table>
            </p>

            <p><b>Actores:</b><br> {{ $actores }}</p>

            <p><b>Beneficiarios:</b><br> {{ $beneficiarios }}</p>

            <p><b>Tiempo de Ejecución:
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duración:</b> {{ $tiempo }}
                
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Cronograma de Actividades:</b>
                <br><br>

                <table class="split_this_table" style="font-family: Arial, Georgia, Serif;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <td align="center" class="bordesDetHead" style="font-size:7pt;width: 60%;"><strong>DESCRIPCION</strong></td>
                            <td align="center" class="bordesDerHead" style="font-size:7pt;width: 20%;"><strong>FECHA INICIO</strong></td>
                            <td align="center" class="bordesDetHead" style="font-size:7pt;width: 20%;"><strong>FECHA FINAL</strong></td>                       
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($actividades as $actividad)
                            <tr>
                                <td align="left" class="bordesDet1" style="padding-left: 1mm;padding-bottom: 0.5mm;font-size:7pt;" >{{ $actividad['proyactdes'] }}</td>
                                <td align="center" class="bordesDer1" style="padding-bottom: 0.5mm;font-size:7pt;">{{ $actividad['proyactfecini'] }}</td>
                                <td align="right" class="bordesDer1" style="padding-bottom: 0.5mm;padding-right: 1mm;font-size:7pt;">{{ $actividad['proyactfecfin'] }}</td>                            
                            </tr>
                        @endforeach 
                        
                        <tr>
                            <td align="center" class="bordesLeftRightDown" style="height: 1mm;font-size:7pt;"></td>
                            <td align="center" class="bordesDer" style="height: 1mm;font-size:7pt;"></td>
                            <td align="center" class="bordesDer" style="height: 1mm;font-size:7pt;"></td>
                            <td align="center" class="bordesDer" style="height: 1mm;font-size:7pt;"></td>                           
                        </tr>
                    </tbody>
                </table>                
            </p>

            <p><b>Costos:
                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fuentes de Financiación:</b> {{ $fuentefinanciacion }}
                <br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor Total:</b> {{ '$ '.number_format ( $valortotal , 0 , "," , "." ) }}
                <br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor Cofinanciado:</b> {{ '$ '.number_format ( $valorcofinanciado , 0 , "," , "." ) }}
                <br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor Contrapartida SINCHI:</b>
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Dinero: </b> {{ '$ '.number_format ( $totaldinero , 0 , "," , "." ) }}
                    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Especie: </b> {{ '$ '.number_format ( $totalespecie , 0 , "," , "." ) }}
            </p>        
            
            <p>
                <b>Equipo Propuesto para la Ejecución</b>
                <br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Investigador Responsable:</b> {{ $coordinador }}
                <br><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Equipo Técnico:</b>                               
                    @foreach ($equiposinchi as $equipo)
                        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        {{ $equipo->persnom }}                                              
                    @endforeach 
            </p>
            
            <P>
                <b>Contacto administrativo y financiero SINCHI:</b><br>{{ $perscaf. ' Dir: '. $dircaf . ' Tel: '. $telcaf}}
            </P>
            
            <p>
                <b>Contacto administrativo en la fuente de Financiación:</b><br>{{ $persaff. ' Dir: '. $diraff . ' Tel: '. $telaff }}
            </p>
            
            <p>
                <b>Contacto técnico en la fuente de Financiación:</b><br>{{ $persttf. ' Dir: '. $dirttf . ' Tel: '. $telttf }}
            </p>
        </div>        
    </body>
</html>