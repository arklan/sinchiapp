<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>PI PROYECTO</title>
        <script type="text/javascript" src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>
        <script>
            function subst() {
                    var lastpage = 0;
                    var currentpage = 0;
                var vars = {};
                var query_strings_from_url = document.location.search.substring(1).split('&');
                for (var query_string in query_strings_from_url) {
                    if (query_strings_from_url.hasOwnProperty(query_string)) {
                        var temp_var = query_strings_from_url[query_string].split('=', 2);
                        vars[temp_var[0]] = decodeURI(temp_var[1]);
                        if (temp_var[0] == 'topage') {
                            lastpage = decodeURI(temp_var[1]);
                        }
                        if (temp_var[0] == 'page') {
                            currentpage = decodeURI(temp_var[1]);
                        }
                    }
                }
                var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages','tipoImpresion'];
                for (var css_class in css_selector_classes) {
                    if (css_selector_classes.hasOwnProperty(css_class)) {
                        var element = document.getElementsByClassName(css_selector_classes[css_class]);
                        for (var j = 0; j < element.length; ++j) {
                            element[j].textContent = vars[css_selector_classes[css_class]];
                        }
                    }
                }
            
                    $('#txtTotal').hide();
                    $('#txtPasa').show();
                    if (currentpage == lastpage) {
                        $('#txtTotal').show();
                        $('#txtPasa').hide();
                    }
            }
        </script>
        <style>
            body
            {
                margin:     0;
                padding:    0;
                color:black
            }
            table
            {
                table-layout: fixed;
            }
            thead { display: table-header-group }
            tfoot { display: table-row-group }
            tr { page-break-inside: avoid }
            .bordes {
                border: thin solid rgba(0,0,0,1);
            }
            .bordesLeftRightDown {
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-width: thin;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesDet2 {
                border-left-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-left-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-left-width: thin;
            }
            .bordesDet1 {
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-right-style: solid;
                border-left-width: thin;
                border-right-width: thin;
            }
            .bordesDet {
                border-left-color: rgba(255,255,255,1);
                border-right-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-width: thin;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesLeftRightUpDown {
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-top-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-right-style: solid;
                border-top-style: solid;
                border-bottom-style: solid;
                border-left-width: thin;
                border-right-width: thin;
                border-top-width: thin;
                border-bottom-width: thin;
            }
            .bordesLeftDown {
                border-left-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-left-style: solid;
                border-bottom-style: solid;
                border-left-width: thin;
                border-bottom-width: thin;
            }
            .bordesRightDown {
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-right-style: solid;
                border-bottom-style: solid;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesDer1 {
                border-right-color: rgba(255,255,255,1);
                border-right-color: rgba(0,0,0,1);
                border-right-style: solid;
                border-right-width: thin;
            }
            .bordesDer {
                border-right-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-right-style: solid;
                border-bottom-style: solid;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesDetHead1 {
                border-top-color: rgba(255,255,255,1);
                border-left-color: rgba(255,255,255,1);
                border-right-color: rgba(255,255,255,1);
                border-top-color: rgba(0,0,0,1);
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-top-style: solid;
                border-left-style: solid;
                border-right-style: solid;
                border-top-width: thin;
                border-left-width: thin;
                border-right-width: thin;
            }
            .bordesUpDown {
                border-top-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-top-style: solid;
                border-bottom-style: solid;
                border-top-width: thin;
                border-bottom-width: thin;
            }
            .bordesDown {
                border-bottom-color: rgba(0,0,0,1);
                border-bottom-style: solid;
                border-bottom-width: thin;
            }
            .bordesDetHead {
                border-top-color: rgba(255,255,255,1);
                border-left-color: rgba(255,255,255,1);
                border-right-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-top-color: rgba(0,0,0,1);
                border-left-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-top-style: solid;
                border-left-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-top-width: thin;
                border-left-width: thin;
                border-right-width: thin;
                border-bottom-width: thin;
            }
            .bordesDerHead {
                border-top-color: rgba(255,255,255,1);
                border-right-color: rgba(255,255,255,1);
                border-bottom-color: rgba(255,255,255,1);
                border-top-color: rgba(0,0,0,1);
                border-right-color: rgba(0,0,0,1);
                border-bottom-color: rgba(0,0,0,1);
                border-top-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-top-width: thin;
                border-right-width: thin;
                border-bottom-width: thin;
            }
        </style>
    </head>

    @foreach ($proyecto as $item)
        <?php

            $convenio = $item['convnum'];
            $nombreconvenio = $item['convnom'];
            $valortotal = $item['convvaltot'];
            $descripcion = $item['proydesc'];
            $nombre = $item['proynomcor'];
            $area = $item['arenom'];
            $tipo = $item['proytip'];
            $fechainicio = $item['convfecsus'];
            list($annoinicio,$mesinicio, $díainicio) = explode('-', $fechainicio);
            
            $fechaincorporacion = $item['convfecinc'];
            list($annoincorpora,$mesincorpora, $díaincorpora) = explode('-', $fechaincorporacion);
            
            $fechaterminacion = $item['convfectereje'];
            list($annoterminacion,$mesterminacion, $díaterminacion) = explode('-', $fechaterminacion);
            
            $fuentefinanciacion = $item['convfuefin'];
            $cofinanciado = $item['convvalcof'];
            $contrapartida = $item['convvalctp'];    
            $dinero = $item['convvaldin']; 
            $especie = $item['convtotesp']; 
            
            $estrategia = $item['poaestr'];
            $programapei =  $item['proinvpeides'];
            $objetivogeneral = $item['proyobjgen'];

            $moneda = $item['monsim'];

            /*
            $estrategia = $item['poaestr'];
            $programapenia =  $item['proinvpendes'];
            $lineapenia =  $item['lininvpendes'];
            $programapei =  $item['proinvpeides'];
            $lineapei =  $item['lininvpeides'];
            $accionpicia =  $item['aretemnom'];
            $ciunom =  $item['ciunom'];
            $antecedentes = $item['proyant'];
            $justificacion = $item['proyjus'];
            $objetivogeneral = $item['proyobjgen'];
            $objetivosespecificos = $item['proyobjesp'];
            $actores = $item['proyact'];
            $beneficiarios = $item['proyben'];  
            $tiempo = $item['proytieeje'];
            $fuentefinanciacion = $item['proyfuefin'];  
            $valortotal = $item['proyvaltot'];    
            $valorcofinanciado = $item['proyvalcof'];
            $totaldinero = $item['proyvaldin'];
            $totalespecie = $item['proytotesp'];
            $coordinador = $item['nombrecoordinador'];    
            $perscaf = $item['perscaf'];
            $dircaf = $item['dircaf'];
            $telcaf = $item['telcaf'];
            $persaff = $item['persaff'];
            $persttf = $item['persttf'];   */ 
            
            $fechaini = new DateTime($fechainicio);
            $fechafin = new DateTime($fechaterminacion);

            $diferencia = $fechaini->diff($fechafin);

            $meses = ( $diferencia->y * 12 ) + $diferencia->m;
        ?>
    @endforeach

    <body>                
        <table style="width:100%;border-spacing: 5px 15px;" cellpadding="0" cellspacing="0" >

            <tr>
                <td>
                    I. Programa al que se circunscribe el Proyecto
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%;border-spacing:5px;">                            
                        
                        @foreach ($programaspei as $item)                           

                            <tr>
                                <td style="background:#C2C2C2">
                                    {{ $item['proinvpeides'] }}
                                </td>                               
                            </tr>    
                        @endforeach
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    II. Nombre del Proyecto
                </td>
            </tr>
            <tr>    
                <td style="background:#C2C2C2">
                    {{ $descripcion }}
                </td> 
            </tr>

             <tr>
                <td>
                    III. Objetivos
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width:100%;border-spacing:5px;">
                        <tr>
                            <td style="width:30%;background:#C2C2C2;">
                                Objetivo General
                            </td>
                            <td style="width:70%;background:#C2C2C2;">
                                {{ $objetivogeneral }}
                            </td>    
                        </tr>

                        <tr>
                            <td colspan="2" style="background:#C2C2C2;text-align:center">
                                OBJETIVOS ESPECIFICOS
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="background:#C2C2C2">
                                <table style="width: 100%">
                                    @foreach ($objetivos as $item)
                                        <?php                                              
                                            $objdescripcion = $item['proyobjdes'];
                                            $numero = $item['proyobjcod'];                                            
                                        ?>

                                        <tr>
                                            <td>
                                                {{ $numero.'. '.$objdescripcion }}
                                            </td>    
                                        </tr>    
                                    @endforeach
                                </table>    
                            </td>    
                        </tr>
                    </table>    
                </td>
            </tr> 

            <tr>
                <td>
                    IV. Duración del Proyecto
                </td>
            </tr>
            <tr>    
                <td>

                    <table style="width: 100%;">
                        <tr>
                            <td style="background:#C2C2C2;text-align:center">
                                TIEMPO (meses)
                            </td>
                            <td style="background:#C2C2C2;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">FECHA DE INICIO</td>
                                    </tr>
                                    
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">dia/ mes / año</td>
                                    </tr>    
                                </table>   
                            </td>
                            <td style="background:#C2C2C2;">

                                <table style="width: 100%;">
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">FECHA DE TERMINACION</td>
                                    </tr>
                                    
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">dia/ mes / año</td>
                                    </tr>    
                                </table>   

                            </td>     
                        </tr>
                        
                        <tr>
                            <td style="background:#C2C2C2;text-align:center">
                                {{ $meses }}
                            </td>
                            
                            <td style="background:#C2C2C2;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">{{ $díainicio }}</td>
                                        <td style="background:#C2C2C2;text-align:center">{{ $mesinicio }}</td>
                                        <td style="background:#C2C2C2;text-align:center">{{ $annoinicio }}</td>
                                    </tr>    
                                </table>    
                            </td>
                            
                            <td style="background:#C2C2C2;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">{{ $díaterminacion }}</td>
                                        <td style="background:#C2C2C2;text-align:center">{{ $mesterminacion }}</td>
                                        <td style="background:#C2C2C2;text-align:center">{{ $annoterminacion }}</td>
                                    </tr>    
                                </table>    
                            </td> 
                        </tr>    
                    </table>
                </td> 
            </tr>

            <tr>
                <td>
                    V. Productos Esperados de este trabajo intelectual (numérico)
                </td>
            </tr>
            <tr>    
                <td>   
                                        
                    <table style="width: 100%;border-spacing:0;" border="1">
                        <thead style="background:#943A4A;color:white;">
                            <td>1. DERECHOS DE AUTOR (Producción técnica, científica, literaria)</td>                           
                        </thead>    
                        <?php $numproducto=0; ?>
                        @foreach ($derechosautor as $item)
                            <?php       
                                $numproducto += 1;

                                $producto = $item['prodderautdes'];                               
                                                                           
                            ?>

                            <tr>
                                <td>
                                    {{ $numproducto.'. '.$producto }}
                                </td>                                
                            </tr>    
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td>    
                    <table style="width: 100%;border-spacing:0;" border="1">
                        <thead style="background:#943A4A;color:white;">
                            <td>2. PROPIEDAD INDUSTRIAL* (revisar acapite definiciones)</td>                           
                        </thead>    
                        <?php $numproducto=0; ?>
                        @foreach ($propiedadindustrial as $item)
                            <?php       
                                $numproducto += 1;

                                $producto = $item['prodproinddes'];                               
                                                                           
                            ?>

                            <tr>
                                <td>
                                    {{ $numproducto.'. '.$producto }}
                                </td>                                
                            </tr>    
                        @endforeach
                    </table>
                </td>
            </tr>

            <tr>
                <td>    
                    <table style="width: 100%;border-spacing:0;" border="1">
                        <thead style="background:#943A4A;color:white;">
                            <td>3. OBTENCION Y MEJORAS  DE NUEVOS ORGANISMOS VIVOS</td>                           
                        </thead>    
                        <?php $numproducto=0; ?>
                        @foreach ($organismosvivos as $item)
                            <?php       
                                $numproducto += 1;

                                $producto = $item['orgvivdes'];                               
                                                                           
                            ?>

                            <tr>
                                <td>
                                    {{ $numproducto.'. '.$producto }}
                                </td>                                
                            </tr>    
                        @endforeach
                    </table>
                </td> 
            </tr>    

            <tr>
                <td>
                    VI. Contrato o Convenio. El Proyecto se desarrolla en el marco del convenio:
                </td>
            </tr>
            <tr>    
                <td style="background:#C2C2C2">
                    {{ $convenio.' '.$nombreconvenio }}
                </td> 
            </tr>

            <tr>
                <td>
                    VII. Los Organismos Financiadores
                </td>
            </tr>
            <tr>    
                <td>

                    <table style="width: 100%;border-spacing:0;" border="1">
                        
                        <tr>
                            <td style="background:#943A4A;color:white;width:40%">
                                Valor Total del Proyecto
                            </td>
                            <td colspan="3">
                                {{ $moneda. ' ' . number_format ( $valortotal , 0 , "," , "." ) }}
                            </td>
                        </tr>

                        <tr>
                            <td rowspan="2" style="text-align:center">
                                Empresas o Entidad Financiadora

                                <u>{{ $fuentefinanciacion }}</u>
                            </td>
                            <td>
                                Recurrente
                            </td>
                            <td>
                                No Recurrente
                            </td>
                            <td>
                                Porcentaje %
                            </td>     
                        </tr>

                        <tr>
                            <td>{{ $moneda. ' ' . number_format ( $cofinanciado , 0 , "," , "." ) }}</td>   
                            <td>{{ $moneda. ' ' . number_format ( $contrapartida , 0 , "," , "." ) }}</td>
                            <td>0</td>                         
                        </tr>

                        <tr >
                            <td rowspan="2" style="text-align:center">
                                Instituto Sinchi
                            </td>
                            <td>
                                Recurrente
                            </td>
                            <td>
                                No Recurrente
                            </td>
                            <td>
                                Porcentaje %
                            </td>                                                         
                        </tr> 
                        <tr>
                            <td>{{ $moneda. ' ' .number_format ( $dinero , 0 , "," , "." ) }}</td>   
                            <td>{{ $moneda. ' ' .number_format ( $especie , 0 , "," , "." ) }}</td>
                            <td>0</td>                         
                        </tr>   
                    </table>
                </td> 
            </tr>

            <tr>
                <td style="text-align:justify;">
                    <p><b>Nota:</b> En todos los casos el Comité de Propiedad Intelectual aprobará los 
                    porcentajes (%) de los Derechos Patrimoniales con base en la normatividad vigente. 
                    Si el proyecto es de Colciencias, no se requiere destinar algún tipo de 
                    Porcentaje (%) para dicha entidad. Serán propiedad del Instituto y/o de la entidad 
                    cooperante, financiadora o cofinanciadora, los resultados obtenidos de las 
                    investigaciones científicas y tecnológicas financiadas que se hayan realizado 
                    bajo la modalidad de contratos, convenios de cooperación, adelantadas por 
                    personas naturales, o jurídicas contratadas para tal fin, según los términos del 
                    contrato previo y debidamente suscrito. En caso de esperar beneficios económicos 
                    por la comercialización que pudiere hacer el Instituto respecto de los resultados 
                    de una investigación, se deberán señalar los porcentajes a que tendrían derecho 
                    sus integrantes. De los beneficios netos que perciba el Instituto, se distribuirá 
                    un porcentaje entre el grupo, según el grado de participación en la ejecución 
                    del proyecto de investigación y según el acta de declaración de resultados que 
                    se realice.</p>                    
                    <p><b>
                    Constancia.</b> Los abajo firmantes, conocemos la propuesta técnico-económica del proyecto y La Política de Propiedad Intelectual del Instituto Amazónico de Investigación SINCHI y estamos de acuerdo con las condiciones pactadas para la realización del proyecto, obra o creación que se expresan en la presente Acta de Propiedad Intelectual.</p> 

                </td>
            </tr>

            <!--
            <tr>

                <td>
                    <table style="width:100%">
                        <tr>
                            <td style="width:50%">
                                I. Fecha de Presentación del Proyecto
                            </td>
                            <td style="width:50%">

                                <table style="width:100%;background:#C2C2C2;">
                                    <tr>
                                        <td>
                                            Dia    
                                        </td>
                                        <td>
                                            {{ $díaincorpora }}
                                        </td>
                                        <td>
                                            Mes
                                        </td>
                                        <td>
                                            {{ $mesincorpora }}
                                        </td>
                                        <td>
                                            Año
                                        </td>
                                        <td>
                                            {{ $annoincorpora }}
                                        </td>    
                                    </tr>    
                                </table>

                            </td>
                        </tr>
                    </table>    
                </td>
            </tr>
            
            <tr>
                <td>
                    II. Dependencia en la cual se presenta el proyecto
                </td>                
            </tr>
            <tr>
                <td style="background:#C2C2C2">

                    {{ $area }}

                </td>
            </tr>

            <tr>
                <td>
                    III. Programa misional al que se circunscribe el Proyecto
                </td>                 
            </tr>    
            <tr>
                <td style="background:#C2C2C2">

                    {{ $programapei }}

                </td>
            </tr>

            <tr>
                <td>
                    IV. Actividad generadora del proyecto
                </td>
            </tr>
            <tr>    
                <td style="background:#C2C2C2">

                    {{ $estrategia }}

                </td> 
            </tr>

            <tr>
                <td>
                    V. Nombre del Proyecto
                </td>
            </tr>
            <tr>    
                <td style="background:#C2C2C2">

                    {{ $descripcion }}

                </td> 
            </tr>
            
            <tr>
                <td>
                    VI. Tipo de Proyecto
                </td>
            </tr>
            <tr>    
                <td style="background:#C2C2C2">

                    {{ $tipo }}

                </td> 
            </tr>
            <tr>
                <td>
                    <table style="width:100%;border-spacing:5px;">
                        <tr>
                            <td colspan="2" style="background:#C2C2C2;text-align:center">
                                OBJETIVOS DEL PROYECTO
                            </td>
                        </tr>    

                        <tr>
                            <td style="width:30%;background:#C2C2C2;">
                                Objetivo General
                            </td>
                            <td style="width:70%;background:#C2C2C2;">
                                {{ $objetivogeneral }}
                            </td>    
                        </tr>

                        <tr>
                            <td colspan="2" style="background:#C2C2C2;text-align:center">
                                OBJETIVOS ESPECIFICOS
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="background:#C2C2C2">
                                <table style="width: 100%">
                                    @foreach ($objetivos as $item)
                                        <?php                                              
                                            $objdescripcion = $item['proyobjdes'];
                                            $numero = $item['proyobjcod'];                                            
                                        ?>

                                        <tr>
                                            <td>
                                                {{ $numero.'. '.$objdescripcion }}
                                            </td>    
                                        </tr>    
                                    @endforeach
                                </table>    
                            </td>    
                        </tr>     

                    </table>    
                </td>
            </tr> 
            
            <tr>
                <td>
                    VII. Duración del Proyecto
                </td>
            </tr>
            <tr>    
                <td>

                    <table style="width: 100%;">
                        <tr>
                            <td style="background:#C2C2C2;text-align:center">
                                TIEMPO (meses)
                            </td>
                            <td style="background:#C2C2C2;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">FECHA DE INICIO</td>
                                    </tr>
                                    
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">dia/ mes / año</td>
                                    </tr>    
                                </table>   
                            </td>
                            <td style="background:#C2C2C2;">

                                <table style="width: 100%;">
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">FECHA DE TERMINACION</td>
                                    </tr>
                                    
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">dia/ mes / año</td>
                                    </tr>    
                                </table>   

                            </td>     
                        </tr>
                        
                        <tr>
                            <td style="background:#C2C2C2;text-align:center">
                                {{ $meses }}
                            </td>
                            
                            <td style="background:#C2C2C2;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">{{ $díainicio }}</td>
                                        <td style="background:#C2C2C2;text-align:center">{{ $mesinicio }}</td>
                                        <td style="background:#C2C2C2;text-align:center">{{ $annoinicio }}</td>
                                    </tr>    
                                </table>    
                            </td>
                            
                            <td style="background:#C2C2C2;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="background:#C2C2C2;text-align:center">{{ $díaterminacion }}</td>
                                        <td style="background:#C2C2C2;text-align:center">{{ $mesterminacion }}</td>
                                        <td style="background:#C2C2C2;text-align:center">{{ $annoterminacion }}</td>
                                    </tr>    
                                </table>    
                            </td> 
                        </tr>    
                    </table>    

                </td> 
            </tr>

            <tr>
                <td>
                    VIII. Productos Esperados de este trabajo intelectual (numérico)
                </td>
            </tr>
            <tr>    
                <td>   
                                        
                    <table style="width: 100%;border-spacing:0;" border="1">
                        <thead style="background:#943A4A;color:white;">
                            <td>Tipo de Producto</td>
                            <td>Científico</td>
                            <td>Técnico</td>
                            <td>Literario/humanístico</td>
                        </thead>    
                        <?php $numproducto=0; ?>
                        @foreach ($productos as $item)
                            <?php       
                                $numproducto += 1;

                                $producto = $item['proddes'];
                                $cientifico = $item['proyprodcie'];
                                $tecnico = $item['proyprodtec'];
                                $literario = $item['proyprodlih'];
                                                                           
                            ?>

                            <tr>
                                <td>
                                    {{ $numproducto.'. '.$producto }}
                                </td>
                                <td>
                                    {{ $cientifico }}
                                </td>
                                <td>
                                    {{ $tecnico }}
                                </td> 
                                <td>
                                    {{ $literario }}
                                </td>   
                            </tr>    
                        @endforeach
                    </table>    
                   
                </td> 
            </tr>

            <tr>
                <td>
                    IX. Nombre de los Integrantes y su vinculación
                </td>
            </tr>
            <tr>
                <td>
                    Para diligenciar esta información, por favor utilice los siguientes criterios: - 
                    * Según se trate (ej. Investigador principal, investigadores) -  
                    ** Número de horas semanales - ** Porcentaje de participación en los derechos patrimoniales   
                    *** Titular de los derechos de propiedad intelectual 
                </td>                    
            </tr>
            <tr>
                <td>
                    <table style="width: 100%;border-spacing:0;" border="0">
                        <?php       
                            $numero = 0;
                        ?>        
                        @foreach ($equipo as $item3)
                            <?php       
                                $numero += 1;

                                $nombre = $item3->persnom;
                                $identificacion = $item3->perside;
                                $rol = $item3->proyeterol;
                                $titular = $item3->proyetetit;
                                $tipocontrato = $item3->perstitcont;
                                $dedicacion = $item3->proyeteded;
                                $porcentaje = $item3->proyetepor;
                                
                                $numero2 = 0;
                            ?>

                            <tr>
                                <td>
                                    <table style="width:100%;border-spacing:0;" border="1">
                                        <tr>
                                            <td>
                                                {{ $numero }} 
                                            </td>
                                            <td>
                                                Nombres Y Apellidos
                                            </td>
                                            <td colspan="3">
                                                {{ $nombre }}
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="2">
                                                No. Identificación
                                            </td>
                                            <td>
                                                {{ $identificacion }}
                                            </td>
                                            <td>
                                                Rol del Integrante*
                                            </td> 
                                            <td>
                                                {{ $rol }}
                                            </td>    
                                        </tr>    
                                        
                                        <tr>
                                            <td colspan="2">
                                                Tipo de Vinculación con el instituto
                                            </td>
                                            <td>
                                                {{ $tipocontrato }}
                                            </td>
                                            <td>
                                                Dedicación al Proyecto*
                                            </td> 
                                            <td>
                                                {{ $dedicacion }}
                                            </td>    
                                        </tr>

                                        <tr>
                                            <td colspan="2">Compromisos con el Proyecto</td>
                                            <td colspan="3">
                                                <table style="width: 100%;border-spacing:0;" borer="1">    
                                                    @foreach ($compromisos as $item2)
                                                        <?php

                                                            if($item2->perscod == $item3->perscod){
                                                                $numero2 += 1;
                                                                $descripcionactual = $item2->proyetcdes;

                                                                echo '<tr>';
                                                                echo '<td>'.$numero2.'. '.$descripcionactual.'</td>';
                                                                echo '</tr>';    
                                                            }                                                       
                                                                                                    
                                                        ?>
                                                    @endforeach
                                                </table>                
                                            </td>
                                        </tr> 
                                        
                                        <tr>
                                            <td colspan="2">
                                                Porcentaje *** (%)
                                            </td>
                                            <td>
                                                {{ $porcentaje }}
                                            </td>
                                            <td colspan="2">

                                                <table style="width:100%;border-spacing:0;" border="1">
                                                    <tr>
                                                        <td style="width:40%">
                                                            Titular****
                                                        </td>    
                                                        <td style="width:20%">
                                                            SI
                                                        </td>
                                                        <td style="width:10%">
                                                            {{ $titular == 1 ? 'X' : '' }}
                                                        </td>
                                                        <td style="width:20%">
                                                            NO
                                                        </td>
                                                        <td style="width:10%">
                                                            {{ $titular == 0 ? 'X' : '' }}
                                                        </td> 
                                                    </tr>    
                                                </table>                                                
                                            </td> 
                                        </tr>
                                    </table>
                                </td>
                            </tr>        
                            <br><br>    

                        @endforeach

                        <tr>
                            
                        </tr>
                        <tr>
                            
                        </tr>
                        <tr>
                            
                        </tr>    
                    </table>    
                </td>    
            </tr>
            
            <tr>
                <td>
                    X. Contrato o Convenio
                </td>
            </tr>
            <tr>    
                <td style="background:#C2C2C2">

                    {{ $convenio }}

                </td> 
            </tr>

            <tr>
                <td>
                    XI. Organismos Financiadores
                </td>
            </tr>
            <tr>    
                <td>

                    <table style="width: 100%;border-spacing:0;" border="1">
                        
                        <tr>
                            <td style="background:#943A4A;color:white;width:40%">
                                Valor Total del Proyecto
                            </td>
                            <td colspan="3">
                                $ {{ number_format ( $valortotal , 0 , "," , "." ) }}
                            </td>
                        </tr>

                        <tr>
                            <td rowspan="2">
                                Empresas o Entidad Financiadora

                                {{ $fuentefinanciacion }}
                            </td>
                            <td>
                                Recurrente
                            </td>
                            <td>
                                No Recurrente
                            </td>
                            <td>
                                Porcentaje %
                            </td>     
                        </tr>

                        <tr>
                            <td>$ {{ number_format ( $cofinanciado , 0 , "," , "." ) }}</td>   
                            <td>$ {{ number_format ( $contrapartida , 0 , "," , "." ) }}</td>
                            <td>0</td>                         
                        </tr>

                        <tr >
                            <td rowspan="2">
                                Instituto Sinchi
                            </td>
                            <td>
                                Recurrente
                            </td>
                            <td>
                                No Recurrente
                            </td>
                            <td>
                                Porcentaje %
                            </td>                                                         
                        </tr> 
                        <tr>
                            <td>$ {{ number_format ( $dinero , 0 , "," , "." ) }}</td>   
                            <td>$ {{ number_format ( $especie , 0 , "," , "." ) }}</td>
                            <td>0</td>                         
                        </tr>   
                    </table> 

                </td> 
            </tr>

            <tr>
                <td>
                    XII. Derechos Patrimoniales para el grupo de trabajo
                </td>    
            </tr>
            <tr> 
                <td>
                    <table style="width: 100%;border-spacing:0;" border="1">                                
                        <tr>
                            <td style="background:#943A4A;color:white;width:40%">
                                Vinculación al Proyecto
                            </td>
                            <td style="background:#943A4A;color:white;width:40%">
                                Porcentaje (%)
                            </td>
                        </tr>

                        @foreach ($equipo as $item10)
                            <?php

                                $nombre = $item10->persnom;
                                $identificacion = $item10->perside;                                
                                $porcentaje = $item10->proyetepor;
                                
                                if($porcentaje > 0){

                                    echo '<tr>';
                                    echo '<td>'.$nombre.'</td>'; 
                                    echo '<td>'.$porcentaje.'</td>';    
                                    echo '</tr>';    
                                }
                            ?>
                        @endforeach        

                    </table>    
                </td>
            </tr>
            
            <tr>
                <td style="text-align:justify;">
                    <p><b>Nota:</b> En todos los casos el Comité de Propiedad Intelectual aprobará los 
                    porcentajes (%) de los Derechos Patrimoniales con base en la normatividad vigente. 
                    Si el proyecto es de Colciencias, no se requiere destinar algún tipo de Porcentaje (%) para dicha entidad. Serán propiedad del Instituto y/o de la entidad cooperante, financiadora o cofinanciadora, los resultados obtenidos de las investigaciones científicas y tecnológicas financiadas que se hayan realizado bajo la modalidad de contratos, convenios de cooperación, adelantadas por personas naturales, o jurídicas contratadas para tal fin, según los términos del contrato previa y debidamente suscrito. En caso de esperar beneficios económicos por la comercialización que pudiere hacer el Instituto respecto de los resultados de una investigación, se deberán señalar los porcentajes a que tendrían derecho sus integrantes. De los beneficios netos que perciba el Instituto, se distribuirá un porcentaje entre el grupo, según el grado de participación en la formulación y en la ejecución del proyecto de investigación.</p>
                    
                    <p><b>
                    Constancia.</b> Los abajo firmantes, conocemos la propuesta técnico-económica del proyecto y La Política de Propiedad Intelectual del Instituto Amazónico de Investigación SINCHI y estamos de acuerdo con las condiciones pactadas para la realización del proyecto, obra o creación que se expresan en la presente Acta de Propiedad Intelectual.</p> 

                </td>
            </tr> 
            
            <tr>
                <td>
                    <b>Firmas.</b>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table style = "width: 100%;border-spacing:0;" border="1">
                        <tr>
                            <td style="background:#943A4A;color:white;width:40%;text-align:center;">
                                Rol de los integrantes del grupo
                            </td>
                            <td style="background:#943A4A;color:white;width:40%;text-align:center;">
                                Nombre
                            </td>
                            <td style="background:#943A4A;color:white;width:40%;text-align:center;">
                                Cédula
                            </td>
                            <td style="background:#943A4A;color:white;width:40%;text-align:center;">
                                Firma
                            </td>    
                        </tr>
                        
                        @foreach ($equipo as $item11)
                            <?php

                                $nombre = $item11->persnom;
                                $identificacion = $item11->perside;
                                $rol = $item11->proyeterol;
                               
                                echo '<tr>';
                                echo '<td>'.$rol.'</td>'; 
                                echo '<td>'.$nombre.'</td>';
                                echo '<td>'.$identificacion.'</td>';
                                echo '<td></td>';    
                                echo '</tr>';    
                                
                            ?>
                        @endforeach 
                    </table>
                </td>    
            </tr>   

            <tr>
                <td>
                    
                    <table style="width:100%">
                        <tr>
                            <td style="text-align: center">
                                ___________________________________________
                                Representante I.A.I. SINCHI
                            </td>
                            <td style="text-align: center">
                                ___________________________________________
                                Representante Legal entidad Financiadora.
                            </td>    
                        </tr>    
                    </table>

                </td>    
            </tr>    --> 
        </table>    
               
    </body>
</html>