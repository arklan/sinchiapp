<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>FT PROYECTO</title>
        <script type="text/javascript" src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>
        <script>
            function subst() {
                  var lastpage = 0;
                  var currentpage = 0;
                var vars = {};
                var query_strings_from_url = document.location.search.substring(1).split('&');
                for (var query_string in query_strings_from_url) {
                    if (query_strings_from_url.hasOwnProperty(query_string)) {
                        var temp_var = query_strings_from_url[query_string].split('=', 2);
                        vars[temp_var[0]] = decodeURI(temp_var[1]);
                        if (temp_var[0] == 'topage') {
                            lastpage = decodeURI(temp_var[1]);
                        }
                        if (temp_var[0] == 'page') {
                            currentpage = decodeURI(temp_var[1]);
                        }
                    }
                }
                var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages','tipoImpresion'];
                for (var css_class in css_selector_classes) {
                    if (css_selector_classes.hasOwnProperty(css_class)) {
                        var element = document.getElementsByClassName(css_selector_classes[css_class]);
                        for (var j = 0; j < element.length; ++j) {
                            element[j].textContent = vars[css_selector_classes[css_class]];
                        }
                    }
                }
          
                  $('#txtTotal').hide();
                  $('#txtPasa').show();
                  if (currentpage == lastpage) {
                      $('#txtTotal').show();
                      $('#txtPasa').hide();
                  }
            }
        </script>        
    </head>   

    <body>

        <table style="font-family: Arial, Georgia, Serif; font-size: 12pt; color: #000000;" cellpadding="0" cellspacing="0" width="100%" border="0">
            <tr>
                <td rowspan="2" style="vertical-align:central;width:20%;text-align:center">
                    <img src="{{ asset('assets/img/logo.png') }}" alt="" height="100" width="70" /></td>
                </td> 
                <td style="vertical-align:central;width:80%;text-align:center">
                    INSTITUTO AMAZÓNICO DE INVESTIGACIONES CIENTÍFICAS - SINCHI
                    <br><br>
                </td>   
            </tr>
            <tr>
                <td style="vertical-align:top;width:33%;text-align:center">
                    Ficha Técnica de proyecto - Acción Institucional
                </td>
            </tr>        
        </table> 
        
        <table style="font-family: Arial, Georgia, Serif; font-size: 8pt; color: #000000;" cellpadding="0" cellspacing="0" width="100%" border="0">
            <tr>
                <td style="vertical-align:central;width:33%;text-align:left">
                    Fecha: {{ date('d F Y') }} 
                </td>
                <td style="vertical-align:central;width:34%;text-align:left">
                    P2 - 009 Versión 5
                </td>
                <td style="text-align: right;width:33%;">
                    Página <span class="page"></span>
                </td>    
            </tr>    
        </table>
        <br><br>
    </body>
</html>