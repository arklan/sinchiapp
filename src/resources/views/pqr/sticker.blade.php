<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Sticker picking</title>
        <script type="text/javascript" src="{{ asset('assets/js/jquery-3.4.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/JsBarcode.all.min.js') }}"></script>
        <style>
            
            body
            {
                margin:     0;
                padding:    0;
                color:black
            }

        </style>

    </head>

    <body>

        <table width="100%">

            @foreach ($headers as $header)

            <?php 
            
                $consecutivo = $header->consecutivo;

            ?>

            <tr>

                <td style="font-size:12pt;text-align:center">
                    <img src="{{ public_path('logo.png') }}" width="150" height="150">
                </td>

            </tr>

            <tr>

                <td style="font-size:12pt;text-align:center">
                    Instituto Amazónico de Investigaciones Científicas SINCHI
                </td>

            </tr>

            <tr>

                <td style="text-align:center;padding:10px">
                    <svg id="barcode"></svg>
                </td>

            </tr>

            <tr>

                <td style="font-size:12pt;text-align:center">
                    Fecha y hora radicado: {{ $header->fecha_creacion }}
                </td>

            </tr>

            @endforeach

        </table>

        <script>

            var documento = "<?php echo $consecutivo ?>";

            $(document).ready(function() {

                JsBarcode("#barcode",documento, {fontSize: 12, margin: 0, height: 20, displayValue: true});

            });

        </script>

    </body>

</html>