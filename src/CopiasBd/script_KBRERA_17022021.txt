DROP TABLE IF EXISTS `resguardos_indigenas`;
CREATE TABLE IF NOT EXISTS `resguardos_indigenas` (
  `resindcod` smallint(6) NOT NULL,
  `ciucod` char(5) NOT NULL,
  `resindnom` varchar(200) NOT NULL,
  PRIMARY KEY (`resindcod`),
  KEY `FCiudad` (`ciucod`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `resguardos_indigenas`
--

INSERT INTO `resguardos_indigenas` (`resindcod`, `ciucod`, `resindnom`) VALUES
('1001', '86001', 'PREDIO PUTUMAYO');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `resguardos_indigenas`
--
ALTER TABLE `resguardos_indigenas`
  ADD CONSTRAINT `FResIndCiudad` FOREIGN KEY (`ciucod`) REFERENCES `ciudades` (`ciucod`);

UPDATE `opciones` SET `opcico` = 'foundation' WHERE `opciones`.`opccod` = 57;
UPDATE `opciones` SET `opcico` = 'groups' WHERE `opciones`.`opccod` = 58;
UPDATE `opciones` SET `opcico` = 'account_balance' WHERE `opciones`.`opccod` = 59;  

DROP TABLE IF EXISTS `asociaciones`;
CREATE TABLE IF NOT EXISTS `asociaciones` (
  `asocod` smallint(6) NOT NULL,
  `asonom` varchar(200) NOT NULL,
  PRIMARY KEY (`asocod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `asociaciones_resguardos`;
CREATE TABLE IF NOT EXISTS `asociaciones_resguardos` (
  `asocod` smallint(6) NOT NULL,
  `resindcod` smallint(6) NOT NULL,
  PRIMARY KEY (`asocod`,`resindcod`),
  KEY `RAsocRes_Resguardo` (`resindcod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asociaciones_resguardos`
--
ALTER TABLE `asociaciones_resguardos`
  ADD CONSTRAINT `RAsocRes_Asociacion` FOREIGN KEY (`asocod`) REFERENCES `asociaciones` (`asocod`),
  ADD CONSTRAINT `RAsocRes_Resguardo` FOREIGN KEY (`resindcod`) REFERENCES `resguardos_indigenas` (`resindcod`);

DROP TABLE IF EXISTS `comunidades`;
CREATE TABLE IF NOT EXISTS `comunidades` (
  `comcod` smallint(6) NOT NULL,
  `comnom` varchar(200) NOT NULL,
  `asocod` smallint(6) NOT NULL,
  PRIMARY KEY (`comcod`),
  KEY `IAsociacion` (`asocod`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comunidades`
--
ALTER TABLE `comunidades`
  ADD CONSTRAINT `RComunidadAsociacion` FOREIGN KEY (`asocod`) REFERENCES `asociaciones` (`asocod`);