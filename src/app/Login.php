<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    protected $connection = 'mysql';
    protected $table = 'users';
    protected $fillable = ['iduser', 'nombre', 'usuario','password','sexo','email','estado','perscod'];
    protected $primaryKey = 'iduser';
    public $timestamps = false;
}