<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpPlanesAdquisicion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tmpplanes_adquisicion';
    protected $fillable = ['tmpplaadqcod', 'plaadqcod', 'plaadqfec', 'proycod','estjurcod','plaadqacj','estplacod','plaadqacp'];
    protected $primaryKey = 'tmpplaadqcod';
    public $timestamps = false;
}