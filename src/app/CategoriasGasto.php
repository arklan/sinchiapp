<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriasGasto extends Model
{
    protected $connection = 'mysql';
    protected $table = 'categorias_gasto';
    protected $fillable = ['catgascod', 'catgasnom'];
    protected $primaryKey = 'catgascod';
    public $timestamps = false;
}