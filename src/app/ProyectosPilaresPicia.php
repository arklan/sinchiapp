<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ProyectosPilaresPicia extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'proyectos_pilares_picia';
	    protected $fillable = ['proycod', 'pilpiccod'];
	    protected $primaryKey = 'pilpiccod';
	    public $timestamps = false;	
	}

?>