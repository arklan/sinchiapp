<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanesAdquisicionNecesidades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'planesadquisiciones_necesidades';
    protected $fillable = ['plaadqcod', 'plaadqneccod', 'plaadqnecdes','plaadqnecval','metselcod','plaadqnecfeccont'];
    protected $primaryKey = 'plaadqneccod';
    public $timestamps = false;
}