<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pqr extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pqrs_reclamos';
    protected $fillable = ['observacion', 'remitente', 'email', 'direccion', 'telefono', 'fecha_asignacion', 'analista', 'id_estado', 'respuesta', 'fecha_cierre', 'threadId', 'responsable', 'codmunicipio', 'tipo_comunicacion', 'medio_solicitante', 'asunto', 'detalle', 'dias_respuesta', 'radicado_vu', 'fecha_radicado_vu', 'fecha_radicado'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
