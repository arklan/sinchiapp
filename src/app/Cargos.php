<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'cargos';
    protected $fillable = ['cargcod', 'cargnom'];
    protected $primaryKey = 'cargcod';
    public $timestamps = false;
}