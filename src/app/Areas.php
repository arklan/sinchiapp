<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'areas';
    protected $fillable = ['arecod', 'arenom','areenccod'];
    protected $primaryKey = 'arecod';
    public $timestamps = false;
}