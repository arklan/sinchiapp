<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipios extends Model
{
    protected $connection = 'mysql';
    protected $table = 'municipios';
    protected $fillable = ['idmunicipio', 'municipio'];
    protected $primaryKey = 'idmunicipio';
    public $timestamps = false;
}
