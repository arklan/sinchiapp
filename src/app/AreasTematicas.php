<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreasTematicas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'areas_tematicas';
    protected $fillable = ['aretemcod', 'aretemnom'];
    protected $primaryKey = 'aretemcod';
    public $timestamps = false;
}