<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersAlertas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'users_configuracion';
    protected $fillable = ['iduser', 'useconfnsi', 'useconfnco'];
    protected $primaryKey = 'iduser';
    public $timestamps = false;
}