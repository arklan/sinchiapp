<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccionesPicia extends Model
{
    protected $connection = 'mysql';
    protected $table = 'acciones_picia';
    protected $fillable = ['accpiccod', 'accpicdes','lininvpiccod'];
    protected $primaryKey = 'accpiccod';
    public $timestamps = false;
}