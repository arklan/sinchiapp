<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'contratos';
    protected $fillable = ['contcod', 'conttip', 'contnom', 'conobj', 'confecfir', 'confecapr', 'confecini', 'confecter', 'conejedia', 'confeccdp', 'connumcdp', 'confecrp', 'connumrp', 'conrubpre'];
    protected $primaryKey = 'ciucod';
    public $timestamps = false;
}