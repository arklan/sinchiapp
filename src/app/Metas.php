<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'metas';
    protected $fillable = ['metcod', 'metnom','	lininvpencod'];
    protected $primaryKey = 'metcod';
    public $timestamps = false;
}