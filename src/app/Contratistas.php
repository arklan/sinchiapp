<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratistas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'contratistas';
    protected $fillable = ['contacod','tipidecod', 'contide', 'contape', 'contnom','contfecnac','contmes', 
                            'contval','contdir','conttel','contema'];
    protected $primaryKey = 'contacod';
    public $timestamps = false;
}

?>