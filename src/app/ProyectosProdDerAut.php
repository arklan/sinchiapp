<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosProdDerAut extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_productos_derautor';
    protected $fillable = ['proycod', 'prodderautcod'];
    protected $primaryKey = 'prodderautcod';
    public $timestamps = false;
}

?>