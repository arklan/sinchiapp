<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesConvenio extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_convenio';
    protected $fillable = ['id', 'id_solicitud','contrapartida','valor','especie','cantespecie','dinero','cantdinero'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
