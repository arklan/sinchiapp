<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropRoles extends Model
{
    protected $connection = 'mysql';
    protected $table = 'prop_roles';
    protected $fillable = ['id', 'id_rol', 'id_prop'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
