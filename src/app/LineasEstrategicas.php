<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineasEstrategicas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'lineas_estrategicas';
    protected $fillable = ['linestcod', 'linestdesc','capcod'];
    protected $primaryKey = 'linestcod';
    public $timestamps = false;
}