<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionesActa extends Model
{
    protected $connection = 'mysql';
    protected $table = 'reuniones_acta';
    protected $fillable = ['reuactcod', 'reuactfec', 'reuactpdf', 'reuactest', 'reucod'];
    protected $primaryKey = 'reuactcod';
    public $timestamps = false;
}