<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comunidades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'comunidades';
    protected $fillable = ['comcod','comnom','asocod'];
    protected $primaryKey = 'comcod';
    public $timestamps = false;
}

?>