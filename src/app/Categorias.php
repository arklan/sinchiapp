<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    protected $connection = 'mysql';
    protected $table = 'categorias';
    protected $fillable = ['catcod', 'catdes'];
    protected $primaryKey = 'catcod';
    public $timestamps = false;
}