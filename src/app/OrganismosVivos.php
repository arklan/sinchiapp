<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganismosVivos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'organismos_vivos';
    protected $fillable = ['orgvivcod', 'orgvivdes'];
    protected $primaryKey = 'orgvivcod';
    public $timestamps = false;
}