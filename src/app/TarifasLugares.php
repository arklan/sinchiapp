<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TarifasLugares extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tarifas_lugares';
    protected $fillable = ['tarlugcod','moncod','tarlugdes'];
    protected $primaryKey = 'tarlugcod';
    public $timestamps = false;
}