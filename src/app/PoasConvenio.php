<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poas_convenio extends Model
{
    protected $connection = 'mysql';
    protected $table = 'poas_convenio';
    protected $fillable = ['poacod'];
    protected $primaryKey = 'poacod';
    public $timestamps = false;
}