<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionesTareaPersonal extends Model
{

    protected $connection = 'mysql';
    protected $table = 'reuniones_tarea_personal';
    protected $fillable = ['reutarperscod', 'reutarcod', 'perscod'];
    protected $primaryKey = 'reutarperscod';
    public $timestamps = false;
}