<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpPlaAdqNecActividades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tmpplanesadquisiciones_necesidadesactividades';
    protected $fillable = ['tmpplaadqcod', 'plaadqneccod', 'proyactsec','catgascod',
                            'plaadqnecacttag','plaadqnecactval','plaadqnecacttmo'];
    protected $primaryKey = 'proyactsec';
    public $timestamps = false;
}