<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HisPlanesAdquisicionNec extends Model
{
    protected $connection = 'mysql';
    protected $table = 'hisplanesadquisiciones_necesidades';
    protected $fillable = ['hisplaadqcod', 'plaadqneccod', 'plaadqnecdes','plaadqnecval','metselcod',
                            'plaadqnecfeccont'];
    protected $primaryKey = 'hisplaadqcod';
    public $timestamps = false;
}