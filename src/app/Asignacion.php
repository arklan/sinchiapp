<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pqr_asignacion';
    protected $fillable = ['documento', 'id_pqr', 'id_area', 'responsable_salida'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
