<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionesPersonalAsistentes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'reuniones_personal_asistentes';
    protected $fillable = ['reupercodasi', 'perscod','reucod','reuperasi','reupersolapro'];
    protected $primaryKey = 'reupercodasi';
    public $timestamps = false;
}