<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesPlanesadquisicionNecesidades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_planesadquisiciones_necesidades';
    protected $fillable = ['id', 'id_planadquision','id_necesidad','id_solicitud'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
