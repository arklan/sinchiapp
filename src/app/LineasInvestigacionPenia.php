<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineasInvestigacionPenia extends Model
{
    protected $connection = 'mysql';
    protected $table = 'lineas_investigacion_penia';
    protected $fillable = ['lininvpencod', 'liniinvpendesc','proinvpencod'];
    protected $primaryKey = 'lininvpencod';
    public $timestamps = false;
}