<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ProyectosLinInvPicia extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'proyectos_lininv_picia';
	    protected $fillable = ['proycod', 'lininvpiccod'];
	    protected $primaryKey = 'lininvpiccod';
	    public $timestamps = false;	
	}

?>