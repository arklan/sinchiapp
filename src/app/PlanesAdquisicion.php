<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanesAdquisicion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'planes_adquisicion';
    protected $fillable = ['plaadqcod', 'plaadqfec', 'plaadqultfec','proycod','estjurcod','plaadqacj',
                            'estplacod','plaadqacp'];
    protected $primaryKey = 'plaadqcod';
    public $timestamps = false;
}