<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosEquipo extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_equipotecnico';
    protected $fillable = ['proycod', 'perscod','rolcod','proyeteded','proyetepor','proyetetit'];
    protected $primaryKey = 'perscod';
    public $timestamps = false;
}

?>