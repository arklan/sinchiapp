<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConveniosSocios extends Model
{
    protected $connection = 'mysql';
    protected $table = 'convenios_socios';
    protected $fillable = ['convcod', 'convsoccod','convsocfuefin','convsocvalcof','convsocvalctp',
                            'convsocvaltot','convsocvaldin'];
    protected $primaryKey = 'convsoccod';
    public $timestamps = false;
}