<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResguardosIndMunicipios extends Model
{
    protected $connection = 'mysql';
    protected $table = 'resguardos_ciudades';
    protected $fillable = ['resindcod', 'ciucod'];
    protected $primaryKey = 'resindcod';
    public $timestamps = false;
}