<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    protected $connection = 'mysql';
    protected $table = 'bitacora';
    protected $fillable = ['BitSec', 'iduser', 'BitFecReg','BitModDes','BitAccDes','BitEquIp','BitMem','BitHos'];
    protected $primaryKey = 'cliecod';
    public $timestamps = false;
}