<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;
    class ProyectosEquipoComp extends Model
    {
        protected $connection = 'mysql';
        protected $table = 'proyectos_equipotecnico_compromisos';
        protected $fillable = ['proycod','perscod', 'proyetccod', 'proyetcdes'];
        protected $primaryKey = 'proyetccod';
        public $timestamps = false;
    }

?>