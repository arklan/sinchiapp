<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reuniones extends Model
{
    protected $connection = 'mysql';
    protected $table = 'reuniones';
    protected $fillable = ['reucod', 'reutit','proycod','plaadqcod','plaadqneccod','solcod','reufechini','reufecfin','reulug'];
    protected $primaryKey = 'reucod';
    public $timestamps = false;
}