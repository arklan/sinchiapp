<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormasPago extends Model
{
    protected $connection = 'mysql';
    protected $table = 'formas_pago';
    protected $fillable = ['forpagcod', 'forpagdes'];
    protected $primaryKey = 'forpagcod';
    public $timestamps = false;
}