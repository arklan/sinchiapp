<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionesPersonalVisualizacion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'reuniones_personal_visualizacion';
    protected $fillable = ['reupercodvis', 'perscod','reucod'];
    protected $primaryKey = 'reupercodvis';
    public $timestamps = false;
}