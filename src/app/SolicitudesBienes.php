<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitudes_Bienes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_bienes';
    protected $fillable = ['solbieite', 'solbiedes', 'solbiecan'];
    protected $primaryKey = 'solbieite';
    public $timestamps = false;
}