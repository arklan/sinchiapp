<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoComunicacion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pqr_tipo_comunicacion';
    protected $fillable = ['descripcion', 'dias_respuesta'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
