<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HisPlanesAdquisicion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'hisplanes_adquisicion';
    protected $fillable = ['hisplaadqcod', 'plaadqcod', 'plaadqfec', 'proycod','estjurcod','plaadqacj',
                            'estplacod','plaadqacp','plaadqver'];
    protected $primaryKey = 'hisplaadqcod';
    public $timestamps = false;
}