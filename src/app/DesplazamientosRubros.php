<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesplazamientosRubros extends Model
{
    protected $connection = 'mysql';
    protected $table = 'desplazamientos_rubros';
    protected $fillable = ['descod', 'desrubcod','plaadqcod','plaadqneccod','proyactsec',
                            'prescod','desrubvaldis','desrubcom','desrubsal'];
    protected $primaryKey = 'desrubcod';
    public $timestamps = false;
}