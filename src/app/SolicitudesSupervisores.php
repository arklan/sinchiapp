<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesSupervisores extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solproc_supervisores';
    protected $fillable = ['id', 'id_solproc', 'codigo'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
