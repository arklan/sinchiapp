<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpPlaAdqNecesidadesUnspsc extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tmpplanesadquisiciones_necesidadesunspsc';
    protected $fillable = ['tmpplaadqcod', 'plaadqneccod', 'unspsccod','plaadqnecunstmod'];
    protected $primaryKey = 'unspsccod';
    public $timestamps = false;
}