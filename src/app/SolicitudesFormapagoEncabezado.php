<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesFormapagoEncabezado extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_formapago_encabezado';
    protected $fillable = ['id','id_solicitud','tiempo','porcentaje'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
