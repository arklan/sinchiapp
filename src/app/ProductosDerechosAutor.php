<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosDerechosAutor extends Model
{
    protected $connection = 'mysql';
    protected $table = 'productos_derechos_autor';
    protected $fillable = ['prodderautcod', 'prodderautdes'];
    protected $primaryKey = 'prodderautcod';
    public $timestamps = false;
}