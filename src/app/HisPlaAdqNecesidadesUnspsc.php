<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HisPlaAdqNecesidadesUnspsc extends Model
{
    protected $connection = 'mysql';
    protected $table = 'hisplanesadquisiciones_necesidadesunspsc';
    protected $fillable = ['hisplaadqcod', 'plaadqneccod', 'unspsccod'];
    protected $primaryKey = 'unspsccod';
    public $timestamps = false;
}