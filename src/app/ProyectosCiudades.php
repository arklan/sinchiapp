<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosCiudades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_ciudades';
    protected $fillable = ['proycod','ciucod'];
    protected $primaryKey = 'proycod';
    public $timestamps = false;
}

?>