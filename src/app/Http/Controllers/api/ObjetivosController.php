<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ObjetivosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Objetivos;
use DB;

class ObjetivosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Objetivos = Objetivos::select('objetivos.objcod','objetivos.objdesc','objetivos.linestcod','lineas_estrategicas.linestdesc')
                     ->join('lineas_estrategicas','objetivos.linestcod','=','lineas_estrategicas.linestcod') 
                     ->orderby('objetivos.objdesc')                     
                     ->get();
                     
            if ($Objetivos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Objetivos, new ObjetivosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function FiltroXLineaEst(Request $request)
    {       
       try{

            $linea = $request->linea;

            $Objetivos = Objetivos::select('objetivos.objcod','objetivos.objdesc','objetivos.linestcod','lineas_estrategicas.linestdesc')
                     ->join('lineas_estrategicas','objetivos.linestcod','=','lineas_estrategicas.linestcod')  
                     ->where('objetivos.linestcod',$linea)
                     ->orderby('objetivos.objdesc')                     
                     ->get();
                     
            if ($Objetivos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Objetivos, new ObjetivosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Objetivos::max('objcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Objetivos::select(DB::raw('count(objcod) as total'))                      
                      ->where('objdesc', $descripcion)
                      ->where('objcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Descripción ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ObjetivosAddUpdate(Request $request)
    {
        $Objetivos = $request->data;

        $codigo = $Objetivos['codigo'];
        $descripcion = strtoupper(trim($Objetivos['descripcion']));
        $codigolinest = $Objetivos['codigolinest'];      

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $ObjetivosAdd = new Objetivos();
            
            $ObjetivosAdd->objcod = $consecutivo;           
            $ObjetivosAdd->objdesc = $descripcion;
            $ObjetivosAdd->linestcod = $codigolinest;           
            
            $result = $ObjetivosAdd->save();
        
        } else {

            $ObjetivosUpd = Objetivos::where('objcod', $codigo)->first();
          
            $ObjetivosUpd->objdesc = $descripcion;          
            $ObjetivosUpd->linestcod = $codigolinest; 

            // Guardamos en base de datos
            $result = $ObjetivosUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Objetivos::where('objcod', $Id)->delete(); 
        return $result;

    }
}

?>