<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosFormulacionTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosFormulacion;
use DB;

class ProyectosFormulacionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosFormulacion = ProyectosFormulacion::select('proyectos_preguntas_formulacion.formnum',
                                        'proyectos_preguntas_formulacion.proycod', 'proyectos_preguntas_formulacion.proyfprsino',
                                        'proyectos_preguntas_formulacion.proyfprobs','preguntas_formulacion.formpreg') 
                        ->join('preguntas_formulacion','proyectos_preguntas_formulacion.formnum','=','preguntas_formulacion.formnum')                                        
                        ->orderby('formnum')                     
                        ->get();
                     
            if ($ProyectosFormulacion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosFormulacion, new ProyectosFormulacionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];

            $ProyectosFormulacion = ProyectosFormulacion::select('proyectos_preguntas_formulacion.formnum',
                                        'proyectos_preguntas_formulacion.proycod', 'proyectos_preguntas_formulacion.proyfprsino',
                                        'proyectos_preguntas_formulacion.proyfprobs','preguntas_formulacion.formpreg') 
                        ->join('preguntas_formulacion','proyectos_preguntas_formulacion.formnum','=','preguntas_formulacion.formnum')                        
                        ->where('proyectos_preguntas_formulacion.proycod', $proyecto)
                        ->orderby('proyectos_preguntas_formulacion.formnum')                     
                        ->get();
                     
            if ($ProyectosFormulacion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosFormulacion, new ProyectosFormulacionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }    

    private function buscarCodigoProyecto($Proyecto, $Codigo){

        try{
           
            $ok = false;

            $Encontrado = ProyectosFormulacion::select(DB::raw('count(formnum) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('formnum',$Codigo)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    public function agregaPreguntasProyectos(Request $request)
    {
        $proyecto = $request->proyecto;

        $sql = "INSERT INTO proyectos_preguntas_formulacion (proycod,formnum,proyfprsino,proyfprobs)
                    SELECT ".$proyecto.",p.formnum,0,'' FROM 
                            preguntas_formulacion p LEFT JOIN proyectos_preguntas_formulacion pry ON 
                        p.formnum = pry.formnum AND pry.proycod = ".$proyecto." 
                    WHERE pry.proycod IS NULL";

        $result = DB::insert($sql);            

        return array( 'respuesta' => $result); 
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = '';
        
        for ($i=0; $i < count($data); $i++) {             
                        
            $codigo = $data[$i]['codigo'];           
            $proyecto = $data[$i]['proyecto'];
            $sino = $data[$i]['sino'];
            $observacion = $data[$i]['observacion'];

            if($codigo == 0)
                $encontrado = false;
            else    
                $encontrado = $this->buscarCodigoProyecto($proyecto, $codigo);

            if($encontrado){

                $query = "UPDATE proyectos_preguntas_formulacion SET proyfprsino = '".$sino."',proyfprobs = '".$observacion."' 
                          WHERE proycod=".$proyecto." AND formnum = ".$codigo;
                $result = DB::update($query); 
            
            } else {                

                $dataAdd = new ProyectosActividades();
                
                $dataAdd->formnum     = $codigo;
                $dataAdd->proycod     = $proyecto;
                $dataAdd->proyfprsino = $sino;
                $dataAdd->proyfprobs  = $observacion;               
                
                $result = $dataAdd->save();
            
            }
        }

        return array( 'respuesta' => $result);         
    }
}

?>