<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\CausalesContratacionTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CausalesContratacion;
use DB;

class CausalesContratacionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $CausalesContratacion = CausalesContratacion::select('cauconcod','caucondes')
                     ->orderby('caucondes')                     
                     ->get();
                     
            if ($CausalesContratacion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($CausalesContratacion, new CausalesContratacionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = CausalesContratacion::max('cauconcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = CausalesContratacion::select(DB::raw('count(cauconcod) as total'))                      
                      ->where('caucondes', $Descripcion)
                      ->where('cauconcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CausalesContratacionAddUpdate(Request $request)
    {
        $CausalesContratacion = $request->data;

        $codigo = $CausalesContratacion['codigo'];
        $descripcion = strtoupper(trim($CausalesContratacion['descripcion']));       

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $CausalesContratacionAdd = new CausalesContratacion();
            
            $CausalesContratacionAdd->cauconcod = $consecutivo;           
            $CausalesContratacionAdd->caucondes = $descripcion;           
            
            $result = $CausalesContratacionAdd->save();
        
        } else {

            $CausalesContratacionUpd = CausalesContratacion::where('cauconcod', $codigo)->first();
          
            $CausalesContratacionUpd->caucondes = $descripcion;          

            // Guardamos en base de datos
            $result = $CausalesContratacionUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = CausalesContratacion::where('cauconcod', $Id)->delete(); 
        return $result;

    }
}

?>