<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\EstadosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Estados;
use DB;

class EstadosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Estados = Estados::select('estados.estcod','estados.estdes','tipos_estados.tipestcod','tipos_estados.tipestdes')
                        ->join('tipos_estados','estados.tipestcod','=','tipos_estados.tipestcod')     
                        ->orderby('estados.estdes')                     
                        ->get();
                     
            if ($Estados->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Estados, new EstadosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function EstadosXTipo(Request $request)
    {     
       $tipo = $request['tipo'];      
        
       try{

            $Estados = Estados::select('estados.estcod','estados.estdes','tipos_estados.tipestcod','tipos_estados.tipestdes')
                        ->join('tipos_estados','estados.tipestcod','=','tipos_estados.tipestcod')    
                        ->where('tipos_estados.tipestcod',$tipo) 
                        ->orderby('estados.estcod')                     
                        ->get();
                     
            if ($Estados->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Estados, new EstadosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Estados::max('estcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Estados::select(DB::raw('count(estcod) as total'))                      
                      ->where('estdes', $Nombre)
                      ->where('estcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo = $data['codigo'];
        $descripcion = strtoupper(trim($data['descripcion']));
        $tipocodigo = $data['tipocodigo'];
       
        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new Estados();
            
            $dataAdd->estcod = $consecutivo;           
            $dataAdd->estdes = $descripcion;
            $dataAdd->tipestcod = $tipocodigo;           
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = Estados::where('estcod', $codigo)->first();
          
            $dataUpd->estdes = $descripcion;
            $dataUpd->tipestcod = $tipocodigo;
            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Estados::where('estcod', $Id)->delete(); 
        return $result;

    }
}

?>