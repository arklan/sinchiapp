<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\LineasInvestigacionTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LineasInvestigacion;
use DB;

class LineasInvestigacionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $LineasInvestigacion = LineasInvestigacion::select('lininvcod','lininvdes')
                     ->orderby('lininvdes')                     
                     ->get();
                     
            if ($LineasInvestigacion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($LineasInvestigacion, new LineasInvestigacionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = LineasInvestigacion::max('lininvcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = LineasInvestigacion::select(DB::raw('count(lininvcod) as total'))                      
                      ->where('lininvdes', $Descripcion)
                      ->where('lininvcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Descripcion ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function LineasInvestigacionAddUpdate(Request $request)
    {
        $LineasInvestigacion = $request->data;

        $codigo = $LineasInvestigacion['codigo'];
        $Descripcion = strtoupper(trim($LineasInvestigacion['Descripcion']));       

        $mensaje = $this->buscarDuplicado($Descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $LineasInvestigacionAdd = new LineasInvestigacion();
            
            $LineasInvestigacionAdd->lininvcod = $consecutivo;           
            $LineasInvestigacionAdd->lininvdes = $Descripcion;           
            
            $result = $LineasInvestigacionAdd->save();
        
        } else {

            $LineasInvestigacionUpd = LineasInvestigacion::where('lininvcod', $codigo)->first();
          
            $LineasInvestigacionUpd->lininvdes = $Descripcion;          

            // Guardamos en base de datos
            $result = $LineasInvestigacionUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = LineasInvestigacion::where('lininvcod', $Id)->delete(); 
        return $result;

    }
}

?>