<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\CategoriasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Categorias;
use DB;

class CategoriasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Categorias = Categorias::select('catcod','catdes')
                     ->orderby('catdes')                     
                     ->get();
                     
            if ($Categorias->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Categorias, new CategoriasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Categorias::max('catcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Categorias::select(DB::raw('count(catcod) as total'))                      
                      ->where('catdes', $Nombre)
                      ->where('catcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoriaAddUpdate(Request $request)
    {
        $categoria = $request->data;

        $codigo = $categoria['codigo'];
        $descripcion = strtoupper(trim($categoria['descripcion']));       

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $categoriaAdd = new Categorias();
            
            $categoriaAdd->catcod = $consecutivo;           
            $categoriaAdd->catdes = $descripcion;           
            
            $result = $categoriaAdd->save();
        
        } else {

            $categoriaUpd = Categorias::where('catcod', $codigo)->first();
          
            $categoriaUpd->catdes = $descripcion;          

            // Guardamos en base de datos
            $result = $categoriaUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Categorias::where('catcod', $Id)->delete(); 
        return $result;

    }
}

?>