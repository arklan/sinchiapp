<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosProdPropIndTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosProdPropInd;
use DB;

class ProyectosProdPropIndController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Data = ProyectosProdPropInd::select('proyectos_productos_proindustrial.prodproindcod','proyectos_productos_proindustrial.proycod', 
                                                 'productos_propiedad_industrial.prodproinddes')   
                                         ->join('productos_propiedad_industrial','proyectos_productos_proindustrial.prodproindcod','=','productos_propiedad_industrial.prodproindcod')  
                                         ->orderby('proyectos_productos_proindustrial.prodproindcod')                     
                                         ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new ProyectosProdPropIndTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];

            $data = ProyectosProdPropInd::select('proyectos_productos_proindustrial.prodproindcod','proyectos_productos_proindustrial.proycod', 
                                               'productos_propiedad_industrial.prodproinddes')   
                                        ->join('productos_propiedad_industrial','proyectos_productos_proindustrial.prodproindcod','=','productos_propiedad_industrial.prodproindcod')                        
                                        ->where('proyectos_productos_proindustrial.proycod', $proyecto)
                                        ->orderby('proyectos_productos_proindustrial.prodproindcod')                     
                                        ->get();
                     
            if ($data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($data, new ProyectosProdPropIndTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;
        $proyecto = $request->proyecto;

        try{

            $result = ProyectosProdPropInd::where('prodproindcod', $Id)->where('proycod', $proyecto)->delete();
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }
    
    public function seleccionadosProyectos(Request $request)
    {
        try{
            
            $proyecto = $request['proyecto'];
                     
            $sql = "SELECT o.prodproindcod codigo,o.prodproinddes descripcion,IF(po.proycod IS NULL,0,1) Seleccionado 
                    FROM productos_propiedad_industrial o LEFT JOIN 
                        proyectos_productos_proindustrial po ON o.prodproindcod = po.prodproindcod and po.proycod=".$proyecto;            
            
            $result = DB::select($sql);
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = false;
        
        if (count($data) > 0 ){

            for ($i=0; $i < count($data); $i++) {

                if($i==0){

                    $proyecto = $data[$i]['proyecto'];
                    $result = ProyectosProdPropInd::where('proycod', $proyecto)->delete();
                }

                $dataSave = new ProyectosProdPropInd();

                $dataSave->proycod = $data[$i]['proyecto'];
                $dataSave->prodproindcod = $data[$i]['codigo'];
                
                $result = $dataSave->save();
            }
        }

        return array( 'respuesta' => $result);        

    }
    
}

?>