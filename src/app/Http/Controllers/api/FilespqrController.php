<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Adjunto;

class FilesController extends Controller
{

    public function download(Request $request)
    {
        $filename = $request->filename;
        return response()->download(public_path('pqr_adjuntos/'.$filename));
    }

    public function upload(Request $request)
    {
        $random = str_random(10);
        $extension = $request->file('archivo')->getClientOriginalExtension();
        $filename = $random.'.'.$extension;
        $path = $request->file('archivo')->move(public_path('pqr_adjuntos/'),$filename);
        $fileURL = url('/pqr_adjuntos/'.$filename);

        return response()->json(['url' => $fileURL,'filename' => $filename],200);
    }

    public function delete(Request $request)
    {

        $archivo = $request->archivo;
        $id = $request->id;
        
        $respuesta = '';

        try {
            
            $rutacompleta = public_path('pqr_adjuntos/'.$archivo);

            unlink($rutacompleta);

            $result = Adjunto::where('id', $id)->delete(); 

            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se eiminó el Archivo con éxito'],200);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);

        }
        
        return $respuesta;
        
    }

    public function deleteOficio(Request $request)
    {

        $archivo = $request->archivo;
        
        $respuesta = '';

        try {
            
            $rutacompleta = public_path('pqr_adjuntos/'.$archivo);

            unlink($rutacompleta);

            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se eiminó el Archivo con éxito'],200);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);

        }
        
        return $respuesta;
        
    }

}