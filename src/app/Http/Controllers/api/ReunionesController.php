<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ReunionesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Reuniones;
use App\ReunionesExterno;
use App\Transformers\ReunionesExternoTransformer;
use App\ReunionesPersonalAsistentes;
use App\Transformers\ReunionesPersonalAsistentesTransformer;
use App\ReunionesOrden;
use App\Transformers\ReunionesOrdenTransformer;
use App\ReunionesAgenda;
use App\Transformers\ReunionesAgendaTransformer;
use App\ReunionesTarea;
use App\Transformers\ReunionesTareaTransformer;
use DB;

class ReunionesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Reuniones = Reuniones::select('reuniones.reucod','reuniones.reutit',DB::raw('CASE WHEN reuniones.proycod IS NOT NULL THEN 1 WHEN reuniones.plaadqcod IS NOT NULL THEN 2 WHEN reuniones.plaadqneccod IS NOT NULL THEN 3 WHEN reuniones.solcod IS NOT NULL THEN 4 END as proceso'),DB::raw('CASE WHEN reuniones.proycod IS NOT NULL THEN reuniones.proycod WHEN reuniones.plaadqcod IS NOT NULL THEN reuniones.proycod WHEN reuniones.plaadqneccod IS NOT NULL THEN reuniones.proycod WHEN reuniones.solcod IS NOT NULL THEN reuniones.proycod END as SubProceso'), 'reuniones.reufec','reuniones.reuhorini','reuniones.reuhorfin','reuniones.reuenl','reuniones.perscodcit','reuniones.perscodact','reuniones.reulug','reuniones.sedcod','sede.sednom','reuniones.reures','p1.persape as apecitador','p1.persnom as nomcitador','p2.persape as apeacta','p2.persnom as nomacta', DB::raw("(SELECT reuactcod FROM reuniones_acta WHERE reucod = reuniones.reucod ORDER BY reuactcod DESC limit 1) as codacta"), DB::raw("(SELECT reuactest FROM reuniones_acta WHERE reucod = reuniones.reucod ORDER BY reuactcod DESC limit 1) as estadoacta, reuniones.reuadj"))             
                                  ->join('sede','sede.sedcod','=','reuniones.sedcod')
                                  ->join('personal as p1','p1.perscod','=','reuniones.perscodcit')
                                  ->join('personal as p2','p2.perscod','=','reuniones.perscodact')
                                  ->orderby('reuniones.reutit')                     
                                  ->get();
                     
            if ($Reuniones->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Reuniones, new ReunionesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Reuniones::max('reucod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Reuniones::select(DB::raw('count(reucod) as total'))                      
                      ->where('reutit', $Nombre)
                      ->where('reucod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    private function AsistentesxCodReunion($codigo)
    {       

       try{

            $ReunionesPersonalAsistentes = ReunionesPersonalAsistentes::select('reuniones_personal_asistentes.reupercodasi','reuniones_personal_asistentes.perscod','reuniones_personal_asistentes.reucod','reuniones_personal_asistentes.reuperasi','reuniones_personal_asistentes.reupersolapro', 'personal.persape','personal.persnom') 
                     ->join('personal','personal.perscod','=','reuniones_personal_asistentes.perscod') 
                     ->where('reuniones_personal_asistentes.reucod',$codigo)
                     ->orderby('personal.persnom','personal.persape')                     
                     ->get();
                     
            return $ReunionesPersonalAsistentes;
            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reunionAddUpdate(Request $request)
    {
        $reunion = $request->data;

        $codigo = $reunion['codigo'];
        $titulo = strtoupper(trim($reunion['titulo']));       
        $proyecto = $reunion['codproyecto'];
        $planaqui = $reunion['codplanaqui'];
        $nesecidad = $reunion['codnecesidad'];
        $solicitud = $reunion['codsolicitud'];
        $fecha = $reunion['fecha'];
        $horainicial = $reunion['horaini'];
        $horafinal = $reunion['horafin'];
        $lugar = $reunion['lugar'];
        $enlace = $reunion['enlace'];
        $citador = $reunion['citador'];
        $responacta = $reunion['elaboracta'];
        $sede = $reunion['sede'];
        $adjunto = $reunion['adjunto'];

        $mensaje = $this->buscarDuplicado($titulo, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){            

            $consecutivo =$this->consecutivo();

            $ReunionAdd = new Reuniones();
            
            $ReunionAdd->Reucod = $consecutivo;           
            $ReunionAdd->Reutit = $titulo;
            IF( $proyecto > 0)
            {
                $ReunionAdd->proycod = $proyecto;
            }
            IF( $planaqui > 0)
            {
                $ReunionAdd->plaadqcod = $planaqui;
            }
            IF( $nesecidad > 0)
            {
                $ReunionAdd->plaadqneccod = $nesecidad;
            }
            IF( $solicitud > 0)
            {
                $ReunionAdd->solcod = $solicitud;
            }

            $ReunionAdd->reufec = $fecha;
            $ReunionAdd->reuhorini = $horainicial;
            $ReunionAdd->reuhorfin = $horafinal;
            $ReunionAdd->reuenl = $enlace;
            $ReunionAdd->perscodcit = $citador;
            $ReunionAdd->reulug = $lugar;           
            $ReunionAdd->perscodact = $responacta;           
            $ReunionAdd->sedcod = $sede;

            if($adjunto != ''){

                $arcpdf = "adjunto_".$consecutivo.".pdf";
                $success = \File::copy(public_path('temp/'.$adjunto),storage_path('adjuntoreunion/'.$arcpdf));

                if($success){
                    $ReunionAdd->reuadj = $arcpdf;
                }
                
            }


            $result = $ReunionAdd->save();
            
            

            

        } else {

            $reunionUpd = Reuniones::where('reucod', $codigo)->first();
          
            $reunionUpd->Reutit = $titulo;
            IF( $proyecto > 0)
            {
                $reunionUpd->proycod = $proyecto;
            }else{
                $reunionUpd->proycod = null;
            }
            IF( $planaqui > 0)
            {
                $reunionUpd->plaadqcod = $planaqui;
            }else{
                $reunionUpd->plaadqcod = null;
            }
            IF( $nesecidad > 0)
            {
                $reunionUpd->plaadqneccod = $nesecidad;
            }else{
                $reunionUpd->plaadqneccod = null;
            }
            IF( $solicitud > 0)
            {
                $reunionUpd->solcod = $solicitud;
            }else{
                $reunionUpd->solcod = null;
            }

            $reunionUpd->reufec = $fecha;
            $reunionUpd->reuhorini = $horainicial;
            $reunionUpd->reuhorfin = $horafinal;
            $reunionUpd->reuenl = $enlace;
            $reunionUpd->perscodcit = $citador;
            $reunionUpd->reulug = $lugar;           
            $reunionUpd->perscodact = $responacta;             
            $reunionUpd->sedcod = $sede;
            // Guardamos en base de datos

            $arcpdf = "adjunto_".$codigo.".pdf";                

            if(\File::exists(storage_path('adjuntoreunion/'.$arcpdf))) {
                
                \File::delete(storage_path('adjuntoreunion/'.$arcpdf)); 
            }

            if($adjunto != ''){                

                $success = \File::copy(public_path('temp/'.$adjunto),storage_path('adjuntoreunion/'.$arcpdf));
                
                if($success){
                    $reunionUpd->reuadj = $arcpdf;
                }
                
            }else{
                $reunionUpd->reuadj = null;
            }

            $result = $reunionUpd->save();
            
        } 

        return array( 'respuesta' => $result);     
    }

    public function updResumen(Request $request){

        $codigo = $request->data['codigo'];
        $resumen = $request->data['resumen'];

        $reunionUpd = Reuniones::where('reucod', $codigo)->first();
          
        $reunionUpd->reures = $resumen;
        
        // Guardamos en base de datos
        $result = $reunionUpd->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Reuniones::where('reucod', $Id)->delete(); 
        return $result;

    }
    private function GetReunionXId($Codigo)
    {       
       try{

            $Reuniones = Reuniones::select('reuniones.reucod','reuniones.reutit',DB::raw('CASE WHEN reuniones.proycod IS NOT NULL THEN 1 WHEN reuniones.plaadqcod IS NOT NULL THEN 2 WHEN reuniones.plaadqneccod IS NOT NULL THEN 3 WHEN reuniones.solcod IS NOT NULL THEN 4 END as proceso'),DB::raw('CASE WHEN reuniones.proycod IS NOT NULL THEN reuniones.proycod WHEN reuniones.plaadqcod IS NOT NULL THEN reuniones.proycod WHEN reuniones.plaadqneccod IS NOT NULL THEN reuniones.proycod WHEN reuniones.solcod IS NOT NULL THEN reuniones.proycod END as SubProceso'), 'reuniones.reufec','reuniones.reuhorini','reuniones.reuhorfin','reuniones.reuenl','reuniones.perscodcit','reuniones.perscodact','reuniones.reulug','reuniones.sedcod','sede.sednom','reuniones.reures','p1.persape as apecitador','p1.persnom as nomcitador','p2.persape as apeacta','p2.persnom as nomacta', DB::raw("(SELECT reuactcod FROM reuniones_acta WHERE reucod = reuniones.reucod ORDER BY reuactcod DESC limit 1) as codacta"), DB::raw("(SELECT reuactest FROM reuniones_acta WHERE reucod = reuniones.reucod ORDER BY reuactcod DESC limit 1) as estadoacta"))             
                                  ->join('sede','sede.sedcod','=','reuniones.sedcod')
                                  ->join('personal as p1','p1.perscod','=','reuniones.perscodcit')
                                  ->join('personal as p2','p2.perscod','=','reuniones.perscodact')
                                  ->where('reuniones.reucod',$Codigo)
                                  ->orderby('reuniones.reutit')                     
                                  ->get();
                     
            return $Reuniones;                
            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 
    
    private function ReunionesExternoxCodReunion($Codigo)
    {       

       try{

            $ReunionesExterno = ReunionesExterno::select('reuniones_externo.reuextcod','reuniones_externo.reuextnom','reuniones_externo.reuextema','reuniones_externo.reucod')                      
                     ->where('reuniones_externo.reucod',$Codigo)
                     ->orderby('reuniones_externo.reuextnom')                     
                     ->get();
                     
            return $ReunionesExterno;
            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    private function ReunionesOrdenxCodReunion($Codigo)
    {       
               
       try{

            $ReunionesOrden = ReunionesOrden::select('reuniones_orden.reuordcod', 'reuniones_orden.reuordnum', 'reuniones_orden.reuordtem', 'reuniones_orden.perscod', 'reuniones_orden.reuordobs', 'reuniones_orden.reucod','personal.persape','personal.persnom')                      
                     ->join('Personal','personal.perscod','=','reuniones_orden.perscod')
                     ->where('reuniones_orden.reucod',$Codigo)
                     ->orderby('reuniones_orden.reuordnum')                     
                     ->get();
                     
            return $ReunionesOrden;                
            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    private function ReunionesAgendaxCodReunion($Codigo)
    {       
               
       try{

            $ReunionesAgenda = ReunionesAgenda::select('reuagecod', 'reuagenum', 'reuagedes', 'reucod')                                           
                     ->where('reucod',$Codigo)
                     ->orderby('reuagenum')                     
                     ->get();
                     
            return $ReunionesAgenda;
            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    private function ReunionesTareaXCodReunion($Codigo)
    {       
       try{

        
            $ReunionesTarea = ReunionesTarea::select('reutarcod','reutar','reutarrecapr','reutarfeclim','reutartip', DB::raw('CASE WHEN reutarest = 1 THEN "PROGRAMADO" WHEN reutarest = 2 THEN "EJECUTADO" WHEN reutarest = 3 THEN "VENCIDO" END as reutarestdes'),'reutarest','reucod') 
                                            ->where('reucod', $Codigo)
                                            ->orderby('reutarcod')                     
                                            ->get();
            
            $tareas=[];

            foreach ($ReunionesTarea as $key => $row) {

                    $ReunionesTareaResponsables = ReunionesTarea::select('reuniones_tarea.reutarcod','reuniones_tarea.reutar','reuniones_tarea.reutarrecapr','reuniones_tarea.reutarfeclim','reuniones_tarea.reutartip','reuniones_tarea.reutarest','reuniones_tarea.reucod','personal.perscod','personal.persape','personal.persnom') 
                     ->join('reuniones_tarea_personal','reuniones_tarea_personal.reutarcod','=','reuniones_tarea.reutarcod')
                     ->join('Personal','personal.perscod','=','reuniones_tarea_personal.perscod')
                     ->where('reuniones_tarea.reutarcod', $row['reutarcod'])
                     ->orderby('reuniones_tarea.reutarcod')                     
                     ->get();

                    array_push($tareas,['reutarcod'=> $row['reutarcod'], 
                                        'reutar'=> $row['reutar'], 
                                        'reutarrecapr'=>$row['reutarrecapr'],
                                        'reutarfeclim'=>$row['reutarfeclim'],
                                        'reutartip'=>$row['reutartip'],
                                        'reutarest'=>$row['reutarest'],
                                        'reutarestdes'=>$row['reutarestdes'],
                                        'reucod'=>$row['reucod'],
                                        'reurespons'=>$ReunionesTareaResponsables
                                        ]);

            }               

            return $tareas;

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function UploadAdjunto(Request $request)
    {        
        
        $adjunto = $request->archivo;

        $nombreadjunto = 'adjreu_'.str_replace(":", "", str_replace("-", "", date('d-m-Y_h:i:s'))).'.pdf';

        $adjunto->move(public_path('temp'),$nombreadjunto);

        return response()->json(["data"=>["nombrepdf" => $nombreadjunto, "ruta" => public_path("temp")] ]);

    }

    public function getActaReuniones(Request $request)
    {        
        $reunion = $request->input('reunion', '');        
        $mode = $request->input('mode', 'I');

        return $this->generateActaReunionPdf($reunion, $mode);

    }

    public function getActa(Request $request){
        
        $archivo = $request->input('archivo', '');     
        return response()->download(storage_path('actareunionpdf/'.$archivo), null, [], null);
    }

    public function getveradjunto(Request $request){
        
        $archivo = $request->input('archivo', '');     
        return response()->download(storage_path('adjuntoreunion/'.$archivo), null, [], null);
    }

    public function getActaReunionesEmail(Request $request)
    {        
        $reunion = $request->id;      
                
        $rta = $this->generateActaReunionPdf($reunion, "S");

        return  response()->json(["data"=>["nombrepdf" => $rta, "ruta" => public_path("temp")] ]);

    }

    public function generateActaReunionPdf ($reunioncod, $mode){        
        
        $datos['reunion'] = $this->GetReunionXId($reunioncod);       
        $datos['asistexternos'] = $this->ReunionesExternoxCodReunion($reunioncod);
        $datos['asistinternos'] = $this->AsistentesxCodReunion($reunioncod); 
        $datos['orden'] = $this->ReunionesOrdenxCodReunion($reunioncod);     
        $datos['desarrollo'] = $this->ReunionesAgendaxCodReunion($reunioncod);
        $datos['tareas'] = $this->ReunionesTareaXCodReunion($reunioncod);
      

        $fechaDoc = str_replace(":", "", str_replace("-", "", date('d-m-Y_h:i:s')));
        
        $namePDF = 'ACTAREU_'.$fechaDoc."_".$reunioncod.".pdf";

        // $headerPDF = view()->make('Reuniones.ActaPIHeader', $datos)->render();
        
        $pdf = \PDF::loadView('Formatos.ActaReunion',$datos);
        $pdf->setOption('no-outline', true);
        $pdf->setOption('disable-smart-shrinking', true);
        $pdf->setOption('dpi', 600);
        $pdf->setOption('image-quality', 100);
        $pdf->setOption('copies', 1);
        $pdf->setOption('page-size', 'Letter');
        $pdf->setOption('margin-top', 50);
        $pdf->setOption('margin-bottom', 45);
        $pdf->setOption('header-spacing', 0);
        $pdf->setOption('footer-spacing', 0);
        // $pdf->setOption('header-html', $headerPDF);
        
        $rta = '';
        switch ($mode) {
            case 'I':
                $rta = $pdf->stream($namePDF);
                break;
            
            case 'D':
                $rta = $pdf->download($namePDF);
                break;
            
            case 'O':
                $rta = $pdf->output();
                break;            
            
            case 'S':
                    $pdf->save(public_path("/temp/".$namePDF));
                    $rta = $namePDF;
                    break; 
            default:
                # code...
                break;
        }

        return $rta;        
    }
}

?>