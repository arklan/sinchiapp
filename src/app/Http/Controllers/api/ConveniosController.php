<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ConveniosTransformer;
use App\Transformers\ConveniosSociosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Convenios;
use App\ConveniosSocios;

use App\ConveniosFecPreInfTec;
use App\ConveniosFecPreInfFin;

use DB;

use Illuminate\Support\Facades\Storage;

class ConveniosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Convenios = Convenios::select('convenios.convcod','convenios.convnum','convenios.convnom','convenios.convfecsus',
                                            'convenios.convfectereje','convenios.convfecinc','convenios.convtieeje',
                                            'convenios.moncod','convenios.convtas','convenios.convvalcof',
                                            'convenios.convvalctp','convenios.convvaltot','convenios.convvaldin',
                                            'convenios.convtotesp','convenios.estcod','convenios.perscod',
                                            'convenios.convenc','convenios.convobj','convenios.convcaf',
                                            'convenios.convaff','convenios.convtff','convenios.convfuefin',
                                            'convenios.convarcgen','convenios.convarcoda','convenios.convarccon',
                                            'estados.estdes','personal.persnom','personal.persape',
                                            DB::raw('CONCAT(caf.persnom, " ", caf.persape) as perscaf'),
                                            DB::raw('CONCAT(aff.persnom, " ", aff.persape) as persaff'),
                                            DB::raw('CONCAT(ttf.persnom, " ", ttf.persape) as persttf'),
                                            'monedas.monsim','encargado.persnom as convencnom',
                                            'encargado.persape as convencape','convenios.convtecfuefin',
                                            'convenios.convcontaff')
                     ->join('estados','convenios.estcod','=','estados.estcod')
                     ->join('personal','convenios.perscod','=','personal.perscod')
                     ->join('monedas','convenios.moncod','=','monedas.moncod')
                     ->leftJoin('personal as caf','convenios.convcaf','=','caf.perscod')
                     ->leftJoin('personal as aff','convenios.convaff','=','aff.perscod')
                     ->leftJoin('personal as ttf','convenios.convtff','=','ttf.perscod')                     
                     ->leftjoin('personal as encargado','convenios.convenc','=','encargado.perscod')
                     ->orderby('convcod')    
                     //->limit(20)                  
                     ->get();
                     
            if ($Convenios->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Convenios, new ConveniosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function filtroXConvenio(Request $request)
    {       
       try{

            $convenio = $request->convenio;

            $Convenios = Convenios::select('convenios.convcod', 'convenios.convnum','convenios.convnom','convenios.convfecsus',
                                            'convenios.convfectereje','convenios.convfecinc','convenios.convtieeje',
                                            'convenios.moncod','convenios.convtas','convenios.convvalcof',
                                            'convenios.convvalctp','convenios.convvaltot','convenios.convvaldin',
                                            'convenios.convtotesp','convenios.estcod','convenios.perscod',
                                            'convenios.convenc','convenios.convobj','convenios.convcaf',
                                            'convenios.convaff','convenios.convtff','convenios.convfuefin',
                                            'convenios.convarcgen','convenios.convarcoda','convenios.convarccon',
                                            'estados.estdes','personal.persnom','personal.persape',
                                            DB::raw('CONCAT(caf.persnom, " ", caf.persape) as perscaf'),
                                            DB::raw('CONCAT(aff.persnom, " ", aff.persape) as persaff'),
                                            DB::raw('CONCAT(ttf.persnom, " ", ttf.persape) as persttf'),
                                            'monedas.monsim','encargado.persnom as convencnom',
                                            'encargado.persape as convencape','convenios.convtecfuefin',
                                            'convenios.convcontaff')
                     ->join('estados','convenios.estcod','=','estados.estcod')
                     ->join('personal','convenios.perscod','=','personal.perscod')
                     ->join('monedas','convenios.moncod','=','monedas.moncod')
                     ->leftJoin('personal as caf','convenios.convcaf','=','caf.perscod')
                     ->leftJoin('personal as aff','convenios.convaff','=','aff.perscod')
                     ->leftJoin('personal as ttf','convenios.convtff','=','ttf.perscod')                     
                     ->leftjoin('personal as encargado','convenios.convenc','=','encargado.perscod')
                     ->where('convenios.convcod',$convenio)
                     ->orderby('convcod')                                         
                     ->get();
                     
            if ($Convenios->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Convenios, new ConveniosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function ConvenioSociosXConvenio(Request $request)
    {       
       try{

            $convenio = $request->convenio;

            $Convenios = ConveniosSocios::select('convenios.convcod','convenios.moncod','convenios.convtas','convenios_socios.convsocvalcof',
                                           'convenios_socios.convsocvalctp','convenios_socios.convsocvaltot','convenios_socios.convsocvaldin',
                                           'convenios_socios.convsocfuefin','convenios_socios.convsoctotesp','monedas.monsim')
                     ->join('convenios','convenios_socios.convcod','=','convenios.convcod')                     
                     ->join('monedas','convenios.moncod','=','monedas.moncod')                    
                     ->where('convenios.convcod',$convenio)
                     ->orderby('convcod')                                         
                     ->get();
                     
            if ($Convenios->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Convenios, new ConveniosSociosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Convenios::max('convcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Numero,$Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Convenios::select(DB::raw('count(convcod) as total'))                      
                      ->where('convnum', $Numero)
                      ->where('convcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El No. del Convenio ya existe';

                } else {

                    $Duplica = Convenios::select(DB::raw('count(convcod) as total'))                      
                                ->where('convnom', $Nombre)
                                ->where('convcod','!=', $Codigo)
                                ->get();
                    
                    if (!$Duplica->isEmpty()){

                        $total = $Duplica[0]->total;

                        if($total != '0'){

                            $mensaje = 'El Nombre del Convenio ya existe';

                        }

                    }
                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DataAddUpdate(Request $request)
    {
        $data = $request->data;
        $FechasPIT = $request->FechasPIT;
        $FechasPIF = $request->FechasPIF;
        $socios = $request->Socios;

        $codigo             = $data['codigo'];
        $numero             = trim($data['numero']);
        $fechasuscripcion   = $data['fechasuscripcion'];
        $fechaterejecucion  = $data['fechaterejecucion']; 
        $fechaincorporacion = $data['fechaincorporacion'];
        $tiempo             = $data['tiempo'];   
        $moneda             = $data['moneda'];
        $tasa               = $data['tasa'];
        $cofinanciado       = $data['cofinanciado'];
        $contrapartida      = $data['contrapartida'];
        $totaldinero        = $data['totaldinero'];
        $totalespecie       = $data['totalespecie'];        
        $valortotal         = $data['valortotal'];
        $codigoestado       = $data['codigoestado'];            
        $codigocoordinador  = $data['codigocoordinador'];
        $codigoencargado    = $data['codigoencargado'];
        $objeto             = $data['objeto'];
        $fuentefinanciacion = $data['fuentefinanciacion'];
        $codigocontactoafn  = $data['contactoafn'];
        $codigocontactoaff  = $data['contactoaff'];
        $codigocontactotff  = $data['contactotff'];        
        $nombre             = $data['nombre'];
        $contactotecfuefin  = strtoupper(trim($data['contactotecfuefin']));
        $contactoadmfuefin  = strtoupper(trim($data['contactoadmfuefin']));

        //archivos
        $fileconvenio = $data['archivogeneral'];
        $fileotda     = $data['archivootda'];
        $filecont     = $data['archivocont'];

        $mensaje = $this->buscarDuplicado($numero,$nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();
           
            $ConveniosAdd = new Convenios();
            
            $ConveniosAdd->convcod       = $consecutivo;           
            $ConveniosAdd->convnum       = $numero; 
            $ConveniosAdd->convnom       = $nombre; 
            $ConveniosAdd->convfecsus    = $fechasuscripcion;
            $ConveniosAdd->convfectereje = $fechaterejecucion;
            $ConveniosAdd->convfecinc    = $fechaincorporacion;
            $ConveniosAdd->convtieeje    = $tiempo;
            $ConveniosAdd->moncod        = $moneda;
            $ConveniosAdd->convtas       = $tasa;
            $ConveniosAdd->convvalcof    = $cofinanciado;           
            $ConveniosAdd->convvalctp    = $contrapartida;
            $ConveniosAdd->convvaldin    = $totaldinero;
            $ConveniosAdd->convtotesp    = $totalespecie;
            $ConveniosAdd->convvaltot    = $valortotal;
            $ConveniosAdd->estcod        = $codigoestado;
            $ConveniosAdd->perscod       = $codigocoordinador; 
            $ConveniosAdd->convenc       = $codigoencargado;
            $ConveniosAdd->convobj       = $objeto;
            $ConveniosAdd->convfuefin    = $fuentefinanciacion;
            $ConveniosAdd->convcaf       = $codigocontactoafn;
            $ConveniosAdd->convaff       = $codigocontactoaff;
            $ConveniosAdd->convtff       = $codigocontactotff;            
            $ConveniosAdd->convarcgen    = $fileconvenio['file_name'];
            $ConveniosAdd->convarcoda    = $fileotda['file_name'];
            $ConveniosAdd->convarccon    = $filecont['file_name'];
            $ConveniosAdd->convtecfuefin = $contactotecfuefin;
            $ConveniosAdd->convcontaff   = $contactoadmfuefin;
            
            $result = $ConveniosAdd->save();
        
        } else {

            $ConveniosUpd = Convenios::where('convcod', $codigo)->first();
          
            $ConveniosUpd->convnum       = $numero; 
            $ConveniosUpd->convnom       = $nombre;
            $ConveniosUpd->convfecsus    = $fechasuscripcion;
            $ConveniosUpd->convfectereje = $fechaterejecucion;
            $ConveniosUpd->convfecinc    = $fechaincorporacion;
            $ConveniosUpd->convtieeje    = $tiempo;
            $ConveniosUpd->moncod        = $moneda;
            $ConveniosUpd->convtas       = $tasa;
            $ConveniosUpd->convvalcof    = $cofinanciado;           
            $ConveniosUpd->convvalctp    = $contrapartida;
            $ConveniosUpd->convvaldin    = $totaldinero;
            $ConveniosUpd->convtotesp    = $totalespecie;
            $ConveniosUpd->convvaltot    = $valortotal;
            $ConveniosUpd->estcod        = $codigoestado;
            $ConveniosUpd->perscod       = $codigocoordinador; 
            $ConveniosUpd->convenc       = $codigoencargado;
            $ConveniosUpd->convobj       = $objeto;
            $ConveniosUpd->convfuefin    = $fuentefinanciacion;
            $ConveniosUpd->convcaf       = $codigocontactoafn;
            $ConveniosUpd->convaff       = $codigocontactoaff;
            $ConveniosUpd->convtff       = $codigocontactotff;            
            $ConveniosUpd->convarcgen    = $fileconvenio['file_name'];
            $ConveniosUpd->convarcoda    = $fileotda['file_name'];
            $ConveniosUpd->convarccon    = $filecont['file_name'];
            $ConveniosUpd->convtecfuefin = $contactotecfuefin;
            $ConveniosUpd->convcontaff   = $contactoadmfuefin;          

            // Guardamos en base de datos
            $result = $ConveniosUpd->save();
           
        } 

        $codigoactual = 0;

        if($codigo=='0'){

            $codigoactual = $consecutivo;                    

        } else {

            $codigoactual = $codigo;

        }

        //Fechas Present. Informes Técnicos
        if(count($FechasPIT) > 0){

            $arrDataActual = [];            

            if($codigo!='0'){

                $result = ConveniosFecPreInfTec::where('convcod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($FechasPIT); $i++) {                

                $arrDataActual[] = 
                    [                        
                        'convcod'  => $codigoactual,
                        'convfpit' => $FechasPIT[$i]['fechapresentacion']                        
                    ];
                                               
            }

            ConveniosFecPreInfTec::insert($arrDataActual);
        }

        //Fechas Present. Informes Financieros
        if(count($FechasPIF) > 0){

            $arrDataActual = [];            

            if($codigo!='0'){

                $result = ConveniosFecPreInfFin::where('convcod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($FechasPIF); $i++) {                

                $arrDataActual[] = 
                    [                        
                        'convcod'  => $codigoactual,
                        'convfpif' => $FechasPIF[$i]['fechapresentacion']                        
                    ];
                                               
            }

            ConveniosFecPreInfFin::insert($arrDataActual);
        }

        //convenios socios        
        if(count($socios) > 0){

            $arrDataActual = [];            

            if($codigo!='0'){

                $result = ConveniosSocios::where('convcod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($socios); $i++) {                

                $arrDataActual[] = 
                    [                        
                        'convcod'       => $codigoactual,
                        'convsoccod'    => $i + 1,
                        'convsocfuefin' => $socios[$i]['fuentefinanciacion'],
                        'convsocvalcof' => $socios[$i]['cofinanciado'],
                        'convsocvalctp' => $socios[$i]['contrapartida'],
                        'convsocvaldin' => $socios[$i]['totaldinero'],
                        'convsoctotesp' => $socios[$i]['totalespecie'],
                        'convsocvaltot' => $socios[$i]['valortotal'],                        
                    ];
                                               
            }            

            ConveniosSocios::insert($arrDataActual);
        }
        
        /************************** archivos    ************************ 
         * *************************************************************/        

        $path = public_path('/convenios/C' . $codigoactual);
        $pathorigen = public_path('tmp/'); 

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        //archivo del convenio
        if($fileconvenio['file_name'] != '' && $fileconvenio['file_name_tmp'] != ''){
            $filename = $fileconvenio['file_name'];        

            copy($pathorigen.$fileconvenio['file_name_tmp'], $path.'/'.$filename);            
        }         

        //archivo contrapartida
        if($filecont['file_name'] != ''  && $filecont['file_name_tmp'] != ''){
            $filename = $filecont['file_name'];        

            copy($pathorigen.$filecont['file_name_tmp'], $path.'/'.$filename);            
        }

        //archivo otros datos
        if($fileotda['file_name'] != '' && $fileotda['file_name_tmp'] != ''){
            $filename = $fileotda['file_name'];        

            copy($pathorigen.$fileotda['file_name_tmp'], $path.'/'.$filename);                   
        }
        
        if (file_exists($pathorigen.$fileconvenio['file_name_tmp']) && $fileconvenio['file_name_tmp'] != '')           
            unlink($pathorigen.$fileconvenio['file_name_tmp']);
        
        if (file_exists($pathorigen.$filecont['file_name_tmp']) && $filecont['file_name_tmp'] != '')    
            unlink($pathorigen.$filecont['file_name_tmp']);
        
        if (file_exists($pathorigen.$fileotda['file_name_tmp']) && $fileotda['file_name_tmp'] != '')    
            unlink($pathorigen.$fileotda['file_name_tmp']);

        return array( 'respuesta' => $result);     
    }

    public function eliminarArchivo(Request $request){

        $ruta = $request->ruta;
        $archivo = $request->archivo;
        $codigo = $request->codigo;
        $tiparchivo = $request->tiparchivo;       

        try {
            
            $ConveniosUpd = Convenios::where('convcod', $codigo)->first();
            
            if($tiparchivo == 'archivogeneral')
                $ConveniosUpd->convarcgen    = '';
            
            if($tiparchivo == 'archivootda')
                $ConveniosUpd->convarcoda = '';

            if($tiparchivo == 'archivocont')
                $ConveniosUpd->convarccon = '';

            $result = $ConveniosUpd->save();    

            $ruracompleta = public_path($ruta.$archivo);

            if (file_exists($ruracompleta) && $archivo != '')
                unlink($ruracompleta);

            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se eiminó el Archivo con éxito'],200);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);

        }
        
        return $respuesta;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Convenios::where('convcod', $Id)->delete(); 
        return $result;

    }

    public function DelConvenioSocio(Request $request)
    {
        $fuentefinanciacion = $request->fuentefinanciacion;
        $convenio = $request->convenio;

        $result = ConveniosSocios::where('convcod', $convenio)->where('convsocfuefin', $fuentefinanciacion)->delete(); 
        return $result;

    }
}

?>