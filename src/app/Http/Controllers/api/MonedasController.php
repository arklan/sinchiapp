<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\MonedasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Monedas;
use DB;

class MonedasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Monedas = Monedas::select('moncod','mondes','monsim','montas')
                     ->orderby('mondes')                     
                     ->get();
                     
            if ($Monedas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Monedas, new MonedasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Monedas::max('moncod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Descripcion, $simbolo, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Monedas::select(DB::raw('count(moncod) as total'))                      
                      ->where('mondes', $Descripcion)
                      ->where('moncod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Descripcion ya existe';

                }
            }  else {

                $Duplica = Monedas::select(DB::raw('count(moncod) as total'))                      
                            ->where('monsim', $simbolo)
                            ->where('moncod','!=', $Codigo)
                            ->get();

                if (!$Duplica->isEmpty()) {
    
                    $total = $Duplica[0]->total;
    
                    if($total != '0'){
    
                        $mensaje = 'El Símbolo ya existe';
    
                    }
                }             

            }          

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DataAddUpdate(Request $request)
    {
        $Monedas = $request->data;

        $codigo = $Monedas['codigo'];
        $Descripcion = strtoupper(trim($Monedas['Descripcion']));    
        $simbolo = strtoupper(trim($Monedas['Simbolo']));       
        $tasa = $Monedas['tasa'];

        $mensaje = $this->buscarDuplicado($Descripcion, $simbolo, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo = $this->consecutivo();

            $MonedasAdd = new Monedas();
            
            $MonedasAdd->moncod = $consecutivo;           
            $MonedasAdd->mondes = $Descripcion; 
            $MonedasAdd->monsim = $simbolo; 
            $MonedasAdd->montas = $tasa;           
            
            $result = $MonedasAdd->save();
        
        } else {

            $MonedasUpd = Monedas::where('moncod', $codigo)->first();
          
            $MonedasUpd->mondes = $Descripcion;          
            $MonedasUpd->monsim = $simbolo;
            $MonedasUpd->montas = $tasa; 
            // Guardamos en base de datos
            $result = $MonedasUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Monedas::where('moncod', $Id)->delete(); 
        return $result;

    }
}

?>