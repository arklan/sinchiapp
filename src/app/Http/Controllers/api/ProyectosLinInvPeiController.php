<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosLinInvPeiTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosLinInvPei;
use DB;

class ProyectosLinInvPeiController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosLinInvPei = ProyectosLinInvPei::select('proycod','lininvpeicod')                        
                        ->orderby('lininvpeicod')                     
                        ->get();
                     
            if ($ProyectosLinInvPei->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosLinInvPei, new ProyectosLinInvPeiTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = '';

        for ($i=0; $i < count($data); $i++) {             
        
            $codigo = $data[$i]['codigo'];
            $proyecto = $data[$i]['proyecto'];
            
            if($i==0){

                $result = ProyectosLinInvPei::where('proycod', $proyecto)->delete();
            }

            $dataAdd = new ProyectosLinInvPei();
            
            $dataAdd->lininvpeicod  = $codigo;
            $dataAdd->proycod    = $proyecto;

            $result = $dataAdd->save();            
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
                     
            $ProyectosLinInvPei = ProyectosLinInvPei::select('lininvpeicod','proycod')                        
                        ->where('proycod', $proyecto)                        
                        ->orderby('lininvpeicod')                     
                        ->get();
                     
        return $this->response->collection($ProyectosLinInvPei, new ProyectosLinInvPeiTransformer);

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function seleccionadosProyectos(Request $request)
    {
        try{
            
            $proyecto = $request['proyecto'];
            $programas = $request['programas'];

                     
            $sql = "SELECT l.lininvpeicod codigo,l.lininvpeides descripcion, IF(p.proycod IS NULL,0,1) Activo 
                    FROM lineas_investigacion_pei l LEFT JOIN 
                    proyectos_lininv_pei p ON l.lininvpeicod = p.lininvpeicod AND p.proycod=".$proyecto." 
                    WHERE l.proinvpeicod IN ".$programas." ORDER BY l.lininvpeicod";            
            
            $result = DB::select($sql);              

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
}

?>