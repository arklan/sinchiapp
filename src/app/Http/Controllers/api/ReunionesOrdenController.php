<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ReunionesOrdenTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ReunionesOrden;
use DB;

class ReunionesOrdenController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ReunionesOrden = ReunionesOrden::select('reuordcod', 'reuordnum', 'reuordtem', 'perscod', 'reuordobs', 'reucod') 
                     ->orderby('reuordnum')                     
                     ->get();
                     
            if ($ReunionesOrden->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesOrden, new ReunionesOrdenTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function ReunionesOrdenxCodReunion(Request $request)
    {       
       $CodReunion = $request->id;
        
       try{

            $ReunionesOrden = ReunionesOrden::select('reuniones_orden.reuordcod', 'reuniones_orden.reuordnum', 'reuniones_orden.reuordtem', 'reuniones_orden.perscod', 'reuniones_orden.reuordobs', 'reuniones_orden.reucod','personal.persape','personal.persnom')                      
                     ->join('Personal','personal.perscod','=','reuniones_orden.perscod')
                     ->where('reuniones_orden.reucod',$CodReunion)
                     ->orderby('reuniones_orden.reuordnum')                     
                     ->get();
                     
            if ($ReunionesOrden->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesOrden, new ReunionesOrdenTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function consecutivo()
    {      

        $maxVal  = ReunionesOrden::max('reuordcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReunionesOrdenAdd(Request $request)
    {
        $ReunionesOrden = $request->data;


        $codigo = $ReunionesOrden['codigo'];     
        $numorden = $ReunionesOrden['numorden'];
        $tematica = $ReunionesOrden['tematica'];
        $observaciones = $ReunionesOrden['observaciones'];
        $codigoreunion = $ReunionesOrden['codigoreunion'];
        $codigopersonal = $ReunionesOrden['codigopersonal'];
       
        if($codigo=='0'){
            $consecutivo =$this->consecutivo();

            $ReunionesOrdenAdd = new ReunionesOrden();

            $ReunionesOrdenAdd->reuordcod = $consecutivo;           
            $ReunionesOrdenAdd->reuordnum = $numorden;
            $ReunionesOrdenAdd->reuordtem = $tematica;
            $ReunionesOrdenAdd->perscod   = $codigopersonal;
            $ReunionesOrdenAdd->reuordobs = $observaciones;
            $ReunionesOrdenAdd->reucod    = $codigoreunion;

            $result = $ReunionesOrdenAdd->save();
        } else {

            $ReunionesOrdenUpd = ReunionesOrden::where('reuordcod', $codigo)->first();
         
            $ReunionesOrdenUpd->reuordnum = $numorden;
            $ReunionesOrdenUpd->reuordtem = $tematica;
            $ReunionesOrdenUpd->perscod   = $codigopersonal;
            $ReunionesOrdenUpd->reuordobs = $observaciones;
            $ReunionesOrdenUpd->reucod    = $codigoreunion;

            // Guardamos en base de datos
            $result = $ReunionesOrdenUpd->save();
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;
        
        $result = ReunionesOrden::where('reuordcod', $Id)->delete(); 
        return $result;

    }
}

?>