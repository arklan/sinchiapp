<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\UnspscTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Unspsc;
use DB;

class UnspscController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Unspsc = Unspsc::select('unspsccod','unspscdes')
                     ->orderby('unspscdes')                     
                     ->get();
                     
            if ($Unspsc->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Unspsc, new UnspscTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
    
    public function dataXFiltro(Request $request)
    {       

       $datafiltro = $request->data[0];  

       try{

            //print_r($datafiltro);

            $tipofiltro = $datafiltro['tipo'];
            $valor = $datafiltro['valor'];

            if($tipofiltro == 'L'){ //clase
                
                $data = Unspsc::select('unspsc.unspsccod','unspsc.unspscdes')
                                    ->join('clases','unspsc.clascod','=','clases.clascod')
                                    ->Where('clases.clasnom', 'like', '%' . $valor . '%')
                                    ->orderby('unspsc.unspsccod')                     
                                    ->get();

            } elseif ($tipofiltro == 'F') { //familia               

                $data = Unspsc::select('unspsc.unspsccod','unspsc.unspscdes')
                                    ->join('clases','unspsc.clascod','=','clases.clascod')
                                    ->join('familia','clases.famcod','=','familia.famcod')
                                    ->Where('familia.famnom', 'like', '%' . $valor . '%')
                                    ->orderby('unspsc.unspsccod')                     
                                    ->get();


            } elseif ($tipofiltro == 'S') { //segmento

                $data = Unspsc::select('unspsc.unspsccod','unspsc.unspscdes')
                                    ->join('clases','unspsc.clascod','=','clases.clascod')
                                    ->join('familia','clases.famcod','=','familia.famcod')
                                    ->join('segmento','familia.segmcod','=','segmento.segmcod')
                                    ->Where('segmento.segmnom', 'like', '%' . $valor . '%')
                                    ->orderby('unspsc.unspsccod')                     
                                    ->get();

            } elseif ($tipofiltro == 'U') { //unspsc

                $data = Unspsc::select('unspsccod','unspscdes')                                    
                                    ->Where('unspscdes', 'like', '%' . $valor . '%')
                                    ->orderby('unspsccod')                     
                                    ->get();

            } elseif ($tipofiltro == 'C') { // código unspsc

                $data = Unspsc::select('unspsccod','unspscdes')                                    
                                    ->Where('unspsccod', 'like', '%' . $valor . '%')
                                    ->orderby('unspsccod')                     
                                    ->get();

            }

            return $this->response->collection($data, new UnspscTransformer);

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Unspsc::max('unspsccod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($nombre, $codigo){

        try{
           
            $mensaje = '';

            $Duplica = Unspsc::select(DB::raw('count(unspsccod) as total'))                      
                      ->where('unspscdes', $nombre)
                      ->where('unspsccod','!=', $codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo = $data['codigo'];
        $nombre = strtoupper(trim($data['nombre']));
       
        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new Unspsc();
            
            $dataAdd->unspsccod = $consecutivo;           
            $dataAdd->unspscdes = $nombre;           
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = Unspsc::where('unspsccod', $codigo)->first();
          
            $dataUpd->unspscdes = $nombre;
         
            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        try{

            $result = Unspsc::where('unspsccod', $Id)->delete(); 
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }
}

?>