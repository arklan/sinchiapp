<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ReunionesTareaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ReunionesTarea;
use App\ReunionesTareaPersonal;
use DB;

class ReunionesTareaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{
            $ReunionesTarea = ReunionesTarea::select('reutarcod','reutar','reutarrecapr','reutarfeclim','reutartip','reutarest','reucod') 
                                            ->orderby('reutarcod')                     
                                            ->get();
            
                     
            if ($ReunionesTarea->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesTarea, new ReunionesTareaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function ReunionesTareaXCodReunion(Request $request)
    {       
       try{
            $CodReunion = $request->id;

            $ReunionesTarea = ReunionesTarea::select('reutarcod','reutar','reutarrecapr','reutarfeclim','reutartip','reutarest','reucod') 
                                            ->where('reucod', $CodReunion)
                                            ->orderby('reutarcod')                     
                                            ->get();
            
                     
            if ($ReunionesTarea->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesTarea, new ReunionesTareaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function PersonasXCodTareas(Request $request)
    {       
       try{
            $CodReunionTareas = $request->id;
            
            $ReunionesTarea = ReunionesTarea::select('reuniones_tarea.reutarcod','reuniones_tarea.reutar','reuniones_tarea.reutarrecapr','reuniones_tarea.reutarfeclim','reuniones_tarea.reutartip','reuniones_tarea.reutarest','reuniones_tarea.reucod','personal.perscod','personal.persape','personal.persnom') 
                     ->join('reuniones_tarea_personal','reuniones_tarea_personal.reutarcod','=','reuniones_tarea.reutarcod')
                     ->join('Personal','personal.perscod','=','reuniones_tarea_personal.perscod')
                     ->where('reuniones_tarea.reutarcod', $CodReunionTareas)
                     ->orderby('reuniones_tarea.reutarcod')                     
                     ->get();
                     
            if ($ReunionesTarea->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesTarea, new ReunionesTareaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = ReunionesTarea::max('reutarcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    } 

    public function consecutivotareaspersonas()
    {      

        $maxVal  = ReunionesTareaPersonal::max('reutarperscod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    } 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reuniontareaAddUpdate(Request $request)
    {
        $reuniontarea = $request->data;


        $codigo = $reuniontarea['codigo'];
        $tarea = $reuniontarea['tareas'];       
        $recursos = $reuniontarea['recursos'];
        $fecha = $reuniontarea['fechalimite'];
        $tipo = $reuniontarea['tipo'];
        $estado = $reuniontarea['estado'];
        $codigoreunion = $reuniontarea['codigoreunion'];
        $responsables = $reuniontarea['responsables'];        

        if($codigo=='0'){
            
            $consecutivo =$this->consecutivo();

            $ReuniontareaAdd = new ReunionesTarea();
            
            $ReuniontareaAdd->reutarcod     = $consecutivo;           
            $ReuniontareaAdd->reutar        = $tarea;
            $ReuniontareaAdd->reutarrecapr  = $recursos;
            $ReuniontareaAdd->reutarfeclim  = $fecha;
            $ReuniontareaAdd->reutartip     = $tipo;
            $ReuniontareaAdd->reutarest     = $estado;
            $ReuniontareaAdd->reucod        = $codigoreunion;

            $result = $ReuniontareaAdd->save();
            
            if($result){
                
                foreach ($responsables as $key => $row) {
                    $ReuniontareaPersonalAdd = new ReunionesTareaPersonal();         

                    $consecutivotareaspersonas =$this->consecutivotareaspersonas();

                    $ReuniontareaPersonalAdd->reutarperscod     = $consecutivotareaspersonas;
                    $ReuniontareaPersonalAdd->reutarcod            = $consecutivo;
                    $ReuniontareaPersonalAdd->perscod           = $row;

                    $result = $ReuniontareaPersonalAdd->save();
                }
            }

        } else {

            $ReuniontareaUpd = ReunionesTarea::where('reutarcod', $codigo)->first();
          
              
            $ReuniontareaUpd->reutar        = $tarea;
            $ReuniontareaUpd->reutarrecapr  = $recursos;
            $ReuniontareaUpd->reutarfeclim  = $fecha;
            $ReuniontareaUpd->reutartip     = $tipo;
            $ReuniontareaUpd->reutarest     = $estado;

            $result = $ReuniontareaUpd->save();
            
            if($result){
                
                $this->destroyTareasPersonal($codigo);
                foreach ($responsables as $key => $row) {
                    $ReuniontareaPersonalAdd = new ReunionesTareaPersonal();               
                    
                    $consecutivotareaspersonas =$this->consecutivotareaspersonas();

                    $ReuniontareaPersonalAdd->reutarperscod     = $consecutivotareaspersonas;
                    $ReuniontareaPersonalAdd->reutarcod         = $codigo;
                    $ReuniontareaPersonalAdd->perscod           = $row;

                    $result = $ReuniontareaPersonalAdd->save();
                }
            }
           
        } 

        return array( 'respuesta' => $result);     
    }

    public function ReunionesTareasImportar(Request $request)
    {
        $reuniontareaimportar = $request->data['importar'];
        $codigoreunion = $request->data['codreunion'];


        foreach ($reuniontareaimportar as $key => $tareasimportar) {            
            

            if($tareasimportar['seleccionado'] == 1){
                $tarea = $tareasimportar['tarea'];       
                $recursos = $tareasimportar['recursos'];
                $fecha = $tareasimportar['fecha'];
                $tipo = $tareasimportar['tipo'];
                $estado = $tareasimportar['codestado'];
                $codigoreunion = $codigoreunion;
                $responsables = $tareasimportar['responsables'];                
                    
                $consecutivo =$this->consecutivo();

                $ReuniontareaAdd = new ReunionesTarea();
                
                $ReuniontareaAdd->reutarcod     = $consecutivo;           
                $ReuniontareaAdd->reutar        = $tarea;
                $ReuniontareaAdd->reutarrecapr  = $recursos;
                $ReuniontareaAdd->reutarfeclim  = $fecha;
                $ReuniontareaAdd->reutartip     = $tipo;
                $ReuniontareaAdd->reutarest     = $estado;
                $ReuniontareaAdd->reucod        = $codigoreunion;

                $result = $ReuniontareaAdd->save();
                
                if($result){
                    
                    foreach ($responsables as $key => $row) {
                        $ReuniontareaPersonalAdd = new ReunionesTareaPersonal();         

                        $consecutivotareaspersonas =$this->consecutivotareaspersonas();

                        $ReuniontareaPersonalAdd->reutarperscod     = $consecutivotareaspersonas;
                        $ReuniontareaPersonalAdd->reutarcod            = $consecutivo;
                        $ReuniontareaPersonalAdd->perscod           = $row['codigo'];

                        $result = $ReuniontareaPersonalAdd->save();
                    }
                }
            }
            

        }

        return array( 'respuesta' => $result);
            
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = ReunionesTarea::where('reutarcod', $Id)->delete(); 
        return $result;

    }

    private function destroyTareasPersonal($CodigoTarea)
    {
        $Id = $CodigoTarea;

        $result = ReunionesTareaPersonal::where('reutarcod', $Id)->delete(); 
        return $result;

    }
}

?>