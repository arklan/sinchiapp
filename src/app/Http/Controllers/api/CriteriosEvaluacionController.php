<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\CriteriosEvaluacionTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CriteriosEvaluacion;
use DB;

class CriteriosEvaluacionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $CriteriosEvaluacion = CriteriosEvaluacion::select('crievacod','crievades')
                     
                     ->orderby('crievades')                     
                     ->get();
                     
            if ($CriteriosEvaluacion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($CriteriosEvaluacion, new CriteriosEvaluacionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = CriteriosEvaluacion::max('crievacod');

        if (is_numeric($maxVal)) {

            $Codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $Codigo = 1;
        }
            
        return $Codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = CriteriosEvaluacion::select(DB::raw('count(crievacod) as total'))                      
                      ->where('crievades', $Descripcion)
                      ->where('crievacod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Descripcion ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CriteriosEvaluacionAddUpdate(Request $request)
    {
        $CriteriosEvaluacion = $request->data;

        $Codigo = $CriteriosEvaluacion['codigo'];
        $Descripcion = strtoupper(trim($CriteriosEvaluacion['descripcion']));       

        $mensaje = $this->buscarDuplicado($Descripcion, $Codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($Codigo=='0'){

            $consecutivo =$this->consecutivo();

            $CriteriosEvaluacionAdd = new CriteriosEvaluacion();
            
            $CriteriosEvaluacionAdd->crievacod = $consecutivo;           
            $CriteriosEvaluacionAdd->crievades = $Descripcion;           
            
            $result = $CriteriosEvaluacionAdd->save();
        
        } else {

            $CriteriosEvaluacionUpd = CriteriosEvaluacion::where('crievacod', $Codigo)->first();
          
            $CriteriosEvaluacionUpd->crievades = $Descripcion;          

            // Guardamos en base de datos
            $result = $CriteriosEvaluacionUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = CriteriosEvaluacion::where('crievacod', $Id)->delete(); 
        return $result;

    }
}

?>