<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ResguardosIndigenasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ResguardosIndigenas;
use App\Asociaciones;
use App\Comunidades;
use App\AsociacionesResguardos;
use App\Transformers\AsociacionesTransformer;
use App\Transformers\ComunidadesTransformer;
use App\Transformers\AsociacionesResguardosTransformer;

use DB;

class AsociacionesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            
            $Data = Asociaciones::select('asocod','asonom')
                     ->orderby('asonom')                     
                     ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new AsociacionesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function asociacionesComunidades(Request $request)
    {       
       try{

            $codigo = $request['codigo'];
            $Data = Comunidades::select('comunidades.comcod','comunidades.comnom','comunidades.asocod','asociaciones.asonom')
                                ->join('asociaciones','comunidades.asocod','=','asociaciones.asocod')   
                                ->where('comunidades.asocod', $codigo)  
                                ->orderby('comunidades.comnom')                     
                                ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new ComunidadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
    
    public function asociacionesResguardos(Request $request)
    {       
       try{

            $codigo = $request['codigo'];
            $Data = AsociacionesResguardos::select('asociaciones_resguardos.asocod','asociaciones_resguardos.resindcod',
                                                'asociaciones.asonom','resguardos_indigenas.resindnom')
                                ->join('asociaciones','asociaciones_resguardos.asocod','=','asociaciones.asocod')
                                ->join('resguardos_indigenas','asociaciones_resguardos.resindcod','=','resguardos_indigenas.resindcod')   
                                ->where('asociaciones_resguardos.asocod', $codigo)  
                                ->orderby('resguardos_indigenas.resindnom')                     
                                ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new AsociacionesResguardosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
    
    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';            

            if($mensaje == ''){

                $Duplica = Asociaciones::select(DB::raw('count(asocod) as total'))                      
                        ->where('asonom', $Nombre)
                        ->where('asocod','!=', $Codigo)
                        ->get();

                $total = 0;          
                if (!$Duplica->isEmpty()) {
                    
                    $total = $Duplica[0]->total;

                    if($total != '0'){

                        $mensaje = 'El Nombre de la Asociación ya existe';

                    }
                }            
            }

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    public function consecutivo()
    {      

        $maxVal  = Asociaciones::max('asocod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    public function consecutivoComunidad()
    {      

        $maxVal  = Comunidades::max('comcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    public function DataAddUpdate(Request $request)
    {
        $data        = $request->data;
        $resguardos  = $request->resguardos;
        $comunidades = $request->comunidades;

        $codigo       = $data['codigo'];
        $nombre       = strtoupper(trim($data['nombre']));       
       
        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        $codigoactual = 0;
        if($codigo == '0'){            

            $consecutivo = $this->consecutivo();
            $codigoactual = $consecutivo;

            $dataAdd = new Asociaciones();
            
            $dataAdd->asocod = $consecutivo;           
            $dataAdd->asonom = $nombre;
            
            $result = $dataAdd->save();
        
        } else {

            $codigoactual = $codigo;
            $dataUpd = Asociaciones::where('asocod', $codigo)->first();
          
            $dataUpd->asonom = $nombre;                   

            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        }

        //comunidades
        if(count($comunidades) > 0){

            $arrDataActual = [];            
            $consecutivocomunidad = $this->consecutivoComunidad();

            if($codigo != '0'){

                Comunidades::where('asocod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($comunidades); $i++) {

                $arrDataActual[] = 
                    [                        
                        'asocod' => $codigoactual,
                        'comcod' => $consecutivocomunidad,
                        'comnom' => $comunidades[$i]['nombre'],                         
                    ];
                
                $consecutivocomunidad += 1;   
            }

            $result = Comunidades::insert($arrDataActual);
        }

        //resguardos
        if(count($resguardos) > 0){

            $arrDataActual = [];            

            if($codigo != '0'){

                AsociacionesResguardos::where('asocod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($resguardos); $i++) {

                $arrDataActual[] = 
                    [                        
                        'asocod'    => $codigoactual,
                        'resindcod' => $resguardos[$i]['codigoresguardo']                        
                    ];                                                
            }

            $result = AsociacionesResguardos::insert($arrDataActual);
        }

        return array( 'respuesta' => $result);     
    }

    public function destroy(Request $request)
    {
        $Id = $request->id;        

        try {
            
            $result = AsociacionesResguardos::where('asocod', $Id)->delete();
            $result = Comunidades::where('asocod', $Id)->delete();
            $result = Asociaciones::where('asocod', $Id)->delete(); 
            return $result;          

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }

    public function deleteAsociacionResguardo(Request $request)
    {
        $codigo    = $request->codigo;
        $resguardo = $request->resguardo;

        try {
            
            $result = AsociacionesResguardos::where('asocod', $codigo)->where('resindcod', $resguardo)->delete();            
            return $result;          

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }

    public function deleteAsociacionComunidad(Request $request)
    {
        $codigo    = $request->codigo;
        $comunidad = $request->comunidad;

        try {
            
            $result = Comunidades::where('asocod', $codigo)->where('comcod', $comunidad)->delete();            
            return $result;          

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }
}

?>