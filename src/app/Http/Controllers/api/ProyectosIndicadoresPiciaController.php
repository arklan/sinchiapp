<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosIndicadoresPiciaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosIndicadoresPicia;
use DB;

class ProyectosIndicadoresPiciaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosIndicadoresPicia = ProyectosIndicadoresPicia::select('proycod','indpiccod')                        
                        ->orderby('indpiccod')                     
                        ->get();
                     
            if ($ProyectosIndicadoresPicia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosIndicadoresPicia, new ProyectosIndicadoresPiciaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = '';

        for ($i=0; $i < count($data); $i++) {             
        
            $codigo = $data[$i]['codigo'];
            $proyecto = $data[$i]['proyecto'];
            
            if($i==0){

                $result = ProyectosIndicadoresPicia::where('proycod', $proyecto)->delete();
            }

            $dataAdd = new ProyectosIndicadoresPicia();
            
            $dataAdd->indpiccod  = $codigo;
            $dataAdd->proycod    = $proyecto;

            $result = $dataAdd->save();            
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
                     
            $ProyectosIndicadoresPicia = ProyectosIndicadoresPicia::select('indpiccod','proycod')                        
                        ->where('proycod', $proyecto)                        
                        ->orderby('indpiccod')                     
                        ->get();
            
            return $this->response->collection($ProyectosIndicadoresPicia, new ProyectosIndicadoresPiciaTransformer);            
            /*if ($ProyectosIndicadoresPicia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosIndicadoresPicia, new ProyectosIndicadoresPiciaTransformer);                
            }*/

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function seleccionadosProyectos(Request $request)
    {
        try{
            
            $proyecto = $request['proyecto'];
            $acciones = $request['acciones'];
                     
            $sql = "SELECT l.indpiccod codigo,l.indpicdes descripcion, IF(p.proycod IS NULL,0,1) Activo 
                    FROM indicadores_picia l LEFT JOIN 
                    proyectos_indicadores_picia p ON l.indpiccod = p.indpiccod AND p.proycod=".$proyecto." 
                    WHERE l.accpiccod IN ".$acciones." ORDER BY l.indpiccod";
            
            $result = DB::select($sql);
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
}

?>