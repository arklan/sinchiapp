<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\Novedades_AdministrativasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Novedades_Administrativas;
use DB;

class Novedades_AdministrativasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Novedades_Administrativas = Novedades_Administrativas::select('novadmcod','novadmfec','tipo_novedad.tipnovcod')
                     ->join('tipo_novedad','tipo_novedad.tipnovcod','=','tipo_novedad.tipnovcod')   
                     ->orderby('novadmcod')                     
                     ->get();
                     
            if ($Novedades_Administrativas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Novedades_Administrativas, new Novedades_AdministrativasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Novedades_Administrativas::max('novadmcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Novedades_Administrativas::select(DB::raw('count(novadmcod) as total'))                      
                      ->where('novadmcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Descripcion ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Novedades_AdministrativasAddUpdate(Request $request)
    {
        $Novedades_Administrativas = $request->data;

        $codigo = $Novedades_Administrativas['codigo'];
        

        $mensaje = $this->buscarDuplicado($codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $Novedades_AdministrativasAdd = new Novedades_Administrativas();
            
            $Novedades_AdministrativasAdd->novadmcod = $consecutivo;           
                        
            $result = $Novedades_AdministrativasAdd->save();
        
        } else {

            $Novedades_AdministrativasUpd = Novedades_Administrativas::where('novadmcod', $codigo)->first();
          
            
            // Guardamos en base de datos
            $result = $Novedades_AdministrativasUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Novedades_Administrativas::where('novadmcod', $Id)->delete(); 
        return $result;

    }
}

?>