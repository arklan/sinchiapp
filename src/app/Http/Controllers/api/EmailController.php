<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Login;

use DB;

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../vendor/autoload.php';                          // Set email format to HTML


class EmailController extends BaseController
{

    public function datosmail(){

        $mail = new PHPMailer();

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'ludwigdsi@gmail.com';          // SMTP username
        $mail->Password = 'BALLESballes92';                        // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
    
        $mail->setFrom('noreply@sinchi.com.co','Correo Automático SINCHI');
    
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';
        
        return $mail;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function recovery(Request $request)
    {

        $email = $request->email;

        $usuario = Login::select('usuario','password','nombre')
                          ->where('email',"$email")
                          ->get();

        if ($usuario->isEmpty()) {

            return array( 'success' => false, 'message' => 'No existe ningún usuario registrado con ese email.');

        }
        else {

            $mail = $this->datosmail();

            $mail->addAddress($email, $usuario['0']->nombre);

            $mail->Subject = "SINCHI: Usuario y contraseña de acceso";

            $message = "Cordial Saludo, <br>
                        ".$usuario['0']->nombre."
                        <br><br>
                        A continuación relacionamos los datos de ingreso al sistema:
                        <br><br> 
                        Usuario: ".$usuario['0']->usuario."<br>
                        Contraseña: ".$usuario['0']->password."
                        <br><br>
                        Agradeciendo su atención,<br><br>
                        
                        <b>SINCHI</b><br><br> 
        
                        <i>Este es un servicio de notificaciones generado automáticamente por medio de correo electrónico, no lo responda.</i> <br>";
        
            $mail->Body = $message;
        
            if (!$mail->send()) {
        
                return array( 'success' => false, 'message' => 'No se puedo enviar el mensaje. Error');
        
            } 
            else {
        
                return array( 'success' => true, 'message' => 'El mensaje fue enviado correctamente.');
        
            } 

        }

    }

    public function send(Request $request)
    {
       
        $destinatario = $request->email;
        $nombre = $request->nombre;
        $asunto = $request->asunto;
        $mensaje = $request->mensaje;
        $adjunto = $request->adjunto;

        $mail = $this->datosmail();

        $mail->addAddress($destinatario, $nombre);

        $mail->Subject = $asunto;
    
        $mail->Body = $mensaje;

        $mail->addAttachment($adjunto); 
    
        if (!$mail->send()) {
    
            return array( 'success' => false, 'message' => 'NO se puedo enviar el mensaje. Error', 'error' => $mail->ErrorInfo);
    
        } 
        else {
    
            return array( 'success' => true, 'message' => 'El mensaje fue enviado correctamente.');
    
        } 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

?>