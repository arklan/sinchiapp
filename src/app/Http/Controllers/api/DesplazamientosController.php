<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\DesplazamientosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Desplazamientos;
use DB;

use App\DesplazamientosDestinos;
use App\Transformers\DesplazamientosDestinosTransformer;

use App\DesplazamientosRubros;
use App\Transformers\DesplazamientosRubrosTransformer;

class DesplazamientosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Desplazamientos = Desplazamientos::select('desplazamientos.descod','desplazamientos.desfec','desplazamientos.destipben','desplazamientos.desbentipide',
                                    'desplazamientos.desbennom','desplazamientos.desbenide','desplazamientos.desbencar','desplazamientos.desbendir','desplazamientos.desbentel',
                                    'desplazamientos.desbenema','desplazamientos.desbenfecnac','desplazamientos.desbenvalcon','desplazamientos.desbenmescon',
                                    'desplazamientos.desreqtiq','desplazamientos.desobjgen','desplazamientos.desvia','desplazamientos.desviapagant',
                                    'desplazamientos.desviaree','desplazamientos.desvaltiq','desplazamientos.desgasvia','desplazamientos.desvalgasvia',
                                    'desplazamientos.desobs','desplazamientos.desvalvia','tipos_identificacion.tipidelet','desplazamientos.desbenvalbas',
                                    'desplazamientos.desenvvb','desplazamientos.userid','desplazamientos.desestpla','desplazamientos.desobspla',
                                    'desplazamientos.destipliq','desplazamientos.desarccerarl',
                                    'desplazamientos.desvalliq','desplazamientos.desestfin','desplazamientos.desobsfin',
                                    'desplazamientos.desestdir','desplazamientos.desobsdir','desplazamientos.desnumregpre',
                                    'desplazamientos.desarcregpre','desplazamientos.desestpre','desplazamientos.desobspre',
                                    'desplazamientos.despagtes',
                                    DB::raw("IFNULL(cargos.cargnom,'') AS cargnom"))
                     ->join('tipos_identificacion','desplazamientos.desbentipide','=','tipos_identificacion.tipidecod')
                     ->leftJoin('cargos','desplazamientos.desbencar','=','cargos.cargcod')                    
                     ->orderby('desplazamientos.descod')
                     ->get();
                     
            if ($Desplazamientos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Desplazamientos, new DesplazamientosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function desplazamientoDestinos()
    {       
       try{

            $Desplazamientos = DesplazamientosDestinos::select('desplazamientos_destinos.descod','desplazamientos_destinos.desdescod',
                                    'desplazamientos_destinos.desdesnacori','desplazamientos_destinos.desdesintori',
                                    'desplazamientos_destinos.desdesnacdes','desplazamientos_destinos.desdesintdes',
                                    'desplazamientos_destinos.desdesnomdor','desplazamientos_destinos.desdesnomdfi',
                                    'desplazamientos_destinos.desdesnacfid','desplazamientos_destinos.desdesnacfre',
                                    'desplazamientos_destinos.desdesnactip','desplazamientos_destinos.desdestar',
                                    'desplazamientos_destinos.moncod','desplazamientos_destinos.desdestas',
                                    'desplazamientos_destinos.desdestarpes','desplazamientos_destinos.desdestottar',
                                    'monedas.monsim','desplazamientos_destinos.desdesdvj')      
                     ->join('monedas','desplazamientos_destinos.moncod','=','monedas.moncod')                              
                     ->orderby('desplazamientos_destinos.desdescod')
                     ->get();
                     
            if ($Desplazamientos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Desplazamientos, new DesplazamientosDestinosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function desplazamientoDestinosOrdenFecha(Request $request)
    {       
       try{

            $codigo = $request->codigo;
            $Desplazamientos = DesplazamientosDestinos::select('desplazamientos_destinos.descod','desplazamientos_destinos.desdescod',
                                    'desplazamientos_destinos.desdesnacori','desplazamientos_destinos.desdesintori',
                                    'desplazamientos_destinos.desdesnacdes','desplazamientos_destinos.desdesintdes',
                                    'desplazamientos_destinos.desdesnomdor','desplazamientos_destinos.desdesnomdfi',
                                    'desplazamientos_destinos.desdesnacfid','desplazamientos_destinos.desdesnacfre',
                                    'desplazamientos_destinos.desdesnactip','desplazamientos_destinos.desdestar',
                                    'desplazamientos_destinos.moncod','desplazamientos_destinos.desdestas',
                                    'desplazamientos_destinos.desdestarpes','desplazamientos_destinos.desdestottar',
                                    'monedas.monsim','desplazamientos_destinos.desdesdvj')      
                     ->join('monedas','desplazamientos_destinos.moncod','=','monedas.moncod') 
                     ->where('desplazamientos_destinos.descod',$codigo)                             
                     ->orderby('desplazamientos_destinos.desdesnacfid')
                     ->get();
                     
            if ($Desplazamientos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Desplazamientos, new DesplazamientosDestinosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function desplazamientoDestinoXCodigo(Request $request)
    {       
       try{

            $codigo = $request->codigo;

            $Desplazamientos = DesplazamientosDestinos::select('desplazamientos_destinos.descod','desplazamientos_destinos.desdescod',
                                'desplazamientos_destinos.desdesnacori','desplazamientos_destinos.desdesintori',
                                'desplazamientos_destinos.desdesnacdes','desplazamientos_destinos.desdesintdes',
                                'desplazamientos_destinos.desdesnomdor','desplazamientos_destinos.desdesnomdfi',
                                'desplazamientos_destinos.desdesnacfid','desplazamientos_destinos.desdesnacfre',
                                'desplazamientos_destinos.desdesnactip','desplazamientos_destinos.desdestar',
                                'desplazamientos_destinos.moncod','desplazamientos_destinos.desdestas',
                                'desplazamientos_destinos.desdestarpes','desplazamientos_destinos.desdestottar',
                                'monedas.monsim','desplazamientos_destinos.desdesdvj')      
                     ->join('monedas','desplazamientos_destinos.moncod','=','monedas.moncod')                     
                     ->where('desplazamientos_destinos.descod',$codigo)
                     ->orderby('desplazamientos_destinos.desdescod')
                     ->get();
                     
            if ($Desplazamientos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Desplazamientos, new DesplazamientosDestinosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function desplazamientoRubrosXCodigo(Request $request)
    {       
       try{

            $codigo = $request->codigo;

            $sql = "SELECT c.convnum numero,p.proynomcor nombre,pn.plaadqnecdes descripcion,
                            pna.plaadqcod planadquisicion,pna.plaadqneccod codigo,
                            pna.proyactsec secuenciaactividad,pna.prescod presupuesto,pna.plaadqnecactrpr rubro,
                            pna.plaadqnecactval valor, rb.desrubcom comprometido, rb.desrubsal saldo,
                            rb.desrubvaldis valordistribuido,rb.desrubcod codigorubro 
                    FROM convenios c
                    INNER JOIN proyectos p ON c.convcod = p.convcod 
                    INNER JOIN planes_adquisicion pl ON p.proycod = pl.proycod
                    INNER JOIN planesadquisiciones_necesidades pn ON pl.plaadqcod = pn.plaadqcod
                    INNER JOIN planesadquisiciones_necesidades_actividades pna 
                            ON pn.plaadqcod = pna.plaadqcod AND pn.plaadqneccod = pna.plaadqneccod     
                    INNER JOIN desplazamientos_rubros rb ON pna.plaadqcod = rb.plaadqcod AND 
                                pna.plaadqneccod = rb.plaadqneccod AND pna.proyactsec = rb.proyactsec AND 
                                pna.prescod = rb.prescod
                    WHERE rb.descod = ".$codigo.";";
                     
            $result = $result =DB::select($sql);
                
            return $result;            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Desplazamientos::max('descod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }    

    public function dataAddUpdate(Request $request)
    {
        date_default_timezone_set('America/Bogota');

        $data = $request->data;
        $rubros = $request->rubros;
        $destinos = $request->destinos;

        $codigo = $data['codigo'];
        $fecha = date('Y\-m\-d');
        $tipobeneficiario = $data['tipobeneficiario'];
        $tipoidentcodigo = $data['tipoidentcodigo'];
        $identificacion = $data['identificacion'];
        $beneficiario = $data['beneficiario'];
        $cargocodigo = $data['cargocodigo'];
        $direccion = $data['direccion'];
        $telefono = $data['telefono'];
        $email = $data['email'];
        $fechanacimiento = $data['fechanacimiento'];
        $valorcontrato = $data['valorcontrato'];
        $mesescontrato = $data['mesescontrato'];
        $requieretiquetes = $data['requieretiquetes'];
        $objetivogeneral = $data['objetivogeneral'];
        $viaticos = $data['viaticos'];
        $pagoanticipado = $data['pagoanticipado'];
        $reembolso = $data['reembolso'];
        $valortiquetes = $data['valortiquetes'];
        $gastosviaje = $data['gastosviaje'];
        $valorgastosviaje = $data['valorgastosviaje'];
        $observacion = $data['observacion'];
        $valorviaticos = $data['valorviaticos'];
        $valorbaseben = $data['valorbaseben'];
        $enviovobo = 1;
        $usuario = 1; //$data['usuario'];
        $tipoliquidacion = '';
        $valorliquidado = 0;
        $archivoarl = '';

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new Desplazamientos();
            
            $dataAdd->descod = $consecutivo;           
            $dataAdd->desfec = $fecha;
            $dataAdd->destipben = $tipobeneficiario; 
            $dataAdd->desbentipide = $tipoidentcodigo;
            $dataAdd->desbenide = $identificacion; 
            $dataAdd->desbennom = $beneficiario;
            $dataAdd->desbencar = $cargocodigo;
            $dataAdd->desbendir = $direccion;
            $dataAdd->desbentel = $telefono;
            $dataAdd->desbenema = $email;
            $dataAdd->desbenfecnac = $fechanacimiento;
            $dataAdd->desbenvalcon = $valorcontrato;
            $dataAdd->desbenmescon = $mesescontrato; 
            $dataAdd->desreqtiq = $requieretiquetes;
            $dataAdd->desobjgen = $objetivogeneral; 
            $dataAdd->desvia = $viaticos;
            $dataAdd->desvalvia = $valorviaticos;
            $dataAdd->desviapagant = $pagoanticipado; 
            $dataAdd->desviaree = $reembolso;
            $dataAdd->desvaltiq = $valortiquetes;
            $dataAdd->desgasvia = $gastosviaje;
            $dataAdd->desvalgasvia = $valorgastosviaje;
            $dataAdd->desobs = $observacion;
            $dataAdd->desbenvalbas = $valorbaseben;
            $dataAdd->desenvvb = $enviovobo;
            $dataAdd->userid = $usuario;
            $dataAdd->destipliq = $tipoliquidacion;
            $dataAdd->desvalliq = $valorliquidado;
            $dataAdd->desarccerarl = $archivoarl; 
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = Desplazamientos::where('descod', $codigo)->first();
                      
            $dataUpd->desfec = $fecha;
            $dataUpd->destipben = $tipobeneficiario; 
            $dataUpd->desbentipide = $tipoidentcodigo;
            $dataUpd->desbenide = $identificacion; 
            $dataUpd->desbennom = $beneficiario;
            $dataUpd->desbencar = $cargocodigo;
            $dataUpd->desbendir = $direccion;
            $dataUpd->desbentel = $telefono;
            $dataUpd->desbenema = $email;
            $dataUpd->desbenfecnac = $fechanacimiento;
            $dataUpd->desbenvalcon = $valorcontrato;
            $dataUpd->desbenmescon = $mesescontrato; 
            $dataUpd->desreqtiq = $requieretiquetes;
            $dataUpd->desobjgen = $objetivogeneral; 
            $dataUpd->desvia = $viaticos;
            $dataUpd->desvalvia = $valorviaticos;
            $dataUpd->desviapagant = $pagoanticipado; 
            $dataUpd->desviaree = $reembolso;
            $dataUpd->desvaltiq = $valortiquetes;
            $dataUpd->desgasvia = $gastosviaje;
            $dataUpd->desvalgasvia = $valorgastosviaje;
            $dataUpd->desobs = $observacion;
            $dataUpd->desbenvalbas = $valorbaseben;
            $dataUpd->desenvvb = $enviovobo;
            $dataUpd->userid = $usuario;

            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        } 
        
        $codigoactual = 0;

        if($codigo=='0'){

            $codigoactual = $consecutivo;                    

        } else {

            $codigoactual = $codigo;

        }

        //rubros        
        if(count($rubros) > 0){

            $arrDataActual = [];

            if($codigo != '0'){

                $result = DesplazamientosRubros::where('descod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($rubros); $i++) {
                
                $arrDataActual[] = 
                    [                        
                        'descod'       => $codigoactual,
                        'desrubcod'    => $i + 1,
                        'plaadqcod'    => $rubros[$i]['plan'],
                        'plaadqneccod' => $rubros[$i]['necesidad'],
                        'proyactsec'   => $rubros[$i]['actividad'],
                        'prescod'      => $rubros[$i]['presupuesto'],
                        'desrubcom'    => $rubros[$i]['comprometido'],
                        'desrubsal'    => $rubros[$i]['saldo']
                    ];                                                
            }

            $result = DesplazamientosRubros::insert($arrDataActual);
        }

        //destinos
        if(count($destinos) > 0){

            $arrDataActual = [];

            if($codigo != '0'){

                $result = DesplazamientosDestinos::where('descod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($destinos); $i++) {
                
                $arrDataActual[] = 
                    [                        
                        'descod'       => $codigoactual,
                        'desdescod'    => $destinos[$i]['codigo'],
                        'desdesnacori' => $destinos[$i]['origennacional'],
                        'desdesintori' => $destinos[$i]['origeninternacional'],
                        'desdesnacdes' => $destinos[$i]['destinonacional'],
                        'desdesintdes' => $destinos[$i]['destinointernacional'],
                        'desdesnacfid' => $destinos[$i]['fechaida'],
                        'desdesnacfre' => $destinos[$i]['fecharegreso'],
                        'desdesnactip' => $destinos[$i]['tipotransporte'],
                        'desdesnomdor' => $destinos[$i]['txtdestinoorigen'],
                        'desdesnomdfi' => $destinos[$i]['txtdestinofinal'],
                        'desdestar'    => $destinos[$i]['tarifa'], 
                        'moncod'       => $destinos[$i]['moneda'],
                        'desdestas'    => $destinos[$i]['tasa'],
                        'desdestottar' => $destinos[$i]['totaltarifa'],
                        'desdestarpes' => $destinos[$i]['tarifapesos'],
                        'desdesdvj'    => $destinos[$i]['diasviaje'],                        
                    ];                                                
            }

            $result = DesplazamientosDestinos::insert($arrDataActual);
        }         

        return array( 'respuesta' => $result);     
    }   
    
    public function eliminarArchivo(Request $request){

        $ruta = $request->ruta;
        $archivo = $request->archivo;
        $codigo = $request->codigo;
        $tiparchivo = $request->tiparchivo;       

        try {
            
            $dataUpd = Desplazamientos::where('descod', $codigo)->first();
            
            if($tiparchivo == 'archivoarl')
                $dataUpd->desarccerarl = '';            
            
            $result = $dataUpd->save();    

            $ruracompleta = public_path($ruta.$archivo);

            if (file_exists($ruracompleta) && $archivo != '')
                unlink($ruracompleta);

            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se eiminó el Archivo con éxito'],200);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);

        }
        
        return $respuesta;
    }

    public function liquidarViaticos(Request $request){

        $data = $request->data;
        $rubros = $request->rubros;

        $codigo = $data['codigo'];
        $tipoliquidacion = $data['tipoliquidacion'];
        $valorliquidado = $data['valorliquidado'];
        $filearl = $data['archivoarl'];
        
        try {
            
            $dataUpd = Desplazamientos::where('descod', $codigo)->first();

            $dataUpd->destipliq = $tipoliquidacion;
            $dataUpd->desvalliq = $valorliquidado; 
            $dataUpd->desarccerarl = $filearl['file_name'];
                        
            $result = $dataUpd->save(); 

            //actualiza rubros
            if(count($rubros) > 0){

                $arrDataActual = [];            

                for ($i=0; $i < count($rubros); $i++) {                

                    $sql = "UPDATE desplazamientos_rubros SET desrubvaldis = '".$rubros[$i]['valordistribuido']."' 
                            WHERE descod = ".$codigo." AND 	desrubcod = ".$rubros[$i]['codigorubro'];
                    
                    $result =DB::update($sql);                                                
                }                
            }           

            /**************************** archivos    ************************ 
             ******************************************************************/        

            $path = public_path('/desplazamientos/D' . $codigo);
            $pathorigen = public_path('tmp/'); 

            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            //archivo ARL
            if($filearl['file_name'] != '' && $filearl['file_name_tmp'] != ''){
                $filename = $filearl['file_name'];        

                if (file_exists($pathorigen.$filearl['file_name_tmp']))
                    copy($pathorigen.$filearl['file_name_tmp'], $path.'/'.$filename);            
            }         

            if (file_exists($pathorigen.$filearl['file_name_tmp']) && $filearl['file_name_tmp'] != '')           
                unlink($pathorigen.$filearl['file_name_tmp']);

            return $result;

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);
            return $respuesta;

        }       
        
    }

    public function updDatosPresupuesto(Request $request){

        $codigo = $request->codigo;

        $vobopresupuesto = $request->vobopresupuesto;
        $obspresupuesto = $request->obspresupuesto;
        $numerorp = $request->numerorp;
        $archivorp = $request->archivorp;
       
        $filerp = $archivorp;
        
        try {
            
            $dataUpd = Desplazamientos::where('descod', $codigo)->first();

            $dataUpd->desestpre = $vobopresupuesto;
            $dataUpd->desobspre = $obspresupuesto;
            $dataUpd->desnumregpre = $numerorp;             
            $dataUpd->desarcregpre = $filerp['file_name'];
                        
            $result = $dataUpd->save();                      

            /**************************** archivos    ************************ 
             ******************************************************************/        

            $path = public_path('/desplazamientos/D' . $codigo);
            $pathorigen = public_path('tmp/'); 

            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }

            //archivo RP
            if($filerp['file_name'] != '' && $filerp['file_name_tmp'] != ''){
                $filename = $filerp['file_name'];        

                if (file_exists($pathorigen.$filerp['file_name_tmp']))
                    copy($pathorigen.$filerp['file_name_tmp'], $path.'/'.$filename);            
            }         

            if (file_exists($pathorigen.$filerp['file_name_tmp']) && $filerp['file_name_tmp'] != '')           
                unlink($pathorigen.$filerp['file_name_tmp']);

            return array( 'respuesta' => $result);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);
            return $respuesta;

        }
        
        
    }

    public function pagoPresupuesto(Request $request){

        $codigo = $request->codigo;
        
        try {
            
            $dataUpd = Desplazamientos::where('descod', $codigo)->first();

            $dataUpd->despagtes = 'S';
                                   
            $result = $dataUpd->save();

            return array( 'respuesta' => $result);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);
            return $respuesta;

        }
        
        
    }

    public function cambiarestadosVB(Request $request){

        $codigo = $request->codigo;
        $tipo = $request->tipo;
        $estado = $request->estado;
        $observacion = $request->observacion;
        
        $dataUpd = Desplazamientos::where('descod', $codigo)->first();

        $result = 0;

        if($tipo=='PL'){
            
            $dataUpd->desestpla = $estado;
            $dataUpd->desobspla = $observacion;
            
            if($estado=='21' || $estado==21){
                $dataUpd->desenvvb = 0;
            }

            $result = $dataUpd->save();

        } else if($tipo=='FI'){
            
            $dataUpd->desestfin = $estado;
            $dataUpd->desobsfin = $observacion;
            
            /*if($estado=='21' || $estado==21){
                $dataUpd->desenvvb = 0;
            }*/

            $result = $dataUpd->save();

        } else if($tipo=='DIR'){
            
            $dataUpd->desestdir = $estado;
            $dataUpd->desobsdir = $observacion;
            
            /*if($estado=='21' || $estado==21){
                $dataUpd->desenvvb = 0;
            }*/

            $result = $dataUpd->save();

        }

        return array( 'respuesta' => $result);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Desplazamientos::where('descod', $Id)->delete(); 
        return $result;

    }
}

?>