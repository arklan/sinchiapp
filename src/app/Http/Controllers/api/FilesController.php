<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FilesController extends Controller
{
    public function download(Request $request)
    {
        $filename = $request->filename;
        return response()->download(public_path('solcontrata_files/'.$filename),'documentos');
    }

    public function upload(Request $request)
    {
        $random = str_random(10);
        $extension = $request->file('archivo')->getClientOriginalExtension();
        $filename = $random.'.'.$extension;
        $path = $request->file('archivo')->move(public_path('solcontrata_files/'),$filename);
        $fileURL = url('/solcontrata_files/'.$filename);
        return response()->json(['url' => $fileURL],200);
    }

    public function downloadGen(Request $request)
    {
        $directorio = $request->directorio;
        $filename = $request->filename;
        
        return response()->download(public_path($directorio.'/'.$filename),'documentos');
    }

    public function uploadGen(Request $request)
    {      

        $random = str_random(10);
        $extension = $request->file('archivo')->getClientOriginalExtension();
        $filename = $random.'.'.$extension;
        $path = $request->file('archivo')->move(public_path('tmp/'),$filename);
        $fileURL = url('/tmp/'.$filename);

        return response()->json(['url' => $fileURL,'filenametmp' => $filename],200);
    }

    public function deleteFile(Request $request)
    {
        $ruta = $request->ruta;
        $archivo = $request->archivo;
        
        $respuesta = '';

        try {
            
            $ruracompleta = public_path($ruta.$archivo);

            unlink($ruracompleta);

            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se eiminó el Archivo con éxito'],200);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);

        }
        
        return $respuesta;
        
    }

}
