<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ReunionesActaAprobacionesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ReunionesActaAprobaciones;
use App\ReunionesPersonalAsistentes;
use App\Http\Controllers\api\ReunionesActaController;
use App\Http\Controllers\api\EmailController;

use DB;

class ReunionesActaAprobacionesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $ReunionesActa;
    
    public function __construct()
    {
        $this->ReunionesActa = new ReunionesActaController();
        $this->EnviarEmail = new EmailController();
    }


    public function index()
    {       
       try{

            $ReunionesActaAprobaciones = ReunionesActaAprobaciones::select('reuactaprcod', 'reuactaprfec', 'reuactaprest', 'reuactaprcom', 'reuactcod','reupercodasi') 
                     ->orderby('reuactaprcod')                     
                     ->get();
                     
            if ($ReunionesActaAprobaciones->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesActaAprobaciones, new ReunionesActaAprobacionesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    /* public function ReunionesActaAprobacionesxCodActa(Request $request)
    {       
       $CodActa = $request->id;
        
       try{

            $ReunionesActaAprobaciones = ReunionesActaAprobaciones::select('reuniones_acta_aprobaciones.reuactaprcod', 'reuniones_acta_aprobaciones.reuactaprfec', 'reuniones_acta_aprobaciones.reuactaprest', 'reuniones_acta_aprobaciones.reuactaprcom', 'reuniones_acta_aprobaciones.reuactcod','reuniones_acta_aprobaciones.reupercodasi','reuniones_personal_asistentes.perscod')
                     ->join('reuniones_personal_asistentes','reuniones_acta_aprobaciones.reupercodasi','=','reuniones_personal_asistentes.reupercodasi')                     
                     ->where('reuniones_acta_aprobaciones.reuactcod',$CodActa)
                     ->where('reuniones_personal_asistentes.reupersolapro',true)
                     ->orderby('reuniones_acta_aprobaciones.reuactaprcod','DESC')                     
                     ->get();
                     
            if ($ReunionesActaAprobaciones->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesActaAprobaciones, new ReunionesActaAprobacionesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } */
    
    public function ReunionesActaAprobacionesxCodActa(Request $request)
    {       
        

        $CodActa = $request->CodActa;
        $CodReunion = $request->CodReunion;
        
        $sql="SELECT personal.perscod as codigopersonal, CONCAT(personal.persnom, ' ', personal.persape) as aprobador , ra.reuactaprfec as fecha, ra.reuactaprest as estado, CASE WHEN ra.reuactaprest = 1 THEN 'APROBADA' WHEN ra.reuactaprest = 2 THEN 'RECHAZADA' END as estadodescripcion,ra.reuactaprcom as observacion FROM reuniones_personal_asistentes LEFT JOIN (SELECT * FROM reuniones_acta_aprobaciones WHERE reuactcod = ".$CodActa.") ra ON reuniones_personal_asistentes.reupercodasi = ra.reupercodasi JOIN personal ON personal.perscod = reuniones_personal_asistentes.perscod WHERE reuniones_personal_asistentes.reucod= ".$CodReunion." and reuniones_personal_asistentes.reupersolapro=true";

       try{
            $ReunionesActaAprobaciones = DB::select($sql);
            
                                 
            return $ReunionesActaAprobaciones;
            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    private function ReunionesDatosRespActaxCodReunion($CodReunion)
    {       
        
        
        $sql="SELECT r.reutit, u.email, u.nombre FROM reuniones r JOIN users u ON r.perscodact = u.perscod WHERE r.reucod = ".$CodReunion;

       try{
            $ReunionesDatos = DB::select($sql);
            
                                 
            return $ReunionesDatos;
            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = ReunionesActaAprobaciones::max('reuactaprcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReunionesActaAprobacionesAdd(Request $request)
    {
        $ReunionesActaAprobaciones = $request->data;

        $codigo = $ReunionesActaAprobaciones['codigo'];  
        $comentario = $ReunionesActaAprobaciones['comentario'];
        $estado = $ReunionesActaAprobaciones['estado'];
        $codigoacta = $ReunionesActaAprobaciones['codigoacta'];
        $codigoreunion = $ReunionesActaAprobaciones['codigoreunion'];
        $codigoasist = $ReunionesActaAprobaciones['codigoasist'];
       
        if($codigo=='0'){
            $consecutivo =$this->consecutivo();

            $ReunionesActaAprobacionesAdd = new ReunionesActaAprobaciones();

            $ReunionesActaAprobacionesAdd->reuactaprcod     = $consecutivo;           
            $ReunionesActaAprobacionesAdd->reuactaprfec     = date('Y-m-d H:i:s');
            $ReunionesActaAprobacionesAdd->reuactaprest     = $estado;
            $ReunionesActaAprobacionesAdd->reuactaprcom     = $comentario;
            $ReunionesActaAprobacionesAdd->reuactcod        = $codigoacta;
            $ReunionesActaAprobacionesAdd->reupercodasi     = $codigoasist;

            $result = $ReunionesActaAprobacionesAdd->save();
        } else {

            $ReunionesActaAprobacionesUpd = ReunionesActaAprobaciones::where('reuactaprcod', $codigo)->first();
         
            
            $ReunionesActaAprobacionesAdd->reuactest = $estado;

            // Guardamos en base de datos
            $result = $ReunionesActaAprobacionesUpd->save();
        }

        $requestAprob = new Request();
        $requestAprob->setMethod('POST');
        $requestAprob->request->add(['CodActa' => $codigoacta, 'CodReunion' => $codigoreunion]);
        
        $validarAprbTotal = $this->ReunionesActaAprobacionesxCodActa($requestAprob);

        $aprovada = false;
        $rechazada = false;
        $pendiente = false;

        foreach ($validarAprbTotal as $key => $row) {            

            switch ($row->estado) {
                case 1:
                    $aprovada = true;
                    break;
                case 2:
                    $rechazada = true;
                    break;
                default:
                    $pendiente = true;
            }
        }

  
        $requestUpdActa = new Request();
        $requestUpdActa->setMethod('POST');


        if($aprovada && !$rechazada && !$pendiente){
            $requestUpdActa->request->add(['data' => ['codigo' => $codigoacta, 'estado' => 3, 'pdf' => '', 'codigoreunion' => '']]);
            
            $this->ReunionesActa->ReunionesActaAdd($requestUpdActa);

            $requestEmail = new Request();
            $requestEmail->setMethod('POST');

            $datosReunion = $this->ReunionesDatosRespActaxCodReunion($codigoreunion);

            $Asunto = "Cordial Saludo,<br><br> " .
                      "Se aprovó el acta de la reunión con el titulo : " . $datosReunion[0]->reutit . " por favor revisar el registro." .	
                      "<br><br>Agradeciendo su atención,<br><br> " .                        
                      "<b>SINCHI</b><br><br> " .
                      "<i>Este es un servicio de notificaciones generado automáticamente por medio de " .
                      "correo electrónico, no lo responda.</i> <br>";

            $requestEmail->request->add(['email' => $datosReunion[0]->email, 
                                         'nombre' => $datosReunion[0]->nombre, 
                                         'asunto' => 'Aprobación del acta de reunión',
                                         'mensaje' => $Asunto,
                                         'adjunto' => ''
                                         ]);



            $this->EnviarEmail->send($requestEmail);


        }elseif($rechazada){
            $requestUpdActa->request->add(['data' => ['codigo' => $codigoacta, 'estado' => 2, 'pdf' => '', 'codigoreunion' => '']]);
            
            $this->ReunionesActa->ReunionesActaAdd($requestUpdActa);

            $requestEmail = new Request();
            $requestEmail->setMethod('POST');

            $datosReunion = $this->ReunionesDatosRespActaxCodReunion($codigoreunion);


            $Asunto = "Cordial Saludo,<br><br> " .
                      "Se rechazó el acta de la reunión con el titulo : " . $datosReunion[0]->reutit . " por favor revisar el registro." .	
                      "<br><br>Agradeciendo su atención,<br><br> " .                        
                      "<b>SINCHI</b><br><br> " .
                      "<i>Este es un servicio de notificaciones generado automáticamente por medio de " .
                      "correo electrónico, no lo responda.</i> <br>";

            $requestEmail->request->add(['email' => $datosReunion[0]->email, 
                                         'nombre' => $datosReunion[0]->nombre, 
                                         'asunto' => 'Rechazó del acta de reunión',
                                         'mensaje' => $Asunto,
                                         'adjunto' => ''
                                         ]);



            $this->EnviarEmail->send($requestEmail);

        }

        
        
        

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;
        
        $result = ReunionesActaAprobaciones::where('reuactaprcod', $Id)->delete(); 
        return $result;

    }
}

?>