<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Proyectos;
use App\Poas;
use App\ProyectosActividades;
use App\ProyectosEquipo;
use App\ProyectosObjetivos;
use App\ProyectosProductos;
use App\ProyectosEquipoComp;

use App\ProyectosLinInvPicia;
use App\ProyectosAccionesPicia;
use App\ProyectosIndicadoresPicia;
use App\ProyectosProgInvPei;
use App\ProyectosLinInvPei;

use App\ProyectosPilaresPicia;
use App\ProyectosProdDerAut;
use App\ProyectosProdPropInd;
use App\ProyectosOrgVivos;

//use App\ProyectosFecPreInfTec;
//use App\ProyectosFecPreInfFin;

use DB;

class ProyectosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Proyectos = Proyectos::select('proyectos.proycod','proyectos.proyant','proyectos.estcod','proyectos.convcod','convenios.convnum',
                                 'proyectos.poacod','proyectos.proyobj','proyectos.proyjusimp','proyectos.proyjusper','proyectos.proyjusipa',
                                 'proyectos.proyobjgen','proyectos.proyact','proyectos.proyben','estados.estdes',                                 
                                 'personal.persnom','personal.persape','poas.poades','poas.poaestr','poas.poaind','poas.poaunimed',
                                 'poas.poametsec','poas.poametins','poas.poaobs','poas.capcod','poas.linestcod','poas.objcod','poas.odscod',
                                 'poas.metcod','poas.proinvpencod','poas.lininvpencod','capitulos.capdes','lineas_estrategicas.linestdesc',
                                 'objetivos.objdesc','ods.odsdesc','metas.metnom','penia.proinvpendes as proinvpendes',
                                 'lineapenia.lininvpendes as lininvpendes','proyectos.proydesc','proyectos.proynomcor','convenios.moncod',
                                 'monedas.monsim','convenios.convtas','proyectos.proytip','proyectos.conmarcod',
                                 'convenios.convfecsus','convenios.convfectereje','convenios.convfecinc','convenios.perscod',
                                 'convenios.convenc','proyectos.proyvbpla','proyectos.proyvbdir')
                        ->join('convenios','proyectos.convcod','=','convenios.convcod') 
                        ->join('personal','convenios.perscod','=','personal.perscod')  
                        ->join('monedas','convenios.moncod','=','monedas.moncod')      
                        ->join('estados','proyectos.estcod','=','estados.estcod')                        
                        ->join('poas','proyectos.poacod','=','poas.poacod')                       
                        ->join('capitulos','poas.capcod','=','capitulos.capcod')
                        ->join('lineas_estrategicas','poas.linestcod','=','lineas_estrategicas.linestcod')
                        ->join('objetivos','poas.objcod','=','objetivos.objcod')
                        ->join('ods','poas.odscod','=','ods.odscod')
                        ->join('metas','poas.metcod','=','metas.metcod')
                        ->join('programas_investigacion_penia as penia','poas.proinvpencod','=','penia.proinvpencod')
                        ->join('lineas_investigacion_penia as lineapenia','poas.lininvpencod','=','lineapenia.lininvpencod')                        
                        ->orderby('proyectos.proycod')   
                        //->limit(15)                  
                        ->get();
                     
            if ($Proyectos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Proyectos, new ProyectosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    
    
    public function listGrilla(Request $request)
    {       
       try{            

            $convenio = $request->convenio;
            $nombreconvenio = $request->nombreconvenio;

            $tipo = $request->tipo;
                        
            $filtro = '';
            $voboactualiza = 'S';

            if($tipo == 'upd')
                $filtro = " WHERE proyvbpla = 1";           

            else if($tipo == 'apb'){
                $filtro = " WHERE proyvbpla = 1 AND proyapract = ''";
                $voboactualiza = '';
            }   

            if($convenio != ''){

                if($filtro == '')
                    $filtro = ' WHERE convenios.convnum = '.$convenio;
                else 
                    $filtro .= ' AND convenios.convnum = '.$convenio;
            }

            if($nombreconvenio != ''){

                if($filtro == '')
                    $filtro = " WHERE proyectos.proynomcor like '%".$nombreconvenio."%'";
                else 
                    $filtro .= " AND proyectos.proynomcor like '%".$nombreconvenio."%'";
            }

            $sql = "select proyectos.proycod codigo,convenios.convnum convenio,convenios.convfecsus fechasuscripcion,
                            proyectos.proynomcor nombre,estados.estdes descripcionestado,
                            convenios.convvaltot valortotal,convenios.perscod codigocoordinador,
                            CONCAT(personal.persnom,' ',personal.persape) nombrecoordinador, 
                            convenios.convfectereje fechaterejecucion,convenios.moncod codigomoneda,
                            monedas.monsim moneda,convenios.convtas tasa,
                            proyectos.proyvbpla vbplaneacion, proyectos.proyvbdir vbdireccion,(proyver + 1) version
                        from proyectos  
                        inner join (SELECT convcod,max(proyver) version
                                    FROM proyectos
                                    WHERE proyapract = '".$voboactualiza."' 
                                    group by convcod) version ON proyectos.convcod = version.convcod and 
                                                        proyectos.proyver = version.version 
                        inner join convenios on proyectos.convcod = convenios.convcod          
                        inner join estados on proyectos.estcod = estados.estcod
                        inner join personal on convenios.perscod = personal.perscod                           
                        inner join monedas on convenios.moncod = monedas.moncod ".$filtro."                       
                        order by proyectos.proycod DESC limit 100;";

            $data = DB::select($sql);             

            return $data;
            
            /*$Proyectos = Proyectos::select('proyectos.proycod','convenios.convnum','convenios.convfecsus','proyectos.proynomcor',
                                 'estados.estdes','convenios.convvaltot','convenios.perscod', 'personal.persnom','personal.persape', 
                                 'convenios.convfectereje','convenios.moncod','monedas.monsim',
                                 'convenios.convtas','proyectos.proyvbpla')
                        ->join('convenios','proyectos.convcod','=','convenios.convcod')          
                        ->join('estados','proyectos.estcod','=','estados.estcod')
                        ->join('personal','convenios.perscod','=','personal.perscod')                           
                        ->join('monedas','convenios.moncod','=','monedas.moncod')                        
                        ->orderby('proyectos.proycod')   
                        ->limit(20)                  
                        ->get();
                     
            if ($Proyectos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Proyectos, new ProyectosTransformer);                
            }*/

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }  

    public function FiltroXId(Request $request)
    {       
       try{

            $proycod = $request->proyecto;
            
            $Proyectos = Proyectos::select('proyectos.proycod','proyectos.proyant','proyectos.estcod','proyectos.convcod','convenios.convnum',
                                    'proyectos.poacod','proyectos.proyobj','proyectos.proyjusimp','proyectos.proyjusper','proyectos.proyjusipa',
                                    'proyectos.proyobjgen','proyectos.proyact','proyectos.proyben','estados.estdes',                                 
                                    'personal.persnom','personal.persape','poas.poades','poas.poaestr','poas.poaind','poas.poaunimed',
                                    'poas.poametsec','poas.poametins','poas.poaobs','poas.capcod','poas.linestcod','poas.objcod','poas.odscod',
                                    'poas.metcod','poas.proinvpencod','poas.lininvpencod','capitulos.capdes','lineas_estrategicas.linestdesc',
                                    'objetivos.objdesc','ods.odsdesc','metas.metnom','penia.proinvpendes as proinvpendes',
                                    'lineapenia.lininvpendes as lininvpendes','proyectos.proydesc','proyectos.proynomcor','convenios.moncod',
                                    'monedas.monsim','convenios.convtas','proyectos.proytip','proyectos.conmarcod',
                                    'convenios.convfecsus','convenios.convfectereje','convenios.convfecinc','convenios.perscod',
                                    'convenios.convenc','proyectos.proyvbpla','proyectos.proyvbdir','convenios.convnom','convenios.convvaltot')
                        ->join('convenios','proyectos.convcod','=','convenios.convcod') 
                        ->join('personal','convenios.perscod','=','personal.perscod')  
                        ->join('monedas','convenios.moncod','=','monedas.moncod')      
                        ->join('estados','proyectos.estcod','=','estados.estcod')                        
                        ->join('poas','proyectos.poacod','=','poas.poacod')                       
                        ->join('capitulos','poas.capcod','=','capitulos.capcod')
                        ->join('lineas_estrategicas','poas.linestcod','=','lineas_estrategicas.linestcod')
                        ->join('objetivos','poas.objcod','=','objetivos.objcod')
                        ->join('ods','poas.odscod','=','ods.odscod')
                        ->join('metas','poas.metcod','=','metas.metcod')
                        ->join('programas_investigacion_penia as penia','poas.proinvpencod','=','penia.proinvpencod')
                        ->join('lineas_investigacion_penia as lineapenia','poas.lininvpencod','=','lineapenia.lininvpencod')
                        ->where('proyectos.proycod',$proycod)                  
                        ->orderby('proyectos.proycod')
                        //->limit(15)
                        ->get();
                     
            if ($Proyectos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Proyectos, new ProyectosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function FiltroXProyecto($proycod)
    {       
       try{

            $Proyectos = Proyectos::select('proyectos.proycod','proyectos.proyant','proyectos.estcod','proyectos.convcod','convenios.convnum',
                                    'proyectos.poacod','proyectos.proyobj','proyectos.proyjusimp','proyectos.proyjusper','proyectos.proyjusipa',
                                    'proyectos.proyobjgen','proyectos.proyact','proyectos.proyben','estados.estdes',                                 
                                    'personal.persnom','personal.persape','poas.poades','poas.poaestr','poas.poaind','poas.poaunimed',
                                    'poas.poametsec','poas.poametins','poas.poaobs','poas.capcod','poas.linestcod','poas.objcod','poas.odscod',
                                    'poas.metcod','poas.proinvpencod','poas.lininvpencod','capitulos.capdes','lineas_estrategicas.linestdesc',
                                    'objetivos.objdesc','ods.odsdesc','metas.metnom','penia.proinvpendes as proinvpendes',
                                    'lineapenia.lininvpendes as lininvpendes','proyectos.proydesc','proyectos.proynomcor','convenios.moncod',
                                    'monedas.monsim','convenios.convtas','proyectos.proytip','proyectos.conmarcod','convenios.convfecinc',
                                    'convenios.convfecsus','convenios.convfectereje','convenios.convfecinc','convenios.convtieeje','convenios.convfuefin',
                                    'convenios.convvalcof','convenios.convvaltot','convenios.convvaldin','convenios.convtotesp','convenios.convvalctp',
                                    DB::raw('CONCAT(caf.persnom," ",caf.persape) as perscaf'),'caf.persdir as dircaf','caf.perstel as telcaf',
                                    DB::raw('CONCAT(aff.persnom," ",aff.persape) as persaff'),'aff.persdir as diraff','aff.perstel as telaff',
                                    DB::raw('CONCAT(ttf.persnom," ",ttf.persape) as persttf'),'ttf.persdir as dirttf','ttf.perstel as telttf',
                                    DB::raw('CONCAT(personal.persnom," ",personal.persape) as nombrecoordinador'),
                                    'convenios.perscod','convenios.convenc','proyectos.proyvbpla','proyectos.proyvbdir','convenios.convnom')
                        ->join('convenios','proyectos.convcod','=','convenios.convcod') 
                        ->join('personal','convenios.perscod','=','personal.perscod')  
                        ->join('monedas','convenios.moncod','=','monedas.moncod')      
                        ->join('estados','proyectos.estcod','=','estados.estcod')                        
                        ->join('poas','proyectos.poacod','=','poas.poacod')                       
                        ->join('capitulos','poas.capcod','=','capitulos.capcod')
                        ->join('lineas_estrategicas','poas.linestcod','=','lineas_estrategicas.linestcod')
                        ->join('objetivos','poas.objcod','=','objetivos.objcod')
                        ->join('ods','poas.odscod','=','ods.odscod')
                        ->join('metas','poas.metcod','=','metas.metcod')
                        ->join('programas_investigacion_penia as penia','poas.proinvpencod','=','penia.proinvpencod')
                        ->join('lineas_investigacion_penia as lineapenia','poas.lininvpencod','=','lineapenia.lininvpencod')

                        ->leftjoin('personal as caf','convenios.convcaf','=','caf.perscod')
                        ->leftjoin('personal as aff','convenios.convaff','=','aff.perscod')
                        ->leftjoin('personal as ttf','convenios.convtff','=','ttf.perscod')

                        ->where('proyectos.proycod',$proycod)                  
                        ->orderby('proyectos.proycod')        
                        //->limit(15)             
                        ->get();
                     
            if ($Proyectos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $Proyectos;                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function FiltroXVoBo(Request $request)
    {       
       try{

            $convenio = isset($request->convenio) ? $request->convenio : '';
            $nombreconvenio = isset($request->nombreconvenio) ? $request->nombreconvenio : '';
                        
            $filtro = '';            

            if($convenio != ''){
                
                $filtro .= ' AND convenios.convnum = '.$convenio;
            }

            if($nombreconvenio != ''){
                
                $filtro .= " AND proyectos.proynomcor like '%".$nombreconvenio."%'";
            }    

            /*$query = "select proyectos.proycod,proyectos.convcod,proyectos.proynomcor,convenios.convnum,
                        convenios.convnom,convenios.convvaltot,monedas.monsim,convenios.perscod,
                        convenios.convfecsus,convenios.convfectereje,convenios.convfecinc,
                        convenios.convtas
                    from proyectos inner join convenios on proyectos.convcod = convenios.convcod
                    inner join personal on convenios.perscod = personal.perscod 
                    inner join monedas on convenios.moncod = monedas.moncod
                    where proyectos.proyvbpla = 1 ".$filtro." 
                    order by proyectos.proycod DESC limit 100;";

            $Proyectos = Proyectos::select($query)->get();*/

            if($convenio == '' && $nombreconvenio == ''){
                $Proyectos = Proyectos::select('proyectos.proycod','proyectos.convcod','proyectos.proynomcor',
                                            'convenios.convnum','convenios.convnom','convenios.convvaltot',
                                            'monedas.monsim','convenios.perscod','convenios.convfecsus',
                                            'convenios.convfectereje','convenios.convfecinc','convenios.convtas')
                            ->join('convenios','proyectos.convcod','=','convenios.convcod')
                            ->join('personal','convenios.perscod','=','personal.perscod') 
                            ->join('monedas','convenios.moncod','=','monedas.moncod')
                            ->where('proyectos.proyvbpla',1)                  
                            ->orderby('proyectos.proycod')
                            ->get();

            } else if($convenio != ''){

                $Proyectos = Proyectos::select('proyectos.proycod','proyectos.convcod','proyectos.proynomcor',
                                            'convenios.convnum','convenios.convnom','convenios.convvaltot',
                                            'monedas.monsim','convenios.perscod','convenios.convfecsus',
                                            'convenios.convfectereje','convenios.convfecinc','convenios.convtas')
                            ->join('convenios','proyectos.convcod','=','convenios.convcod')
                            ->join('personal','convenios.perscod','=','personal.perscod') 
                            ->join('monedas','convenios.moncod','=','monedas.moncod')
                            ->where('proyectos.proyvbpla',1)
                            ->where('convenios.convnum',$convenio)                  
                            ->orderby('proyectos.proycod')
                            ->get();

            }  else if($nombreconvenio != ''){

                $Proyectos = Proyectos::select('proyectos.proycod','proyectos.convcod','proyectos.proynomcor',
                                            'convenios.convnum','convenios.convnom','convenios.convvaltot',
                                            'monedas.monsim','convenios.perscod','convenios.convfecsus',
                                            'convenios.convfectereje','convenios.convfecinc','convenios.convtas')
                            ->join('convenios','proyectos.convcod','=','convenios.convcod')
                            ->join('personal','convenios.perscod','=','personal.perscod') 
                            ->join('monedas','convenios.moncod','=','monedas.moncod')
                            ->where('proyectos.proyvbpla',1)
                            ->where('convenios.convnom','like', '%'.$nombreconvenio.'%')                  
                            ->orderby('proyectos.proycod')
                            ->get();

            }

            return $this->response->collection($Proyectos, new ProyectosTransformer);

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Proyectos::max('proycod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    public function consecutivoPoa()
    {      

        $maxVal  = Poas::max('poacod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    public function consecutivoActividades()
    {      

        $maxVal  = ProyectosActividades::max('proyactcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Proyectos::select(DB::raw('count(proycod) as total'))                      
                      ->where('proynomcor', $Nombre)
                      ->where('proycod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre corto del Proyecto ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        $poa = $request->poa;

        $LinInvPicia = $request->LineasInvPicia;
        $AccionesPicia = $request->AccionesPicia;      
        $IndicadoresPicia = $request->IndicadoresPicia;

        $ProgramasPei = $request->ProgramasPei;
        $LinInvPei = $request->LinInvPei;

        $pilarespicia = $request->pilarespicia;        
        
        //$FechasPIT = $request->FechasPIT;
        //$FechasPIF = $request->FechasPIF;

        $codigoproy = $data['codigo'];
        $codigopoa  = $poa['codigo'];
        $nombrecorto  = $data['nombre'];

        $mensaje = $this->buscarDuplicado($nombrecorto, $codigoproy);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigoproy=='0'){

            $consecutivo =$this->consecutivo();
            $consecutivoPoa =$this->consecutivoPoa();
            
            $poaAdd = new Poas();
            
            $poaAdd->poacod       = $consecutivoPoa;           
            $poaAdd->poades       = $poa['descripcion'];
            $poaAdd->poaestr      = $poa['estrategia'];
            $poaAdd->poaind       = $poa['indicador']; 
            $poaAdd->poaunimed    = $poa['unidadmedida'];
            $poaAdd->poametsec    = $poa['metasectorial'];
            $poaAdd->poametins    = $poa['metainstitucional'];
            $poaAdd->poaobs       = $poa['observacion'];            
            $poaAdd->capcod       = $poa['capitulo'];
            $poaAdd->linestcod    = $poa['linea'];
            $poaAdd->objcod       = $poa['objetivo'];            
            $poaAdd->odscod       = $poa['ods'];
            $poaAdd->metcod       = $poa['meta'];
            $poaAdd->proinvpencod = $poa['proyectopenia'];
            $poaAdd->lininvpencod = $poa['lineainvpenia'];           


            $result = $poaAdd->save();

            if($result){
                $dataAdd = new Proyectos();
                
                $dataAdd->proycod       = $consecutivo;
                $dataAdd->convcod       = $data['codigoconvenio'];
                $dataAdd->proyant       = $data['antecedentes'];                
                $dataAdd->estcod        = $data['codigoestado'];
                $dataAdd->poacod        = $consecutivoPoa;
                $dataAdd->proyobj       = $data['objeto'];
                $dataAdd->proyjusimp    = $data['justificaimportancia'];
                $dataAdd->proyjusper    = $data['justificapertenencia'];
                $dataAdd->proyjusipa    = $data['justificaimpacto'];
                $dataAdd->proyobjgen    = $data['objetivogeneral'];
                $dataAdd->proyact       = $data['actores'];
                $dataAdd->proyben       = $data['beneficiarios'];
                $dataAdd->proydesc      = $data['descripcion'];
                $dataAdd->proynomcor    = $data['nombre'];
                $dataAdd->proytip       = $data['tipo'];
                $dataAdd->conmarcod     = $data['conveniomarco'];
                
                $dataAdd->proyver       = 0;
                $dataAdd->proyapract    = 'S';
                
                $result = $dataAdd->save();

            } else {

                return array( 'respuesta' => $result);

            }
        
        } else {

            $poaUpd = Poas::where('poacod', $codigopoa)->first();

            $poaUpd->poades       = $poa['descripcion'];
            $poaUpd->poaestr      = $poa['estrategia'];
            $poaUpd->poaind       = $poa['indicador']; 
            $poaUpd->poaunimed    = $poa['unidadmedida'];
            $poaUpd->poametsec    = $poa['metasectorial'];
            $poaUpd->poametins    = $poa['metainstitucional'];
            $poaUpd->poaobs       = $poa['observacion'];            
            $poaUpd->capcod       = $poa['capitulo'];
            $poaUpd->linestcod    = $poa['linea'];
            $poaUpd->objcod       = $poa['objetivo'];            
            $poaUpd->odscod       = $poa['ods'];
            $poaUpd->metcod       = $poa['meta'];
            $poaUpd->proinvpencod = $poa['proyectopenia'];
            $poaUpd->lininvpencod = $poa['lineainvpenia'];

            $result = $poaUpd->save();

            if($result){

                $dataUpd = Proyectos::where('proycod', $codigoproy)->first();

                //$dataUpd->proynumcon    = $data['convenio'];
                //$dataUpd->proyfecsus    = $data['fechasuscripcion'];
                //$dataUpd->proyfectereje = $data['fechaterejecucion']; 
                //$dataUpd->proyfecinc    = $data['fechaincorporacion'];
                $dataUpd->convcod       = $data['codigoconvenio'];
                $dataUpd->proyant       = $data['antecedentes'];
                //$dataUpd->proyvaltot    = $data['valortotal'];
                $dataUpd->estcod        = $data['codigoestado'];            
                //$dataUpd->perscod       = $data['codigocoordinador'];
                $dataUpd->poacod        = $codigopoa;
                $dataUpd->proyobj       = $data['objeto'];            
                //$dataUpd->proyfuefin    = $data['fuentefinanciacion'];
                //$dataUpd->proyvalcof    = $data['cofinanciado'];
                //$dataUpd->proyvalctp    = $data['contrapartida'];
                //$dataUpd->proyvaldin    = $data['totaldinero'];
                //$dataUpd->proytotesp    = $data['totalespecie'];
                $dataUpd->proyjusimp    = $data['justificaimportancia'];
                $dataUpd->proyjusper    = $data['justificapertenencia'];
                $dataUpd->proyjusipa    = $data['justificaimpacto'];
                $dataUpd->proyobjgen    = $data['objetivogeneral'];                
                //$dataUpd->proytieeje    = $data['tiempo'];
                $dataUpd->proyact       = $data['actores'];
                $dataUpd->proyben       = $data['beneficiarios'];               
                //$dataUpd->proycaf       = $data['contactoafn'];
                //$dataUpd->proyaff       = $data['contactoaff'];
                //$dataUpd->proytff       = $data['contactotff'];
                //$dataUpd->moncod        = $data['moneda'];
                //$dataUpd->proytas       = $data['tasa'];
                $dataUpd->proydesc      = $data['descripcion'];
                $dataUpd->proynomcor    = $data['nombre'];
                $dataUpd->proytip       = $data['tipo'];
                $dataUpd->conmarcod     = $data['conveniomarco'];

                //$dataUpd->proyenc       = $data['codigoencargado'];
                //$dataUpd->proyobjcon    = $data['objetoconvenio'];
                //$dataUpd->proyestcon    = $data['estadoconvenio'];
                //$dataAdd->proyarccongen    = $data['archivocongeneral'];
                //$dataAdd->proyarcodacongen = $data['archivootdacongeneral'];
                //$dataAdd->proyarcconcongen = $data['archivocontcongeneral'];

                $result = $dataUpd->save();

                if(!$result){

                    return array( 'respuesta' => $result);
    
                }

            }
           
        }        
        
        $codigoactualproy = 0;

        if($codigoproy=='0'){

            $codigoactualproy = $consecutivo;                    

        } else {

            $codigoactualproy = $codigoproy;

        }

        //lineas de investigacion PICIA
        if(count($LinInvPicia) > 0){

            $arrDataActual = [];            

            if($codigoproy!='0'){

                $result = ProyectosLinInvPicia::where('proycod', $codigoproy)->delete(); 

            }

            for ($i=0; $i < count($LinInvPicia); $i++) {

                $arrDataActual[] = 
                    [                        
                        'proycod'       => $codigoactualproy,
                        'lininvpiccod'  => $LinInvPicia[$i]['codigo']                        
                    ];                                                
            }

            ProyectosLinInvPicia::insert($arrDataActual);
        }

        //Acciones PICIA
        if(count($AccionesPicia) > 0){

            $arrDataActual = [];            

            if($codigoproy!='0'){

                $result = ProyectosAccionesPicia::where('proycod', $codigoproy)->delete(); 

            }

            for ($i=0; $i < count($AccionesPicia); $i++) {

                $arrDataActual[] = 
                    [                        
                        'proycod'    => $codigoactualproy,
                        'accpiccod'  => $AccionesPicia[$i]['codigo']                        
                    ];                                            
            }

            ProyectosAccionesPicia::insert($arrDataActual);
        }

        //Indicadores PICIA
        if(count($IndicadoresPicia) > 0){

            $arrDataActual = [];            

            if($codigoproy!='0'){

                $result = ProyectosIndicadoresPicia::where('proycod', $codigoproy)->delete(); 

            }

            for ($i=0; $i < count($IndicadoresPicia); $i++) {
                $arrDataActual[] = 
                    [                        
                        'proycod'    => $codigoactualproy,
                        'indpiccod'  => $IndicadoresPicia[$i]['codigo']                        
                    ];                    
            }

            ProyectosIndicadoresPicia::insert($arrDataActual);
        }

        //Programas Inv. PEI
        if(count($ProgramasPei) > 0){

            $arrDataActual = [];            

            if($codigoproy!='0'){

                $result = ProyectosProgInvPei::where('proycod', $codigoproy)->delete(); 

            }

            for ($i=0; $i < count($ProgramasPei); $i++) {

                //if($ProgramasPei[$i]['Activo']==1){

                $arrDataActual[] = 
                    [                        
                        'proycod'      => $codigoactualproy,
                        'proinvpeicod' => $ProgramasPei[$i]['codigo']                        
                    ];
                //    }                            
            }

            ProyectosProgInvPei::insert($arrDataActual);
        }

        //Lineas Inv. PEI
        if(count($LinInvPei) > 0){

            $arrDataActual = [];            

            if($codigoproy!='0'){

                $result = ProyectosLinInvPei::where('proycod', $codigoproy)->delete(); 

            }

            for ($i=0; $i < count($LinInvPei); $i++) {

                //if($LinInvPei[$i]['Activo']==1){

                $arrDataActual[] = 
                    [                        
                        'proycod'      => $codigoactualproy,
                        'lininvpeicod' => $LinInvPei[$i]['codigo']                        
                    ];
                //    }                            
            }

            ProyectosLinInvPei::insert($arrDataActual);
        }
        
        //pilares PICIA
        if(count($pilarespicia) > 0){

            $arrDataActual = [];            

            if($codigoproy!='0'){

                $result = ProyectosPilaresPicia::where('proycod', $codigoproy)->delete(); 

            }

            for ($i=0; $i < count($pilarespicia); $i++) {                

                $arrDataActual[] = 
                    [                        
                        'proycod'    => $codigoactualproy,
                        'pilpiccod'  => $pilarespicia[$i]['codigo']                        
                    ];                                             
            }

            ProyectosPilaresPicia::insert($arrDataActual);
        }

        //Fechas Present. Informes Técnicos
        /*if(count($FechasPIT) > 0){

            $arrDataActual = [];            

            if($codigoproy!='0'){

                $result = ProyectosFecPreInfTec::where('proycod', $codigoproy)->delete(); 

            }

            for ($i=0; $i < count($FechasPIT); $i++) {                

                $arrDataActual[] = 
                    [                        
                        'proycod'      => $codigoactualproy,
                        'proyfpit' => $FechasPIT[$i]['fechapresentacion']                        
                    ];
                                               
            }

            ProyectosFecPreInfTec::insert($arrDataActual);
        }

        //Fechas Present. Informes Financieros
        if(count($FechasPIF) > 0){

            $arrDataActual = [];            

            if($codigoproy!='0'){

                $result = ProyectosFecPreInfFin::where('proycod', $codigoproy)->delete(); 

            }

            for ($i=0; $i < count($FechasPIF); $i++) {                

                $arrDataActual[] = 
                    [                        
                        'proycod'  => $codigoactualproy,
                        'proyfpif' => $FechasPIF[$i]['fechapresentacion']                        
                    ];
                                               
            }

            ProyectosFecPreInfFin::insert($arrDataActual);
        }*/

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Proyectos::where('proycod', $Id)->delete(); 
        return $result;

    }

    public function ActividadesXProyecto($proycod)
    {
        try{           
            

            $ProyectosActividades = ProyectosActividades::select('proyactcod','proycod', 'proyactdes','proyactfecini','proyactfecfin')                        
                        ->where('proycod', $proycod)
                        ->orderby('proyactcod')                     
                        ->get();

            return $ProyectosActividades;            

        }catch(Exception $e){

            return $e->getMessage();

        }
    } 

    public function EquipoTecnico($proycod)
    {
        try{           
            
            $EquipoTecnico = DB::select("SELECT personal.perscod,CONCAT(personal.persnom,' ',personal.persape) persnom,personal.perside,
                                           roles.roldes proyeterol,proyectos_equipotecnico.proyeteded,
                                           proyectos_equipotecnico.proyetetit,tipos_contratos.tipcontdes perstitcont,proyectos_equipotecnico.proyetepor  
                                           FROM personal INNER JOIN proyectos_equipotecnico ON personal.perscod = proyectos_equipotecnico.perscod 
                                           INNER JOIN roles ON proyectos_equipotecnico.rolcod = roles.rolcod 
                                           INNER JOIN tipos_contratos ON personal.tipcontcod = tipos_contratos.tipcontcod
                                           WHERE proyectos_equipotecnico.proycod = ".$proycod);

            return $EquipoTecnico;            

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    private function EquipoCompromisos($proyecto)
    {
        try{
           
            $ProyectosEquipoComp = ProyectosEquipoComp::select('proyetccod','proycod','perscod','proyetcdes')                        
                        ->where('proycod', $proyecto) 
                        ->orderby('perscod')                       
                        ->orderby('proyetccod')                     
                        ->get();
                     
            return $ProyectosEquipoComp; 

        }catch(Exception $e){

            return $e->getMessage();

        }
    }  

    private function ObjetivosXProyecto($proyecto)
    {
        try{
           
            $ProyectosObjetivos = ProyectosObjetivos::select('proyobjcod','proycod', 'proyobjdes')                        
                        ->where('proycod', $proyecto)
                        ->orderby('proyobjcod')                     
                        ->get();

            return $ProyectosObjetivos;

        }catch(Exception $e){

            return $e->getMessage();

        }
    } 

    private function ProductosxProyecto($proyecto)
    {
        try{
            
            $ProyectosProductos = ProyectosProductos::select('proyectos_productos.prodcod','proyectos_productos.proycod',
                                                             'proyectos_productos.proyobjcod',
                                                             'productos.proddes','proyectos_productos.prodcod as codigoreal') 
                                     ->join('productos','proyectos_productos.prodcod','=','productos.prodcod')
                                     ->where('proyectos_productos.proycod', $proyecto)
                                     ->orderby('proyectos_productos.prodcod')  
                                     ->distinct()                   
                                     ->get();
                     
            return $ProyectosProductos;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    private function ProgInvPeixProyecto($proyecto)
    {
        try{
            
            $data = ProyectosProgInvPei::select('programas_investigacion_pei.proinvpeides') 
                                     ->join('programas_investigacion_pei','proyectos_proginv_pei.proinvpeicod','=','programas_investigacion_pei.proinvpeicod')
                                     ->where('proyectos_proginv_pei.proycod', $proyecto)
                                     ->orderby('programas_investigacion_pei.proinvpeides')  
                                     ->distinct()                   
                                     ->get();
                     
            return $data;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    private function LinInvPeixProyecto($proyecto)
    {
        try{
            
            $data = ProyectosLinInvPei::select('lineas_investigacion_pei.lininvpeides') 
                                     ->join('lineas_investigacion_pei','proyectos_lininv_pei.lininvpeicod','=','lineas_investigacion_pei.lininvpeicod')
                                     ->where('proyectos_lininv_pei.proycod', $proyecto)
                                     ->orderby('lineas_investigacion_pei.lininvpeides')  
                                     ->distinct()                   
                                     ->get();
                     
            return $data;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    private function AccionPiciaxProyecto($proyecto)
    {
        try{
            
            $data = ProyectosAccionesPicia::select('acciones_picia.accpicdes') 
                                     ->join('acciones_picia','proyectos_acciones_picia.accpiccod','=','acciones_picia.accpiccod')
                                     ->where('proyectos_acciones_picia.proycod', $proyecto)
                                     ->orderby('acciones_picia.accpicdes')  
                                     ->distinct()                   
                                     ->get();
                     
            return $data;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    private function ObjetivosProyecto($proyecto)
    {
        try{
            
            $data = ProyectosObjetivos::select('proyobjdes')                                      
                                     ->where('proycod', $proyecto)
                                     ->orderby('proyobjdes')  
                                     ->distinct()                   
                                     ->get();
                     
            return $data;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    private function DerechosAutorProyecto($proyecto)
    {
        try{
            
            $data = ProyectosProdDerAut::select('productos_derechos_autor.prodderautdes')   
                                        ->join('productos_derechos_autor','proyectos_productos_derautor.prodderautcod','=','productos_derechos_autor.prodderautcod')                        
                                        ->where('proyectos_productos_derautor.proycod', $proyecto)
                                        ->orderby('proyectos_productos_derautor.prodderautcod') 
                                        ->distinct()                     
                                        ->get();
            return $data;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    private function PropiedadIndustrialProyecto($proyecto)
    {
        try{
            
            $data = ProyectosProdPropInd::select('productos_propiedad_industrial.prodproinddes')   
                                        ->join('productos_propiedad_industrial','proyectos_productos_proindustrial.prodproindcod','=','productos_propiedad_industrial.prodproindcod')                        
                                        ->where('proyectos_productos_proindustrial.proycod', $proyecto)
                                        ->orderby('proyectos_productos_proindustrial.prodproindcod')                     
                                        ->get();           
            return $data;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    private function OrganismosVivosProyecto($proyecto)
    {
        try{
            
            $data = ProyectosOrgVivos::select('organismos_vivos.orgvivdes')   
                                        ->join('organismos_vivos','proyectos_organismos_vivos.orgvivcod','=','organismos_vivos.orgvivcod')                        
                                        ->where('proyectos_organismos_vivos.proycod', $proyecto)
                                        ->orderby('proyectos_organismos_vivos.orgvivcod')                     
                                        ->get();
            return $data;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function buscarproyectoPlanAdq(Request $request){

        try{
           
            $proyecto = $request['proyecto'];

            $sql = "select plaadqcod codigo from planes_adquisicion
                    where proycod = ".$proyecto;    

            $result = DB::select($sql);                    

            return $result; 

        }catch(Exception $e){

            return $e->getMessage();

        }


    }

    public function generateFTPdf ($proycod, $mode){
        
        $datos['proyecto'] = $this->FiltroXProyecto($proycod);
        $datos['actividades'] = $this->ActividadesXProyecto($proycod);
        $datos['equiposinchi'] = $this->EquipoTecnico($proycod);
        $datos['programaspei'] = $this->ProgInvPeixProyecto($proycod);
        $datos['lininvpei'] = $this->LinInvPeixProyecto($proycod);
        $datos['accionespicia'] = $this->AccionPiciaxProyecto($proycod);
        $datos['objetivos'] = $this->ObjetivosProyecto($proycod);

        $fechaDoc = str_replace("-", "", date('d-m-Y'));
        
        $namePDF = 'FTPROYECTO_'.$fechaDoc."_".$proycod.".pdf";

        $headerPDF = view()->make('Formatos.FTProyectoHeader', $datos)->render();        

        $pdf = \PDF::loadView('Formatos.FTProyectobody', $datos);
        $pdf->setOption('no-outline', true);
        $pdf->setOption('disable-smart-shrinking', true);
        $pdf->setOption('dpi', 600);
        $pdf->setOption('image-quality', 100);
        $pdf->setOption('copies', 1);
        $pdf->setOption('page-size', 'Letter');
        $pdf->setOption('margin-top', 50);
        $pdf->setOption('margin-bottom', 45);
        $pdf->setOption('header-spacing', 0);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-html', $headerPDF);
        
        $rta = '';
        switch ($mode) {
            case 'I':
                $rta = $pdf->stream($namePDF);
                break;
            
            case 'D':
                $rta = $pdf->download($namePDF);
                break;
            
            case 'O':
                $rta = $pdf->output();
                break;
            
            default:
                # code...
                break;
        }

        return $rta;        
    }    

    public function getFTPDF(Request $request)
    {        
        $proyecto = $request->input('proyecto', '');        
        $mode = $request->input('mode', 'I');

        return $this->generateFTPdf($proyecto, $mode);

    }

    public function generateActaPIPdf ($proycod, $mode){        
        
        $datos['proyecto'] = $this->FiltroXProyecto($proycod);       
        $datos['equipo'] = $this->EquipoTecnico($proycod);
        $datos['objetivos'] = $this->ObjetivosXProyecto($proycod); 
        $datos['productos'] = $this->ProductosxProyecto($proycod);     
        $datos['compromisos'] = $this->EquipoCompromisos($proycod);

        $datos['programaspei'] = $this->ProgInvPeixProyecto($proycod);
        $datos['derechosautor'] = $this->DerechosAutorProyecto($proycod);
        $datos['propiedadindustrial'] = $this->PropiedadIndustrialProyecto($proycod);
        $datos['organismosvivos'] = $this->OrganismosVivosProyecto($proycod);

        $fechaDoc = str_replace("-", "", date('d-m-Y'));
        
        $namePDF = 'ACTAPI_'.$fechaDoc."_".$proycod.".pdf";

        $headerPDF = view()->make('Formatos.ActaPIHeader', $datos)->render();
        
        $pdf = \PDF::loadView('Formatos.ActaPIbody', $datos);
        $pdf->setOption('no-outline', true);
        $pdf->setOption('disable-smart-shrinking', true);
        $pdf->setOption('dpi', 600);
        $pdf->setOption('image-quality', 100);
        $pdf->setOption('copies', 1);
        $pdf->setOption('page-size', 'Letter');
        $pdf->setOption('margin-top', 50);
        $pdf->setOption('margin-bottom', 45);
        $pdf->setOption('header-spacing', 0);
        $pdf->setOption('footer-spacing', 0);
        $pdf->setOption('header-html', $headerPDF);
        
        $rta = '';
        switch ($mode) {
            case 'I':
                $rta = $pdf->stream($namePDF);
                break;
            
            case 'D':
                $rta = $pdf->download($namePDF);
                break;
            
            case 'O':
                $rta = $pdf->output();
                break;
            
            default:
                # code...
                break;
        }

        return $rta;        
    }

    public function getActaPI(Request $request)
    {        
        $proyecto = $request->input('proyecto', '');        
        $mode = $request->input('mode', 'I');

        return $this->generateActaPIPdf($proyecto, $mode);

    }

    public function actualizaTasa(Request $request)
    {
        $proyecto = $request->proyecto;
        $tasa = $request->tasa;
        
        $dataUpd = Proyectos::where('proycod', $proyecto)->first();
          
        $dataUpd->proytas = $tasa;
        $result = $dataUpd->save();

        return array( 'respuesta' => $result);     
    }

    private function EstadoProyectoObjetivos($proyecto)
    {
        //$proyecto = $request->proyecto;
        
        $sql = "SELECT po.proyobjcod objetivo,po.proyobjdes txtobjetivo,po.proyobjpes pesoobj,(SUM(IFNULL(prod.total,0)/100) * (IFNULL(po.proyobjpes,0)/100)) * 100 as total 
                FROM proyectos_objetivos po LEFT JOIN 
                    (
                        SELECT pr.proycod,pr.proyobjcod,pr.prodcod,pr.proyproddes,pr.proyprodpes,((pr.proyprodpes/100) * SUM(IFNULL(act.total,0)/100)) * 100 as total
                        FROM proyectos_productos pr 
                        LEFT JOIN 
                                    (SELECT pa.proycod,pa.proyobjcod,pa.prodcod,pa.proyactdes txtactividad,pa.proyactpes peso, 
                                        (SUM((ps.proyspames1/100) + (ps.proyspames2/100) + 
                                            (ps.proyspames3/100) + (ps.proyspames4/100) + (ps.proyspames5/100) +
                                            (ps.proyspames6/100) + (ps.proyspames7/100) + (ps.proyspames8/100) + (ps.proyspames9/100) + (ps.proyspames10/100) +
                                            (ps.proyspames11/100) + (ps.proyspames12/100)) * (pa.proyactpes/100)) * 100 as total
                                        FROM proyectos_actividades pa LEFT JOIN proyectos_seguimiento_planaccion ps ON pa.proyactsec = ps.proyactsec 
                                        GROUP BY pa.proycod,pa.proyobjcod,pa.prodcod,pa.proyactdes,pa.proyactpes) AS act ON 
                            pr.proycod = act.proycod AND pr.proyobjcod = act.proyobjcod AND pr.prodcod = act.prodcod       
                        GROUP BY pr.proycod,pr.proyobjcod,pr.prodcod,pr.proyproddes,pr.proyprodpes
                    )  prod ON po.proycod = prod.proycod AND po.proyobjcod = prod.proyobjcod
                WHERE po.proycod = ".$proyecto."
                GROUP BY po.proyobjcod,po.proyobjdes,po.proyobjpes";
        
        $result = DB::select($sql);                    
        
        return $result;     
    }

    private function EstadoProyectoProductos($proyecto)
    {
        //$proyecto = $request->proyecto;
        
        $sql = "SELECT pr.proycod,pr.proyobjcod,pr.prodcod producto,pr.proyproddes txtproducto,pr.proyprodpes pesoprod,((pr.proyprodpes/100) * SUM(IFNULL(act.total,0)/100)) * 100 as total
                FROM proyectos_productos pr
                LEFT JOIN 
                            (SELECT pa.proycod,pa.proyobjcod,pa.prodcod,pa.proyactdes txtactividad,pa.proyactpes peso, 
                                (SUM((ps.proyspames1/100) + (ps.proyspames2/100) + 
                                    (ps.proyspames3/100) + (ps.proyspames4/100) + (ps.proyspames5/100) +
                                    (ps.proyspames6/100) + (ps.proyspames7/100) + (ps.proyspames8/100) + (ps.proyspames9/100) + (ps.proyspames10/100) +
                                    (ps.proyspames11/100) + (ps.proyspames12/100)) * (pa.proyactpes/100)) * 100 as total
                                FROM proyectos_actividades pa LEFT JOIN proyectos_seguimiento_planaccion ps ON pa.proyactsec = ps.proyactsec 
                                GROUP BY pa.proycod,pa.proyobjcod,pa.prodcod,pa.proyactdes,pa.proyactpes) AS act ON 
                    pr.proycod = act.proycod AND pr.proyobjcod = act.proyobjcod AND pr.prodcod = act.prodcod
                WHERE pr.proycod = ".$proyecto." 
                GROUP BY pr.proycod,pr.proyobjcod,pr.prodcod,pr.proyproddes,pr.proyprodpes";
        
        $result = DB::select($sql);                    
        
        return $result;     
    }

    private function EstadoProyectoActividades($proyecto)
    {
        //$proyecto = $request->proyecto;
        
        $sql = "SELECT pa.proyactsec actividad,pa.proyactcod codigo,pa.proyobjcod,pa.prodcod,pa.proyactdes txtactividad,pa.proyactpes pesoact, (SUM((ps.proyspames1/100) + (ps.proyspames2/100) + 
                        (ps.proyspames3/100) + (ps.proyspames4/100) + (ps.proyspames5/100) +
                        (ps.proyspames6/100) + (ps.proyspames7/100) + (ps.proyspames8/100) + (ps.proyspames9/100) + (ps.proyspames10/100) +
                        (ps.proyspames11/100) + (ps.proyspames12/100)) * (pa.proyactpes/100)) * 100 as total
                FROM proyectos_actividades pa LEFT JOIN proyectos_seguimiento_planaccion ps ON pa.proyactsec = ps.proyactsec 
                WHERE pa.proycod = ".$proyecto." 
                GROUP BY pa.proyactsec,pa.proyactcod,pa.proyobjcod,pa.prodcod,pa.proyactdes,pa.proyactpes";
        
        $result = DB::select($sql);                    
        
        return $result;     
    }

    public function EstadoActual(Request $request){

        $proyecto = $request->proyecto;

        $objetivos = $this->EstadoProyectoObjetivos($proyecto);
        $productos = $this->EstadoProyectoProductos($proyecto);
        $actividades = $this->EstadoProyectoActividades($proyecto);

        return array( 'objetivos' => $objetivos, 'productos' => $productos, 'actividades' => $actividades);
    }

    public function actualizaVoBo(Request $request)
    {
        $proyecto = $request->proyecto;
        $vistobueno = $request->vistobueno;
        $tipo = $request->tipo;
        
        $dataUpd = Proyectos::where('proycod', $proyecto)->first();
        if ($tipo == 'direccion') {
            $dataUpd->proyvbdir = $vistobueno;
        } else {
            $dataUpd->proyvbpla = $vistobueno;
        }
        $result = $dataUpd->save();

        return array( 'respuesta' => $result);     
    }

    private function versionMaxConvenio($convenio)
    {
        $maxVal  = Proyectos::where('convcod', $convenio)->max('proyver');

        if (is_numeric($maxVal)) {

            $rta = $maxVal + 1;

        } else {
            $rta = 1;            
        }
            
        return $rta;

    }

    private function buscarCodigoProyectoMax($convenio)
    {
        $version = $this->versionMaxConvenio($convenio);
        $version = $version - 1;

        $data  = Proyectos::where('convcod', $convenio)->where('proyver', $version)->get();

        $result = $this->response->collection($data, new ProyectosTransformer);
        
        $codigo = $result[0]->codigo;
        
        return $codigo;

    }

    public function voboActualizacion(Request $request)
    {
        $proyecto    = $request->proyecto;
        $convenio    = $request->proyecto;
        $vistobueno  = $request->vistobueno;
        $observacion = $request->observacion;
        $poa         = $request->poa;

        if($vistobueno == 'S'){

            $version = $this->versionMaxConvenio($convenio);     
            
            $dataUpd = Proyectos::where('proycod', $proyecto)->first();
          
            $dataUpd->proyver    = $version;
            $dataUpd->proyapract = 'S';

            $result = $dataUpd->save();

        }else if($vistobueno == 'N'){

            //se debe eliminar los registros en las tablas

            $sql = "DELETE d FROM proyectos_seguimiento_detalle d INNER JOIN 
                        proyectos_actividades a ON d.proyactsec = a.proyactsec 
                        WHERE a.proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_seguimiento_planaccion WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_programacion_planaccion WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_actividades WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_productos WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_objetivos WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_obsplaneacion WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_proginv_pei WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_productos_proindustrial WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_productos_derautor WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_preguntas_formulacion WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_pilares_picia WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_otrosi WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_organismos_vivos WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_obssubdcientifica WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_lininv_picia WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_lininv_pei WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_indicadores_picia WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_equipotecnico_compromisos WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_equipotecnico WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_ciudades WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos_acciones_picia WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM proyectos WHERE proycod = ".$proyecto;
            $result = DB::delete($sql);

            $sql = "DELETE FROM poas WHERE poacod = ".$poa;
            $result = DB::delete($sql);

            //agrega la observación a la version máxima
            $codigomaxproyecto = $this->buscarCodigoProyectoMax($convenio);

            $sql = "INSERT INTO proyectos_obsplaneacion (proycod,proyobsplacod,proyobsplades) 
                        values(".$codigomaxproyecto.")";
            $result = DB::insert($sql);            


        }

    }

}

?>