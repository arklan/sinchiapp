<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosActividadesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosActividades;
use DB;

class ProyectosActividadesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosActividades = ProyectosActividades::select('proyactcod','proycod','proyobjcod','prodcod',
                                                                 'proyactdes','proyactfecini','proyactfecfin','proyactpes',
                                                                 'proyactkpi','proyactdeskpi','proyactsec','proyacttip','proyactval')                        
                        ->orderby('proyactsec')                     
                        ->get();
                     
            if ($ProyectosActividades->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosActividades, new ProyectosActividadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = ProyectosActividades::max('proyactcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo, $Proyecto){

        try{
           
            $mensaje = '';

            $Duplica = ProyectosActividades::select(DB::raw('count(proyactcod) as total'))                      
                      ->where('proyactdes', $Nombre)
                      ->where('proycod', $Proyecto)
                      ->where('proyactcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe para la actividades del proyecto';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    private function buscarCodigoActividad($Proyecto, $CodigoObj, $CodigoProducto, $CodigoActividad){

        try{
           
            $ok = false;

            $Encontrado = ProyectosActividades::select(DB::raw('count(proyactcod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('proyobjcod',$CodigoObj)
                      ->where('prodcod',$CodigoProducto)
                      ->where('proyactsec',$CodigoActividad)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
    
        $result = '';
        
        for ($i=0; $i < count($data); $i++) {             
                        
            $codigo = isset($data[$i]['codigo']) ? $data[$i]['codigo'] : 0;
            $secuencia = isset($data[$i]['secuencia']) ? $data[$i]['secuencia'] : 0;
            $proyecto = $data[$i]['proyecto'];
            $objetivo = $data[$i]['objetivo'];
            $producto = $data[$i]['producto'];
            $descripcion = strtoupper(trim($data[$i]['descripcion']));
            $fechainicio = $data[$i]['fechainicio'];
            $fechafin = $data[$i]['fechafin'];
            $peso  = $data[$i]['peso'];
            $kpi  = $data[$i]['kpi'];
            $descripcionkpi = $data[$i]['descripcionkpi'];   
            $tipo = $data[$i]['tipo'];
            $valor = $data[$i]['valor'];
                    
            $mensaje = ''; //$this->buscarDuplicado($descripcion, $codigo, $proyecto);

            if($mensaje != ''){

                return $this->response->errorNotFound($mensaje);

            }

            if($codigo == 0)
                $encontrado = false;
            else    
                $encontrado = $this->buscarCodigoActividad($proyecto, $objetivo, $producto, $secuencia);

            if($encontrado){

                $query = "UPDATE proyectos_actividades SET proyactdes = '".$descripcion."',proyactfecini = '".$fechainicio."', 
                          proyactfecfin = '".$fechafin."',proyactpes = '".$peso."',proyactkpi = '".$kpi."',
                          proyactdeskpi = '".$descripcionkpi."',proyacttip = '".$tipo."',proyactval = '".$valor."'     
                          WHERE proycod=".$proyecto." AND proyobjcod = ".$objetivo." 
                          AND prodcod = ".$producto."  AND proyactsec = ".$secuencia;
                $result = DB::update($query); 
            
            } else {                

                $dataAdd = new ProyectosActividades();
                
                $dataAdd->proyactcod    = $codigo;
                $dataAdd->proycod       = $proyecto;
                $dataAdd->proyobjcod    = $objetivo;
                $dataAdd->prodcod       = $producto;
                $dataAdd->proyactdes    = $descripcion;
                $dataAdd->proyactfecini = $fechainicio;           
                $dataAdd->proyactfecfin = $fechafin;                
                $dataAdd->proyactpes = $peso;
                $dataAdd->proyactkpi = $kpi;
                $dataAdd->proyactdeskpi = $descripcionkpi;
                $dataAdd->proyacttip = $tipo;
                $dataAdd->proyactval = $valor;
                
                $result = $dataAdd->save();
            
            }
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
            $objetivo = $request['objetivo'];
            $producto = $request['producto'];

            $ProyectosActividades = ProyectosActividades::select('proyactcod','proycod','proyobjcod','prodcod', 
                                                                'proyactdes','proyactfecini','proyactfecfin','proyactpes',
                                                                'proyactkpi','proyactdeskpi','proyactsec','proyacttip','proyactval')                        
                        ->where('proycod', $proyecto)
                        ->where('proyobjcod', $objetivo)
                        ->where('prodcod', $producto)
                        ->orderby('proyactsec')                     
                        ->get();
                     
            if ($ProyectosActividades->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosActividades, new ProyectosActividadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function BuscarXProyectoObjetivo(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
            $objetivo = $request['objetivo'];            

            $ProyectosActividades = ProyectosActividades::select('proyactcod','proycod','proyobjcod','prodcod', 
                                                                'proyactdes','proyactfecini','proyactfecfin','proyactpes',
                                                                'proyactkpi','proyactdeskpi','proyactsec','proyacttip','proyactval')                        
                        ->where('proycod', $proyecto)
                        ->where('proyobjcod', $objetivo)                     
                        ->orderby('proyactcod')                     
                        ->get();
                     
            if ($ProyectosActividades->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosActividades, new ProyectosActividadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function FiltrarxActividad(Request $request)
    {
        try{
           
            //$proyecto = $request['proyecto'];
            //$objetivo = $request['objetivo']; 
            $actividad = $request['actividad'];            

            $ProyectosActividades = ProyectosActividades::select('proyactcod','proycod','proyobjcod','prodcod', 
                                                                'proyactdes','proyactfecini','proyactfecfin','proyactpes',
                                                                'proyactkpi','proyactdeskpi','proyactsec','proyacttip','proyactval')                        
                        //->where('proycod', $proyecto)
                        //->where('proyobjcod', $objetivo) 
                        ->where('proyactsec', $actividad)                    
                        ->orderby('proyactsec')                     
                        ->get();
                     
            if ($ProyectosActividades->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosActividades, new ProyectosActividadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $actividad = $request->actividad;
        $proyecto = $request->proyecto;
        $objetivo = $request->objetivo;
        $producto = $request->producto;

        try{

            $result = ProyectosActividades::where('proycod', $proyecto)
                                            ->where('proyobjcod', $objetivo)
                                            ->where('prodcod', $producto)
                                            ->where('proyactsec', $actividad)->delete(); 
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }

    public function FiltrarProyectoAll(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];

            $sql = "select a.proyactsec secuencia,a.proyactfecini fechainicio,a.proyactfecfin fechafinal,
                        a.proyactpes peso,o.proyobjdes objetivo,p.proyproddes producto,
                        a.proyactdes descripcion,a.proyactcod nactividad,
                        CONCAT(a.proyobjcod,'.',a.prodcod,'.',a.proyactcod,'.',' ',a.proyactdes) as descripcionfull,
                        CONCAT(a.proyobjcod,a.prodcod,a.proyactcod) codigofull
                    from proyectos_actividades as a inner join proyectos_productos p on 
                        a.proycod=p.proycod AND a.proyobjcod=p.proyobjcod AND a.prodcod = p.prodcod
                    inner join proyectos_objetivos o ON p.proyobjcod = o.proyobjcod 
                        AND p.proycod = o.proycod
                    where a.proycod = ".$proyecto;    

            $result = DB::select($sql);                    

            return $result; 

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    
}

?>