<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\NotificacionesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notificaciones;
use DB;

class NotificacionesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Notificaciones = Notificaciones::select('notificaciones.notcod','notificaciones.iduser','notificaciones.notfec',
                                'notificaciones.notmen','notificaciones.notvis','users.nombre','notificaciones.opccod', 'opciones.opcnom')
                     ->join('users','notificaciones.iduser','=','users.iduser')
                     ->join('opciones','notificaciones.opccod','=','opciones.opccod')           
                     ->orderby('notificaciones.notfec','desc')                     
                     ->get();
                     
            if ($Notificaciones->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Notificaciones, new NotificacionesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function ByUser(Request $request)
    {       
        $data = $request;
        $usuario = $data['usuario'];

       try{

            $Notificaciones = Notificaciones::select('notificaciones.notcod','notificaciones.iduser','notificaciones.notfec',
                                'notificaciones.notmen','notificaciones.notvis','notificaciones.notnumint','users.nombre',
                                'notificaciones.opccod','opciones.opcnom')
                     ->join('users','notificaciones.iduser','=','users.iduser')  
                     ->join('opciones','notificaciones.opccod','=','opciones.opccod')
                     ->where('notificaciones.iduser',$usuario)  
                     ->where('notificaciones.notvis',0)       
                     ->orderby('notificaciones.notfec','desc')                     
                     ->get();
                     
            if ($Notificaciones->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Notificaciones, new NotificacionesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Notificaciones::max('notcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DataAdd(Request $request)
    {
        date_default_timezone_set('America/Bogota');

        $data = $request->data;
        
        $fecha   = date('Y\-m\-d H:i:s');
        $usuario = $data['iduser'];
        $mensaje = strtoupper(trim($data['mensaje']));
        $codigoopcion =  $data['codigoopcion'];
        $numerointerno =  $data['numerointerno'];      

        $consecutivo =$this->consecutivo();

        $dataAdd = new Notificaciones();
        
        $dataAdd->notcod = $consecutivo;
        $dataAdd->iduser = $usuario;
        $dataAdd->notfec = $fecha;
        $dataAdd->notmen = $mensaje;
        $dataAdd->notvis = 0;
        $dataAdd->opccod = $codigoopcion;
        $dataAdd->notnumint = $numerointerno;
        
        $result = $dataAdd->save();

        return array( 'respuesta' => $result);     
    }

    public function OcultarNotificaciones(Request $request)
    {
        $data = $request->data;
        
        $notificaciones = [];

        if(count($data) > 0){
            foreach ($data as $value) {
                 
                array_push($notificaciones,$value['codigo']);
            }

            $result = Notificaciones::whereIn('notcod', $notificaciones)->update(array('notvis' => 1));            
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Notificaciones::where('notcod', $Id)->delete(); 
        return $result;

    }
}

?>