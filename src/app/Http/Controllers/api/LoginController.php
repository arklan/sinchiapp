<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\LoginTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Login;
use DB;

class LoginController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $login = Login::select('users.iduser','users.nombre','users.usuario','users.email','users.sexo','users.estado','perfiles.perfnom',
                                'users.perfcod',DB::raw("(CASE WHEN users.estado='A' THEN 'ACTIVO' ELSE 'INACTIVO' END) as estnom"),
                                'users.perscod','personal.persnom','personal.persape')
                    ->join('perfiles','users.perfcod','=','perfiles.perfcod') 
                    ->join('personal','users.perscod','=','personal.perscod')           
                    ->orderBy('users.nombre')->get();
        return $this->response->collection($login, new LoginTransformer);
    }

    public function consecutivoUser()
    {      

        $maxVal  = Login::max('iduser');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function guardarUsuario(Request $request)
    {        

        $usuario = $request->data;

        $codigo = $usuario['codigo'];
        $nombre = strtoupper(trim($usuario['nombre']));
        $user   = trim($usuario['usuario']);        
        $sexo   = trim($usuario['sexo']);
        $email  = trim($usuario['email']);
        $estado = trim($usuario['estado']);        
        $perfil = trim($usuario['perfil']);
        $personal = $usuario['codigopersonal'];

        $mensaje = $this->buscarDuplicadoUsuario($nombre, $user, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivoUser();

            $data = [
                    'iduser'   => $consecutivo,
                    'nombre'   => $nombre,
                    'usuario'  => $user,
                    'email'    => $email,
                    'sexo'     => $sexo,                    
                    'password' => $user,
                    'estado'   => $estado,
                    'perfcod'  => $perfil,
                    'perscod'  => $personal
            ];
            
            $result = Login::insert($data);
        
        } else {

            $data = [                
                    'nombre'   => $nombre,
                    'usuario'  => $user,
                    'email'    => $email,
                    'sexo'     => $sexo, 
                    'estado'   => $estado,
                    'perfcod'  => $perfil,
                    'perscod'  => $personal                   
            ];

            $result = Login::where(['iduser' => $codigo])
                        ->update($data);
        } 

        return array( 'respuesta' => $result);          
    }

    public function resetearPassword(Request $request)
    {
        $userid = $request->userid;
        $nomusuario = $request->nomusuario;

        $result = Login::where(['iduser' => $userid])->update(['password' => $nomusuario]);
                  
        return $result;          
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function buscarDuplicadoUsuario($Nombre, $usuario, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Login::select(DB::raw('count(iduser) as total'))                      
                      ->where('nombre', $Nombre)
                      ->where('iduser','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                } else {

                    $Duplica2 = Login::select(DB::raw('count(iduser) as total'))                     
                      ->where('usuario', $usuario)
                      ->where('iduser','!=', $Codigo)
                      ->get();  

                    if (!$Duplica2->isEmpty()) {  

                        $total = $Duplica2[0]->total;
                        if($total != '0'){

                            $mensaje = 'El Usuario ya existe';

                        }

                    }

                }
            }
            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    public function show($id)
    {
        //
    }

    public function search(Request $request) 
    {

        $usuario = $request->username; 
        $clave = $request->password;

        try{

            $login = Login::select('iduser','nombre','usuario','sexo','perscod','perfcod')                 
                     ->where('usuario',$usuario)
                     ->where('estado','A')
                     ->whereRaw("BINARY password = '$clave'")->get();

            if ($login->isEmpty()) {
                //throw new Exception('Usuario y contraseña incorrectos');
                return $this->response->errorNotFound('Usuario y/o contraseña incorrectos, o el Usuario esta inactivo');
            }
            else {

                return $this->response->collection($login, new LoginTransformer);
            }

        }catch(Exception $e){

            return $e->getMessage();

        }

    }
    
    public function personalFaltanteUser(Request $request) 
    {

        $codigo = $request->codigo;         

        try{

            if($codigo != '0'){

                $first = DB::table('personal')
                            ->select('perscod','persape','persnom',DB::raw('CONCAT(persnom," ",persape) AS nombrecompleto'))
                            ->where('perscod',$codigo);

                $data = DB::table('personal') 
                        ->leftJoin('users', 'personal.perscod', '=', 'users.perscod')
                        ->select('personal.perscod AS codigo','personal.persape','personal.persnom',DB::raw('CONCAT(personal.persnom," ",personal.persape) AS nombrecompleto'))
                        ->whereNull('users.perscod')
                        ->where('personal.persest','1')
                        ->union($first)
                        ->orderBy('persnom')
                        ->orderBy('persape')
                        ->get();

            } else {

                $data = DB::table('personal') 
                        ->leftJoin('users', 'personal.perscod', '=', 'users.perscod')
                        ->select('personal.perscod','personal.persape','personal.persnom',DB::raw('CONCAT(personal.persnom," ",personal.persape) AS nombrecompleto'))
                        ->whereNull('users.perscod')
                        ->where('personal.persest','1')
                        ->orderBy('personal.persnom')
                        ->orderBy('personal.persape')
                        ->get();
            }
            
            return $data;           

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updatePassword(Request $request)
    {

        $buspwd = Login::whereRaw("BINARY password = '$request->passwordant'")->where('iduser', $request->usuario)->get();

        if ($buspwd->isEmpty()) {

            return array( 'success' => false, 'message' => 'La Contraseña anterior no coincide');

        } else {

            $mtusuario = Login::find($request->usuario);

            if ( trim($request->password) != '' ) {

                $mtusuario->password = trim($request->password);

            }     
           
            $mtusuario->save();

            return array( 'success' => true, 'message' => 'Contraseña actualizada correctamente.');
        }    

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        //$result1 = DB::table('opciones_usuarios')->where(['user_id' => $Id])->delete();
        $result = Login::where(['iduser' => $Id])->delete();

        return $result;
    }

    public function usuariosPorPerfil(Request $request){

        $codigoperfil = $request->codigoperfil;

        $login = Login::select('users.iduser','users.nombre','users.usuario','users.email','users.sexo','users.estado','perfiles.perfnom',
                                'users.perfcod',DB::raw("(CASE WHEN users.estado='A' THEN 'ACTIVO' ELSE 'INACTIVO' END) as estnom"),
                                'users.perscod','personal.persnom','personal.persape')
                    ->join('perfiles','users.perfcod','=','perfiles.perfcod') 
                    ->join('personal','users.perscod','=','personal.perscod')    
                    ->where('users.perfcod',$codigoperfil)       
                    ->orderBy('users.nombre')->get();
        return $this->response->collection($login, new LoginTransformer);

    }
}
