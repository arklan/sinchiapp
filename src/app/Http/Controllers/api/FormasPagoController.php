<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\FormasPagoTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FormasPago;
use DB;

class FormasPagoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $FormasPago = FormasPago::select('forpagcod','forpagdes')
                     ->orderby('forpagdes')                     
                     ->get();
                     
            if ($FormasPago->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($FormasPago, new FormasPagoTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = FormasPago::max('forpagcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($descripcion, $codigo){

        try{
           
            $mensaje = '';

            $Duplica = FormasPago::select(DB::raw('count(forpagcod) as total'))                      
                      ->where('forpagdes', $descripcion)
                      ->where('forpagcod','!=', $codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El descripcion ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function FormasPagoAddUpdate(Request $request)
    {
        $FormasPago = $request->data;

        $codigo = $FormasPago['codigo'];
        $descripcion = strtoupper(trim($FormasPago['descripcion']));       

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $FormasPagoAdd = new FormasPago();
            
            $FormasPagoAdd->forpagcod = $consecutivo;           
            $FormasPagoAdd->forpagdes = $descripcion;           
            
            $result = $FormasPagoAdd->save();
        
        } else {

            $FormasPagoUpd = FormasPago::where('forpagcod', $codigo)->first();
          
            $FormasPagoUpd->forpagdes = $descripcion;          

            // Guardamos en base de datos
            $result = $FormasPagoUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = FormasPago::where('forpagcod', $Id)->delete(); 
        return $result;

    }
}

?>