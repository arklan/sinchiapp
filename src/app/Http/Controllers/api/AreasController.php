<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\AreasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Areas;
use DB;

class AreasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Areas = Areas::select('areas.arecod','areas.arenom','areas.areenccod',
                                    'personal.persnom','personal.persape')
                     ->join('personal','areas.areenccod','=','personal.perscod')               
                     ->orderby('areas.arenom')                     
                     ->get();
                     
            if ($Areas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Areas, new AreasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function dataXArea(Request $request)
    {       
       try{

            $area = $request['area'];

            $Areas = Areas::select('areas.arecod','areas.arenom','areas.areenccod',
                                    'personal.persnom','personal.persape')
                     ->join('personal','areas.areenccod','=','personal.perscod')     
                     ->where('areas.arecod',$area)          
                     ->orderby('areas.arenom')                     
                     ->get();
                     
            if ($Areas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Areas, new AreasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function userXArea(Request $request)
    {       
       try{

            $area = $request['area'];

            $Areas = Areas::select('areas.arecod','areas.arenom','areas.areenccod',
                                    'personal.persnom','personal.persape','users.iduser')
                     ->join('personal','areas.areenccod','=','personal.perscod')  
                     ->join('users','personal.perscod','=','users.perscod')   
                     ->where('areas.arecod',$area)          
                     ->orderby('areas.arenom')                     
                     ->get();
                     
            if ($Areas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Areas, new AreasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Areas::max('arecod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Areas::select(DB::raw('count(arecod) as total'))                      
                      ->where('arenom', $Nombre)
                      ->where('arecod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function areaAddUpdate(Request $request)
    {
        $area = $request->data;

        $codigo = $area['codigo'];
        $nombre = strtoupper(trim($area['nombre']));
        $encargadocodigo = $area['encargadocodigo'];      

        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $areaAdd = new Areas();
            
            $areaAdd->arecod = $consecutivo;           
            $areaAdd->arenom = $nombre;
            $areaAdd->areenccod = $encargadocodigo;           
            
            $result = $areaAdd->save();
        
        } else {

            $areaUpd = Areas::where('arecod', $codigo)->first();
          
            $areaUpd->arenom = $nombre;          
            $areaUpd->areenccod = $encargadocodigo;

            // Guardamos en base de datos
            $result = $areaUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Areas::where('arecod', $Id)->delete(); 
        return $result;

    }
}

?>