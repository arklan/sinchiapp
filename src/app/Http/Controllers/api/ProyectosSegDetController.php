<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosSegDetTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosSegDet;
use DB;

use Illuminate\Support\Facades\Storage;

class ProyectosSegDetController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosSegDet = ProyectosSegDet::select('proyactsec','proyspaano','proyseddetmes','proyseddetlog',
                                                       'proysegdetnec','proysegdetale','proysegdetain','proysegdetsop',
                                                       'proysegdetosc')                        
                        ->orderby('proyactsec')
                        ->orderby('proyspaano') 
                        ->orderby('proyseddetmes')                     
                        ->get();
                     
            if ($ProyectosSegDet->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosSegDet, new ProyectosSegDetTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    private function buscarSegLlave($codigo, $anno, $mes){

        try{
           
            $ok = false;

            $Encontrado = ProyectosSegDet::select(DB::raw('count(proyactsec) as total'))
                      ->where('proyactsec', $codigo)
                      ->where('proyspaano',$anno)
                      ->where('proyseddetmes',$mes)                     
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {   

        $data = $request->data;
        
        $result = '';
     
        $codigo = $data['codigo'];
        $anno = $data['anno'];
        $mes = $data['mes'];
        $logros = $data['logros'];
        $necesidades = $data['necesidades'];
        $alertas = $data['alertas'];
        $ampliacioninfo = $data['ampliacioninfo'];
                
        $filegeneral = $data['archivo'];

        $encontrado = $this->buscarSegLlave($codigo, $anno, $mes);

        if($encontrado){

            $query = "UPDATE proyectos_seguimiento_detalle SET proyseddetlog = '".$logros."',proysegdetnec = '".$necesidades."', 
                          proysegdetale = '".$alertas."',proysegdetain = '".$ampliacioninfo."',proysegdetsop = '".$filegeneral['file_name']."' 
                      WHERE proyactsec = ".$codigo." AND proyspaano = ".$anno." AND proyseddetmes = ".$mes;
            $result = DB::update($query);

        } else {

            $dataAdd = new ProyectosSegDet();
                
            $dataAdd->proyactsec    = $codigo;           
            $dataAdd->proyspaano    = $anno;
            $dataAdd->proyseddetmes = $mes;
            $dataAdd->proyseddetlog = $logros;           
            $dataAdd->proysegdetnec = $necesidades;                
            $dataAdd->proysegdetale = $alertas;
            $dataAdd->proysegdetain = $ampliacioninfo;
            $dataAdd->proysegdetsop = $filegeneral['file_name'];
            $dataAdd->proysegdetosc = '';           
                
            $result = $dataAdd->save();
        }

        /************************** archivos    ************************ 
         * *************************************************************/        
        $path = public_path('/proyectos/S' . strval($codigo).strval($anno).strval($mes));
        $pathorigen = public_path('tmp/'); 

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        //archivo general
        if($filegeneral['file_name'] != '' && $filegeneral['file_name_tmp'] != ''){
            $filename = $filegeneral['file_name'];        

            copy($pathorigen.$filegeneral['file_name_tmp'], $path.'/'.$filename);            
        }
        
        if (file_exists($pathorigen.$filegeneral['file_name_tmp']) && $filegeneral['file_name_tmp'] != '')            
            unlink($pathorigen.$filegeneral['file_name_tmp']);

        return array( 'respuesta' => $result);     
    }    

    public function eliminarArchivo(Request $request){

        $ruta = $request->ruta;
        $archivo = $request->archivo;
        $codigo = $request->codigo;
        $anno = $request->anno;
        $mes = $request->mes;

        try {
            $query = "UPDATE proyectos_seguimiento_detalle SET proysegdetsop = ''  
                    WHERE proyactsec = ".$codigo." AND proyspaano = ".$anno." AND proyseddetmes = ".$mes;
            $result = DB::update($query);

            $ruracompleta = public_path($ruta.$archivo);

            if (file_exists($ruracompleta))
                unlink($ruracompleta);

            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se eiminó el Archivo con éxito'],200);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);

        }
        
        return $respuesta;
    }

    public function actualizarObsSubDirCientifica(Request $request)
    { 
        
        $result = '';
     
        $codigo = $request->codigo;
        $anno = $request->anno;
        $mes = $request->mes;
        $obssubdircientifica = $request->obssubdircientifica;
        
        $query = "UPDATE proyectos_seguimiento_detalle SET proysegdetosc = '".$obssubdircientifica."'  
                    WHERE proyactsec = ".$codigo." AND proyspaano = ".$anno." AND proyseddetmes = ".$mes;
        $result = DB::update($query);    

        return array( 'respuesta' => $result);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $codigo = $request['codigo'];
            $anno = $request['anno'];
            $mes = $request['mes'];

            $ProyectosSegDet = ProyectosSegDet::select('proyactsec','proyspaano','proyseddetmes','proyseddetlog',
                                                        'proysegdetnec','proysegdetale','proysegdetain','proysegdetsop',
                                                        'proysegdetosc')
                                                ->where('proyactsec', $codigo)
                                                ->where('proyspaano', $anno)
                                                ->where('proyseddetmes', $mes)
                                                ->orderby('proyactsec')
                                                ->orderby('proyspaano') 
                                                ->orderby('proyseddetmes')                   
                                                ->get();
                     
            if ($ProyectosSegDet->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosSegDet, new ProyectosSegDetTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    } 
    
    /*private function uploadFile($file, $codigoactividad,$mes,$anno) {

        print_r($file);

        $fileName = time().'.pdf';
        $file->move(public_path('upload'), $fileName);

        

        return '';
       

        $filename = 'SegDeta_'.$codigoactividad.'_'.$mes.'_'.$anno.'.pdf';// $file->getClientOriginalName();

        $path = hash( 'sha256', time());

        $contents = Storage::get($file['file']);

        if(Storage::disk('uploadsProyDetalle')->put('file.jpg', $contents))
            return $filename;
        else
            return '';    

       

    }*/

    public function uploadFile(Request $request) {

        print_r($request->File);
        //print_r($_FILES);

        return;

        //$fileName = time().'.pdf';
        //$file->move(public_path('upload'), $fileName);

        //return '';

        /*$file = $request['file'];//Input::file('file');
        $codigoactividad = $request['codigo'];
        $mes = $request['mes'];
        $anno = $request['anno'];*/

        $filename = 'SegDeta_'.$codigoactividad.'_'.$mes.'_'.$anno.'.pdf';// $file->getClientOriginalName();

        $path = hash( 'sha256', time());

        $contents = Storage::get($file['file']);

        if(Storage::disk('uploadsProyDetalle')->put('file.jpg', $contents))
            return $filename;
        else
            return '';     

        /*if(Storage::disk('uploadsProyDetalle')->put($path.'/'.$filename,  File::get($file))) {
            
            return $filename;
        }
        return '';*/

    }
}

?>