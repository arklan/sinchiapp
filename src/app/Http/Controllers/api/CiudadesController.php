<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\CiudadesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ciudades;
use DB;

class CiudadesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            
            $Ciudades = Ciudades::select('ciudades.ciucod','ciudades.ciunom','ciudades.depcod','departamentos.depnom')
                     ->join('departamentos','ciudades.depcod','=','departamentos.depcod')
                     ->orderby('ciudades.ciunom')                     
                     ->get();
                     
            if ($Ciudades->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Ciudades, new CiudadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
    
    public function FiltroXDepartamento(Request $request)
    {       
       try{

            $departamento = $request['departamento'];
            
            $Ciudades = Ciudades::select('ciudades.ciucod','ciudades.ciunom','ciudades.depcod','departamentos.depnom')
                     ->join('departamentos','ciudades.depcod','=','departamentos.depcod')
                     ->where('ciudades.depcod', $departamento)
                     ->orderby('ciudades.ciunom')                     
                     ->get();
                     
            if ($Ciudades->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Ciudades, new CiudadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    
}

?>