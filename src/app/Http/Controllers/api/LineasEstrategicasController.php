<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\LineasEstrategicasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LineasEstrategicas;
use DB;

class LineasEstrategicasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $LineasEstrategicas = LineasEstrategicas::select('lineas_estrategicas.linestcod','lineas_estrategicas.linestdesc',
                                                        'lineas_estrategicas.capcod','capitulos.capdes') 
                                                      ->join('capitulos','lineas_estrategicas.capcod','=','capitulos.capcod')   
                                                      ->orderby('lineas_estrategicas.linestdesc')                     
                                                      ->get();
                     
            if ($LineasEstrategicas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($LineasEstrategicas, new LineasEstrategicasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function FiltroXCapitulo(Request $request)
    {       
       try{

            $capitulo = $request->capitulo;

            $data = LineasEstrategicas::select('lineas_estrategicas.linestcod','lineas_estrategicas.linestdesc',
                                    'lineas_estrategicas.capcod','capitulos.capdes') 
                                ->join('capitulos','lineas_estrategicas.capcod','=','capitulos.capcod')   
                                ->where('lineas_estrategicas.capcod',$capitulo)
                                ->orderby('lineas_estrategicas.linestdesc')                     
                                ->get();
                     
            if ($data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($data, new LineasEstrategicasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
    
    public function consecutivo()
    {      

        $maxVal  = LineasEstrategicas::max('linestcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = LineasEstrategicas::select(DB::raw('count(linestcod) as total'))                      
                      ->where('linestdesc', $Descripcion)
                      ->where('linestcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Descripción ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function LineasEstrategicasAddUpdate(Request $request)
    {
        $LineasEstrategicas = $request->data;

        $codigo = $LineasEstrategicas['codigo'];
        $Descripcion = strtoupper(trim($LineasEstrategicas['descripcion'])); 
        $codigocapitulo = $LineasEstrategicas['codigocapitulo'];      

        $mensaje = $this->buscarDuplicado($Descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $LineasEstrategicasAdd = new LineasEstrategicas();
            
            $LineasEstrategicasAdd->linestcod = $consecutivo;           
            $LineasEstrategicasAdd->linestdesc = $Descripcion;
            $LineasEstrategicasAdd->capcod = $codigocapitulo;           
            
            $result = $LineasEstrategicasAdd->save();
        
        } else {

            $LineasEstrategicasUpd = LineasEstrategicas::where('linestcod', $codigo)->first();
          
            $LineasEstrategicasUpd->linestdesc = $Descripcion;          
            $LineasEstrategicasUpd->capcod = $codigocapitulo;

            // Guardamos en base de datos
            $result = $LineasEstrategicasUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = LineasEstrategicas::where('linestcod', $Id)->delete(); 
        return $result;

    }
}

?>