<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\UsersAlertasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UsersAlertas;
use DB;

class UsersAlertasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $UsersAlertas = UsersAlertas::select('users_configuracion.iduser','users_configuracion.useconfnsi','users_configuracion.useconfnco',
                                            'users.nombre')
                        ->join('users','users_configuracion.iduser','=','users.iduser')                        
                        ->orderby('users.nombre')                     
                        ->get();
                     
            if ($UsersAlertas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($UsersAlertas, new UsersAlertasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function UserAlertaXId(Request $request)
    {       
        $data = $request;

        $codigo = $data['codigo'];       

       try{

            $UsersAlertas = UsersAlertas::select('users_configuracion.iduser','users_configuracion.useconfnsi','users_configuracion.useconfnco',
                                            'users.nombre')
                        ->join('users','users_configuracion.iduser','=','users.iduser')  
                        ->where('users_configuracion.iduser',$codigo) 
                        ->get();
                     
            

                return $this->response->collection($UsersAlertas, new UsersAlertasTransformer);                
            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function UserAlertaXPersonal(Request $request)
    {       
        $data = $request;

        $codigo = $data['codigopersonal'];       

       try{

            $UsersAlertas = UsersAlertas::select('users_configuracion.iduser','users_configuracion.useconfnsi','users_configuracion.useconfnco',
                                            'users.nombre','users.email')
                        ->join('users','users_configuracion.iduser','=','users.iduser')
                        ->where('users.perscod',$codigo) 
                        ->get();

                return $this->response->collection($UsersAlertas, new UsersAlertasTransformer);                
            

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    private function buscarUsuario($UserId){

        try{

            $Data = UsersAlertas::select(DB::raw('count(iduser) as total'))                      
                      ->where('iduser', $UserId)                     
                      ->get();

            $total = 0;          
            if (!$Data->isEmpty()) {
                
                $total = $Data[0]->total;
                
            }        

            return $total;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;        

        $codigo = $data['codigo'];
        $issystem = $data['issystem'];
        $isemail = $data['isemail'];       

        $busqueda = $this->buscarUsuario($codigo);

        if($busqueda==0){

            $dataAdd = new UsersAlertas();
            
            $dataAdd->iduser       = $codigo;           
            $dataAdd->useconfnsi   = $issystem;
            $dataAdd->useconfnco   = $isemail;         

            $result = $dataAdd->save();
        
        } else {

            $dataUpd = UsersAlertas::where('iduser', $codigo)->first();
          
            $dataUpd->useconfnsi  = $issystem;
            $dataUpd->useconfnco  = $isemail;
           
            $result = $dataUpd->save();
           
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = UsersAlertas::where('iduser', $Id)->delete(); 
        return $result;

    }
}

?>