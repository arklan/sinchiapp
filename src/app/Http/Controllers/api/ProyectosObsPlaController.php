<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosObsPlaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosObsPla;
use DB;

class ProyectosObsPlaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosObsPla = ProyectosObsPla::select('proyobsplacod','proycod', 'proyobsplades')                        
                        ->orderby('proyobsplacod')                     
                        ->get();
                     
            if ($ProyectosObsPla->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosObsPla, new ProyectosObsPlaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = ProyectosObsPla::max('proyobsplacod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];

            $ProyectosObsPla = ProyectosObsPla::select('proyobsplacod','proycod', 'proyobsplades')                        
                        ->where('proycod', $proyecto)
                        ->orderby('proyobsplacod')                     
                        ->get();
                     
            if ($ProyectosObsPla->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosObsPla, new ProyectosObsPlaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;
        $proyecto = $request->proyecto;

        try{

            $result = ProyectosObsPla::where('proyobsplacod', $Id)->where('proycod', $proyecto)->delete();
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }    

    private function buscarCodigoProyecto($Proyecto, $Codigo){

        try{
           
            $ok = false;

            $Encontrado = ProyectosObsPla::select(DB::raw('count(proyobsplacod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('proyobsplacod',$Codigo)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    private function buscarDuplicado($Proyecto,$codigo, $descripcion){

        try{
           
            $mensaje = '';

            $Encontrado = ProyectosObsPla::select(DB::raw('count(proyobsplacod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('proyobsplades',$descripcion)
                      ->where('proyobsplacod','!=',$codigo)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $mensaje = 'La Observación ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $codigo = $data['codigo'];
        $proyecto = $data['proyecto'];
        $observacion = $data['observacion'];

        $mensaje = $this->buscarDuplicado($proyecto,$codigo,$observacion);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo == '0'){
            
            $consecutivo = $this->consecutivo();
            $dataSave = new ProyectosObsPla();

            $dataSave->proycod = $data['proyecto'];
            $dataSave->proyobsplacod = $consecutivo;
            $dataSave->proyobsplades = $data['observacion'];                    

            $result = $dataSave->save();

        } else {

            $query = "UPDATE proyectos_obsplaneacion SET proyobsplades = '".$data['observacion']."'   
                            WHERE proycod=".$data['proyecto']." AND proyobsplacod = ".$data['codigo'];
            $result = DB::update($query);

        }

        return array( 'respuesta' => $result);        
    }
}

?>