<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ComunidadesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comunidades;
use DB;

class ComunidadesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Comunidades = Comunidades::select('comunidades.comcod','comunidades.comnom','comunidades.asocod','asociaciones.asonom')
                        ->join('asociaciones','comunidades.asocod','=','asociaciones.asocod')     
                        ->orderby('comunidades.comnom')                     
                        ->get();
                     
            if ($Comunidades->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Comunidades, new ComunidadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
    
    public function consecutivo()
    {      

        $maxVal  = Comunidades::max('comcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Comunidades::select(DB::raw('count(comcod) as total'))                      
                      ->where('comnom', $Nombre)
                      ->where('comcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo           = $data['codigo'];
        $nombre           = strtoupper(trim($data['nombre']));
        $codigoasociacion = $data['codigoasociacion'];
       
        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new Comunidades();
            
            $dataAdd->comcod = $consecutivo;           
            $dataAdd->comnom = $nombre;
            $dataAdd->asocod = $codigoasociacion;           
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = Comunidades::where('comcod', $codigo)->first();
          
            $dataUpd->comnom = $nombre;
            $dataUpd->asocod = $codigoasociacion;
            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        } 

        return array( 'respucoma' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Comunidades::where('comcod', $Id)->delete(); 
        return $result;

    }
}

?>