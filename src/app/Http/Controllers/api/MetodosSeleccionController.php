<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\MetodosSeleccionTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MetodosSeleccion;
use DB;

class MetodosSeleccionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $MetodosSeleccion = MetodosSeleccion::select('metselcod','metseldes')
                     ->orderby('metseldes')                     
                     ->get();
                     
            if ($MetodosSeleccion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($MetodosSeleccion, new MetodosSeleccionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = MetodosSeleccion::max('metselcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($descripcion, $codigo){

        try{
           
            $mensaje = '';

            $Duplica = MetodosSeleccion::select(DB::raw('count(metselcod) as total'))                      
                      ->where('metseldes', $descripcion)
                      ->where('metselcod','!=', $codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripcion ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function MetodosSeleccionAddUpdate(Request $request)
    {
        $MetodosSeleccion = $request->data;

        $codigo = $MetodosSeleccion['codigo'];
        $descripcion = strtoupper(trim($MetodosSeleccion['descripcion']));       

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $MetodosSeleccionAdd = new MetodosSeleccion();
            
            $MetodosSeleccionAdd->metselcod = $consecutivo;           
            $MetodosSeleccionAdd->metseldes = $descripcion;           
            
            $result = $MetodosSeleccionAdd->save();
        
        } else {

            $MetodosSeleccionUpd = MetodosSeleccion::where('metselcod', $codigo)->first();
          
            $MetodosSeleccionUpd->metseldes = $descripcion;          

            // Guardamos en base de datos
            $result = $MetodosSeleccionUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = MetodosSeleccion::where('metselcod', $Id)->delete(); 
        return $result;

    }
}

?>