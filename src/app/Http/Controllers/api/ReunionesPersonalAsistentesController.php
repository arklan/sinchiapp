<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ReunionesPersonalAsistentesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ReunionesPersonalAsistentes;
use DB;

class ReunionesPersonalAsistentesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ReunionesPersonalAsistentes = ReunionesPersonalAsistentes::select('reupercodasi','perscod','reucod','reuperasi','reupersolapro') 
                     ->orderby('reupercodasi')                     
                     ->get();
                     
            if ($ReunionesPersonalAsistentes->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesPersonalAsistentes, new ReunionesPersonalAsistentesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function AsistentesxCodReunion(Request $request)
    {       
       $CodReunion = $request->id;

       try{

            $ReunionesPersonalAsistentes = ReunionesPersonalAsistentes::select('reuniones_personal_asistentes.reupercodasi','reuniones_personal_asistentes.perscod','reuniones_personal_asistentes.reucod','reuniones_personal_asistentes.reuperasi','reuniones_personal_asistentes.reupersolapro', 'personal.persape','personal.persnom') 
                     ->join('personal','personal.perscod','=','reuniones_personal_asistentes.perscod') 
                     ->where('reuniones_personal_asistentes.reucod',$CodReunion)
                     ->orderby('personal.persnom','personal.persape')                     
                     ->get();
                     
            if ($ReunionesPersonalAsistentes->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesPersonalAsistentes, new ReunionesPersonalAsistentesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function AsistentesAprobarxCodReunion(Request $request)
    {       
       $CodReunion = $request->id;

       try{

            $ReunionesPersonalAsistentes = ReunionesPersonalAsistentes::select('reuniones_personal_asistentes.reupercodasi','reuniones_personal_asistentes.perscod','reuniones_personal_asistentes.reucod','reuniones_personal_asistentes.reuperasi','reuniones_personal_asistentes.reupersolapro', 'personal.persape','personal.persnom') 
                     ->join('personal','personal.perscod','=','reuniones_personal_asistentes.perscod') 
                     ->where('reuniones_personal_asistentes.reucod',$CodReunion)
                     ->where('reuniones_personal_asistentes.reupersolapro',true)
                     ->orderby('personal.persnom','personal.persape')                     
                     ->get();
                     
            if ($ReunionesPersonalAsistentes->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesPersonalAsistentes, new ReunionesPersonalAsistentesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    
    public function consecutivo()
    {      

        $maxVal  = ReunionesPersonalAsistentes::max('reupercodasi');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReunionesPersonalAsistentesAdd(Request $request)
    {
        $ReunionesPersonalAsistentes = $request->data;

        $CodReunion = $ReunionesPersonalAsistentes['codigoreunion'];
        $asistentes =  $ReunionesPersonalAsistentes['asistentes'];        

        $result = $this->destroy($CodReunion);

        foreach ($asistentes as $row) {
            $consecutivo =$this->consecutivo();

            $ReunionAsistentesAdd = new ReunionesPersonalAsistentes();

            $ReunionAsistentesAdd->reupercodasi = $consecutivo;           
            $ReunionAsistentesAdd->reucod = $CodReunion;
            $ReunionAsistentesAdd->perscod = $row['codigo'];
            if($row['asistio'] == 1){
                $ReunionAsistentesAdd->reuperasi = true;
            }else{
                $ReunionAsistentesAdd->reuperasi = false;
            }
            if($row['aprobacion'] == 1){
                $ReunionAsistentesAdd->reupersolapro = true;
            }else{
                $ReunionAsistentesAdd->reupersolapro = false;
            }

            $result = $ReunionAsistentesAdd->save();
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function destroy($CodigoReunion)
    {

        $result = ReunionesPersonalAsistentes::where('reucod', $CodigoReunion)->delete(); 
        return $result;

    }
}

?>