<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ConveniosFecPreInfFinTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ConveniosFecPreInfFin;
use DB;

class ConveniosFecPreInfFinController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ConveniosFecPreInfFin = ConveniosFecPreInfFin::select('convcod','convfpif')                        
                        ->orderby('convfpif')                     
                        ->get();
                     
            if ($ConveniosFecPreInfFin->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ConveniosFecPreInfFin, new ConveniosFecPreInfFinTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function PorConvenio(Request $request)
    {
        try{
           
            $codigo = $request['codigo'];
                      
            $ConveniosFecPreInfFin = ConveniosFecPreInfFin::select('convcod','convfpif')                        
                        ->where('convcod', $codigo)                     
                        ->orderby('convfpif')                     
                        ->get();
                     
            if ($ConveniosFecPreInfFin->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ConveniosFecPreInfFin, new ConveniosFecPreInfFinTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }   
    
    public function destroy(Request $request)
    {
        $fecha = $request->fecha;
        $codigo = $request->codigo;
     
        try{

            $result = ConveniosFecPreInfFin::where('convcod', $codigo)
                                            ->where('convfpif', $fecha)
                                            ->delete(); 
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }
    
}

?>