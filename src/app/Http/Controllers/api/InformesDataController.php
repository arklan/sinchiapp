<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

class InformesDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function infoViews()
    {
        $vistas = DB::reconnect('mysql')->select(DB::raw("SELECT TABLE_NAME as vista FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='VIEW' AND TABLE_SCHEMA='bdsinchicont'"));
        return $vistas;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function infoCampos(Request $request)
    {
        $vista = $request->input('vista');
        $campos = DB::reconnect('mysql')->select(DB::raw("select COLUMN_NAME as campo, '' as op, '' as agrupado from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='$vista'"));
        return $campos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function launchConsulta(Request $request)
    {
        $consulta = $request->consulta;
        // return ['consulta' => $consulta];
        ini_set('memory_limit', '-1');
        $result = DB::reconnect('mysql')->select(DB::raw("$consulta"));
        return $result; 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
