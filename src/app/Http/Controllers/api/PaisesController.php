<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\PaisesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Paises;
use DB;

class PaisesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            
            $Paises = Paises::select('paiscod','paisnom','paisreg','paiscon')                     
                     ->orderby('paisnom')                     
                     ->get();
                     
            if ($Paises->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Paises, new PaisesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
    
    public function paisXFiltro(Request $request)
    {       
       try{

            $tipofiltro  = $request->tipofiltro;
            $valorfiltro = $request->valorfiltro;
            
            if($tipofiltro == 'C'){

                $Paises = Paises::select('paiscod','paisnom','paisreg','paiscon')                     
                                ->where('paiscon','like','%'.$valorfiltro.'%')
                                ->orderby('paisnom')                     
                                ->get();

            } 

            else if($tipofiltro == 'R'){

                $Paises = Paises::select('paiscod','paisnom','paisreg','paiscon')                     
                                ->where('paisreg','like','%'.$valorfiltro.'%')
                                ->orderby('paisnom')                     
                                ->get();

            }
                     
            if ($Paises->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Paises, new PaisesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
    
}

?>