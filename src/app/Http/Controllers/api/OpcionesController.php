<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\OpcionesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Opciones;
use DB;

class OpcionesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }    

    public function buscarOpcion(Request $request)
    {

        $usuarioid = $request->usuarioid;
        $codigo = $request->codigo;  
       
        try{

            $opciones = Opciones::select('opciones.opccod','opciones.opccodpad','opciones.opcnom','opciones.opcico','opciones.opclin')   
                        ->join('opciones_perfiles', 'opciones.opccod', '=', 'opciones_perfiles.opccod')  
                        ->join('users', 'opciones_perfiles.perfcod', '=', 'users.perfcod')            
                        ->where('users.iduser',$usuarioid)
                        ->where('opciones.opccod',$codigo)                       
                        ->get();

            $padre = $this->response->collection($opciones, new OpcionesTransformer);
            
            return $padre;            

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function Permisos(Request $request)
    {

        $usuarioid = $request->usuarioid; 
       
        try{

            $opciones = Opciones::select('opciones.opccod','opciones.opccodpad','opciones.opcnom','opciones.opcico','opciones.opclin')   
                        ->join('opciones_perfiles', 'opciones.opccod', '=', 'opciones_perfiles.opccod')  
                        ->join('users', 'opciones_perfiles.perfcod', '=', 'users.perfcod')            
                        ->where('users.iduser',$usuarioid)
                        ->where('opciones.opccodpad',0)
                        ->where('opciones.opcesmod',1) 
                        ->orderBy('opciones.opcord')
                        ->orderBy('opciones.opccodpad')
                        ->get();

            //$padre = $this->response->collection($opciones, new OpcionesTransformer);
            $opcionesF = [];

            $anterior = 0;
            $i=0;
            foreach($opciones as $valor){

                $codigo = $valor['opccod'];
                $submenu = [];
                $submenu = $this->SubMenu($usuarioid,$codigo);
                  
                if(count($submenu) > 0){

                    $ItemsAdd = array('menu' => $valor , 'submenu' => $submenu);
                    
                    array_push($opcionesF,$ItemsAdd);

                }else{
                    
                    $ItemsAdd = array('menu' => $valor);
                    array_push($opcionesF,$ItemsAdd);
                
                }

                $i++;

            }    

            return $opcionesF;            

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    private function SubMenu($usuarioid,$OpcCod)
    {
       
        try{

            $submenu = Opciones::select('opciones.opccod','opciones.opccodpad','opciones.opcnom','opciones.opcico','opciones.opclin')   
                        ->join('opciones_perfiles', 'opciones.opccod', '=', 'opciones_perfiles.opccod')
                        ->join('users', 'opciones_perfiles.perfcod', '=', 'users.perfcod')              
                        ->where('users.iduser',$usuarioid)
                        ->where('opciones.opccodpad',$OpcCod)
                        ->orderBy('opciones.opcord')
                        ->orderBy('opciones.opccodpad')
                        ->get();
           
            return $submenu;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function PermisosActuales(Request $request)
    {

        $perfilid = $request->perfilid; 
       
        try{

            $opciones = Opciones::select('opciones.opccod','opciones.opccodpad','opciones.opcnom','opciones.opcico','opciones.opclin')   
                        ->join('opciones_perfiles', 'opciones.opccod', '=', 'opciones_perfiles.opccod')
                        ->where('opciones_perfiles.perfcod',$perfilid)                        
                        ->orderBy('opciones.opcord')
                        ->orderBy('opciones.opccodpad')
                        ->get();

            if (!$opciones->isEmpty()) {                

                return $this->response->collection($opciones, new OpcionesTransformer);
            }      

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function PermisosFaltantes(Request $request)
    {

        $perfilid = $request->perfilid;
               
        try{

            $opciones = Opciones::select('opciones.opccod','opciones.opcnom','opciones.opcico','opciones.opclin')                        
                        ->leftJoin('opciones_perfiles', function($join) use ($perfilid)
                        { 
                            $join->on('opciones.opccod', '=', 'opciones_perfiles.opccod'); 
                            $join->on('opciones_perfiles.perfcod','=',DB::raw("'".$perfilid."'"));
                        }) 
                        ->where('opciones_perfiles.opccod', '=', NULL) 
                        ->get();

            if (!$opciones->isEmpty()) {                

                return $this->response->collection($opciones, new OpcionesTransformer);
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}