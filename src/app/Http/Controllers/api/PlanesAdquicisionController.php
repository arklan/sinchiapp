<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\PlanesAdquisicionTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PlanesAdquisicion;
use App\PlaAdqNecesidadesUnspsc;

use App\PlanesAdquisicionNecesidades;
use App\Transformers\PlanesAdquisicionNecesidadesTransformer;
use App\Transformers\PlaAdqNecesidadesUnspscTransformer;
use App\PlaAdqNecActividades;
use App\Transformers\PlaAdqNecActividadesTransformer;

use App\PresupuestosCatGas;
use App\Transformers\PresupuestosCatGasTransformer;

use App\Presupuestos;
use App\Transformers\PresupuestosTransformer;

use App\TmpPlanesAdquisicion;
use App\TmpPlanesAdquisicionNec;
use App\TmpPlaAdqNecActividades;
use App\TmpPlaAdqNecesidadesUnspsc;

use App\Transformers\TmpPlanesAdquisicionTransformer;
use App\Transformers\TmpPlanesAdquisicionNecTransformer;
use App\Transformers\TmpPlaAdqNecesidadesUnspscTransformer;
use App\Transformers\TmpPlaAdqNecActividadesTransformer;

use App\HisPlanesAdquisicion;
use App\HisPlanesAdquisicionNec;
use App\HisPlaAdqNecActividades;
use App\HisPlaAdqNecesidadesUnspsc;

use App\Transformers\HisPlanesAdquisicionTransformer;
use App\Transformers\HisPlanesAdquisicionNecTransformer;
use App\Transformers\HisPlaAdqNecesidadesUnspscTransformer;
use App\Transformers\HisPlaAdqNecActividadesTransformer;

use DB;

class PlanesAdquicisionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $PlanesAdquisicion = PlanesAdquisicion::select('planes_adquisicion.plaadqcod',
                                    'planes_adquisicion.plaadqfec','planes_adquisicion.proycod',
                                    'planes_adquisicion.estjurcod','planes_adquisicion.plaadqacj',
                                    'planes_adquisicion.estplacod','planes_adquisicion.plaadqevb',
                                    'proyectos.proynomcor','convenios.convcod','convenios.convnum',
                                    'convenios.convnom','convenios.convvaltot','monedas.monsim',
                                    'convenios.perscod','planes_adquisicion.plaadqbasmon',
                                    'convenios.convtas','planes_adquisicion.plaadqver',
                                    'planes_adquisicion.plaadqultfec')
                     ->join('proyectos','planes_adquisicion.proycod','=','proyectos.proycod')
                     ->join('convenios','proyectos.convcod','=','convenios.convcod')
                     //->join('estados as estjur','planes_adquisicion.estjurcod','=','estjur.estcod')
                     //->join('estados as estpla','planes_adquisicion.estplacod','=','estpla.estcod')                     
                     ->join('monedas','convenios.moncod','=','monedas.moncod')                    
                     ->orderby('planes_adquisicion.plaadqfec')    
                     //->limit(20)                  
                     ->get();
                     
            if ($PlanesAdquisicion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($PlanesAdquisicion, new PlanesAdquisicionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function filtroXConvenio(Request $request)
    {       
       try{

            $convenio = $request->convenio;

            $PlanesAdquisicion = PlanesAdquisicion::select('planes_adquisicion.plaadqcod',
                                            'planes_adquisicion.plaadqfec','planes_adquisicion.proycod',
                                            'planes_adquisicion.estjurcod','planes_adquisicion.plaadqacj',
                                            'planes_adquisicion.estplacod','planes_adquisicion.	plaadqacp',
                                            'planes_adquisicion.plaadqevb','planes_adquisicion.plaadqbasmon',
                                            'proyectos.proynomcor','convenios.convcod','convenios.convnum',
                                            'convenios.convnom','convenios.convvaltot','monedas.monsim',                                            
                                            'convenios.perscod','convenios.convtas','planes_adquisicion.plaadqver',
                                            'planes_adquisicion.plaadqultfec')
                                ->join('proyectos','planes_adquisicion.proycod','=','proyectos.proycod')
                                ->join('convenios','proyectos.convcod','=','convenios.convcod')
                                //->join('estados as estjur','planes_adquisicion.estjurcod','=','estjur.estcod')
                                //->join('estados as estpla','planes_adquisicion.estplacod','=','estpla.estcod')                     
                                ->join('monedas','convenios.moncod','=','monedas.moncod')
                                ->where('convenios.convcod',$convenio)
                                ->orderby('convenios.convcod')                                         
                                ->get();
                     
            if ($PlanesAdquisicion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($PlanesAdquisicion, new PlanesAdquisicionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function filtroXProyecto(Request $request)
    {       
       try{

            $proyecto = $request->proyecto;

            $PlanesAdquisicion = PlanesAdquisicion::select('planes_adquisicion.plaadqcod',
                                            'planes_adquisicion.plaadqfec','planes_adquisicion.proycod',
                                            'planes_adquisicion.estjurcod','planes_adquisicion.plaadqacj',
                                            'planes_adquisicion.estplacod',
                                            'planes_adquisicion.plaadqevb','planes_adquisicion.plaadqbasmon',
                                            'proyectos.proynomcor','convenios.convcod','convenios.convnum',
                                            'convenios.convnom','convenios.convvaltot','monedas.monsim',                                            
                                            'convenios.perscod','convenios.convtas','planes_adquisicion.plaadqver',
                                            'planes_adquisicion.plaadqultfec')
                                ->join('proyectos','planes_adquisicion.proycod','=','proyectos.proycod')
                                ->join('convenios','proyectos.convcod','=','convenios.convcod')                                                 
                                ->join('monedas','convenios.moncod','=','monedas.moncod')
                                ->where('planes_adquisicion.proycod',$proyecto)
                                ->orderby('convenios.convnom')                                         
                                ->get();
                     
            if ($PlanesAdquisicion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($PlanesAdquisicion, new PlanesAdquisicionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function NecesidadesXPlanAdq(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;

            $Data = PlanesAdquisicionNecesidades::select('planesadquisiciones_necesidades.plaadqcod',
                                                 'planesadquisiciones_necesidades.plaadqneccod',
                                                 'planesadquisiciones_necesidades.plaadqnecdes',
                                                 'planesadquisiciones_necesidades.plaadqnecval',
                                                 'planesadquisiciones_necesidades.metselcod',
                                                 'planesadquisiciones_necesidades.plaadqnecfeccont',
                                                 'planesadquisiciones_necesidades.estcod',
                                                 'planesadquisiciones_necesidades.plaadqnecvbf',
                                                 'planesadquisiciones_necesidades.plaadqnecobf',
                                                 'planesadquisiciones_necesidades.plaadqnecvbj',
                                                 'planesadquisiciones_necesidades.plaadqnecobj',
                                                 'planesadquisiciones_necesidades.plaadqnecvbp',
                                                 'planesadquisiciones_necesidades.plaadqnecobp',                                                 
                                                 'metodos_seleccion.metseldes','estados.estdes',
                                                 'planes_adquisicion.plaadqevb')
                     ->join('planes_adquisicion','planesadquisiciones_necesidades.plaadqcod','=','planes_adquisicion.plaadqcod')                                 
                     ->join('metodos_seleccion','planesadquisiciones_necesidades.metselcod','=','metodos_seleccion.metselcod')                     
                     ->join('estados','planesadquisiciones_necesidades.estcod','=','estados.estcod')
                     ->where('planesadquisiciones_necesidades.plaadqcod',$planadquisicion) 
                     ->orderby('planesadquisiciones_necesidades.plaadqneccod')
                     ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new PlanesAdquisicionNecesidadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function NecesidadesXSolicitud(Request $request) {
        $idSolicitud = $request->idSolicitud;
        $Necesidades = DB::reconnect('mysql')->select(DB::raw("SELECT '' as idspn, sn.id as idsn, sn.cod_plan as cod_plan, sn.cod_necesidad as cod_necesidad, pn.plaadqnecdes as descripcion, pn.plaadqnecval as valor, '' as solval FROM solicitudes_necesidades sn INNER JOIN planesadquisiciones_necesidades pn ON sn.cod_plan = pn.plaadqcod AND sn.cod_necesidad = pn.plaadqneccod WHERE sn.id_solcontratacion = ".$idSolicitud));
        return $Necesidades;
    }
    public function solProcNecesidades(Request $request) {
        $idSolProc = $request->idSolProc;
        $Necesidades = DB::reconnect('mysql')->select(DB::raw('SELECT spn.id as idspn, sn.id as idsn, sn.cod_plan, sn.cod_necesidad, sn.descripcion, pn.plaadqnecval as valor, spn.valor as solval FROM solproc_necesidades spn INNER JOIN solicitudes_necesidades sn ON sn.id = spn.id_sol_necesidades INNER JOIN planesadquisiciones_necesidades pn ON pn.plaadqneccod = sn.cod_necesidad AND pn.plaadqcod = sn.cod_plan WHERE spn.id_solproc = '.$idSolProc));
        return $Necesidades;
    }

    public function UnspscXPlanAdq(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;            

            $Data = PlaAdqNecesidadesUnspsc::select('planesadquisiciones_necesidades_unspsc.plaadqcod',
                                                 'planesadquisiciones_necesidades_unspsc.plaadqneccod',
                                                 'planesadquisiciones_necesidades_unspsc.unspsccod',
                                                 'unspsc.unspscdes')
                     ->join('unspsc','planesadquisiciones_necesidades_unspsc.unspsccod','=','unspsc.unspsccod')                     
                     ->where('planesadquisiciones_necesidades_unspsc.plaadqcod',$planadquisicion)                     
                     ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new PlaAdqNecesidadesUnspscTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function UnspscXNecesidad(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;
            $necesidad = $request->necesidad;

            $Data = PlaAdqNecesidadesUnspsc::select('planesadquisiciones_necesidades_unspsc.plaadqcod',
                                                 'planesadquisiciones_necesidades_unspsc.plaadqneccod',
                                                 'planesadquisiciones_necesidades_unspsc.unspsccod',
                                                 'unspsc.unspscdes')
                     ->join('unspsc','planesadquisiciones_necesidades_unspsc.unspsccod','=','unspsc.unspsccod')                     
                     ->where('planesadquisiciones_necesidades_unspsc.plaadqcod',$planadquisicion)
                     ->where('planesadquisiciones_necesidades_unspsc.plaadqneccod',$necesidad) 
                     ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new PlaAdqNecesidadesUnspscTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function ActividadXPlanAdq(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;
            
            $sql = "select pac.plaadqcod planadquisicion,pac.plaadqneccod necesidad,
                            pac.proyactsec secuenciaactividad,pac.prescatgascod categoriagasto,
                            pac.plaadqnecacttag topecategoriagasto,pac.plaadqnecactval valor,
                            pya.proyactdes actividadtxtresumen,pcg.prescatgasdes categoriagastotxt,
                            CONCAT(pya.proyobjcod,'.',pya.prodcod,'.',pya.proyactcod,' ',pya.proyactdes) actividadtxt,
                            pre.presfuefinpgn pgn,pre.presfuefinsgr sgr,pre.presfuefincof cofinanciado,
                            pre.presfuefinfun funcionamiento,pac.plaadqnecactrpr rubro,
                            CONCAT(pya.proyobjcod,pya.prodcod,pya.proyactcod) codigofull,
                            pac.prescod presupuesto  
                        from planesadquisiciones_necesidades_actividades pac 
                        inner join planes_adquisicion pla on pac.plaadqcod = pla.plaadqcod 
                        inner join proyectos pry on pla.proycod = pry.proycod 
                        inner join presupuestos pre on pry.convcod = pre.convcod 
                        inner join presupuestos_categorias_gasto pcg on pre.prescod = pcg.prescod 
                                AND pac.prescatgascod = pcg.prescatgascod 
                        inner join proyectos_actividades pya on pac.proyactsec = pya.proyactsec 
                        where pac.plaadqcod = ".$planadquisicion;
            $result =DB::select($sql);
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function ActividadXNecesidad(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;
            $necesidad = $request->necesidad;

            $sql = "select pac.plaadqcod planadquisicion,pac.plaadqneccod necesidad,
                            pac.proyactsec secuenciaactividad,pac.prescatgascod categoriagasto,
                            pac.plaadqnecacttag topecategoriagasto,pac.plaadqnecactval valor,
                            pya.proyactdes actividadtxtresumen,pcg.prescatgasdes categoriagastotxt,
                            CONCAT(pya.proyobjcod,'.',pya.prodcod,'.',pya.proyactcod,' ',pya.proyactdes) actividadtxt,
                            pre.presfuefinpgn pgn,pre.presfuefinsgr sgr,pre.presfuefincof cofinanciado,
                            pre.presfuefinfun funcionamiento,pac.plaadqnecactrpr rubro,
                            CONCAT(pya.proyobjcod,pya.prodcod,pya.proyactcod) codigofull,
                            pac.prescod presupuesto  
                        from planesadquisiciones_necesidades_actividades pac 
                        inner join planes_adquisicion pla on pac.plaadqcod = pla.plaadqcod 
                        inner join proyectos pry on pla.proycod = pry.proycod 
                        inner join presupuestos pre on pry.convcod = pre.convcod 
                        inner join presupuestos_categorias_gasto pcg on pre.prescod = pcg.prescod 
                                AND pac.prescatgascod = pcg.prescatgascod 
                        inner join proyectos_actividades pya on pac.proyactsec = pya.proyactsec 
                        where pac.plaadqcod = ".$planadquisicion." AND pac.plaadqneccod = ".$necesidad;
            $result =DB::select($sql);
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function ActividadXPlaAdqCatGasto(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;
            $categoria = $request->categoria;

            $sql = "select pac.plaadqcod planadquisicion,pac.plaadqneccod necesidad,
                            pac.proyactsec secuenciaactividad,pac.prescatgascod categoriagasto,
                            pac.plaadqnecacttag topecategoriagasto,pac.plaadqnecactval valor,
                            pya.proyactdes actividadtxtresumen,pcg.prescatgasdes categoriagastotxt,
                            CONCAT(pya.proyobjcod,'.',pya.prodcod,'.',pya.proyactcod,' ',pya.proyactdes) actividadtxt,
                            pre.prestiptopcatgas tipotope,pac.plaadqnecactrpr rubro,
                            pre.presfuefinpgn pgn,pre.presfuefinsgr sgr,pre.presfuefincof cofinanciado,
                            pre.presfuefinfun funcionamiento,
                            CONCAT(pya.proyobjcod,pya.prodcod,pya.proyactcod) codigofull  
                        from planesadquisiciones_necesidades_actividades pac 
                        inner join planes_adquisicion pla on pac.plaadqcod = pla.plaadqcod 
                        inner join proyectos pry on pla.proycod = pry.proycod 
                        inner join presupuestos pre on pry.convcod = pre.convcod 
                        inner join presupuestos_categorias_gasto pcg on pre.prescod = pcg.prescod 
                                AND pac.prescatgascod = pcg.prescatgascod 
                        inner join proyectos_actividades pya on pac.proyactsec = pya.proyactsec 
                        where pac.plaadqcod = ".$planadquisicion." AND pac.prescatgascod = ".$categoria;                   
            
            $result =DB::select($sql);
            
            return $result;

        } catch (Exception $e) {

            return $e->getMessage();

        }     
    }
    


    public function consecutivo()
    {      

        $maxVal  = PlanesAdquisicion::max('plaadqcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    public function consecutivoNecesidades($codigo)
    {      

        $maxVal  = PlanesAdquisicionNecesidades::where('plaadqcod', $codigo)->max('plaadqneccod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($proyecto, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = PlanesAdquisicion::select(DB::raw('count(proycod) as total'))                      
                      ->where('proycod', $proyecto)
                      ->where('plaadqcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Proyecto ya existe para otro Plan de Adquisición';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DataAddUpdate(Request $request)
    {
        date_default_timezone_set('America/Bogota');

        $data = $request->data;
        $necesidades = $request->necesidades;
       
        $codigo               = $data['codigo'];
        $fecha                = date('Y\-m\-d');
        $proyecto             = $data['proyecto'];
        $estadojurudico       = $data['estadojurudico']; 
        $aclaracionjurudica   = $data['aclaracionjurudica'];
        $estadoplaneacion     = $data['estadoplaneacion'];   
        $aclaracionplaneacion = $data['aclaracionplaneacion'];
        $basemoneda = $data['basemoneda'];      
        
        $mensaje = $this->buscarDuplicado($proyecto, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();
           
            $DataAdd = new PlanesAdquisicion();
            
            $DataAdd->plaadqcod    = $consecutivo; 
            $DataAdd->plaadqfec    = $fecha;
            $DataAdd->plaadqultfec = $fecha;          
            $DataAdd->proycod      = $proyecto; 
            $DataAdd->estjurcod    = $estadojurudico; 
            $DataAdd->plaadqacj    = $aclaracionjurudica;
            $DataAdd->estplacod    = $estadoplaneacion;
            $DataAdd->plaadqacp    = $aclaracionplaneacion; 
            $DataAdd->plaadqbasmon = $basemoneda;
            $DataAdd->plaadqver    = 1;         
            
            $result = $DataAdd->save();
        
        } else {

            $DataUpd = PlanesAdquisicion::where('plaadqcod', $codigo)->first();
          
            $DataUpd->proycod      = $proyecto; 
            $DataUpd->estjurcod    = $estadojurudico;
            $DataUpd->plaadqacj    = $aclaracionjurudica;
            $DataUpd->estplacod    = $estadoplaneacion;
            $DataUpd->plaadqacp    = $aclaracionplaneacion;                    
            $DataUpd->plaadqbasmon = $basemoneda;

            // Guardamos en base de datos
            $result = $DataUpd->save();           
        } 

        $codigoactual = 0;

        if($codigo=='0'){

            $codigoactual = $consecutivo;                    

        } else {

            $codigoactual = $codigo;

        }

        //Necesidades
        $consecutivoitem = 0; 
        if(count($necesidades) > 0){

            $arrDataActual = [];

            for ($i=0; $i < count($necesidades); $i++) {                

                $itemactual = $necesidades[$i];

                if($codigo!='0'){

                    $busqueda = PlanesAdquisicionNecesidades::where('plaadqcod', $codigo)
                                            ->where('plaadqnecdes',$itemactual['descripcion'])
                                            ->get();

                    if ($busqueda->isEmpty()) {                        
                        
                        if($consecutivoitem == 0){
                            $consecutivoitem = $this->consecutivoNecesidades($codigo);  

                        } else {

                            $consecutivoitem += 1;

                        }   

                        $itemAdd = new PlanesAdquisicionNecesidades();
            
                        $itemAdd->plaadqcod        = $codigo; 
                        $itemAdd->plaadqneccod     = $consecutivoitem;          
                        $itemAdd->plaadqnecdes     = $itemactual['descripcion']; 
                        $itemAdd->plaadqnecval     = $itemactual['valor']; 
                        $itemAdd->metselcod        = $itemactual['metodoleccion'];
                        $itemAdd->plaadqnecfeccont = $itemactual['fechasolcontrata'];
                        $itemAdd->estcod           = $itemactual['estadocodigo'];
                       
                        $result = $itemAdd->save();        

                    }  else {
                        
                        $sql = "UPDATE planesadquisiciones_necesidades 
                                    SET plaadqnecval = '".$itemactual['valor']."',metselcod = '".$itemactual['metodoleccion']."',
                                        plaadqnecfeccont = '".$itemactual['fechasolcontrata']."',
                                        estcod = '".$itemactual['estadocodigo']."' 
                                    WHERE plaadqcod = ".$codigo." AND plaadqnecdes = '".$itemactual['descripcion']."'";
                        
                        $result =DB::update($sql);
                        
                        //se busca si se envio para vobo y se actualiza el estado
                        $EnvVoBo = PlanesAdquisicion::where('plaadqcod', $codigo)->where('plaadqevb', 'S')->get();

                        if (!$EnvVoBo->isEmpty()){

                            if($itemactual['modificada'] == 'S'){
                                $sql = "UPDATE planesadquisiciones_necesidades 
                                            SET estcod = 19 
                                            WHERE plaadqcod = ".$codigo." AND plaadqnecdes = '".$itemactual['descripcion']."'";
                                
                                $result =DB::update($sql);
                            }    
                        }    

                    }  
    
                } else {

                    if($consecutivoitem == 0){
                        $consecutivoitem = $this->consecutivoNecesidades($codigo);  

                    } else {

                        $consecutivoitem += 1;

                    }   

                    $itemAdd = new PlanesAdquisicionNecesidades();
        
                    $itemAdd->plaadqcod        = $codigoactual; 
                    $itemAdd->plaadqneccod     = $consecutivoitem;          
                    $itemAdd->plaadqnecdes     = $itemactual['descripcion']; 
                    $itemAdd->plaadqnecval     = $itemactual['valor']; 
                    $itemAdd->metselcod        = $itemactual['metodoleccion'];
                    $itemAdd->plaadqnecfeccont = $itemactual['fechasolcontrata'];
                    $itemAdd->estcod           = $itemactual['estadocodigo'];
                    
                    $result = $itemAdd->save(); 
                }                                               
            }            
        }

        return array( 'respuesta' => $result);     
    }

    private function BuscarCumpleEstadoAprobacion($planadquisicion, $necesidad){
        
        $ok = 0;

        $Data = PlanesAdquisicionNecesidades::select(DB::raw('count(plaadqcod) as total'))                      
                    ->where('plaadqnecvbf', 'S')
                    ->where('plaadqnecvbp', 'S')
                    ->where('plaadqnecvbj', 'S')
                    ->get();

        $total = 0;          
        if (!$Data->isEmpty()) {
            
            $total = $Data[0]->total;

            if($total != '0'){

                $ok = 1;

            }
        }           

        return $ok;

    }

    public function ActualizarVoBoNecesidad(Request $request){

        $planadquisicion = $request->planadquisicion;
        $codigo = $request->codigo;
        $tipo = $request->tipo;
        $vobo = $request->vobo;
        $observacion = $request->observacion;

        $sql = '';
        $result = 0;

        if($tipo == 'F'){
            $sql = "UPDATE planesadquisiciones_necesidades 
                    SET plaadqnecvbf = '".$vobo."',plaadqnecobf = '".$observacion."' ";

        } elseif ($tipo == 'J') {
           
            $sql = "UPDATE planesadquisiciones_necesidades 
                    SET plaadqnecvbj = '".$vobo."',plaadqnecobj = '".$observacion."' ";

        } elseif ($tipo == 'P') {
           
            $sql = "UPDATE planesadquisiciones_necesidades 
                    SET plaadqnecvbp = '".$vobo."',plaadqnecobp = '".$observacion."' ";

        }

        if($sql != ''){

            $sql .= " WHERE plaadqcod = ".$planadquisicion." AND plaadqneccod = ".$codigo;
            $result =DB::update($sql);

            if($vobo == 'N'){

                //se modifica el estado
                $sql = "UPDATE planesadquisiciones_necesidades SET estcod = 18 WHERE plaadqcod = ".$planadquisicion." AND plaadqneccod = ".$codigo;
                $result =DB::update($sql);

                $sql = "UPDATE planes_adquisicion SET plaadqevb = 'N' WHERE plaadqcod = ".$planadquisicion;
                $result =DB::update($sql);

            } else {

                //se deben verificar que todos los VoBo's esten en si, para quedar aprobada 
                $ok = $this->BuscarCumpleEstadoAprobacion($planadquisicion, $codigo);

                if($ok == 1) {
                    //cumplio todos los vobo's queda en estado de aprobación
                    $sql = "UPDATE planesadquisiciones_necesidades SET estcod = 17 WHERE plaadqcod = ".$planadquisicion." AND plaadqneccod = ".$codigo;
                    $result =DB::update($sql);
                }
            }
        }        

        return  $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /****************************** UNSPSC ******************************************
    //*******************************************************************************/ 

    private function buscarDuplicadoUnspsc($planadquisicion, $necesidad, $unspsc){

        try{
           
            $mensaje = '';

            $Duplica = PlaAdqNecesidadesUnspsc::select(DB::raw('count(unspsccod) as total'))                      
                      ->where('plaadqcod', $planadquisicion)
                      ->where('plaadqneccod', $necesidad)
                      ->where('unspsccod', $unspsc)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Código UNSPSC ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    public function DataUnspscAddUpdate(Request $request)
    { 
        $data = $request->data;
        
        $result = 0;

        if(count($data) > 0){

            $arrDataActual = [];

            for ($i=0; $i < count($data); $i++) {                

                $itemactual = $data[$i];

                $planadquisicion = $itemactual['planadquisicion'];       
                $necesidad       = $itemactual['necesidad'];
                $unspsc          = $itemactual['codigo'];
                        
                $mensaje = $this->buscarDuplicadoUnspsc($planadquisicion,$necesidad,$unspsc);

                if($mensaje == ''){

                    $dataSave = new PlaAdqNecesidadesUnspsc();

                    $dataSave->plaadqcod    = $planadquisicion;
                    $dataSave->plaadqneccod = $necesidad;
                    $dataSave->unspsccod    = $unspsc;
                    
                    $result = $dataSave->save();
                    
                }                                                                        
            }
        }

        return array( 'respuesta' => $result);
        /*$planadquisicion = $data['planadquisicion'];       
        $necesidad       = $data['necesidad'];
        $unspsc          = $data['unspsc'];
                
        $mensaje = $this->buscarDuplicadoUnspsc($planadquisicion,$necesidad,$unspsc);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        $dataSave = new PlaAdqNecesidadesUnspsc();

        $dataSave->plaadqcod    = $planadquisicion;
        $dataSave->plaadqneccod = $necesidad;
        $dataSave->unspsccod    = $unspsc;
        
        $result = $dataSave->save();

        return array( 'respuesta' => $result);     */
    }

    public function DelUnspsc(Request $request)
    {
        $planadquisicion = $request->planadquisicion;
        $necesidad = $request->necesidad;
        $unspsc = $request->unspsc;

        $result = PlaAdqNecesidadesUnspsc::where('plaadqcod', $planadquisicion)
                    ->where('plaadqneccod', $necesidad)
                    ->where('unspsccod', $unspsc)
                    ->delete(); 
        return $result;

    }

     /****************************** ACTIVIDADES  ******************************************
    //*************************************************************************************/ 

    private function buscarDuplicadoActividad($planadquisicion, $necesidad, $actividad){

        try{
           
            $mensaje = '';

            $Duplica = PlaAdqNecActividades::select(DB::raw('count(proyactsec) as total'))                      
                      ->where('plaadqcod', $planadquisicion)
                      ->where('plaadqneccod', $necesidad)
                      ->where('proyactsec', $actividad)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La Actividad ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    public function DataActividadAddUpdate(Request $request)
    { 
        $data = $request->data;
        
        $planadquisicion    = $data['planadquisicion'];       
        $necesidad          = $data['necesidad'];
        $secuenciaactividad = $data['secuenciaactividad'];
        $categoriagasto     = $data['categoriagasto'];
        $topecategoriagasto = $data['topecategoriagasto'];
        $valor              = $data['valor'];
        $rubro              = $data['rubro'];
        $presupuesto        = $data['presupuesto'];
                
        $mensaje = $this->buscarDuplicadoActividad($planadquisicion,$necesidad,$secuenciaactividad);

        if($mensaje != ''){

            $sql = "UPDATE planesadquisiciones_necesidades_actividades 
                        SET prescatgascod = '".$categoriagasto."',plaadqnecacttag = '".$topecategoriagasto."',
                            plaadqnecactval = '".$valor."',plaadqnecactrpr = '".$rubro."',
                            prescod = ".$presupuesto."   
                        WHERE plaadqcod = ".$planadquisicion." AND plaadqneccod = ".$necesidad." 
                              AND proyactsec = ".$secuenciaactividad;
            
            $result =DB::update($sql);

        } else {

            $dataSave = new PlaAdqNecActividades();

            $dataSave->plaadqcod       = $planadquisicion;
            $dataSave->plaadqneccod    = $necesidad;
            $dataSave->proyactsec      = $secuenciaactividad;
            $dataSave->prescatgascod   = $categoriagasto;
            $dataSave->plaadqnecacttag = $topecategoriagasto;
            $dataSave->plaadqnecactval = $valor;
            $dataSave->plaadqnecactrpr = $rubro;
            $dataSave->prescod         = $presupuesto;

            $result = $dataSave->save();
        }

        return array( 'respuesta' => $result);     
    }

    public function DelActividad(Request $request)
    {
        $planadquisicion = $request->planadquisicion;
        $necesidad = $request->necesidad;
        $secuenciaactividad = $request->secuenciaactividad;

        $result = PlaAdqNecActividades::where('plaadqcod', $planadquisicion)
                    ->where('plaadqneccod', $necesidad)
                    ->where('proyactsec', $secuenciaactividad)
                    ->delete(); 
        return $result;

    }

    public function CatGasXConvenio(Request $request)
    {       
       try{

            $convenio = $request->convenio;

            $Presupuestos = PresupuestosCatGas::select('presupuestos_categorias_gasto.prescod',
                                                 'presupuestos_categorias_gasto.prescatgascod',
                                                 'presupuestos_categorias_gasto.prescatgasdes',
                                                 'presupuestos_categorias_gasto.prescatgastma',
                                                 'presupuestos.prestiptopcatgas')                     
                     ->join('presupuestos','presupuestos_categorias_gasto.prescod','=','presupuestos.prescod')
                     ->where('presupuestos.convcod',$convenio) 
                     ->get();
                     
            if ($Presupuestos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Presupuestos, new PresupuestosCatGasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function enviarParaVoBo(Request $request){

        try{

            $codigo = $request->codigo;

            $DataUpd = PlanesAdquisicion::where('plaadqcod', $codigo)->first();
          
            $DataUpd->plaadqevb  = 'S'; 
           
            // Guardamos en base de datos
            $result = $DataUpd->save();

            //se modifica el estado
            $sql = "UPDATE planesadquisiciones_necesidades SET estcod = 16 WHERE plaadqcod = ".$codigo;
            $result =DB::update($sql);

            return array( 'respuesta' => $result);

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /************************************* VALIDACIONES PARA ENVIO VoBo ****************************
    ************************************************************************************************/
    
    //Necesidades
    public function ValidaNecesidades(Request $request)
    {

        try{

            $codigo = $request->codigo;

            $sql = "SELECT count(*) total,sum(plaadqnecval) totalvalor 
                    FROM planesadquisiciones_necesidades WHERE plaadqcod = ".$codigo;
                     
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }
    
    //Unspsc
    public function ValidaUnspsc(Request $request)
    {

        try{

            $codigo = $request->codigo;

            $sql = "SELECT count(*) total 
                    FROM planesadquisiciones_necesidades_unspsc WHERE plaadqcod = ".$codigo;
                     
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    //Actividades - Necesidad
    public function ValidaActividadNecesidad(Request $request)
    {

        try{

            $codigo = $request->codigo;

            $sql = "SELECT plane.plaadqnecdes necesidad,plane.plaadqnecval valor,
                            IFNULL(sum(plaac.plaadqnecactval),0) totalactividades
                    FROM  planesadquisiciones_necesidades plane 
                        LEFT JOIN  planesadquisiciones_necesidades_actividades plaac ON plane.plaadqcod = plaac.plaadqcod AND plane.plaadqneccod = plaac.plaadqneccod
                    WHERE plane.plaadqcod = ".$codigo." 
                    GROUP BY plane.plaadqnecdes,plane.plaadqnecval
                    HAVING IFNULL(sum(plaac.plaadqnecactval),0) < plane.plaadqnecval";
                     
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    //Categoria Gasto - Necesidad
    public function ValidaActividadCategoria(Request $request)
    {

        try{

            $codigo = $request->codigo;
            $basemoneda = $request->basemoneda;
            $tasa = $request->tasa;

            $valrealprod = 1;

            if($basemoneda == 'P' && $tasa > 0){

                $valrealprod = $tasa; 

            }

            $sql = "SELECT preccg.prescatgasdes categoriagasto,(preccg.prescatgastma * ".$valrealprod.") topecategoria,
                            IFNULL(sum(plaac.plaadqnecactval),0) totalactividades
                    FROM presupuestos pre 
                        INNER JOIN presupuestos_categorias_gasto preccg ON pre.prescod = preccg.prescod 
                        INNER JOIN proyectos pro ON pre.convcod = pro.convcod
                        INNER JOIN planes_adquisicion pla ON pro.proycod = pla.proycod	
                        LEFT JOIN  planesadquisiciones_necesidades_actividades plaac ON 
                                    pla.plaadqcod = plaac.plaadqcod AND preccg.prescatgascod = plaac.prescatgascod
                    WHERE pla.plaadqcod = ".$codigo." 
                    GROUP BY preccg.prescatgasdes,preccg.prescatgastma
                    HAVING IFNULL(sum(plaac.plaadqnecactval),0) < (preccg.prescatgastma * ".$valrealprod.")";
            
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    /*************************** VISTAS PARA FINANCIERA *************************
    ****************************************************************************/
    public function VistaFinNecesidades(Request $request)
    {

        try{

            $codigo = $request->codigo;

            $sql = "SELECT plaadqnecdes descripcion,plaadqnecval valor 
                    FROM planesadquisiciones_necesidades WHERE plaadqcod = ".$codigo;
                     
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    public function VistaFinActNec(Request $request)
    {

        try{

            $codigo = $request->codigo;

            $sql = "SELECT plane.plaadqnecdes necesidad,plane.plaadqnecval valor,
                            IFNULL(sum(plaac.plaadqnecactval),0) totalactividades,
                            (plane.plaadqnecval - IFNULL(sum(plaac.plaadqnecactval),0)) saldo
                    FROM  planesadquisiciones_necesidades plane 
                        LEFT JOIN  planesadquisiciones_necesidades_actividades plaac ON plane.plaadqcod = plaac.plaadqcod AND plane.plaadqneccod = plaac.plaadqneccod
                    WHERE plane.plaadqcod = ".$codigo." 
                    GROUP BY plane.plaadqnecdes,plane.plaadqnecval";
                     
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    public function VistaFinActCat(Request $request)
    {

        try{

            $codigo = $request->codigo;
            $basemoneda = $request->basemoneda;
            $tasa = $request->tasa;

            $valrealprod = 1;

            if($basemoneda == 'P' && $tasa > 0){

                $valrealprod = $tasa; 

            }

            $sql = "SELECT preccg.prescatgasdes categoriagasto,(preccg.prescatgastma * ".$valrealprod.") topecategoria,
                            IFNULL(sum(plaac.plaadqnecactval),0) totalactividades,
                            ((preccg.prescatgastma * ".$valrealprod.") - IFNULL(sum(plaac.plaadqnecactval),0)) saldo
                    FROM presupuestos pre 
                        INNER JOIN presupuestos_categorias_gasto preccg ON pre.prescod = preccg.prescod 
                        INNER JOIN proyectos pro ON pre.convcod = pro.convcod
                        INNER JOIN planes_adquisicion pla ON pro.proycod = pla.proycod	
                        LEFT JOIN  planesadquisiciones_necesidades_actividades plaac ON 
                                    pla.plaadqcod = plaac.plaadqcod AND preccg.prescatgascod = plaac.prescatgascod
                    WHERE pla.plaadqcod = ".$codigo." 
                    GROUP BY preccg.prescatgasdes,preccg.prescatgastma";
            
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    public function VistaFinResActividades(Request $request)
    {

        try{

            $codigo = $request->codigo;
           
            $sql = "SELECT CONCAT(pra.proyobjcod,'.',pra.prodcod,'.',pra.proyactcod,'.',pra.proyactdes) actividad,
                            IFNULL(sum(plaac.plaadqnecactval),0) total
                    FROM proyectos pro 
                        INNER JOIN  proyectos_actividades pra ON pro.proycod = pra.proycod
                        INNER JOIN planes_adquisicion pla ON pro.proycod = pla.proycod	
                        LEFT JOIN  planesadquisiciones_necesidades_actividades plaac ON 
                                    pla.plaadqcod = plaac.plaadqcod AND pra.proyactsec = plaac.proyactsec
                    WHERE pla.plaadqcod = ".$codigo." 
                    GROUP BY pra.proyobjcod,pra.prodcod,pra.proyactcod,pra.proyactdes";
            
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    public function DelNecesidad(Request $request)
    {
        $planadquisicion = $request->planadquisicion;
        $necesidad = $request->necesidad;
        
        try{

            DB::beginTransaction();

            $result = PlaAdqNecesidadesUnspsc::where('plaadqcod', $planadquisicion)
                    ->where('plaadqneccod', $necesidad)
                    ->delete(); 

            $result = PlaAdqNecActividades::where('plaadqcod', $planadquisicion)
                    ->where('plaadqneccod', $necesidad)
                    ->delete(); 

            $result = PlanesAdquisicionNecesidades::where('plaadqcod', $planadquisicion)
                            ->where('plaadqneccod', $necesidad)
                            ->delete(); 

            DB::commit();                                
            
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            DB::rollBack();

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }

    /************************ Modificaciones con VoBo *****************************
     * ****************************************************************************
     * ****************************************************************************/

    public function BuscarPlanAdquisicion(Request $request){

        $planadquisicion = $request->planadquisicion;

        //$maxVal = TmpPlanesAdquisicion::where('plaadqcod', $planadquisicion)->count();        
            
        //return $maxVal;

        $PlanesAdquisicion = TmpPlanesAdquisicion::select('tmpplanes_adquisicion.tmpplaadqcod','tmpplanes_adquisicion.plaadqcod',
                                    'tmpplanes_adquisicion.plaadqfec','tmpplanes_adquisicion.proycod',
                                    'tmpplanes_adquisicion.estjurcod','tmpplanes_adquisicion.plaadqacj',
                                    'tmpplanes_adquisicion.estplacod','tmpplanes_adquisicion.plaadqevb',
                                    'proyectos.proynomcor','convenios.convcod','convenios.convnum',
                                    'convenios.convnom','convenios.convvaltot','monedas.monsim',
                                    'convenios.perscod','tmpplanes_adquisicion.plaadqbasmon',
                                    'convenios.convtas','tmpplanes_adquisicion.plaadqver',
                                    'tmpplanes_adquisicion.tmpplaadqvbp','tmpplanes_adquisicion.tmpplaadqopl',
                                    'tmpplanes_adquisicion.tmpplaadqvbf','tmpplanes_adquisicion.tmpplaadqofi',
                                    'tmpplanes_adquisicion.tmpplaadqvbj','tmpplanes_adquisicion.tmpplaadqoju')
                     ->join('proyectos','tmpplanes_adquisicion.proycod','=','proyectos.proycod')
                     ->join('convenios','proyectos.convcod','=','convenios.convcod')                    
                     ->join('monedas','convenios.moncod','=','monedas.moncod')
                     ->where('tmpplanes_adquisicion.plaadqcod',$planadquisicion)                
                     ->get();
                     
            if ($PlanesAdquisicion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($PlanesAdquisicion, new TmpPlanesAdquisicionTransformer);                
            }
     }

    //agrega a la temporal 
    public function consecutivotmp()
    {      

        $maxVal  = TmpPlanesAdquisicion::max('tmpplaadqcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function delTemporal($tmplanadquisicion){

        //se elimina los codigos unspsc
        $result = TmpPlaAdqNecesidadesUnspsc::where('tmpplaadqcod', $tmplanadquisicion)                        
                        ->delete();
                        
        //se elimina las actividades
        $result = TmpPlaAdqNecActividades::where('tmpplaadqcod', $tmplanadquisicion)                        
                        ->delete();
                        
        //se elimina las necesidades
        $result = TmpPlanesAdquisicionNec::where('tmpplaadqcod', $tmplanadquisicion)                        
                        ->delete();
                        
        //se elimina el plan
        $result = TmpPlanesAdquisicion::where('tmpplaadqcod', $tmplanadquisicion)                        
                        ->delete();                

    }

    public function DataAddTmp(Request $request)
    {
        date_default_timezone_set('America/Bogota');

        $data            = $request->data;
        $necesidades     = $request->necesidades;
        $dataunspsc      = $request->dataunspsc;
        $dataactividades = $request->dataactividades;
        
        $tmplanadquisicion   = $data['tmplanadquisicion'];

        $codigo               = $data['codigo'];
        $fecha                = date('Y\-m\-d');
        $proyecto             = $data['proyecto'];
        $estadojurudico       = $data['estadojurudico']; 
        $aclaracionjurudica   = $data['aclaracionjurudica'];
        $estadoplaneacion     = $data['estadoplaneacion'];   
        $aclaracionplaneacion = $data['aclaracionplaneacion'];
        $basemoneda = $data['basemoneda'];        
        
        //busca si ya existe la remporal y la elimina
        if($tmplanadquisicion != 0 && $tmplanadquisicion != '0')
            $this->delTemporal($tmplanadquisicion);

        $consecutivo =$this->consecutivotmp();
        
        $DataAdd = new TmpPlanesAdquisicion();
        
        $DataAdd->tmpplaadqcod = $consecutivo; 
        $DataAdd->plaadqcod    = $codigo; 
        $DataAdd->plaadqfec    = $fecha;          
        $DataAdd->proycod      = $proyecto; 
        $DataAdd->estjurcod    = $estadojurudico; 
        $DataAdd->plaadqacj    = $aclaracionjurudica;
        $DataAdd->estplacod    = $estadoplaneacion;
        $DataAdd->plaadqacp    = $aclaracionplaneacion; 
        $DataAdd->plaadqbasmon = $basemoneda;         
        
        $result = $DataAdd->save();
        
        $codigoactual = $consecutivo; 

        //Necesidades        
        if(count($necesidades) > 0){

            $arrDataActual = [];

            for ($i=0; $i < count($necesidades); $i++) {                

                $itemactual = $necesidades[$i];                    
                        
                $itemAdd = new TmpPlanesAdquisicionNec();
    
                $itemAdd->tmpplaadqcod     = $codigoactual; 
                $itemAdd->plaadqneccod     = $itemactual['codigo'];          
                $itemAdd->plaadqnecdes     = $itemactual['descripcion']; 
                $itemAdd->plaadqnecval     = $itemactual['valor']; 
                $itemAdd->metselcod        = $itemactual['metodoleccion'];
                $itemAdd->plaadqnecfeccont = $itemactual['fechasolcontrata'];
                $itemAdd->estcod           = $itemactual['estadocodigo'];
                $itemAdd->plaadqnectmo     = isset($itemactual['tipomod']) ? $itemactual['tipomod'] : '';
                
                $result = $itemAdd->save();                                                           
            }            
        }

        //unspsc        
        if(count($dataunspsc) > 0){

            $arrDataActual = [];

            for ($i=0; $i < count($dataunspsc); $i++) {                

                $itemactual = $dataunspsc[$i];
                       
                $necesidad  = $itemactual['necesidad'];
                $unspsc     = $itemactual['unspsc'];
                $tipomod    = isset($itemactual['tipomod']) ? $itemactual['tipomod'] : '';

                $itemAdd = new TmpPlaAdqNecesidadesUnspsc();
    
                $itemAdd->tmpplaadqcod      = $codigoactual; 
                $itemAdd->plaadqneccod      = $necesidad;
                $itemAdd->unspsccod         = $unspsc;
                $itemAdd->plaadqnecunstmod  = $tipomod;
                
                $result = $itemAdd->save();                                                           
            }            
        }
        
        //actividades        
        if(count($dataactividades) > 0){

            $arrDataActual = [];

            for ($i=0; $i < count($dataactividades); $i++) {                

                $itemactual = $dataactividades[$i];                      
                                     
                $necesidad          = $itemactual['necesidad'];
                $secuenciaactividad = $itemactual['secuenciaactividad'];
                $categoriagasto     = $itemactual['categoriagasto'];
                $topecategoriagasto = $itemactual['topecategoriagasto'];
                $valor              = $itemactual['valor'];
                $rubro              = $itemactual['rubro'];
                $presupuesto        = $itemactual['presupuesto'];
                $tipomod            = isset($itemactual['tipomod']) ? $itemactual['tipomod'] : '';

                $itemAdd = new TmpPlaAdqNecActividades();
    
                $itemAdd->tmpplaadqcod      = $codigoactual; 
                $itemAdd->plaadqneccod      = $necesidad;
                $itemAdd->proyactsec        = $secuenciaactividad;
                $itemAdd->prescod           = $presupuesto;
                $itemAdd->prescatgascod     = $categoriagasto;
                $itemAdd->plaadqnecacttag   = $topecategoriagasto;
                $itemAdd->plaadqnecactval   = $valor;
                $itemAdd->plaadqnecactrpr   = $rubro;
                $itemAdd->plaadqnecacttmo   = $tipomod;
                
                $result = $itemAdd->save();                                                           
            }            
        }

        return array( 'respuesta' => $result);     
    }

    //consulta detalles temporales
    public function TmpNecesidadesXTmpPlanAdq(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;

            $Data = TmpPlanesAdquisicionNec::select('tmpplanesadquisiciones_necesidades.tmpplaadqcod',
                                                 'tmpplanesadquisiciones_necesidades.plaadqneccod',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnecdes',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnecval',
                                                 'tmpplanesadquisiciones_necesidades.metselcod',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnecfeccont',
                                                 'tmpplanesadquisiciones_necesidades.estcod',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnecvbf',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnecobf',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnecvbj',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnecobj',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnecvbp',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnecobp',                                                 
                                                 'metodos_seleccion.metseldes','estados.estdes',
                                                 'tmpplanes_adquisicion.plaadqevb',
                                                 'tmpplanesadquisiciones_necesidades.plaadqnectmo')
                     ->join('tmpplanes_adquisicion','tmpplanesadquisiciones_necesidades.tmpplaadqcod','=','tmpplanes_adquisicion.tmpplaadqcod')                                 
                     ->join('metodos_seleccion','tmpplanesadquisiciones_necesidades.metselcod','=','metodos_seleccion.metselcod')                     
                     ->join('estados','tmpplanesadquisiciones_necesidades.estcod','=','estados.estcod')
                     ->where('tmpplanesadquisiciones_necesidades.tmpplaadqcod',$planadquisicion) 
                     ->orderby('tmpplanesadquisiciones_necesidades.plaadqneccod')
                     ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new TmpPlanesAdquisicionNecTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function TmpUnspscXTmpPlanAdq(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;            

            $Data = TmpPlaAdqNecesidadesUnspsc::select('tmpplanesadquisiciones_necesidadesunspsc.tmpplaadqcod',
                                                 'tmpplanesadquisiciones_necesidadesunspsc.plaadqneccod',
                                                 'tmpplanesadquisiciones_necesidadesunspsc.unspsccod',
                                                 'unspsc.unspscdes','tmpplanesadquisiciones_necesidadesunspsc.plaadqnecunstmod')
                     ->join('unspsc','tmpplanesadquisiciones_necesidadesunspsc.unspsccod','=','unspsc.unspsccod')                     
                     ->where('tmpplanesadquisiciones_necesidadesunspsc.tmpplaadqcod',$planadquisicion)                     
                     ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new TmpPlaAdqNecesidadesUnspscTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function TmpActividadXTmpPlanAdq(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;
            
            $sql = "select pac.tmpplaadqcod planadquisicion,pac.plaadqneccod necesidad,
                            pac.proyactsec secuenciaactividad,pac.prescatgascod categoriagasto,
                            pac.plaadqnecacttag topecategoriagasto,pac.plaadqnecactval valor,
                            pya.proyactdes actividadtxtresumen,pcg.prescatgasdes categoriagastotxt,
                            CONCAT(pya.proyobjcod,'.',pya.prodcod,'.',pya.proyactcod,' ',pya.proyactdes) actividadtxt,
                            pre.presfuefinpgn pgn,pre.presfuefinsgr sgr,pre.presfuefincof cofinanciado,
                            pre.presfuefinfun funcionamiento,pac.plaadqnecactrpr rubro,
                            CONCAT(pya.proyobjcod,pya.prodcod,pya.proyactcod) codigofull,
                            pac.prescod presupuesto,pac.plaadqnecacttmo tipomod  
                        from tmpplanesadquisiciones_necesidadesactividades pac 
                        inner join tmpplanes_adquisicion pla on pac.tmpplaadqcod = pla.tmpplaadqcod 
                        inner join proyectos pry on pla.proycod = pry.proycod 
                        inner join presupuestos pre on pry.convcod = pre.convcod 
                        inner join presupuestos_categorias_gasto pcg on pre.prescod = pcg.prescod 
                                AND pac.prescatgascod = pcg.prescatgascod 
                        inner join proyectos_actividades pya on pac.proyactsec = pya.proyactsec 
                        where pac.tmpplaadqcod = ".$planadquisicion;
            $result =DB::select($sql);
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    //Versionamiento
    public function HisPlanAdquisicionBase(Request $request){

        $planadquisicion = $request->planadquisicion;

        $PlanesAdquisicion = HisPlanesAdquisicion::select('hisplanes_adquisicion.hisplaadqcod',
                                    'hisplanes_adquisicion.plaadqcod',
                                    'hisplanes_adquisicion.plaadqfec','hisplanes_adquisicion.proycod',
                                    'hisplanes_adquisicion.estjurcod','hisplanes_adquisicion.plaadqacj',
                                    'hisplanes_adquisicion.estplacod','hisplanes_adquisicion.plaadqevb',
                                    'proyectos.proynomcor','convenios.convcod','convenios.convnum',
                                    'convenios.convnom','convenios.convvaltot','monedas.monsim',
                                    'convenios.perscod','hisplanes_adquisicion.plaadqbasmon',
                                    'convenios.convtas','hisplanes_adquisicion.plaadqver')
                     ->join('proyectos','hisplanes_adquisicion.proycod','=','proyectos.proycod')
                     ->join('convenios','proyectos.convcod','=','convenios.convcod')                    
                     ->join('monedas','convenios.moncod','=','monedas.moncod')
                     ->where('hisplanes_adquisicion.plaadqcod',$planadquisicion)                
                     ->get();
                     
            if ($PlanesAdquisicion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($PlanesAdquisicion, new HisPlanesAdquisicionTransformer);                
            }
    }

    public function HisPlanAdquisicionVersion(Request $request){

        $planadquisicion = $request->planadquisicion;
        $version = $request->version;

        $PlanesAdquisicion = HisPlanesAdquisicion::select('hisplanes_adquisicion.hisplaadqcod',
                                    'hisplanes_adquisicion.plaadqcod',
                                    'hisplanes_adquisicion.plaadqfec','hisplanes_adquisicion.proycod',
                                    'hisplanes_adquisicion.estjurcod','hisplanes_adquisicion.plaadqacj',
                                    'hisplanes_adquisicion.estplacod','hisplanes_adquisicion.plaadqevb',
                                    'proyectos.proynomcor','convenios.convcod','convenios.convnum',
                                    'convenios.convnom','convenios.convvaltot','monedas.monsim',
                                    'convenios.perscod','hisplanes_adquisicion.plaadqbasmon',
                                    'convenios.convtas','hisplanes_adquisicion.plaadqver')
                     ->join('proyectos','hisplanes_adquisicion.proycod','=','proyectos.proycod')
                     ->join('convenios','proyectos.convcod','=','convenios.convcod')                    
                     ->join('monedas','convenios.moncod','=','monedas.moncod')
                     ->where('hisplanes_adquisicion.plaadqcod',$planadquisicion)
                     ->where('hisplanes_adquisicion.plaadqver',$version)                
                     ->get();
                     
            if ($PlanesAdquisicion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($PlanesAdquisicion, new HisPlanesAdquisicionTransformer);                
            }
    }

    public function HisNecesidadesXHisPlanAdq(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;

            $Data = HisPlanesAdquisicionNec::select('hisplanesadquisiciones_necesidades.hisplaadqcod',
                                                 'hisplanesadquisiciones_necesidades.plaadqneccod',
                                                 'hisplanesadquisiciones_necesidades.plaadqnecdes',
                                                 'hisplanesadquisiciones_necesidades.plaadqnecval',
                                                 'hisplanesadquisiciones_necesidades.metselcod',
                                                 'hisplanesadquisiciones_necesidades.plaadqnecfeccont',
                                                 'hisplanesadquisiciones_necesidades.estcod',
                                                 'hisplanesadquisiciones_necesidades.plaadqnecvbf',
                                                 'hisplanesadquisiciones_necesidades.plaadqnecobf',
                                                 'hisplanesadquisiciones_necesidades.plaadqnecvbj',
                                                 'hisplanesadquisiciones_necesidades.plaadqnecobj',
                                                 'hisplanesadquisiciones_necesidades.plaadqnecvbp',
                                                 'hisplanesadquisiciones_necesidades.plaadqnecobp',                                                 
                                                 'metodos_seleccion.metseldes','estados.estdes')
                     ->join('hisplanes_adquisicion','hisplanesadquisiciones_necesidades.hisplaadqcod','=','hisplanes_adquisicion.hisplaadqcod')                                 
                     ->join('metodos_seleccion','hisplanesadquisiciones_necesidades.metselcod','=','metodos_seleccion.metselcod')                     
                     ->join('estados','hisplanesadquisiciones_necesidades.estcod','=','estados.estcod')
                     ->where('hisplanes_adquisicion.hisplaadqcod',$planadquisicion) 
                     ->orderby('hisplanesadquisiciones_necesidades.plaadqneccod')
                     ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new HisPlanesAdquisicionNecTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function HisUnspscXHisPlanAdq(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;            

            $Data = HisPlaAdqNecesidadesUnspsc::select('hisplanesadquisiciones_necesidadesunspsc.hisplaadqcod',
                                                 'hisplanesadquisiciones_necesidadesunspsc.plaadqneccod',
                                                 'hisplanesadquisiciones_necesidadesunspsc.unspsccod',
                                                 'unspsc.unspscdes')
                     ->join('unspsc','hisplanesadquisiciones_necesidadesunspsc.unspsccod','=','unspsc.unspsccod')                     
                     ->where('hisplanesadquisiciones_necesidadesunspsc.hisplaadqcod',$planadquisicion)                     
                     ->get();
                     
            if ($Data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Data, new HisPlaAdqNecesidadesUnspscTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function HisActividadXHisPlanAdq(Request $request)
    {       
       try{

            $planadquisicion = $request->planadquisicion;
            
            $sql = "select pac.hisplaadqcod planadquisicion,pac.plaadqneccod necesidad,
                            pac.proyactsec secuenciaactividad,pac.prescatgascod categoriagasto,
                            pac.plaadqnecacttag topecategoriagasto,pac.plaadqnecactval valor,
                            pya.proyactdes actividadtxtresumen,pcg.prescatgasdes categoriagastotxt,
                            CONCAT(pya.proyobjcod,'.',pya.prodcod,'.',pya.proyactcod,' ',pya.proyactdes) actividadtxt,
                            pre.presfuefinpgn pgn,pre.presfuefinsgr sgr,pre.presfuefincof cofinanciado,
                            pre.presfuefinfun funcionamiento,pac.plaadqnecactrpr rubro,
                            CONCAT(pya.proyobjcod,pya.prodcod,pya.proyactcod) codigofull,
                            pac.prescod presupuesto  
                        from hisplanesadquisiciones_necesidadesactividades pac 
                        inner join hisplanes_adquisicion pla on pac.hisplaadqcod = pla.hisplaadqcod 
                        inner join proyectos pry on pla.proycod = pry.proycod 
                        inner join presupuestos pre on pry.convcod = pre.convcod 
                        inner join presupuestos_categorias_gasto pcg on pre.prescod = pcg.prescod 
                                AND pac.prescatgascod = pcg.prescatgascod 
                        inner join proyectos_actividades pya on pac.proyactsec = pya.proyactsec 
                        where pac.hisplaadqcod = ".$planadquisicion;
            $result =DB::select($sql);
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

     /*************************** VISTAS PARA TMP FINANCIERA *************************
    **********************************************************************************/
    public function VistaFinTmpNecesidades(Request $request)
    {

        try{

            $codigo = $request->codigo;

            $sql = "SELECT plaadqnecdes descripcion,plaadqnecval valor 
                    FROM tmpplanesadquisiciones_necesidades WHERE tmpplaadqcod = ".$codigo;
                     
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    public function VistaFinTmpActNec(Request $request)
    {

        try{

            $codigo = $request->codigo;

            $sql = "SELECT plane.plaadqnecdes necesidad,plane.plaadqnecval valor,
                            IFNULL(sum(plaac.plaadqnecactval),0) totalactividades,
                            (plane.plaadqnecval - IFNULL(sum(plaac.plaadqnecactval),0)) saldo
                    FROM  tmpplanesadquisiciones_necesidades plane 
                        LEFT JOIN  tmpplanesadquisiciones_necesidadesactividades plaac ON plane.tmpplaadqcod = plaac.tmpplaadqcod AND plane.plaadqneccod = plaac.plaadqneccod
                    WHERE plane.tmpplaadqcod = ".$codigo." 
                    GROUP BY plane.plaadqnecdes,plane.plaadqnecval";
                     
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    public function VistaFinTmpActCat(Request $request)
    {

        try{

            $codigo = $request->codigo;
            $basemoneda = $request->basemoneda;
            $tasa = $request->tasa;

            $valrealprod = 1;

            if($basemoneda == 'P' && $tasa > 0){

                $valrealprod = $tasa; 

            }

            $sql = "SELECT preccg.prescatgasdes categoriagasto,(preccg.prescatgastma * ".$valrealprod.") topecategoria,
                            IFNULL(sum(plaac.plaadqnecactval),0) totalactividades,
                            ((preccg.prescatgastma * ".$valrealprod.") - IFNULL(sum(plaac.plaadqnecactval),0)) saldo
                    FROM presupuestos pre 
                        INNER JOIN presupuestos_categorias_gasto preccg ON pre.prescod = preccg.prescod 
                        INNER JOIN proyectos pro ON pre.convcod = pro.convcod
                        INNER JOIN tmpplanes_adquisicion pla ON pro.proycod = pla.proycod	
                        LEFT JOIN  tmpplanesadquisiciones_necesidadesactividades plaac ON 
                                    pla.tmpplaadqcod = plaac.tmpplaadqcod AND preccg.prescatgascod = plaac.prescatgascod
                    WHERE pla.tmpplaadqcod = ".$codigo." 
                    GROUP BY preccg.prescatgasdes,preccg.prescatgastma";
            
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    public function VistaFinResTmpActividades(Request $request)
    {

        try{

            $codigo = $request->codigo;
           
            $sql = "SELECT CONCAT(pra.proyobjcod,'.',pra.prodcod,'.',pra.proyactcod,'.',pra.proyactdes) actividad,
                            IFNULL(sum(plaac.plaadqnecactval),0) total
                    FROM proyectos pro 
                        INNER JOIN  proyectos_actividades pra ON pro.proycod = pra.proycod
                        INNER JOIN tmpplanes_adquisicion pla ON pro.proycod = pla.proycod	
                        LEFT JOIN  tmpplanesadquisiciones_necesidadesactividades plaac ON 
                                    pla.tmpplaadqcod = plaac.tmpplaadqcod AND pra.proyactsec = plaac.proyactsec
                    WHERE pla.tmpplaadqcod = ".$codigo." 
                    GROUP BY pra.proyobjcod,pra.prodcod,pra.proyactcod,pra.proyactdes";
            
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    public function ActualizarTmpVoBo(Request $request){

        $planadquisicion = $request->planadquisicion;        
        $tipo = $request->tipo;
        $vobo = $request->vobo;
        $observacion = $request->observacion;

        $sql = '';
        $result = 0;

        if($tipo == 'F'){
            $sql = "UPDATE tmpplanes_adquisicion 
                    SET tmpplaadqvbf = '".$vobo."',tmpplaadqofi = '".$observacion."' ";

        } elseif ($tipo == 'J') {
           
            $sql = "UPDATE tmpplanes_adquisicion 
                    SET tmpplaadqvbj = '".$vobo."',tmpplaadqoju = '".$observacion."' ";

        } elseif ($tipo == 'P') {
           
            $sql = "UPDATE tmpplanes_adquisicion 
                    SET tmpplaadqvbp = '".$vobo."',tmpplaadqopl = '".$observacion."' ";

        }   

        $sql .= " WHERE tmpplaadqcod = ".$planadquisicion;
        $result =DB::update($sql);

        return  $result;
    }

    /***************************** SE RECHAZA  **************************************/
    public function delTemporalRechazo(Request $request){

        $tmpplanadquisicion = $request->planadquisicion;

        //se elimina los codigos unspsc
        $result = TmpPlaAdqNecesidadesUnspsc::where('tmpplaadqcod', $tmpplanadquisicion)                        
                        ->delete();
                        
        //se elimina las actividades
        $result = TmpPlaAdqNecActividades::where('tmpplaadqcod', $tmpplanadquisicion)                        
                        ->delete();
                        
        //se elimina las necesidades
        $result = TmpPlanesAdquisicionNec::where('tmpplaadqcod', $tmpplanadquisicion)                        
                        ->delete();
                        
        //se elimina el plan
        $result = TmpPlanesAdquisicion::where('tmpplaadqcod', $tmpplanadquisicion)                        
                        ->delete();                

    }

    /**********************************************************************************
     ************************** se crea versionamiento ******************************** 
    */

    private function BuscarVersion($planadquisicion){

        $Data = PlanesAdquisicion::select('plaadqver')                      
                    ->where('plaadqcod', $planadquisicion)                   
                    ->get();

        $version = 0;          
        if (!$Data->isEmpty()) {
            
            $version = $Data[0]->plaadqver;
            
        }
        
        return $version;

    }

    public function consecutivohistorico()
    {      

        $maxVal  = HisPlanesAdquisicion::max('hisplaadqcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    public function creaVersion(Request $request){

        $tmpplanadquisicion = $request->tmpplanadquisicion;
        $planadquisicion    = $request->planadquisicion;

        $nuevaversion = $this->BuscarVersion($planadquisicion);
        $nuevaversion += 1;

        $fecha = date('Y\-m\-d');
        $consehistorico = $this->consecutivohistorico();
        
        DB::beginTransaction();

        try{

            //SE AGREGAN HISTORICOS
            //historico plan de adquisiciones
            $sql = "INSERT INTO hisplanes_adquisicion (hisplaadqcod,plaadqcod,plaadqfec,proycod, estjurcod, 
                        plaadqacj,estplacod,plaadqacp,plaadqevb,plaadqbasmon,plaadqver) 
                    SELECT ".$consehistorico.",plaadqcod, plaadqultfec, proycod,estjurcod,
                        plaadqacj,estplacod,plaadqacp, plaadqevb,plaadqbasmon,plaadqver 
                    FROM planes_adquisicion 
                    WHERE planes_adquisicion.plaadqcod=".$planadquisicion;
            $result =DB::insert($sql);

            //historico necesidades
            $sql = "INSERT INTO hisplanesadquisiciones_necesidades (hisplaadqcod, plaadqneccod, plaadqnecdes,
                        plaadqnecval, metselcod, plaadqnecfeccont, estcod, plaadqnecvbf, plaadqnecobf, 
                        plaadqnecvbp, plaadqnecobp, plaadqnecvbj, plaadqnecobj) 
                    SELECT ".$consehistorico.",plaadqneccod, plaadqnecdes, plaadqnecval,metselcod,plaadqnecfeccont,
                        estcod,plaadqnecvbf,plaadqnecobf,plaadqnecvbp,plaadqnecobp,plaadqnecvbj,plaadqnecobj 
                    FROM planesadquisiciones_necesidades 
                    WHERE planesadquisiciones_necesidades.plaadqcod=".$planadquisicion;
            $result =DB::insert($sql);

            //historico unspsc
            $sql = "INSERT INTO hisplanesadquisiciones_necesidadesunspsc (hisplaadqcod, plaadqneccod, unspsccod) 
                    SELECT ".$consehistorico.",plaadqneccod,unspsccod 
                    FROM planesadquisiciones_necesidades_unspsc 
                    WHERE planesadquisiciones_necesidades_unspsc.plaadqcod=".$planadquisicion;
            $result =DB::insert($sql);   
            
            $sql = "INSERT INTO hisplanesadquisiciones_necesidadesactividades (hisplaadqcod,plaadqneccod,
                        proyactsec,prescod,prescatgascod,plaadqnecacttag,plaadqnecactval,plaadqnecactrpr)  
                    SELECT ".$consehistorico.",plaadqneccod,proyactsec,prescod,prescatgascod,plaadqnecacttag,
                        plaadqnecactval,plaadqnecactrpr 
                    FROM planesadquisiciones_necesidades_actividades 
                    WHERE planesadquisiciones_necesidades_actividades.plaadqcod=".$planadquisicion;
            $result =DB::insert($sql);        
            
            $consecutivonuevoplan = $this->consecutivo();
            
            //se elimina el plan anterior
            //unspsc
            $sql = "DELETE FROM planesadquisiciones_necesidades_unspsc 
                    WHERE plaadqcod = ".$planadquisicion;
            $result =DB::delete($sql);

            //ACTIVIDADES
            $sql = "DELETE FROM planesadquisiciones_necesidades_actividades 
                    WHERE plaadqcod = ".$planadquisicion;
            $result =DB::delete($sql);

            //necesidades
            $sql = "DELETE FROM planesadquisiciones_necesidades 
                    WHERE plaadqcod = ".$planadquisicion;
            $result =DB::delete($sql);

            //plan
            $sql = "DELETE FROM planes_adquisicion 
                    WHERE plaadqcod = ".$planadquisicion;
            $result =DB::delete($sql);

            //SE CREA EL NUEVO PLAN DE ADQUISICIONES
            //plan
            $sql = "INSERT INTO planes_adquisicion (plaadqcod,plaadqfec,plaadqultfec,proycod, estjurcod, 
                            plaadqacj,estplacod,plaadqacp,plaadqevb,plaadqbasmon,plaadqver) 
                    SELECT ".$planadquisicion.",plaadqfec,'".$fecha."',proycod, estjurcod, 
                            plaadqacj,estplacod,plaadqacp,plaadqevb,plaadqbasmon,".$nuevaversion." 
                    FROM tmpplanes_adquisicion 
                    WHERE tmpplanes_adquisicion.tmpplaadqcod = ".$tmpplanadquisicion;
            $result =DB::delete($sql);

            //necesidades
            $sql = "INSERT INTO planesadquisiciones_necesidades (plaadqcod, plaadqneccod, plaadqnecdes,
                        plaadqnecval, metselcod, plaadqnecfeccont, estcod, plaadqnecvbf, plaadqnecobf, 
                        plaadqnecvbp, plaadqnecobp, plaadqnecvbj, plaadqnecobj )
                    SELECT ".$planadquisicion.",plaadqneccod, plaadqnecdes, plaadqnecval,metselcod,plaadqnecfeccont,
                        estcod,plaadqnecvbf,plaadqnecobf,plaadqnecvbp,plaadqnecobp,plaadqnecvbj,plaadqnecobj 
                    FROM tmpplanesadquisiciones_necesidades 
                    WHERE tmpplanesadquisiciones_necesidades.tmpplaadqcod=".$tmpplanadquisicion." AND 
                            tmpplanesadquisiciones_necesidades.plaadqnectmo <> 'E'";
            $result =DB::insert($sql);

            //UNSPSC
            $sql = "INSERT INTO planesadquisiciones_necesidades_unspsc (plaadqcod, plaadqneccod, unspsccod) 
                    SELECT ".$planadquisicion.",un.plaadqneccod,un.unspsccod 
                        FROM tmpplanesadquisiciones_necesidadesunspsc un 
                            INNER JOIN tmpplanesadquisiciones_necesidades nec ON un.tmpplaadqcod = nec.tmpplaadqcod 
                            AND un.plaadqneccod = nec.plaadqneccod AND nec.plaadqnectmo <> 'E'
                        WHERE un.tmpplaadqcod=".$tmpplanadquisicion." AND 
                                un.plaadqnecunstmod <> 'E'";
            $result =DB::insert($sql);
            
            //actividades
            $sql = "INSERT INTO planesadquisiciones_necesidades_actividades (plaadqcod,plaadqneccod,
                        proyactsec,prescod,prescatgascod,plaadqnecacttag,plaadqnecactval,plaadqnecactrpr )                     
                        SELECT ".$planadquisicion.",act.plaadqneccod,act.proyactsec,act.prescod,act.prescatgascod,act.plaadqnecacttag,
                               act.plaadqnecactval,act.plaadqnecactrpr 
                        FROM tmpplanesadquisiciones_necesidadesactividades act 
                        INNER JOIN tmpplanesadquisiciones_necesidades nec ON act.tmpplaadqcod = nec.tmpplaadqcod 
                            AND act.plaadqneccod = nec.plaadqneccod AND nec.plaadqnectmo <> 'E'
                        WHERE act.tmpplaadqcod=".$tmpplanadquisicion." AND act.plaadqnecacttmo <> 'E'";                    
            $result =DB::insert($sql);

            //se elimina temporales
            //se elimina los codigos unspsc
            $result = TmpPlaAdqNecesidadesUnspsc::where('tmpplaadqcod', $tmpplanadquisicion)                        
                            ->delete();
                            
            //se elimina las actividades
            $result = TmpPlaAdqNecActividades::where('tmpplaadqcod', $tmpplanadquisicion)                        
                            ->delete();
                            
            //se elimina las necesidades
            $result = TmpPlanesAdquisicionNec::where('tmpplaadqcod', $tmpplanadquisicion)                        
                            ->delete();
                            
            //se elimina el plan
            $result = TmpPlanesAdquisicion::where('tmpplaadqcod', $tmpplanadquisicion)                        
                            ->delete();     
            
            DB::commit();
            return $result;

        } catch (\Exception $e) {

            $error = $e->getMessage();

            DB::rollback();

            return $error;

        }
    }

    /***********************************  RUBROS PRESUPUESTALES   **********************************
     **************************************************************************** *****************/
    public function RubrosPresupuestales(Request $request)
    {       
       try{

            $convenio        = $request->convenio;
            $nombreproyecto  = $request->nombreproyecto;
            $codigoplan      = $request->codigoplan;
            $codigonecesidad = $request->codigonecesidad; 

            $parametros = array($convenio,$nombreproyecto,$codigoplan,$codigonecesidad);

            $result = DB::select("CALL rubrosconsaldos (?,?,?,?)", $parametros);

            /*$sql = "SELECT pn.plaadqnecdes descripcion,pna.plaadqcod planadquisicion,pna.plaadqneccod codigo,
                           pna.proyactsec secuenciaactividad,pna.prescod presupuesto,pna.plaadqnecactrpr rubro,
                           pna.plaadqnecactval valor 
                    FROM planesadquisiciones_necesidades pn
                    INNER JOIN planesadquisiciones_necesidades_actividades pna 
                            ON pn.plaadqcod = pna.plaadqcod AND pn.plaadqneccod = pna.plaadqneccod     
                    WHERE pn.plaadqnecvbf = 'S' AND pn.plaadqnecvbp = 'S' AND pn.plaadqnecvbj = 'S'";

            $result = $result =DB::select($sql);*/

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
}
    
?>