<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ReunionesExternoTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ReunionesExterno;
use DB;

class ReunionesExternoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ReunionesExterno = ReunionesExterno::select('reuextcod','reuextnom','reuextema','reucod') 
                     ->orderby('reuextcod')                     
                     ->get();
                     
            if ($ReunionesExterno->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesExterno, new ReunionesExternoTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function ReunionesExternoxCodReunion(Request $request)
    {       
       $CodReunion = $request->id;

       try{

            $ReunionesExterno = ReunionesExterno::select('reuniones_externo.reuextcod','reuniones_externo.reuextnom','reuniones_externo.reuextema','reuniones_externo.reucod')                      
                     ->where('reuniones_externo.reucod',$CodReunion)
                     ->orderby('reuniones_externo.reuextnom')                     
                     ->get();
                     
            if ($ReunionesExterno->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesExterno, new ReunionesExternoTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function consecutivo()
    {      

        $maxVal  = ReunionesExterno::max('reuextcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReunionesExternoAdd(Request $request)
    {
        $ReunionesExterno = $request->data;

        $CodReunion = $ReunionesExterno['codigoreunion'];
        $Externo =  $ReunionesExterno['externo'];        
        

        $result = $this->destroy($CodReunion);

        foreach ($Externo as $row) {
            $consecutivo =$this->consecutivo();

            $ReunionExtAdd = new ReunionesExterno();

            $ReunionExtAdd->reuextcod = $consecutivo;           
            $ReunionExtAdd->reucod = $CodReunion;
            $ReunionExtAdd->reuextnom = $row['NombreExterno'];
            $ReunionExtAdd->reuextema = $row['EmailExterno'];

            $result = $ReunionExtAdd->save();
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function destroy($CodigoReunion)
    {

        $result = ReunionesExterno::where('reucod', $CodigoReunion)->delete(); 
        return $result;

    }
}

?>