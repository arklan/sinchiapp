<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\CategoriasGastoTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CategoriasGasto;
use DB;

class CategoriasGastoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $CategoriasGasto = CategoriasGasto::select('catgascod','catgasnom')
                     ->orderby('catgasnom')                     
                     ->get();
                     
            if ($CategoriasGasto->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($CategoriasGasto, new CategoriasGastoTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = CategoriasGasto::max('catgascod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = CategoriasGasto::select(DB::raw('count(catgascod) as total'))                      
                      ->where('catgasnom', $Nombre)
                      ->where('catgascod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoriagastoAddUpdate(Request $request)
    {
        $categoriagasto = $request->data;

        $codigo = $categoriagasto['codigo'];
        $nombre = strtoupper(trim($categoriagasto['nombre']));       

        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $categoriagastoAdd = new CategoriasGasto();
            
            $categoriagastoAdd->catgascod = $consecutivo;           
            $categoriagastoAdd->catgasnom = $nombre;           
            
            $result = $categoriagastoAdd->save();
        
        } else {

            $categoriagastoUpd = CategoriasGasto::where('catgascod', $codigo)->first();
          
            $categoriagastoUpd->catgasnom = $nombre;          

            // Guardamos en base de datos
            $result = $categoriagastoUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = CategoriasGasto::where('catgascod', $Id)->delete(); 
        return $result;

    }
}

?>