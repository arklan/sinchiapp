<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\MetasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Metas;
use DB;

class MetasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Metas = Metas::select('metas.metcod','metas.metnom','metas.lininvpencod','lineas_investigacion_penia.lininvpendes')
                            ->join('lineas_investigacion_penia','metas.lininvpencod','=','lineas_investigacion_penia.lininvpencod')
                            ->orderby('metas.metnom')                     
                            ->get();
                     
            if ($Metas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Metas, new MetasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function FiltroXLinea(Request $request)
    {       
       try{

            $linea = $request->linea;

            $data = Metas::select('metas.metcod','metas.metnom','metas.lininvpencod','lineas_investigacion_penia.lininvpendes')
                                ->join('lineas_investigacion_penia','metas.lininvpencod','=','lineas_investigacion_penia.lininvpencod')
                                ->where('metas.lininvpencod',$linea)
                                ->orderby('metas.metnom')                     
                                ->get();
                     
            if ($data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($data, new MetasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Metas::max('metcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Metas::select(DB::raw('count(metcod) as total'))                      
                      ->where('metnom', $nombre)
                      ->where('metcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function MetasAddUpdate(Request $request)
    {
        $Metas = $request->data;

        $codigo = $Metas['codigo'];
        $nombre = strtoupper(trim($Metas['nombre']));       

        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $MetasAdd = new Metas();
            
            $MetasAdd->metcod = $consecutivo;           
            $MetasAdd->metnom = $nombre;           
            
            $result = $MetasAdd->save();
        
        } else {

            $MetasUpd = Metas::where('metcod', $codigo)->first();
          
            $MetasUpd->metnom = $nombre;          

            // Guardamos en base de datos
            $result = $MetasUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Metas::where('metcod', $Id)->delete(); 
        return $result;

    }
}

?>