<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosEquipoTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosEquipo;
use DB;

class ProyectosEquipoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosEquipo = ProyectosEquipo::select('proyectos_equipotecnico.proycod','proyectos_equipotecnico.perscod', 
                                                        'personal.persnom','personal.persape','proyectos_equipotecnico.rolcod',
                                                        'proyectos_equipotecnico.proyeteded','proyectos_equipotecnico.proyetepor',
                                                        'proyectos_equipotecnico.proyetetit','roles.roldes')                        
                        ->join('personal','proyectos_equipotecnico.perscod','=','personal.perscod') 
                        ->join('roles','proyectos_equipotecnico.rolcod','=','roles.rolcod')            
                        ->orderby('personal.persnom','personal.persape')                     
                        ->get();
                     
            if ($ProyectosEquipo->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosEquipo, new ProyectosEquipoTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];

            $ProyectosEquipo = ProyectosEquipo::select('proyectos_equipotecnico.proycod','proyectos_equipotecnico.perscod', 
                                                       'personal.persnom','personal.persape','proyectos_equipotecnico.rolcod',
                                                       'proyectos_equipotecnico.proyeteded','proyectos_equipotecnico.proyetepor',
                                                       'proyectos_equipotecnico.proyetetit','roles.roldes')
                        ->join('personal','proyectos_equipotecnico.perscod','=','personal.perscod')
                        ->join('roles','proyectos_equipotecnico.rolcod','=','roles.rolcod')                                                        
                        ->where('proycod', $proyecto) 
                        ->orderby('personal.persnom','personal.persape')                       
                        ->get();
                     
            if ($ProyectosEquipo->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosEquipo, new ProyectosEquipoTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $proyecto = $request->proyecto;
        $personal = $request->personal;

        $result = ProyectosEquipo::where('proycod', $proyecto)->where('perscod', $personal)->delete(); 
        return $result;

    }

    private function buscarCodigoIntegrante($Proyecto, $CodigoPersona){

        try{
           
            $ok = false;

            $Encontrado = ProyectosEquipo::select(DB::raw('count(perscod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('perscod',$CodigoPersona)                     
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        //print_r($data);

        if (count($data) > 0 ){

            for ($i=0; $i < count($data); $i++) {

                $Encontrado = $this->buscarCodigoIntegrante($data[$i]['proyecto'] , $data[$i]['persona']);

                if($Encontrado){

                    $dataSave = ProyectosEquipo::where('proycod', $data[$i]['proyecto'])
                                ->where('perscod', $data[$i]['persona'])
                                ->first();
                    
                    $query = "UPDATE proyectos_equipotecnico SET proyeteded = ".$data[$i]['dedicacion'].",rolcod='".$data[$i]['rol']."', 
                                proyetepor = '".$data[$i]['porcentaje']."',proyetetit = ".$data[$i]['titular']." 
                                WHERE proycod=".$data[$i]['proyecto']." AND perscod = ".$data[$i]['persona'];
                       
                    $result = DB::update($query);

                } else {

                    $dataSave = new ProyectosEquipo();

                    $dataSave->proycod    = $data[$i]['proyecto'];
                    $dataSave->perscod    = $data[$i]['persona'];
                    $dataSave->rolcod = $data[$i]['rol'];
                    $dataSave->proyeteded = $data[$i]['dedicacion'];
                    //$dataSave->proyetepor = $data[$i]['porcentaje']; 
                    //$dataSave->proyetetit = $data[$i]['titular'];

                    $result = $dataSave->save();

                }

            }

        }

        return array( 'respuesta' => $result);        

    }

    public function TotalHorasIntegrante(Request $request){

        $proyecto = $request->proyecto;
        $personal = $request->personal;

        try{
           
            $query = "SELECT p.perscod,p.pershorsem,SUM(pe.proyeteded) total
                        FROM personal p INNER JOIN proyectos_equipotecnico pe ON p.perscod = pe.perscod
                        WHERE p.perscod = ".$personal." AND pe.proycod <> ".$proyecto;
   
            $result = DB::select($query);

            return array( 'respuesta' => $result);

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    public function FiltroProyectoAll(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];

            $ProyectosEquipo = ProyectosEquipo::select('proyectos_equipotecnico.proycod','proyectos_equipotecnico.perscod', 
                                                       'personal.persnom','personal.persape','proyectos_equipotecnico.rolcod',
                                                       'proyectos_equipotecnico.proyeteded','proyectos_equipotecnico.proyetepor',
                                                       'proyectos_equipotecnico.proyetetit','roles.roldes')
                        ->join('personal','proyectos_equipotecnico.perscod','=','personal.perscod')
                        ->join('roles','proyectos_equipotecnico.rolcod','=','roles.rolcod')                                                        
                        ->where('proycod', $proyecto) 
                        ->orderby('personal.persnom','personal.persape')                       
                        ->get();
                     
            return $this->response->collection($ProyectosEquipo, new ProyectosEquipoTransformer);

        }catch(Exception $e){

            return $e->getMessage();

        }
    }
    
}

?>