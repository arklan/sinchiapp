<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosEquipoCompTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosEquipoComp;
use DB;

class ProyectosEquipoCompController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosEquipoComp = ProyectosEquipoComp::select('proyetccod','proycod','perscod','proyetcdes')                        
                        ->orderby('proyetccod')                     
                        ->get();
                     
            if ($ProyectosEquipoComp->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosEquipoComp, new ProyectosEquipoCompTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = ProyectosEquipoComp::max('proyetccod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo, $Proyecto, $Personal){

        try{
           
            $mensaje = '';

            $Duplica = ProyectosEquipoComp::select(DB::raw('count(proyetccod) as total'))                      
                      ->where('proyetcdes', $Descripcion)
                      ->where('proycod', $Proyecto)
                      ->where('perscod', $Personal)
                      ->where('proyetccod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    private function buscarCodigoCompromiso($Proyecto, $Personal, $Codigo){

        try{
           
            $ok = false;

            $Encontrado = ProyectosEquipoComp::select(DB::raw('count(proyetccod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('perscod',$Personal)                     
                      ->where('proyetccod',$Codigo)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = '';

        for ($i=0; $i < count($data); $i++) {             
        
            $codigo = $data[$i]['codigo'];
            $proyecto = $data[$i]['proyecto'];
            $personal = $data[$i]['personal'];           
            $descripcion = strtoupper(trim($data[$i]['descripcion']));
                  
            $mensaje = '';

            if($mensaje != ''){

                return $this->response->errorNotFound($mensaje);

            }

            $encontrado = $this->buscarCodigoCompromiso($proyecto, $personal, $codigo);

            if($encontrado){

                $query = "UPDATE proyectos_equipotecnico_compromisos SET proyetcdes = '".$descripcion."' 
                           WHERE proycod=".$proyecto." AND perscod = ".$personal." 
                           AND proyetccod = ".$codigo;
                $result = DB::update($query); 
            
            } else {                

                $dataAdd = new ProyectosEquipoComp();
                
                $dataAdd->proyetccod    = $codigo;
                $dataAdd->proycod       = $proyecto;
                $dataAdd->perscod       = $personal;
                $dataAdd->proyetcdes    = $descripcion;               

                $result = $dataAdd->save();
            
            }
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
            $personal = $request['personal'];
           
            $ProyectosEquipoComp = ProyectosEquipoComp::select('proyetccod','proycod','perscod','proyetcdes')                        
                        ->where('proycod', $proyecto)
                        ->where('perscod', $personal)                       
                        ->orderby('proyetccod')                     
                        ->get();
                     
            if ($ProyectosEquipoComp->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosEquipoComp, new ProyectosEquipoCompTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {        
        $proyecto = $request->proyecto;
        $personal = $request->personal;
        $codigo   = $request->codigo;

        try{

            $result = ProyectosEquipoComp::where('proycod', $proyecto)
                                           ->where('perscod', $personal)
                                           ->where('proyetccod', $codigo)->delete(); 
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }

    
}

?>