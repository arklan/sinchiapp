<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosObjetivosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosObjetivos;
use DB;

class ProyectosObjetivosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosObjetivos = ProyectosObjetivos::select('proyobjcod','proycod', 'proyobjdes','proyobjpes')                        
                        ->orderby('proyobjcod')                     
                        ->get();
                     
            if ($ProyectosObjetivos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosObjetivos, new ProyectosObjetivosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = ProyectosObjetivos::max('proyobjcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];

            $ProyectosObjetivos = ProyectosObjetivos::select('proyobjcod','proycod', 'proyobjdes','proyobjpes')                        
                        ->where('proycod', $proyecto)
                        ->orderby('proyobjcod')                     
                        ->get();
                     
            if ($ProyectosObjetivos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosObjetivos, new ProyectosObjetivosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function FiltroProyectoAll(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];

            $ProyectosObjetivos = ProyectosObjetivos::select('proyobjcod')                        
                        ->where('proycod', $proyecto)
                        ->orderby('proyobjcod')                     
                        ->get();
                     
            return $this->response->collection($ProyectosObjetivos, new ProyectosObjetivosTransformer);

        }catch(Exception $e){

            return $e->getMessage();

        }
    } 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;
        $proyecto = $request->proyecto;

        try{

            $result = ProyectosObjetivos::where('proyobjcod', $Id)->where('proycod', $proyecto)->delete();
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }    

    private function buscarCodigoObjetivo($Proyecto, $CodigoObj){

        try{
           
            $ok = false;

            $Encontrado = ProyectosObjetivos::select(DB::raw('count(proyobjcod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('proyobjcod',$CodigoObj)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        if (count($data) > 0 ){

            for ($i=0; $i < count($data); $i++) {

                $Encontrado = $this->buscarCodigoObjetivo($data[$i]['proyecto'] , $data[$i]['codigo']);

                if($Encontrado){

                    $query = "UPDATE proyectos_objetivos SET proyobjdes = '".$data[$i]['descripcion']."',
                                 proyobjpes = '".$data[$i]['peso']."'  
                                 WHERE proycod=".$data[$i]['proyecto']." AND proyobjcod = ".$data[$i]['codigo'];
                    $result = DB::update($query);             

                } else {

                    $dataSave = new ProyectosObjetivos();

                    $dataSave->proycod = $data[$i]['proyecto'];
                    $dataSave->proyobjcod = $data[$i]['codigo'];
                    $dataSave->proyobjdes = $data[$i]['descripcion'];
                    $dataSave->proyobjpes = $data[$i]['peso'];

                    $result = $dataSave->save();

                }

            }

        }

        return array( 'respuesta' => $result);        

    }
}

?>