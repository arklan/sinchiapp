<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ReunionesPersonalVisualizacionTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ReunionesPersonalVisualizacion;
use DB;

class ReunionesPersonalVisualizacionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ReunionesPersonalVisualizacion = ReunionesPersonalVisualizacion::select('reupercodvis','perscod','reucod') 
                     ->orderby('reupercodvis')                     
                     ->get();
                     
            if ($ReunionesPersonalVisualizacion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesPersonalVisualizacion, new ReunionesPersonalVisualizacionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function VisualizacionxCodReunion(Request $request)
    {       
       $CodReunion = $request->id;

       try{

            $ReunionesPersonalVisualizacion = ReunionesPersonalVisualizacion::select('reuniones_personal_visualizacion.reupercodvis','reuniones_personal_visualizacion.perscod','reuniones_personal_visualizacion.reucod', 'personal.persape','personal.persnom') 
                     ->join('personal','personal.perscod','=','reuniones_personal_visualizacion.perscod') 
                     ->where('reuniones_personal_visualizacion.reucod',$CodReunion)
                     ->orderby('personal.persnom','personal.persape')                     
                     ->get();
                     
            if ($ReunionesPersonalVisualizacion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesPersonalVisualizacion, new ReunionesPersonalVisualizacionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function consecutivo()
    {      

        $maxVal  = ReunionesPersonalVisualizacion::max('reupercodvis');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReunionesPersonalVisualizacionAdd(Request $request)
    {
        $ReunionesPersonalVisualizacion = $request->data;

        $CodReunion = $ReunionesPersonalVisualizacion['codigoreunion'];
        $visualizacion =  $ReunionesPersonalVisualizacion['visualizacion'];        

        $result = $this->destroy($CodReunion);

        foreach ($visualizacion as $row) {
            $consecutivo =$this->consecutivo();

            $ReunionVisualizaAdd = new ReunionesPersonalVisualizacion();

            $ReunionVisualizaAdd->reupercodvis = $consecutivo;           
            $ReunionVisualizaAdd->reucod = $CodReunion;
            $ReunionVisualizaAdd->perscod = $row['codigo'];
            
            $result = $ReunionVisualizaAdd->save();
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function destroy($CodigoReunion)
    {

        $result = ReunionesPersonalVisualizacion::where('reucod', $CodigoReunion)->delete(); 
        return $result;

    }
}

?>