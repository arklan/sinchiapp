<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\CargosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cargos;
use DB;

class CargosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Cargos = Cargos::select('cargcod','cargnom')
                     ->orderby('cargnom')                     
                     ->get();
                     
            if ($Cargos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Cargos, new CargosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Cargos::max('cargcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Cargos::select(DB::raw('count(cargcod) as total'))                      
                      ->where('cargnom', $Nombre)
                      ->where('cargcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cargoAddUpdate(Request $request)
    {
        $cargo = $request->data;

        $codigo = $cargo['codigo'];
        $nombre = strtoupper(trim($cargo['nombre']));       

        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $cargoAdd = new Cargos();
            
            $cargoAdd->cargcod = $consecutivo;           
            $cargoAdd->cargnom = $nombre;           
            
            $result = $cargoAdd->save();
        
        } else {

            $cargoUpd = Cargos::where('cargcod', $codigo)->first();
          
            $cargoUpd->cargnom = $nombre;          

            // Guardamos en base de datos
            $result = $cargoUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Cargos::where('cargcod', $Id)->delete(); 
        return $result;

    }
}

?>