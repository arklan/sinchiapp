<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\PilaresPiciaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PilaresPicia;
use DB;

class PilaresPiciaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $PilaresPicia = PilaresPicia::select('pilpiccod','pilpicdes')
                     ->orderby('pilpicdes')                     
                     ->get();
                     
            if ($PilaresPicia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($PilaresPicia, new PilaresPiciaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = PilaresPicia::max('pilpiccod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = PilaresPicia::select(DB::raw('count(pilpiccod) as total'))                      
                      ->where('pilpicdes', $Descripcion)
                      ->where('pilpiccod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La Descripción ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function PilaresPiciaAddUpdate(Request $request)
    {
        $PilaresPicia = $request->data;

        $codigo = $PilaresPicia['codigo'];
        $descripcion = strtoupper(trim($PilaresPicia['descripcion']));       

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $PilaresPiciaAdd = new PilaresPicia();
            
            $PilaresPiciaAdd->pilpiccod = $consecutivo;           
            $PilaresPiciaAdd->pilpicdes = $descripcion;           
            
            $result = $PilaresPiciaAdd->save();
        
        } else {

            $PilaresPiciaUpd = PilaresPicia::where('pilpiccod', $codigo)->first();
          
            $PilaresPiciaUpd->pilpicdes = $descripcion;          

            // Guardamos en base de datos
            $result = $PilaresPiciaUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = PilaresPicia::where('pilpiccod', $Id)->delete(); 
        return $result;

    }
}

?>