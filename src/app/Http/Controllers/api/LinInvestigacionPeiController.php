<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\LineasInvestigacionPeiTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LineasInvestigacionPei;
use DB;

class LinInvestigacionPeiController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{
            
            $Programas = LineasInvestigacionPei::select('lininvpeicod','lininvpeides','proinvpeicod')                        
                        ->orderby('lininvpeides')                     
                        ->get();
                     
            if ($Programas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Programas, new LineasInvestigacionPeiTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = LineasInvestigacionPei::max('lininvpeicod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = LineasInvestigacionPei::select(DB::raw('count(lininvpeicod) as total'))                      
                      ->where('lininvpeides', $Nombre)
                      ->where('lininvpeicod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo = $data['codigo'];       
        $descripcion = strtoupper(trim($data['descripcion']));
        $codigoproginv = $data['codigoproginv'];
              
        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new LineasInvestigacionPei();
            
            $dataAdd->lininvpeicod = $consecutivo;
            $dataAdd->lininvpeides = $descripcion; 
            $dataAdd->proinvpeicod = $codigoproginv;           
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = LineasInvestigacionPei::where('lininvpeicod', $codigo)->first();
          
            $dataUpd->lininvpeides = $descripcion;            
            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = LineasInvestigacionPei::where('lininvpeicod', $Id)->delete(); 
        return $result;

    }
}

?>