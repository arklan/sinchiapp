<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosProductosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosProductos;
use DB;

class ProyectosProductosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosProductos = ProyectosProductos::select('proyectos_productos.prodcod','proyectos_productos.proycod',
                                                'proyectos_productos.proyobjcod','proyectos_productos.proyproddes',
                                                'proyectos_productos.proyprodpes')
                                    ->join('proyectos_objetivos','proyectos_productos.proycod','=','proyectos_objetivos.proycod')
                                    ->where('proyectos_productos.proyobjcod', 'proyectos_objetivos.proyobjcod')                                                      
                                    ->orderby('proyectos_productos.prodcod')                     
                                    ->get();
                     
            if ($ProyectosProductos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosProductos, new ProyectosProductosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
           
            $ProyectosProductos = ProyectosProductos::select('proyectos_productos.prodcod','proyectos_productos.proycod',
                                                             'proyectos_productos.proyobjcod','proyectos_productos.proyproddes',
                                                             'proyectos_objetivos.proyobjdes','proyectos_productos.proyprodpes') 
                                     ->join('proyectos_objetivos','proyectos_productos.proyobjcod','=','proyectos_objetivos.proyobjcod')                                       
                                     ->where('proyectos_productos.proycod', $proyecto)
                                     ->where('proyectos_objetivos.proycod', $proyecto)
                                     ->orderby('proyectos_productos.prodcod')                     
                                     ->get();
                     
            if ($ProyectosProductos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosProductos, new ProyectosProductosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function FiltroProyectoAll(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
           
            $ProyectosProductos = ProyectosProductos::select('prodcod')
                                     ->where('proycod', $proyecto)                                    
                                     ->orderby('prodcod')                     
                                     ->get();
                     
            return $this->response->collection($ProyectosProductos, new ProyectosProductosTransformer); 

        }catch(Exception $e){

            return $e->getMessage();

        }
    } 

    public function ProductosxObjetivos(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
            $objetivo = $request['objetivo'];

            $ProyectosProductos = ProyectosProductos::select('proyectos_productos.prodcod','proyectos_productos.proycod',
                                                             'proyectos_productos.proyobjcod','proyectos_productos.proyproddes',
                                                             'proyectos_objetivos.proyobjdes','proyectos_productos.prodcod as codigoreal',
                                                             'proyectos_productos.proyprodpes')                                      
                                     ->join('proyectos_objetivos','proyectos_productos.proyobjcod','=','proyectos_objetivos.proyobjcod')
                                     ->where('proyectos_productos.proycod', $proyecto)
                                     ->where('proyectos_productos.proyobjcod', $objetivo)
                                     ->where('proyectos_objetivos.proycod', $proyecto)
                                     ->orderby('proyectos_productos.prodcod')  
                                     ->distinct()                   
                                     ->get();
                     
            if ($ProyectosProductos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {
                
                return $this->response->collection($ProyectosProductos, new ProyectosProductosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $producto = $request->producto;
        $proyecto = $request->proyecto;
        $objetivo = $request->objetivo;

        try{

            $result = ProyectosProductos::where('proycod', $proyecto)->where('proyobjcod', $objetivo)->where('prodcod', $producto)->delete();
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }

    private function buscarCodigoProducto($Proyecto, $CodigoObj, $CodigoProducto){

        try{
           
            $ok = false;

            $Encontrado = ProyectosProductos::select(DB::raw('count(prodcod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('proyobjcod',$CodigoObj)
                      ->where('prodcod',$CodigoProducto)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        print_r($data);

        if (count($data) > 0 ){

            for ($i=0; $i < count($data); $i++) {

                $Encontrado = $this->buscarCodigoProducto($data[$i]['proyecto'] , $data[$i]['objetivo'], $data[$i]['codigo']);

                if($Encontrado){

                    $query = "UPDATE proyectos_productos SET proyproddes = '".$data[$i]['descripcion']."',proyprodpes = '".$data[$i]['peso']."'   
                              WHERE proycod=".$data[$i]['proyecto']." AND proyobjcod = ".$data[$i]['objetivo']." AND prodcod = ".$data[$i]['codigo'];
                    $result = DB::update($query); 

                } else {

                    $dataSave = new ProyectosProductos();

                    $dataSave->proycod     = $data[$i]['proyecto'];
                    $dataSave->proyobjcod  = $data[$i]['objetivo'];
                    $dataSave->prodcod     = $data[$i]['codigo'];
                    $dataSave->proyprodpes = $data[$i]['peso'];
                    $dataSave->proyproddes = $data[$i]['descripcion'];
                   
                    $result = $dataSave->save();

                }
            }
        }

        return array( 'respuesta' => $result);        

    }
    
}

?>