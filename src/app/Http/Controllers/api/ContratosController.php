<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ContratosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contratos;
use DB;

class ContratosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Contratos = Contratos::select('contcod','conttip','contnom','conobj','confecfir','confecapr','confecini','confecter','conejedia','confeccdp','connumcdp','confecrp','connumrp','conrubpre')
                     ->orderby('contnom')                     
                     ->get();
                     
            if ($Contratos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Contratos, new ContratosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Contratos::max('contcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Contratos::select(DB::raw('count(contcod) as total'))                      
                      ->where('contnom', $Nombre)
                      ->where('contcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ContratosAddUpdate(Request $request)
    {
        $Contratos = $request->data;

        $codigo = $Contratos['codigo'];
        $Nombre = strtoupper(trim($Contratos['Nombre']));       

        $mensaje = $this->buscarDuplicado($Nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $ContratosAdd = new Contratos();
            
            $ContratosAdd->contcod = $consecutivo;           
            $ContratosAdd->contnom = $Nombre;           
            
            $result = $ContratosAdd->save();
        
        } else {

            $ContratosUpd = Contratos::where('contcod', $codigo)->first();
          
            $ContratosUpd->contnom = $Nombre;          

            // Guardamos en base de datos
            $result = $ContratosUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Contratos::where('contcod', $Id)->delete(); 
        return $result;

    }
}

?>