<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\TiposContratoTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TiposContrato;
use DB;

class TiposContratoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $TiposContrato = TiposContrato::select('tipcontcod','tipcontdes')
                     ->orderby('tipcontdes')                     
                     ->get();
                     
            if ($TiposContrato->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($TiposContrato, new TiposContratoTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = TiposContrato::max('tipcontcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = TiposContrato::select(DB::raw('count(tipcontcod) as total'))                      
                      ->where('tipcontdes', $descripcion)
                      ->where('tipcontcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                } else {

                    $Duplica = TiposContrato::select(DB::raw('count(tipcontcod) as total'))                      
                          ->where('tipcontcod','!=', $Codigo)
                          ->get();
    
                    if (!$Duplica->isEmpty()) {
                    
                        $total = $Duplica[0]->total;
        
                        if($total != '0'){
        
                            $mensaje = 'La Abreviatura ya existe';
        
                        }
                    }      
    
                }
                
            } else {

                $Duplica = TiposContrato::select(DB::raw('count(tipcontcod) as total'))                      
                      ->where('tipcontcod','!=', $Codigo)
                      ->get();

                if (!$Duplica->isEmpty()) {
                
                    $total = $Duplica[0]->total;
    
                    if($total != '0'){
    
                        $mensaje = 'La Abreviatura ya existe';
    
                    }
                }      

            }           

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo = $data['codigo'];
        $descripcion = strtoupper(trim($data['descripcion']));
              

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new TiposContrato();
            
            $dataAdd->tipcontcod = $consecutivo;           
            $dataAdd->tipcontdes = $descripcion;
                 
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = TiposContrato::where('tipcontcod', $codigo)->first();
          
            $dataUpd->tipcontdes = $descripcion;
            

            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = TiposContrato::where('tipcontcod', $Id)->delete(); 
        return $result;

    }
}

?>