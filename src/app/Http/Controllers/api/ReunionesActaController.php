<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ReunionesActaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ReunionesActa;
use DB;

class ReunionesActaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ReunionesActa = ReunionesActa::select('reuactcod', 'reuactfec', 'reuactpdf', 'reuactest','reucod') 
                     ->orderby('reuactcod')                     
                     ->get();
                     
            if ($ReunionesActa->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesActa, new ReunionesActaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function ReunionesActaxCodReunion(Request $request)
    {       
       $CodReunion = $request->id;
        
       try{

            $ReunionesActa = ReunionesActa::select('reuactcod', 'reuactfec', 'reuactpdf', 'reuactest','reucod', DB::raw('CASE WHEN reuactest = 1 THEN "POR APROBAR" WHEN reuactest = 2 THEN "RECHAZADA" WHEN reuactest = 3 THEN "APROBADA" END as reuactestdes'))                                           
                     ->where('reucod',$CodReunion)
                     ->orderby('reuactfec','DESC')                     
                     ->get();
                     
            if ($ReunionesActa->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesActa, new ReunionesActaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function consecutivo()
    {      

        $maxVal  = ReunionesActa::max('reuactcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReunionesActaAdd(Request $request)
    {
        $ReunionesActa = $request->data;                

        $codigo = $ReunionesActa['codigo'];  
        $pdf = $ReunionesActa['pdf'];
        $estado = $ReunionesActa['estado'];
        $codigoreunion = $ReunionesActa['codigoreunion'];                
        
        if($codigo=='0'){

            $success = \File::copy(public_path('temp/'.$pdf),storage_path('actareunionpdf/'.$pdf));

            $consecutivo =$this->consecutivo();

            $ReunionesActaAdd = new ReunionesActa();

            $ReunionesActaAdd->reuactcod = $consecutivo;           
            $ReunionesActaAdd->reuactfec = date('Y-m-d H:i:s');
            $ReunionesActaAdd->reuactpdf = $pdf;
            $ReunionesActaAdd->reuactest = $estado;
            $ReunionesActaAdd->reucod   = $codigoreunion;

            $result = $ReunionesActaAdd->save();
        } else {

            $ReunionesActaUpd = ReunionesActa::where('reuactcod', $codigo)->first();

            
            $ReunionesActaUpd->reuactest = $estado;

            // Guardamos en base de datos
            $result = $ReunionesActaUpd->save();
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;
        
        $result = ReunionesActa::where('reuactcod', $Id)->delete(); 
        return $result;

    }
}

?>