<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Adjunto;
use App\Pqr;
use DB;

class AdjuntoController extends BaseController
{

    public function saveAdjunto(Request $request)
    {

        $info = $request->data;

        $maxVal  = Pqr::max('id');

        if ($info['oficio'] === 'S') {

            $pqr = Pqr::find($maxVal);

            $pqr->ruta_oficio = $info['ruta'];
            $pqr->nombre_oficio = $info['nombre'];
                       
            $result = $pqr->save();

        } else {

            $adjunto = new Adjunto();
         
            $adjunto->id_pqr = $maxVal;           
            $adjunto->ruta_adjunto = $info['ruta'];
            $adjunto->nombre = $info['nombre'];
                       
            $result = $adjunto->save();

        }

        return array('respuesta' => $result);

    }

    public function updateOficio(Request $request)
    {

        $info = $request->data;
        $id = $info['id'];

        $pqr = Pqr::find($id);

        $pqr->ruta_oficio = $info['ruta'];
        $pqr->nombre_oficio = $info['nombre'];
                   
        $result = $pqr->save();

        return array('respuesta' => $result);

    }

    public function saveAdjuntofromH(Request $request)
    {

        $info = $request->data;

        $adjunto = new Adjunto();
         
        $adjunto->id_pqr = $info['id_pqr'];           
        $adjunto->ruta_adjunto = $info['ruta'];
        $adjunto->nombre = $info['nombre'];
        $adjunto->id_comentario = $info['id_comentario'];
                   
        $result = $adjunto->save();

        return array('respuesta' => $result);

    }

    public function updateAdjunto(Request $request)
    {

        $info = $request->data;

        $adjunto = new Adjunto();
         
        $adjunto->id_pqr = $info['id_pqr'];           
        $adjunto->ruta_adjunto = $info['ruta'];
        $adjunto->nombre = $info['nombre'];
                   
        $result = $adjunto->save();

        return array('respuesta' => $result);

    }

    public function listAdjuntos(Request $request)
    {
        try{

            $id = $request->id;

            $pqr = Adjunto::select('id', 'id_pqr', 'ruta_adjunto', 'nombre')                                     
                    ->where('id_pqr', $id)
                    ->get();
                     
            if ($pqr->isEmpty()) {
                
                return 'No hay datos para Mostrar';
            }
            else {

                return $pqr;

            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

}
