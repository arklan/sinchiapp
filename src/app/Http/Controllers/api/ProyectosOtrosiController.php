<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosOtrosiTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosOtrosi;
use DB;

class ProyectosOtrosiController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosOtrosi = ProyectosOtrosi::select('proycod','proyotscod', 'proyfecsus', 'proyfectereje', 'proyfecinc',
                                                       'proyvaltot','proyvalcof','proyvalctp','proyvaldin','proytotesp','proytieeje')                        
                        ->orderby('proyotscod')                     
                        ->get();
                     
            if ($ProyectosOtrosi->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosOtrosi, new ProyectosOtrosiTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo($proyecto)
    {      

        $maxVal  = ProyectosOtrosi::where('proycod',$proyecto)->max('proyotscod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarCodigo($Proyecto, $CodigoOtroSi){

        try{
           
            $ok = false;

            $Encontrado = ProyectosOtrosi::select(DB::raw('count(proyotscod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('proyotscod',$CodigoOtroSi)                      
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        //print_r($request->data);

        $result = '';

        $codigo             = $data['codigo'];
        $proyecto           = $data['proyecto'];
        $fechasuscripcion   = $data['fechasuscripcion'];
        $fechaterejecucion  = $data['fechaterejecucion'];
        $fechaincorporacion = $data['fechaincorporacion'];
        $valortotal         = $data['valortotal'];
        $cofinanciado       = $data['cofinanciado'];
        $contrapartida      = $data['contrapartida'];
        $totaldinero        = $data['totaldinero'];
        $totalespecie       = $data['totalespecie'];
        $tiempo             = $data['tiempo'];

        if($codigo != '0'){

            $query = "UPDATE proyectos_otrosi SET proyfecsus = '".$fechasuscripcion."',proyfectereje = '".$fechaterejecucion."', 
            proyfecinc = '".$fechaincorporacion."',proyvaltot = '".$valortotal."',proyvalcof = '".$cofinanciado."',
            proyvalctp = '".$contrapartida."',proyvaldin = '".$totaldinero."',proytotesp = '".$totalespecie."',
            proytieeje = '".$tiempo."' WHERE proycod=".$proyecto." AND proyotscod = ".$codigo;

            $result = DB::update($query);            

        } else {
            
            $consecutivo = $this->consecutivo($proyecto);

            $dataAdd = new ProyectosOtrosi();
                
            $dataAdd->proyotscod    = $consecutivo;
            $dataAdd->proycod       = $proyecto;
            $dataAdd->proyfecsus    = $fechasuscripcion;
            $dataAdd->proyfectereje = $fechaterejecucion;
            $dataAdd->proyfecinc    = $fechaincorporacion;
            $dataAdd->proyvaltot    = $valortotal;           
            $dataAdd->proyvalcof    = $cofinanciado;                
            $dataAdd->proyvalctp    = $contrapartida;
            $dataAdd->proyvaldin    = $totaldinero;
            $dataAdd->proytotesp    = $totalespecie;
            $dataAdd->proytieeje    = $tiempo;

            $result = $dataAdd->save();


        }       

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
           
            $ProyectosOtrosi = ProyectosOtrosi::select('proyectos_otrosi.proycod','proyectos_otrosi.proyotscod', 'proyectos_otrosi.proyfecsus',
                                                       'proyectos_otrosi.proyfectereje', 'proyectos_otrosi.proyfecinc',
                                                        'proyectos_otrosi.proyvaltot','proyectos_otrosi.proyvalcof',
                                                        'proyectos_otrosi.proyvalctp','proyectos_otrosi.proyvaldin',
                                                        'proyectos_otrosi.proytotesp','proyectos_otrosi.proytieeje',
                                                        'convenios.moncod','monedas.monsim','convenios.convtas as proytas','convenios.convnum as proynumcon',
                                                        'proyectos.proynomcor','convenios.perscod','personal.persnom','personal.persape') 
                        ->join('proyectos','proyectos_otrosi.proycod','=','proyectos.proycod')
                        ->join('convenios','proyectos.convcod','=','convenios.convcod')
                        ->join('personal','convenios.perscod','=','personal.perscod') 
                        ->join('monedas','convenios.moncod','=','monedas.moncod')                                                       
                        ->where('proyectos_otrosi.proycod', $proyecto)                        
                        ->orderby('proyectos_otrosi.proyotscod')                     
                        ->get();
                     
            if ($ProyectosOtrosi->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosOtrosi, new ProyectosOtrosiTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function MaxFecTerminacion(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
            
            $query = "SELECT proyfectereje FROM proyectos_otrosi 
                      WHERE proycod = ".$proyecto." AND proyotscod in 
                        (select max(proyotscod) from proyectos_otrosi where proycod = ".$proyecto.")";

            $result = DB::select($query);
            
            return $result;
            
        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function FechasMaxOtroSi(Request $request){

        try{
           
            $proyecto = $request['proyecto'];
            
            $query = "SELECT proyfecsus as fechasuscripcion,proyfectereje as fechaterejecucion,proyfecinc
                        FROM proyectos_otrosi 
                        WHERE proycod = ".$proyecto." AND 
                            proyotscod = (
                                    SELECT MAX(proyotscod) Maximo
                                    FROM proyectos_otrosi 
                                    WHERE proyfecsus <> '0000-00-00' AND proyfectereje <> '0000-00-00' AND proycod = ".$proyecto." 
                       )";

            $result = DB::select($query);
            
            return $result;
            
        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $codigo = $request->codigo;
        $proyecto = $request->proyecto;
     
        try{

            $result = ProyectosOtrosi::where('proycod', $proyecto)
                                     ->where('proyotscod', $codigo)
                                     ->delete(); 
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }

    
}

?>