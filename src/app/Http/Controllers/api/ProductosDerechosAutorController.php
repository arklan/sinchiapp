<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProductosDerechosAutorTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProductosDerechosAutor;
use DB;

class ProductosDerechosAutorController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProductosDerechosAutor = ProductosDerechosAutor::select('prodderautcod','prodderautdes')
                     ->orderby('prodderautdes')                     
                     ->get();
                     
            if ($ProductosDerechosAutor->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProductosDerechosAutor, new ProductosDerechosAutorTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = ProductosDerechosAutor::max('prodderautcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = ProductosDerechosAutor::select(DB::raw('count(prodderautcod) as total'))                      
                      ->where('prodderautdes', $Nombre)
                      ->where('prodderautcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DataAddUpdate(Request $request)
    {
        $ProductosDerechosAutor = $request->data;

        $codigo = $ProductosDerechosAutor['codigo'];
        $descripcion = strtoupper(trim($ProductosDerechosAutor['descripcion']));       

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $ProductosDerechosAutorAdd = new ProductosDerechosAutor();
            
            $ProductosDerechosAutorAdd->prodderautcod = $consecutivo;           
            $ProductosDerechosAutorAdd->prodderautdes = $descripcion;           
            
            $result = $ProductosDerechosAutorAdd->save();
        
        } else {

            $ProductosDerechosAutorUpd = ProductosDerechosAutor::where('prodderautcod', $codigo)->first();
          
            $ProductosDerechosAutorUpd->prodderautdes = $descripcion;          

            // Guardamos en base de datos
            $result = $ProductosDerechosAutorUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = ProductosDerechosAutor::where('prodderautcod', $Id)->delete(); 
        return $result;

    }
}

?>