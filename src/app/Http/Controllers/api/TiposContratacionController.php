<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\TiposContratacionTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TiposContratacion;
use DB;

class TiposContratacionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $TiposContratacion = TiposContratacion::select('tipconcod','tipconnom')
                     ->orderby('tipconnom')                     
                     ->get();
                     
            if ($TiposContratacion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($TiposContratacion, new TiposContratacionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = TiposContratacion::max('tipconcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($nombre, $codigo){

        try{
           
            $mensaje = '';

            $Duplica = TiposContratacion::select(DB::raw('count(tipconcod) as total'))                      
                      ->where('tipconnom', $nombre)
                      ->where('tipconcod','!=', $codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function TiposContratacionAddUpdate(Request $request)
    {
        $TiposContratacion = $request->data;

        $codigo = $TiposContratacion['codigo'];
        $nombre = strtoupper(trim($TiposContratacion['nombre']));       

        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $TiposContratacionAdd = new TiposContratacion();
            
            $TiposContratacionAdd->tipconcod = $consecutivo;           
            $TiposContratacionAdd->tipconnom = $nombre;           
            
            $result = $TiposContratacionAdd->save();
        
        } else {

            $TiposContratacionUpd = TiposContratacion::where('tipconcod', $codigo)->first();
          
            $TiposContratacionUpd->tipconnom = $nombre;          

            // Guardamos en base de datos
            $result = $TiposContratacionUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = TiposContratacion::where('tipconcod', $Id)->delete(); 
        return $result;

    }
}

?>