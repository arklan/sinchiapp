<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ReunionesAgendaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ReunionesAgenda;
use DB;

class ReunionesAgendaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ReunionesAgenda = ReunionesAgenda::select('reuagecod', 'reuagenum', 'reuagedes', 'reucod') 
                     ->orderby('reuagenum')                     
                     ->get();
                     
            if ($ReunionesAgenda->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesAgenda, new ReunionesAgendaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function ReunionesAgendaxCodReunion(Request $request)
    {       
       $CodReunion = $request->id;
        
       try{

            $ReunionesAgenda = ReunionesAgenda::select('reuagecod', 'reuagenum', 'reuagedes', 'reucod')                                           
                     ->where('reucod',$CodReunion)
                     ->orderby('reuagenum')                     
                     ->get();
                     
            if ($ReunionesAgenda->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ReunionesAgenda, new ReunionesAgendaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    public function consecutivo()
    {      

        $maxVal  = ReunionesAgenda::max('reuagecod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ReunionesAgendaAdd(Request $request)
    {
        $ReunionesAgenda = $request->data;


        $codigo = $ReunionesAgenda['codigo'];     
        $numagenda = $ReunionesAgenda['numagenda'];
        $descripcion = $ReunionesAgenda['descripcion'];
        $codigoreunion = $ReunionesAgenda['codigoreunion'];
       
        if($codigo=='0'){
            $consecutivo =$this->consecutivo();

            $ReunionesAgendaAdd = new ReunionesAgenda();

            $ReunionesAgendaAdd->reuagecod = $consecutivo;           
            $ReunionesAgendaAdd->reuagenum = $numagenda;
            $ReunionesAgendaAdd->reuagedes = $descripcion;
            $ReunionesAgendaAdd->reucod   = $codigoreunion;

            $result = $ReunionesAgendaAdd->save();
        } else {

            $ReunionesAgendaUpd = ReunionesAgenda::where('reuagecod', $codigo)->first();
         
            $ReunionesAgendaUpd->reuagenum = $numagenda;
            $ReunionesAgendaUpd->reuagedes = $descripcion;

            // Guardamos en base de datos
            $result = $ReunionesAgendaUpd->save();
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;
        
        $result = ReunionesAgenda::where('reuagecod', $Id)->delete(); 
        return $result;

    }
}

?>