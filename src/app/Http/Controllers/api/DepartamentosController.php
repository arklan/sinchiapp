<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\DepartamentosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Departamentos;
use DB;

class DepartamentosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Departamentos = Departamentos::select('depcod','depnom')                     
                     ->orderby('depnom')                     
                     ->get();
                     
            if ($Departamentos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Departamentos, new DepartamentosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Departamentos::max('depcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Departamentos::select(DB::raw('count(depcod) as total'))                      
                      ->where('depnom', $Nombre)
                      ->where('depcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DepartamentosAddUpdate(Request $request)
    {
        $Departamentos = $request->data;

        $codigo = $Departamentos['codigo'];
        $Nombre = strtoupper(trim($Departamentos['Nombre']));       

        $mensaje = $this->buscarDuplicado($Nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $DepartamentosAdd = new Departamentos();
            
            $DepartamentosAdd->depcod = $consecutivo;           
            $DepartamentosAdd->depnom = $Nombre;           
            
            $result = $DepartamentosAdd->save();
        
        } else {

            $DepartamentosUpd = Departamentos::where('depcod', $codigo)->first();
          
            $DepartamentosUpd->depnom = $Nombre;          

            // Guardamos en base de datos
            $result = $DepartamentosUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Departamentos::where('depcod', $Id)->delete(); 
        return $result;

    }
}

?>