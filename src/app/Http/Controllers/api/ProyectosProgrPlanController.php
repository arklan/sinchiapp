<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosProgrPlanTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosProgrPlan;
use DB;

class ProyectosProgrPlanController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosProgrPlan = ProyectosProgrPlan::select('proycod',                                                
                                                'proyobjcod',
                                                'proyactcod',
                                                'proyppaano',
                                                'proyppames1',
                                                'proyppames2',
                                                'proyppames3',
                                                'proyppames4',
                                                'proyppames5',
                                                'proyppames6',
                                                'proyppames7',
                                                'proyppames8',
                                                'proyppames9',
                                                'proyppames10',
                                                'proyppames11',
                                                'proyppames12',
                                                'proyppakmes1',
                                                'proyppakmes2',
                                                'proyppakmes3',
                                                'proyppakmes4',
                                                'proyppakmes5',
                                                'proyppakmes6',
                                                'proyppakmes7',
                                                'proyppakmes8',
                                                'proyppakmes9',
                                                'proyppakmes10',
                                                'proyppakmes11',
                                                'proyppakmes12',
                                                'proyobjdes')
                                    ->orderby('proyactsec')
                                    ->orderby('proyppaano')                     
                                    ->get();
                     
            if ($ProyectosProgrPlan->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosProgrPlan, new ProyectosProgrPlanTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
   
    public function SeguimientoxObjetivos(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
            $objetivo = $request['objetivo'];

            $ProyectosProgrPlan = ProyectosProgrPlan::select('proycod',                                                
                                                                'proyobjcod',
                                                                'proyactcod',
                                                                'proyppaano',
                                                                'proyppames1',
                                                                'proyppames2',
                                                                'proyppames3',
                                                                'proyppames4',
                                                                'proyppames5',
                                                                'proyppames6',
                                                                'proyppames7',
                                                                'proyppames8',
                                                                'proyppames9',
                                                                'proyppames10',
                                                                'proyppames11',
                                                                'proyppames12',
                                                                'proyppakmes1',
                                                                'proyppakmes2',
                                                                'proyppakmes3',
                                                                'proyppakmes4',
                                                                'proyppakmes5',
                                                                'proyppakmes6',
                                                                'proyppakmes7',
                                                                'proyppakmes8',
                                                                'proyppakmes9',
                                                                'proyppakmes10',
                                                                'proyppakmes11',
                                                                'proyppakmes12')
                                    ->where('proycod', $proyecto)
                                    ->where('proyobjcod', $objetivo)
                                    ->orderby('proyactsec') 
                                    ->orderby('proyppaano')                     
                                    ->get();
                     
            if ($ProyectosProgrPlan->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {
                
                return $this->response->collection($ProyectosProgrPlan, new ProyectosProgrPlanTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function ProgxObjetivoActividad(Request $request)
    {
        try{           
            
            $actividad = $request['actividad'];

            $sql = "select ps.proycod as proyecto,ps.proyobjcod as codigoobjetivo,ps.proyactsec as codigoactividad,ps.proyppaano as anno,
                    ps.proyppames1 as mes1,ps.proyppames2 as mes2,ps.proyppames3 as mes3,ps.proyppames4 as mes4,ps.proyppames5 as mes5,
                    ps.proyppames6 as mes6,ps.proyppames7 as mes7,ps.proyppames8 as mes8,ps.proyppames9 as mes9,ps.proyppames10 as mes10,
                    ps.proyppames11 as mes11,ps.proyppames12 as mes12,ps.proyppakmes1 as mesk1,ps.proyppakmes2 as mesk2,ps.proyppakmes3 as mesk3,
                    ps.proyppakmes4 as mesk4,ps.proyppakmes5 as mesk5,ps.proyppakmes6 as mesk6,ps.proyppakmes7 as mesk7,ps.proyppakmes8 as mesk8,
                    ps.proyppakmes9 as mesk9,ps.proyppakmes10 as mesk10,ps.proyppakmes11 as mesk11,ps.proyppakmes12 as mesk12,
                    pa.proyactdes as txtactividad,pa.proyactpes as peso,pa.proyactkpi as kpi,pa.proyactdeskpi as descripcionkpi
                    FROM proyectos_programacion_planaccion ps inner join proyectos_actividades pa ON ps.proyactsec = pa.proyactsec
                    where ps.proyactsec = ".$actividad." order by ps.proyactcod,ps.proyppaano ";
                   

            $ProyectosProgrPlan = DB::select($sql); 
            
            return $ProyectosProgrPlan;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {        
        $proyecto = $request->proyecto;
        $objetivo = $request->objetivo;
        $actividad = $request->actividad;
        $anno = $request->anno;

        try{

            $result = ProyectosProgrPlan::where('proycod', $proyecto)->where('proyobjcod', $objetivo)
                                            ->where('proyactcod', $actividad)
                                            ->where('proyppaano', $anno)->delete();
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }

    private function buscarDataExiste($proyecto, $objetivo, $actividad, $anno){

        try{
           
            $ok = false;

            $Encontrado = ProyectosProgrPlan::select(DB::raw('count(proyobjcod) as total'))
                      ->where('proycod', $proyecto)
                      ->where('proyobjcod',$objetivo)
                      ->where('proyactcod',$actividad)
                      ->where('proyppaano',$anno)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    private function buscarDataExisteSec($actividad, $anno){

        try{
           
            $ok = false;

            $Encontrado = ProyectosProgrPlan::select(DB::raw('count(proyobjcod) as total'))
                      ->where('proyactsec', $actividad)                     
                      ->where('proyppaano',$anno)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        if (count($data) > 0 ){

            for ($i=0; $i < count($data); $i++) {

                $Encontrado = $this->buscarDataExisteSec($data[$i]['codigoactividad'], $data[$i]['anno']);

                if($Encontrado){

                    $query = "UPDATE proyectos_programacion_planaccion SET proyppames1 = ".$data[$i]['mes1'].",
                                proyppames2 = ".$data[$i]['mes2'].",proyppames3 = ".$data[$i]['mes3'].",
                                proyppames4 = ".$data[$i]['mes4'].",proyppames5 = ".$data[$i]['mes5'].",
                                proyppames6 = ".$data[$i]['mes6'].",proyppames7 = ".$data[$i]['mes7'].",
                                proyppames8 = ".$data[$i]['mes8'].",proyppames9 = ".$data[$i]['mes9'].",
                                proyppames10 = ".$data[$i]['mes10'].",proyppames11 = ".$data[$i]['mes11'].",
                                proyppames12 = ".$data[$i]['mes12'].",proyppakmes1 = ".$data[$i]['mesk1'].",
                                proyppakmes2 = ".$data[$i]['mesk2'].",proyppakmes3 = ".$data[$i]['mesk3'].",
                                proyppakmes4 = ".$data[$i]['mesk4'].",proyppakmes5 = ".$data[$i]['mesk5'].",
                                proyppakmes6 = ".$data[$i]['mesk6'].",proyppakmes7 = ".$data[$i]['mesk7'].",
                                proyppakmes8 = ".$data[$i]['mesk8'].",proyppakmes9 = ".$data[$i]['mesk9'].",
                                proyppakmes10 = ".$data[$i]['mesk10'].",proyppakmes11 = ".$data[$i]['mesk11'].",
                                proyppakmes12 = ".$data[$i]['mesk12']."
                                WHERE proycod=".$data[$i]['proyecto']." 
                              AND proyactsec = ".$data[$i]['codigoactividad']."  AND proyppaano = ".$data[$i]['anno'] ;
                    $result = DB::update($query); 

                } else {

                    $dataSave = new ProyectosProgrPlan();

                    $dataSave->proycod    = $data[$i]['proyecto'];
                    $dataSave->proyobjcod = $data[$i]['codigoobjetivo'];
                    $dataSave->proyactcod = $data[$i]['codigoactividad'];
                    $dataSave->proyppaano = $data[$i]['anno'];
                    $dataSave->proyppames1 = $data[$i]['mes1'];
                    $dataSave->proyppames2 = $data[$i]['mes2'];
                    $dataSave->proyppames3 = $data[$i]['mes3'];
                    $dataSave->proyppames4 = $data[$i]['mes4'];
                    $dataSave->proyppames5 = $data[$i]['mes5'];
                    $dataSave->proyppames6 = $data[$i]['mes6'];
                    $dataSave->proyppames7 = $data[$i]['mes7'];
                    $dataSave->proyppames8 = $data[$i]['mes8'];
                    $dataSave->proyppames9 = $data[$i]['mes9'];
                    $dataSave->proyppames10 = $data[$i]['mes10'];
                    $dataSave->proyppames11 = $data[$i]['mes11'];
                    $dataSave->proyppames12 = $data[$i]['mes12'];
                    $dataSave->proyppakmes1 = $data[$i]['mesk1'];
                    $dataSave->proyppakmes2 = $data[$i]['mesk2'];
                    $dataSave->proyppakmes3 = $data[$i]['mesk3'];
                    $dataSave->proyppakmes4 = $data[$i]['mesk4'];
                    $dataSave->proyppakmes5 = $data[$i]['mesk5'];
                    $dataSave->proyppakmes6 = $data[$i]['mesk6'];
                    $dataSave->proyppakmes7 = $data[$i]['mesk7'];
                    $dataSave->proyppakmes8 = $data[$i]['mesk8'];
                    $dataSave->proyppakmes9 = $data[$i]['mesk9'];
                    $dataSave->proyppakmes10 = $data[$i]['mesk10'];
                    $dataSave->proyppakmes11 = $data[$i]['mesk11'];
                    $dataSave->proyppakmes12 = $data[$i]['mesk12'];

                    $result = $dataSave->save();

                }
            }
        }

        return array( 'respuesta' => $result);        

    }

    public function AddBasico(Request $request)
    {
        $data = $request->data;
        
        //print_r($data);

        $proyecto = $data['proyecto'];
        $objetivo = $data['objetivo'];
        $actividad = $data['actividad'];
        $annoini = $data['annoini'];
        $annofin = $data['annofin'];

        $result = true;

        for ($i=$annoini; $i <= $annofin; $i++) {
            
            $Encontrado = $this->buscarDataExisteSec($actividad, $i);

            if(!$Encontrado){

                $dataSave = new ProyectosProgrPlan();

                $dataSave->proycod    = $proyecto;
                $dataSave->proyobjcod = $objetivo;
                $dataSave->proyactsec = $actividad;
                $dataSave->proyppaano = $i;

                $result = $dataSave->save();
            }    

        }

        return array( 'respuesta' => $result);        

    }
    
}

?>