<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\PersonalTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Personal;
use DB;

class PersonalController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Personal = Personal::select('personal.perscod','personal.tipidecod','personal.perside','personal.persape',
                                    'personal.persnom','personal.cargcod','personal.arecod','personal.persdir',
                                    'personal.perstel','personal.ciucod','personal.persema','cargos.cargnom','areas.arenom',
                                    'ciudades.ciunom','departamentos.depnom','tipos_identificacion.tipidedes',
                                    'personal.persest','estados.estdes','personal.tipcontcod','personal.pershorsem',
                                    'tipos_contratos.tipcontdes','personal.perssex','personal.persfecnac',
                                    'personal.perssalbas')
                     ->join('tipos_identificacion','personal.tipidecod','=','tipos_identificacion.tipidecod')
                     ->join('cargos','personal.cargcod','=','cargos.cargcod')
                     ->join('areas','personal.arecod','=','areas.arecod')
                     ->join('ciudades','personal.ciucod','=','ciudades.ciucod')
                     ->join('departamentos','ciudades.depcod','=','departamentos.depcod')
                     ->join('estados','personal.persest','=','estados.estcod')
                     ->join('tipos_contratos','personal.tipcontcod','=','tipos_contratos.tipcontcod')
                     ->orderby('persnom','persape')
                     ->get();
                     
            if ($Personal->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Personal, new PersonalTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function FiltroXEstado(Request $request)
    {      
        $estado = $request['estado'];
        
       try{           

            $Personal = Personal::select('personal.perscod','personal.tipidecod','personal.perside','personal.persape',
                                    'personal.persnom','personal.cargcod','personal.arecod','personal.persdir',
                                    'personal.perstel','personal.ciucod','personal.persema','cargos.cargnom','areas.arenom',
                                    'ciudades.ciunom','departamentos.depnom','tipos_identificacion.tipidedes',
                                    'personal.persest','estados.estdes','personal.tipcontcod','personal.pershorsem',
                                    'tipos_contratos.tipcontdes','personal.perssex','personal.persfecnac',
                                    'personal.perssalbas')
                     ->join('tipos_identificacion','personal.tipidecod','=','tipos_identificacion.tipidecod')
                     ->join('cargos','personal.cargcod','=','cargos.cargcod')
                     ->join('areas','personal.arecod','=','areas.arecod')
                     ->join('ciudades','personal.ciucod','=','ciudades.ciucod')
                     ->join('departamentos','ciudades.depcod','=','departamentos.depcod')
                     ->join('estados','personal.persest','=','estados.estcod')
                     ->join('tipos_contratos','personal.tipcontcod','=','tipos_contratos.tipcontcod')
                     ->where('personal.persest', $estado)
                     ->orderby('persnom','persape')
                     ->get();
                     
            if ($Personal->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Personal, new PersonalTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function FiltroXCargo(Request $request)
    {       
       $cargo = $request['cargo'];

       try{

            $Personal = Personal::select('personal.perscod','personal.tipidecod','personal.perside','personal.persape',
                                    'personal.persnom','personal.cargcod','personal.arecod','personal.persdir',
                                    'personal.perstel','personal.ciucod','personal.persema','cargos.cargnom','areas.arenom',
                                    'ciudades.ciunom','departamentos.depnom','tipos_identificacion.tipidedes',
                                    'personal.persest','estados.estdes','personal.tipcontcod','personal.pershorsem',
                                    'tipos_contratos.tipcontdes','personal.perssex','personal.persfecnac',
                                    'personal.perssalbas')
                     ->join('tipos_identificacion','personal.tipidecod','=','tipos_identificacion.tipidecod')
                     ->join('cargos','personal.cargcod','=','cargos.cargcod')
                     ->join('areas','personal.arecod','=','areas.arecod')
                     ->join('ciudades','personal.ciucod','=','ciudades.ciucod')
                     ->join('departamentos','ciudades.depcod','=','departamentos.depcod')
                     ->join('estados','personal.persest','=','estados.estcod')
                     ->join('tipos_contratos','personal.tipcontcod','=','tipos_contratos.tipcontcod')
                     ->where('personal.cargcod', $cargo)
                     ->orderby('persnom','persape')                     
                     ->get();
                     
            if ($Personal->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Personal, new PersonalTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function FiltroXId(Request $request)
    {       
       $codigo = $request['codigo'];

       try{

            $Personal = Personal::select('personal.perscod','personal.tipidecod','personal.perside','personal.persape',
                                    'personal.persnom','personal.cargcod','personal.arecod','personal.persdir',
                                    'personal.perstel','personal.ciucod','personal.persema','cargos.cargnom','areas.arenom',
                                    'ciudades.ciunom','departamentos.depnom','tipos_identificacion.tipidedes',
                                    'personal.persest','estados.estdes','personal.tipcontcod','personal.pershorsem',
                                    'tipos_contratos.tipcontdes','personal.perssex','personal.persfecnac',
                                    'personal.perssalbas')
                     ->join('tipos_identificacion','personal.tipidecod','=','tipos_identificacion.tipidecod')
                     ->join('cargos','personal.cargcod','=','cargos.cargcod')
                     ->join('areas','personal.arecod','=','areas.arecod')
                     ->join('ciudades','personal.ciucod','=','ciudades.ciucod')
                     ->join('departamentos','ciudades.depcod','=','departamentos.depcod')
                     ->join('estados','personal.persest','=','estados.estcod')
                     ->join('tipos_contratos','personal.tipcontcod','=','tipos_contratos.tipcontcod')
                     ->where('personal.perscod', $codigo)
                     ->orderby('persnom','persape')                     
                     ->get();
                     
            if ($Personal->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Personal, new PersonalTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Personal::max('perscod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre,$Apellido, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Personal::select(DB::raw('count(perscod) as total'))                      
                      ->where('persnom', $Nombre)
                      ->where('persape', $Apellido)
                      ->where('perscod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo = $data['codigo'];
        $tipoid = $data['tipoid'];
        $identificacion = $data['identificacion'];
        $apellido = $data['apellido'];
        $nombre = $data['nombre'];
        $codigocargo = $data['codigocargo'];
        $codigoarea = $data['codigoarea'];
        $direccion = $data['direccion'];
        $telefono = $data['telefono'];
        $codigociudad = $data['codigociudad'];
        $correo = $data['correo'];
        $codigoestado = $data['codigoestado'];
        $codigotipocont = $data['codigotipocont'];
        $horasemanales = $data['horasemanales'];
        $sexo = $data['sexo'];
        $fechanacimiento = $data['fechanacimiento'];
        $salariobasico = $data['salariobasico'];

        $mensaje = $this->buscarDuplicado($nombre,$apellido, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new Personal();
            
            $dataAdd->perscod = $consecutivo;           
            $dataAdd->tipidecod = $tipoid;
            $dataAdd->perside = $identificacion; 
            $dataAdd->persape = $apellido;
            $dataAdd->persnom = $nombre; 
            $dataAdd->cargcod = $codigocargo;
            $dataAdd->arecod = $codigoarea;
            $dataAdd->persdir = $direccion;
            $dataAdd->perstel = $telefono;
            $dataAdd->ciucod = $codigociudad;
            $dataAdd->persema = $correo;
            $dataAdd->persest = $codigoestado; 
            $dataAdd->tipcontcod = $codigotipocont;
            $dataAdd->pershorsem = $horasemanales; 
            $dataAdd->perssex = $sexo;            
            $dataAdd->persfecnac = $fechanacimiento;
            $dataAdd->perssalbas = $salariobasico;
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = Personal::where('perscod', $codigo)->first();
          
            $dataUpd->tipidecod = $tipoid;
            $dataUpd->perside = $identificacion; 
            $dataUpd->persape = $apellido;
            $dataUpd->persnom = $nombre; 
            $dataUpd->cargcod = $codigocargo;
            $dataUpd->arecod = $codigoarea;
            $dataUpd->persdir = $direccion;
            $dataUpd->perstel = $telefono;
            $dataUpd->ciucod = $codigociudad;
            $dataUpd->persema = $correo;
            $dataUpd->persest = $codigoestado; 
            $dataUpd->tipcontcod = $codigotipocont;
            $dataUpd->pershorsem = $horasemanales;
            $dataUpd->perssex = $sexo; 
            $dataUpd->persfecnac = $fechanacimiento;
            $dataUpd->perssalbas = $salariobasico;

            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function perfilAddUpdate(Request $request)
    {
        $perfil = $request->data;

        $codigo = $perfil['codigo'];
        $nombre = strtoupper($perfil['nombre']);       

        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $perfilAdd = new Personal();
            
            $perfilAdd->perscod = $consecutivo;           
            $perfilAdd->persnom = $nombre;
                       
            $result = $perfilAdd->save();
        
        } else {

            $perfilUpd = Personal::where('perscod', $codigo)->first();          
           
            $perfilUpd->persnom = $nombre;          

            // Guardamos en base de datos
            $result = $perfilUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    public function actualizarPermisosPerfil(Request $request)
    {        

        $perfilid = $request->perfilid;
        $permisos = $request->data;
        
        //se elimina los permisos actuales del usaurio
        $result = DB::table('opciones_Personal')->where(['perscod' => $perfilid])->delete();
        
        if(count($permisos) > 0){
            foreach ($permisos as $value) {
                    
                $opciones[] = 
                    [
                        'perscod' => $value['perscod'],
                        'opccod'  => $value['opccod'],                    
                    ];           
            }
            //inserts
            $result = DB::table('opciones_Personal')->insert($opciones);
        }

        return array( 'respuesta' => $result);          
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Personal::where('perscod', $Id)->delete(); 
        return $result;

    }
}

?>