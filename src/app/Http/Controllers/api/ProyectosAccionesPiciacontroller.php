<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosAccionesPiciaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosAccionesPicia;
use DB;

class ProyectosAccionesPiciaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosAccionesPicia = ProyectosAccionesPicia::select('proycod','accpiccod')                        
                        ->orderby('accpiccod')                     
                        ->get();
                     
            if ($ProyectosAccionesPicia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosAccionesPicia, new ProyectosAccionesPiciaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = '';

        for ($i=0; $i < count($data); $i++) {             
        
            $codigo = $data[$i]['codigo'];
            $proyecto = $data[$i]['proyecto'];            
            
            if($i==0){

                $result = ProyectosAccionesPicia::where('proycod', $proyecto)->delete();
            }

            $dataAdd = new ProyectosAccionesPicia();
            
            $dataAdd->accpiccod    = $codigo;
            $dataAdd->proycod      = $proyecto;            

            $result = $dataAdd->save();            
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
                     
            $ProyectosAccionesPicia = ProyectosAccionesPicia::select('accpiccod','proycod')                        
                        ->where('proycod', $proyecto)                        
                        ->orderby('accpiccod')                     
                        ->get();
            
           return $this->response->collection($ProyectosAccionesPicia, new ProyectosAccionesPiciaTransformer);

            /*if ($ProyectosAccionesPicia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosAccionesPicia, new ProyectosAccionesPiciaTransformer);                
            }*/

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function seleccionadosProyectos(Request $request)
    {
        try{
            
            $proyecto = $request['proyecto'];
            $linvinv = $request['linvinv'];            

            $sql = "SELECT l.accpiccod codigo,l.accpicdes descripcion, IF(p.proycod IS NULL,0,1) Activo 
                    FROM acciones_picia l LEFT JOIN 
                        proyectos_acciones_picia p ON l.accpiccod = p.accpiccod AND p.proycod=".$proyecto."   
                    WHERE l.lininvpiccod IN ".$linvinv."
                    ORDER BY  l.accpiccod";
            
            $result = DB::select($sql);            
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
}

?>