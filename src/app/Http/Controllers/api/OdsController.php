<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\OdsTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ods;
use DB;

class OdsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Ods = Ods::select('ods.odscod','ods.odsdesc','ods.objcod','objetivos.objdesc') 
                     ->join('objetivos','ods.objcod','=','objetivos.objcod') 
                     ->orderby('odsdesc')                     
                     ->get();
                     
            if ($Ods->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Ods, new OdsTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function FiltroXObjetivo(Request $request)
    {       
       try{

            $objetivo = $request->objetivo;

            $Ods = Ods::select('ods.odscod','ods.odsdesc','ods.objcod','objetivos.objdesc') 
                     ->join('objetivos','ods.objcod','=','objetivos.objcod') 
                     ->where('ods.objcod',$objetivo)
                     ->orderby('ods.odsdesc')                     
                     ->get();
                     
            if ($Ods->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Ods, new OdsTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Ods::max('odscod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($descripcion, $codigo){

        try{
           
            $mensaje = '';

            $Duplica = Ods::select(DB::raw('count(odscod) as total'))                      
                      ->where('odsdesc', $descripcion)
                      ->where('odscod','!=', $codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El descripcion ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function OdsAddUpdate(Request $request)
    {
        $Ods = $request->data;

        $codigo = $Ods['codigo'];
        $descripcion = strtoupper(trim($Ods['descripcion']));       
        $codigoobjetivo = $Ods['codigoobjetivo'];

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $OdsAdd = new Ods();
            
            $OdsAdd->odscod  =  $consecutivo;           
            $OdsAdd->odsdesc =  $descripcion;
            $OdsAdd->objcod  =  $codigoobjetivo;           
            
            $result = $OdsAdd->save();
        
        } else {

            $OdsUpd = Ods::where('odscod', $codigo)->first();
          
            $OdsUpd->odsdesc = $descripcion; 
            $OdsUpd->objcod  =  $codigoobjetivo;         

            // Guardamos en base de datos
            $result = $OdsUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Ods::where('odscod', $Id)->delete(); 
        return $result;

    }
}

?>