<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SolProceso;
use App\SolicitudesObligaciones;
use App\SolicitudesInformes;
use App\SolicitudesCiudades;
use App\SolicitudesSupervisores;
use App\SolProcNecesidades;
use App\SolProcPagos;
use App\PagosInfoProd;
use App\SolProcEducacion;
use App\SolProcExperiencia;
use App\SolProcAdjuntos;
use App\SolProcProponentes;
use App\SolProcExpProp;
use App\SolProcRoles;
use App\SolProcRolEdu;
use App\SolProcRolExp;
use App\SolProcExpFin;
use App\SolProcExpOrg;
use App\SolProcExpMet;
use App\SolProcCriDes;

use DB;

class SolProcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getSupport(Request $request)
    {
        // return $request;
        $id = $request->id;
        $tipo = $request->tipo;
        $soportes = DB::reconnect('mysql')->select(DB::raw("SELECT id, id_exp as idExp, tipo, fecha_ini as dateIni,fecha_fin as dateEnd, file_name, file_path, descripcion FROM prop_soportes WHERE id_exp = $id AND tipo = '$tipo'"));
        foreach($soportes as $soporte) {
            $soporte->adjunto['file_name'] = $soporte->file_name;
            $soporte->adjunto['file_path'] = $soporte->file_path;
        }
        return $soportes;
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeSupport(Request $request)
    {
        // return $request;
        $items = $request->data;
        $cadena = getIds($items, 'id');
        $idExp = $items[0]['idExp'];
        DB::reconnect('mysql')->delete(DB::raw("DELETE FROM prop_soportes WHERE id NOT IN ($cadena) AND id_exp = $idExp"));
        foreach ($items as $item) {
            if (isset($item['id'])) {
                $id = $item['id'];
                $tipo = $item['tipo'];
                $fechaIni = $item['dateIni'];
                $fechaFin = $item['dateEnd'];
                $file_name = $item['adjunto']['file_name'];
                $file_path = $item['adjunto']['file_path'];
                $descripcion = $item['descripcion'];
                DB::reconnect('mysql')->update(DB::raw("UPDATE prop_soportes SET tipo = '$tipo', fecha_ini = '$fechaIni', fecha_fin = '$fechaFin', file_name = '$file_name', file_path = '$file_path', descripcion = '$descripcion' WHERE id = $id")); 
            } else {
                $id = $item['idExp'];
                $tipo = $item['tipo'];
                $fechaIni = $item['dateIni'];
                $fechaFin = $item['dateEnd'];
                $file_name = $item['adjunto']['file_name'];
                $file_path = $item['adjunto']['file_path'];
                $descripcion = $item['descripcion'];
                DB::reconnect('mysql')->insert(DB::raw("INSERT INTO prop_soportes (id_exp,tipo,fecha_ini,fecha_fin,file_name,file_path,descripcion) VALUES ($id, '$tipo', '$fechaIni', '$fechaFin', '$file_name', '$file_path', '$descripcion')"));
            }
        }
        return $items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inSolProc = $request->solproc;
        $idSol = $inSolProc['idSolContratacion'];
        $obligaciones = isset($inSolProc['obligaciones']) ? $inSolProc['obligaciones'] : [];
        $infoprod = isset($inSolProc['infoprod']) ? $inSolProc['infoprod'] : [];
        $ciudades = isset($inSolProc['ciudad']) ? $inSolProc['ciudad'] : [];
        $supervisores = isset($inSolProc['supervisor']) ? $inSolProc['supervisor'] : [];
        $necesidades = isset($inSolProc['necesidades']) ? $inSolProc['necesidades'] : [];
        $pagos = isset($inSolProc['pagos']) ? $inSolProc['pagos'] : [];
        $educacion = isset($inSolProc['educacion']) ? $inSolProc['educacion'] : [];
        $experiencia = isset($inSolProc['experiencia']) ? $inSolProc['experiencia'] : [];
        $adjuntos = isset($inSolProc['adjuntos']) ? $inSolProc['adjuntos'] : [];
        $proponentes = isset($inSolProc['proponentes']) ? $inSolProc['proponentes'] : [];
        $expProp = isset($inSolProc['expProp']) ? $inSolProc['expProp'] : [];
        $roles = isset($inSolProc['roles']) ? $inSolProc['roles'] : [];
        $expFin = isset($inSolProc['expFin']) ? $inSolProc['expFin'] : [];
        $expMet = isset($inSolProc['expMet']) ? $inSolProc['expMet'] : [];
        $expOrg = isset($inSolProc['expOrg']) ? $inSolProc['expOrg'] : [];
        $criDes = isset($inSolProc['criDes']) ? $inSolProc['criDes'] : [];
        // creando el proceso 
        $solproc = new SolProceso;
        $solproc->id_solcontratacion = $idSol;
        $solproc->obj_contractual = isset($inSolProc['objContractual']) ? $inSolProc['objContractual'] : '';
        $solproc->fecha_limite = isset($inSolProc['fechaLimite']) ? $inSolProc['fechaLimite'] : '';
        $solproc->tiempo_asignado = isset($inSolProc['tiempoAsignado']) ? $inSolProc['tiempoAsignado'] : '';
        $solproc->fecha_terminacion = isset($inSolProc['fechaTerminacion']) ? $inSolProc['fechaTerminacion']: '';
        $solproc->inc_gastos = isset($inSolProc['incGastos']) ? ($inSolProc['incGastos'] == true ? 1 : 0) : 0;
        $solproc->tar_sinchi = isset($inSolProc['tarSinchi']) ? ($inSolProc['tarSinchi'] == true ? 1 : 0) : 0;
        $solproc->val_gastos_viaje = isset($inSolProc['valGastosViaje']) ? $inSolProc['valGastosViaje'] : '';
        $solproc->tipo_contratacion = isset($inSolProc['tipoContratacion']) ? $inSolProc['tipoContratacion'] : '';
        $solproc->tipo_proc = isset($inSolProc['tipoProc']) ? $inSolProc['tipoProc']: '';
        $solproc->nombre = isset($inSolProc['nombre']) ? $inSolProc['nombre'] : '';
        $solproc->correo = isset($inSolProc['correo']) ? $inSolProc['correo'] : '';
        $solproc->rol = isset($inSolProc['rol']) ? $inSolProc['rol']: '';
        $solproc->telefono = isset($inSolProc['telefono']) ? $inSolProc['telefono'] : '';
        $solproc->causal = isset($inSolProc['causal']) ? $inSolProc['causal'] : '';
        $solproc->justificacion = isset($inSolProc['justificacion']) ? $inSolProc['justificacion'] : '';
        $solproc->caracteristicas_bs = isset($inSolProc['caracteristicasBS']) ? $inSolProc['caracteristicasBS'] : '';
        $solproc->acces_info_conf = isset($inSolProc['accesInfoConf']) ? ($inSolProc['accesInfoConf'] == true ? 1 : 0) : 0;
        $solproc->of_economica = isset($inSolProc['odEconomica']) ? $inSolProc['odEconomica'] : '';
        $solproc->porc_of_eco = isset($inSolProc['porcOfEco']) ? $inSolProc['porcOfEco'] : '';
        $solproc->peso_prop = isset($inSolProc['pesoProp']) ? $inSolProc['pesoProp'] : '';
        $solproc->peso_equ = isset($inSolProc['pesoEqu']) ? $inSolProc['pesoEqu'] : '';
        $solproc->peso_fin = isset($inSolProc['pesoFin']) ? $inSolProc['pesoFin'] : '';
        $solproc->peso_org = isset($inSolProc['pesoOrg']) ? $inSolProc['pesoOrg'] : '';
        $solproc->peso_met = isset($inSolProc['pesoMet']) ? $inSolProc['pesoMet'] : '';
        $solproc->peso_eco = isset($inSolProc['pesoEco']) ? $inSolProc['pesoEco'] : ''; 
        $solproc->anticipo = isset($inSolProc['anticipo']) ? $inSolProc['anticipo'] : ''; 
        $solproc->contrapartida = isset($inSolProc['contrapartida']) ? $inSolProc['contrapartida'] : ''; 
        $solproc->conv_valor = isset($inSolProc['convValor']) ? $inSolProc['convValor'] : ''; 
        $solproc->conv_especie = isset($inSolProc['convEspecie']) ? $inSolProc['convEspecie']: ''; 
        $solproc->conv_dinero = isset($inSolProc['convDinero']) ? $inSolProc['convDinero']: ''; 
        $solproc->convenio_marco = isset($inSolProc['convenioMarco']) ? $inSolProc['convenioMarco'] : ''; 
        $solproc->val_arl = isset($inSolProc['valArl']) ? $inSolProc['valArl'] : ''; 
        $solproc->fecha_creacion = date('Y-m-d H:i:s');
        $solproc->save(); 
        if (isset($solproc->id)) {
            if(count($necesidades) > 0) {
                // creando necesidades
                for ($i=0; $i < count($necesidades); $i++) {
                    $dataAdd = new SolProcNecesidades();
                    $dataAdd->id_solproc = $solproc->id;           
                    $dataAdd->id_sol_necesidades = $necesidades[$i]['idsn'];
                    $dataAdd->valor = isset($necesidades[$i]['solval']) ? $necesidades[$i]['solval'] : 0;
                    $dataAdd->save();
                }
            }
            // Insert obligaciones
            if(count($obligaciones) > 0) {
                for ($i=0; $i < count($obligaciones); $i++) {
                    $dataAdd = new SolicitudesObligaciones();        
                    $dataAdd->descripcion = $obligaciones[$i]['obligacion'];
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->save();
                }
            }
    
            // Insert informes
            if(count($infoprod) > 0) {
                for ($i=0; $i < count($infoprod); $i++) {
                    $dataAdd = new SolicitudesInformes();        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->titulo = $infoprod[$i]['titulo'];
                    $dataAdd->tipo = $infoprod[$i]['tipo'];
                    $dataAdd->descripcion = $infoprod[$i]['descripcion'];
                    $dataAdd->save();
                }
            }

            // Insert ciudades
            if(count($ciudades) > 0) {
                for ($i=0; $i < count($ciudades); $i++) {
                    $dataAdd = new SolicitudesCiudades();        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->codigo = $ciudades[$i];
                    $dataAdd->save();
                }
            }
    
            // Insert supervisores
            if(count($supervisores) > 0) {
                for ($i=0; $i < count($supervisores); $i++) {
                    $dataAdd = new SolicitudesSupervisores();        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->codigo = $supervisores[$i];
                    $dataAdd->save();
                }
            }
            // Insert pagos
            if(count($pagos) > 0) {
                $solinfpro = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM solproc_infoprod WHERE id_solproc = '.$solproc->id));
                for ($i=0; $i < count($pagos); $i++) {
                    $dataAdd = new SolProcPagos;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tiempo_ind = $pagos['tiempoInd'];
                    $dataAdd->tiempo_und = $pagos['tiempoUnd'];
                    $dataAdd->dias = $pagos['dias'];
                    $dataAdd->porc_pago = $pagos['porcPago'];
                    $dataAdd->save();
                    $idPago = $dataAdd->id;
                    $pagosIP = $pagos['infoprod']; // es una array
                    for ($i=0; $i <count($pagosIP) ; $i++) {
                        for($j=0; $j <count($solinfpro) ; $j++) {
                            if ($solinfpro[$j]['titulo'] === $pagosIP[$i]['titulo'] && $solinfpro[$j]['tipo'] === $pagosIP[$i]['tipo']) {
                                $dataChild = new PagosInfoProd;
                                $dataChild->id_solpagos = $idPago;
                                $dataChild->id_infoprod = $solinfpro[$j]['id'];
                                $dataChild->id_solproc = $solproc->id;
                                $dataChild->save();
                            }
                        }
                    }
                }
            }
            // Ingresa educacion en solproc
            if(count($educacion) > 0) {
                for ($i=0; $i < count($educacion); $i++) {
                    $dataAdd = new SolProcEducacion;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tipo_educacion = $educacion[$i]['tipoEd'];
                    $dataAdd->tipo = $educacion[$i]['tipo'];
                    $dataAdd->titulo = $educacion[$i]['titulo'];
                    $dataAdd->cumple = isset($educacion[$i]['cumple']) ? ($educacion[$i]['cumple'] == true ? 1 : 0) : 0;
                    $dataAdd->no_aplica = isset($educacion[$i]['noAplica']) ? ($educacion[$i]['noAplica'] == true ? 1 : 0) : 0;
                    $dataAdd->observacion = isset($educacion[$i]['observacion']) ? $educacion[$i]['observacion'] : '';
                    $dataAdd->save();
                }
            }
             // Ingresa experiencia en solproc
            if(count($experiencia) > 0) {
                for ($i=0; $i < count($experiencia); $i++) {
                    $dataAdd = new SolProcExperiencia;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tipo_exp = $experiencia[$i]['tipoExp'];
                    $dataAdd->descripcion = $experiencia[$i]['descripcion'];
                    $dataAdd->tiempo = $experiencia[$i]['tiempo'];
                    $dataAdd->cumple = isset($experiencia[$i]['cumple']) ? ($experiencia[$i]['cumple'] == true ? 1 : 0) : 0;
                    $dataAdd->no_aplica = isset($experiencia[$i]['noAplica']) ? ($experiencia[$i]['noAplica'] == true ? 1 : 0) : 0;
                    $dataAdd->observacion = isset($experiencia[$i]['observacion']) ? $experiencia[$i]['observacion'] : '';
                    $dataAdd->save();
                }
            }
            // guardando adjuntos de solproc
            if(count($adjuntos) > 0) {
                for ($i=0; $i < count($adjuntos); $i++) {
                    $dataAdd = new SolProcAdjuntos;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->doc_solicitado = isset($adjuntos[$i]['docSolicitado']) ? $adjuntos[$i]['docSolicitado'] : '';
                    $dataAdd->tipo = isset($adjuntos[$i]['tipo']) ? $adjuntos[$i]['tipo'] : '';
                    $dataAdd->nombre = isset($adjuntos[$i]['nombre']) ? $adjuntos[$i]['nombre'] : '';
                    $dataAdd->path = isset($adjuntos[$i]['path']) ? $adjuntos[$i]['path'] : '';
                    $dataAdd->aprobado = isset($adjuntos[$i]['aprobado']) ? ($adjuntos[$i]['aprobado'] == true ? 1 : 0) : 0;
                    $dataAdd->no_aplica = isset($adjuntos[$i]['noAplica']) ? ($adjuntos[$i]['noAplica'] == true ? 1 : 0) : 0;
                    $dataAdd->observacion = isset($adjuntos[$i]['observacion']) ? $adjuntos[$i]['observacion'] : '';
                    $dataAdd->save();
                }
            }
            // guardando proponentes
            if(count($proponentes) > 0) {
                for ($i=0; $i < count($proponentes); $i++) {
                    $dataAdd = new SolProcProponentes;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->nombre = isset($proponentes[$i]['nombre']) ? $proponentes[$i]['nombre'] : '';
                    $dataAdd->tel_fijo = isset($proponentes[$i]['telFijo']) ? $proponentes[$i]['telFijo'] : '';
                    $dataAdd->tel_celular = isset($proponentes[$i]['telCelular']) ? $proponentes[$i]['telCelular'] : '';
                    $dataAdd->correo = isset($proponentes[$i]['correo']) ? $proponentes[$i]['correo'] : '';
                    $dataAdd->direccion = isset($proponentes[$i]['direccion']) ? $proponentes[$i]['direccion'] : '';
                    $dataAdd->cert_tyc = isset($proponentes[$i]['certTYC']) ? ($proponentes[$i]['certTYC'] == true ? 1 : 0) : 0;
                    $dataAdd->save();
                }
            }
            if(count($expProp) > 0) {
                for ($i=0; $i < count($expProp); $i++) {
                    $dataAdd = new SolProcExpProp;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tipo = isset($expProp[$i]['tipo']) ? $expProp[$i]['tipo'] : '';
                    $dataAdd->tipo_exp = isset($expProp[$i]['tipoExp']) ? $expProp[$i]['tipoExp'] : '';
                    $dataAdd->determinante = isset($expProp[$i]['determinante']) ? $expProp[$i]['determinante'] : '';
                    $dataAdd->tipo_det = isset($expProp[$i]['tipoDet']) ? $expProp[$i]['tipoDet'] : '';
                    $dataAdd->unidad = isset($expProp[$i]['unidad']) ? $expProp[$i]['unidad'] : '';
                    $dataAdd->cantidad = isset($expProp[$i]['cantidad']) ? $expProp[$i]['cantidad'] : '';
                    $dataAdd->descripcion = isset($expProp[$i]['descripcion']) ? $expProp[$i]['descripcion'] : '';
                    $dataAdd->cumple = isset($expProp[$i]['cumple']) ? ($expProp[$i]['cumple'] == true ? 1 : 0) : 0;
                    $dataAdd->puntaje_und = isset($expProp[$i]['puntajeUnd']) ? $expProp[$i]['puntajeUnd'] : '';
                    $dataAdd->puntaje_max = isset($expProp[$i]['puntajeMax']) ? $expProp[$i]['puntajeMax'] : '';
                    $dataAdd->save();
                }
            }
            if(count($roles) > 0) {
                for ($i=0; $i < count($roles); $i++) {
                    $dataAdd = new SolProcRoles;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tipo = isset($roles[$i]['tipo']) ? $roles[$i]['tipo'] : '';
                    $dataAdd->rol = isset($roles[$i]['rol']) ? $roles[$i]['rol'] : '';
                    $dataAdd->porc_rol = isset($roles[$i]['porcRol']) ? $roles[$i]['porcRol'] : '';
                    $dataAdd->porc_edu = isset($roles[$i]['porcEdu']) ? $roles[$i]['porcEdu'] : '';
                    $dataAdd->porc_exp = isset($roles[$i]['porcExp']) ? $roles[$i]['porcExp'] : '';
                    $dataAdd->save();
                    if (isset($dataAdd->id)) {
                        $vecedu = isset($roles[$i]['educacion']) ? $roles[$i]['educacion'] : [];
                        for ($j=0; $j < count($vecedu); $j++) {
                            $dataEdu = new SolProcRolEdu;        
                            $dataEdu->id_solproc = $solproc->id;
                            $dataEdu->id_solproc_roles = $dataAdd->id;
                            $dataEdu->tipo_educacion = isset($vecedu[$j]['tipoEd']) ? $vecedu[$j]['tipoEd'] : '';
                            $dataEdu->tipo = isset($vecedu[$j]['tipo']) ? $vecedu[$j]['tipo'] : '';
                            $dataEdu->titulo = isset($vecedu[$j]['titulo']) ? $vecedu[$j]['titulo'] : '';
                            $dataEdu->cumple = isset($vecedu[$j]['cumple']) ? ($vecedu[$j]['cumple'] == true ? 1 : 0) : 0;
                            $dataEdu->no_aplica = isset($vecedu[$j]['noAplica']) ? ($vecedu[$j]['noAplica'] == true ? 1 : 0) : 0;
                            $dataEdu->observacion = isset($vecedu[$j]['observacion']) ? $vecedu[$j]['observacion'] : '';
                            $dataEdu->puntajeUnd = isset($vecedu[$j]['puntajeUnd']) ? $vecedu[$j]['puntajeUnd'] : '';
                            $dataEdu->puntajeMax = isset($vecedu[$j]['puntajeMax']) ? $vecedu[$j]['puntajeMax'] : '';
                            $dataEdu->save();
                        }
                        $vecexp = isset($roles[$i]['experiencia']) ? $roles[$i]['experiencia'] : [];
                        for ($k=0; $k < count($vecexp); $k++) {
                            $dataExp = new SolProcRolExp;        
                            $dataExp->id_solproc = $solproc->id;
                            $dataExp->id_solproc_roles = $dataAdd->id;
                            $dataExp->tipo_exp = isset($vecexp[$k]['tipoExp']) ? $vecexp[$k]['tipoExp'] : '';
                            $dataExp->descripcion = isset($vecexp[$k]['descripcion']) ? $vecexp[$k]['descripcion'] : '';
                            $dataExp->tiempo = isset($vecexp[$k]['tiempo']) ? $vecexp[$k]['tiempo'] : '';
                            $dataExp->cantidad = isset($vecexp[$k]['cantidad']) ? $vecexp[$k]['cantidad'] : '';
                            $dataExp->cumple = isset($vecexp[$k]['cumple']) ? ($vecexp[$k]['cumple'] == true ? 1 : 0) : 0;
                            $dataExp->no_aplica = isset($vecexp[$k]['noAplica']) ? ($vecexp[$k]['noAplica'] == true ? 1 : 0) : 0;
                            $dataExp->observacion = isset($vecexp[$k]['observacion']) ? $vecexp[$k]['observacion'] : '';
                            $dataExp->puntajeUnd = isset($vecexp[$k]['puntajeUnd']) ? $vecexp[$k]['puntajeUnd'] : '';
                            $dataExp->puntajeMax = isset($vecexp[$k]['puntajeMax']) ? $vecexp[$k]['puntajeMax'] : '';
                            $dataExp->save();
                        }
                    }
                }
            }
            if(count($expFin) > 0) {
                for ($i=0; $i < count($expFin); $i++) {
                    $dataAdd = new SolProcExpFin;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->indicador = isset($expFin[$i]['indicador']) ? $expFin[$i]['indicador'] : '';
                    $dataAdd->operador = isset($expFin[$i]['operador']) ? $expFin[$i]['operador'] : '';
                    $dataAdd->numero = isset($expFin[$i]['numero']) ? $expFin[$i]['numero'] : '';
                    $dataAdd->inhabilita = isset($expFin[$i]['inhabilita']) ? ($expFin[$i]['inhabilita'] == true ? 1 : 0) : 0;
                    $dataAdd->save();
                }
            }
            if(count($expOrg) > 0) {
                for ($i=0; $i < count($expOrg); $i++) {
                    $dataAdd = new SolProcExpOrg;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->indicador = isset($expOrg[$i]['indicador']) ? $expOrg[$i]['indicador'] : '';
                    $dataAdd->operador = isset($expOrg[$i]['operador']) ? $expOrg[$i]['operador'] : '';
                    $dataAdd->numero = isset($expOrg[$i]['numero']) ? $expOrg[$i]['numero'] : '';
                    // $dataAdd->inhabilita = isset($expOrg[$i]['inhabilita']) ? ($expOrg[$i]['inhabilita'] == true ? 1 : 0) : 0;
                    $dataAdd->save();
                }
            }
            if(count($expMet) > 0) {
                for ($i=0; $i < count($expMet); $i++) {
                    $dataAdd = new SolProcExpMet;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->aspecto = isset($expMet[$i]['aspecto']) ? $expMet[$i]['aspecto'] : '';
                    $dataAdd->puntajemax = isset($expMet[$i]['puntajemax']) ? $expMet[$i]['puntajemax'] : '';
                    $dataAdd->save();
                }
            }
            $affectedRows = SolProcCriDes::where('id_solproc', '=', $solproc->id)->delete();
            if(count($criDes) > 0) {
                for ($i=0; $i < count($criDes); $i++) {
                    $dataAdd = new SolProcCriDes;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->criterio = isset($criDes[$i]['criterio']) ? $criDes[$i]['criterio'] : '';
                    $dataAdd->save();
                }
            }
            return response()->json(['id' => $solproc->id], 201);
        } else {
            return response()->json(['error' => 'No selogro crear proceso'], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Solproc = SolProceso::where('id', '=', $id)->get();
                    
            if ($Solproc->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {
                $solproc = $Solproc[0];

                $infoprod = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM solproc_infoprod WHERE id_solproc = '.$id));
                
                $obligaciones = DB::reconnect('mysql')->select(DB::raw('SELECT descripcion as obligacion FROM solproc_obligaciones WHERE id_solproc = '.$id));
                
                $ciudades = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM solproc_ciudades WHERE id_solproc = '.$id));
                $ciudad = array();
                for ($i=0; $i < count($ciudades); $i++) {
                    array_push($ciudad, strval($ciudades[$i]->codigo));
                }

                $supervisores = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM solproc_supervisores WHERE id_solproc = '.$id));
                $supervisor = array();
                for ($i=0; $i < count($supervisores); $i++) {
                    array_push($supervisor, $supervisores[$i]->codigo);
                }
                $necesidades =  DB::reconnect('mysql')->select(DB::raw('SELECT spn.id as idspn, sn.id as idsn, sn.cod_plan, sn.cod_necesidad, sn.descripcion, pn.plaadqnecval as valor, spn.valor as solval FROM solproc_necesidades spn INNER JOIN solicitudes_necesidades sn ON sn.id = spn.id_sol_necesidades INNER JOIN planesadquisiciones_necesidades pn ON pn.plaadqneccod = sn.cod_necesidad AND pn.plaadqcod = sn.cod_plan WHERE spn.id_solproc = '.$id));

                $pagos = DB::reconnect('mysql')->select(DB::raw('SELECT porc_pago as porcPago, tiempo_ind as tiempoInd, tiempo_und as tiempoUnd, dias, id, id_solproc FROM solproc_pagos WHERE id_solproc = '.$id));
                for ($i=0; $i < count($pagos); $i++) {
                    $pagos[$i]->infoProd = DB::reconnect('mysql')->select(DB::raw("SELECT si.tipo, si.titulo, si.descripcion FROM solproc_pagos sp INNER JOIN pagos_infoprod pi ON pi.id_solpagos = sp.id INNER JOIN solproc_infoprod si ON si.id = pi.id_infoprod WHERE sp.id_solproc = " . $id . " AND sp.id = " . $pagos[$i]->id));
                }

                $educacion = DB::reconnect('mysql')->select(DB::raw('SELECT id, id_solproc, tipo_educacion as tipoEd, titulo, cumple, no_aplica as noAplica, observacion, tipo FROM solproc_educacion WHERE id_solproc = '.$id));

                $experiencia = DB::reconnect('mysql')->select(DB::raw('SELECT id, id_solproc, tipo_exp as tipoExp, descripcion, tiempo, cumple, no_aplica as noAplica, observacion FROM solproc_experiencia WHERE id_solproc = '.$id));

                $adjuntos = DB::reconnect('mysql')->select(DB::raw('SELECT id, id_solproc, tipo, doc_solicitado as docSolicitado, nombre, path, aprobado, no_aplica as noAplica, observacion FROM solproc_adjuntos WHERE id_solproc = '.$id));

                $proponentes = DB::reconnect('mysql')->select(DB::raw('SELECT id, id_solproc, nombre, tel_fijo as telFijo, tel_celular as telCelular, correo, direccion, cert_tyc as certTYC  FROM solproc_proponentes WHERE id_solproc = '.$id));
                
                $expProp = DB::reconnect('mysql')->select(DB::raw('SELECT id, id_solproc, tipo, tipo_exp as tipoExp, determinante, tipo_det as tipoDet, unidad, cantidad, descripcion, cumple, puntaje_und as puntajeUnd, puntaje_max as puntajeMax FROM solproc_expprop WHERE id_solproc = '.$id));

                $roles = DB::reconnect('mysql')->select(DB::raw('SELECT id, id_solproc, tipo, rol, porc_rol as porcRol, porc_edu as porcEdu, porc_exp as porcExp FROM solproc_roles WHERE id_solproc = '.$id));
                for ($i=0; $i < count($roles); $i++) {
                    $roles[$i]->educacion = DB::reconnect('mysql')->select(DB::raw('SELECT id, id_solproc, id_solproc_roles, tipo_educacion as tipoEd, tipo, titulo, cumple, no_aplica as noAplica, observacion, puntajeUnd, puntajeMax FROM solproc_rol_edu WHERE id_solproc_roles = '.$roles[$i]->id));
                    $roles[$i]->experiencia = DB::reconnect('mysql')->select(DB::raw('SELECT id, id_solproc, id_solproc_roles, tipo_exp as tipoExp, descripcion, tiempo, cantidad, cumple, no_aplica as noAplica, observacion, puntajeUnd, puntajeMax FROM solproc_rol_exp WHERE id_solproc_roles = '.$roles[$i]->id));
                }
                $expFin = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM solproc_expfinanciera WHERE id_solproc = '.$id));

                $expOrg = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM solproc_exporg WHERE id_solproc = '.$id));

                $expMet = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM solproc_expmet WHERE id_solproc = '.$id));

                $criDes = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM solproc_desempate WHERE id_solproc = '.$id));
                // $Procesos = DB::reconnect('mysql')->select(DB::raw('SELECT id, obj_contractual as objContractual, fecha_limite as fechaLimite, fecha_creacion as fechaCreacion, tipo_contratacion as tipoContratacion, tipo_proc as tipoProceso, tiempo_asignado as tiempoAsignado FROM solicitudes_procesos WHERE id_solcontratacion = '.$id));
                
                /* $solicitud->necesidades = $Necesidades;
                $solicitud->Procesos = $Procesos; */
                $solproc->infoprod = $infoprod;
                $solproc->obligaciones = $obligaciones;
                $solproc->ciudad = $ciudad;
                $solproc->supervisor = $supervisor;
                $solproc->necesidades = $necesidades;
                $solproc->pagos = $pagos;
                $solproc->educacion = $educacion;
                $solproc->experiencia = $experiencia;
                $solproc->adjuntos = $adjuntos;
                $solproc->proponentes = $proponentes;
                $solproc->expProp = $expProp;
                $solproc->roles = $roles;
                $solproc->expFin = $expFin;
                $solproc->expOrg = $expOrg;
                $solproc->expMet = $expMet;
                $solproc->criDes = $criDes;
                return json_decode(json_encode($solproc), true);    
            }
            
        }catch(Exception $e){

            return $e->getMessage();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $inSolProc = $request->data;
        $idSol = $inSolProc['idSolContratacion'];
        $obligaciones = isset($inSolProc['obligaciones']) ? $inSolProc['obligaciones'] : [];
        $infoprod = isset($inSolProc['infoprod']) ? $inSolProc['infoprod'] : [];
        $ciudades = isset($inSolProc['ciudad']) ? $inSolProc['ciudad'] : [];
        $supervisores = isset($inSolProc['supervisor']) ? $inSolProc['supervisor'] : [];
        $necesidades = isset($inSolProc['necesidades']) ? $inSolProc['necesidades'] : [];
        $pagos = isset($inSolProc['pagos']) ? $inSolProc['pagos'] : [];
        $educacion = isset($inSolProc['educacion']) ? $inSolProc['educacion'] : [];
        $experiencia = isset($inSolProc['experiencia']) ? $inSolProc['experiencia'] : [];
        $adjuntos = isset($inSolProc['adjuntos']) ? $inSolProc['adjuntos'] : [];
        $proponentes = isset($inSolProc['proponentes']) ? $inSolProc['proponentes'] : [];
        $expProp = isset($inSolProc['expProp']) ? $inSolProc['expProp'] : [];
        $roles = isset($inSolProc['roles']) ? $inSolProc['roles'] : [];
        $expFin = isset($inSolProc['expFin']) ? $inSolProc['expFin'] : [];
        $expOrg = isset($inSolProc['expOrg']) ? $inSolProc['expOrg'] : [];
        $expMet = isset($inSolProc['expMet']) ? $inSolProc['expMet'] : [];
        $criDes = isset($inSolProc['criDes']) ? $inSolProc['criDes'] : [];
        // creando el proceso
        $solproc = SolProceso::find($inSolProc['id']);
        // $solproc->id = $inSolProc['id'];
        $solproc->id_solcontratacion = $idSol;
        $solproc->obj_contractual = isset($inSolProc['objContractual']) ? $inSolProc['objContractual'] : '';
        $solproc->fecha_limite = isset($inSolProc['fechaLimite']) ? $inSolProc['fechaLimite'] : '';
        $solproc->tiempo_asignado = isset($inSolProc['tiempoAsignado']) ? $inSolProc['tiempoAsignado'] : '';
        $solproc->fecha_terminacion = isset($inSolProc['fechaTerminacion']) ? $inSolProc['fechaTerminacion']: '';
        $solproc->inc_gastos = isset($inSolProc['incGastos']) ? ($inSolProc['incGastos'] == true ? 1 : 0) : 0;
        $solproc->tar_sinchi = isset($inSolProc['tarSinchi']) ? ($inSolProc['tarSinchi'] == true ? 1 : 0) : 0;
        $solproc->val_gastos_viaje = isset($inSolProc['valGastosViaje']) ? $inSolProc['valGastosViaje'] : '';
        $solproc->tipo_contratacion = isset($inSolProc['tipoContratacion']) ? $inSolProc['tipoContratacion'] : '';
        $solproc->tipo_proc = isset($inSolProc['tipoProc']) ? $inSolProc['tipoProc']: '';
        $solproc->nombre = isset($inSolProc['nombre']) ? $inSolProc['nombre'] : '';
        $solproc->correo = isset($inSolProc['correo']) ? $inSolProc['correo'] : '';
        $solproc->rol = isset($inSolProc['rol']) ? $inSolProc['rol']: '';
        $solproc->telefono = isset($inSolProc['telefono']) ? $inSolProc['telefono'] : '';
        $solproc->causal = isset($inSolProc['causal']) ? $inSolProc['causal'] : '';
        $solproc->justificacion = isset($inSolProc['justificacion']) ? $inSolProc['justificacion'] : '';
        $solproc->caracteristicas_bs = isset($inSolProc['caracteristicasBS']) ? $inSolProc['caracteristicasBS'] : '';
        $solproc->acces_info_conf = isset($inSolProc['accesInfoConf']) ? ($inSolProc['accesInfoConf'] == true ? 1 : 0) : 0;
        $solproc->of_economica = isset($inSolProc['ofEconomica']) ? $inSolProc['ofEconomica'] : '';
        $solproc->porc_of_eco = isset($inSolProc['porcOfEco']) ? $inSolProc['porcOfEco'] : '';
        $solproc->peso_prop = isset($inSolProc['pesoProp']) ? $inSolProc['pesoProp'] : '';
        $solproc->peso_equ = isset($inSolProc['pesoEqu']) ? $inSolProc['pesoEqu'] : '';
        $solproc->peso_fin = isset($inSolProc['pesoFin']) ? $inSolProc['pesoFin'] : '';
        $solproc->peso_org = isset($inSolProc['pesoOrg']) ? $inSolProc['pesoOrg'] : '';
        $solproc->peso_met = isset($inSolProc['pesoMet']) ? $inSolProc['pesoMet'] : '';
        $solproc->peso_eco = isset($inSolProc['pesoEco']) ? $inSolProc['pesoEco'] : ''; 
        $solproc->anticipo = isset($inSolProc['anticipo']) ? $inSolProc['anticipo'] : ''; 
        $solproc->contrapartida = isset($inSolProc['contrapartida']) ? $inSolProc['contrapartida'] : ''; 
        $solproc->conv_valor = isset($inSolProc['convValor']) ? $inSolProc['convValor'] : ''; 
        $solproc->conv_especie = isset($inSolProc['convEspecie']) ? $inSolProc['convEspecie']: ''; 
        $solproc->conv_dinero = isset($inSolProc['convDinero']) ? $inSolProc['convDinero']: ''; 
        $solproc->convenio_marco = isset($inSolProc['convenioMarco']) ? $inSolProc['convenioMarco'] : ''; 
        $solproc->val_arl = isset($inSolProc['valArl']) ? $inSolProc['valArl'] : ''; 
        $solproc->fecha_creacion = date('Y-m-d H:i:s');
        $solproc->save(); 
        if (isset($solproc->id)) {
            $affectedRows = SolProcNecesidades::where('id_solproc', '=', $solproc->id)->delete();
            if(count($necesidades) > 0) {
                // creando necesidades
                for ($i=0; $i < count($necesidades); $i++) {
                    $dataAdd = new SolProcNecesidades();
                    $dataAdd->id_solproc = $solproc->id;           
                    $dataAdd->id_sol_necesidades = $necesidades[$i]['idsn'];
                    $dataAdd->valor = isset($necesidades[$i]['solval']) ? $necesidades[$i]['solval'] : 0;
                    $dataAdd->save();
                }
            }
            // Insert obligaciones
            $affectedRows = SolicitudesObligaciones::where('id_solproc', '=', $solproc->id)->delete();
            if(count($obligaciones) > 0) {
                for ($i=0; $i < count($obligaciones); $i++) {
                    $dataAdd = new SolicitudesObligaciones();        
                    $dataAdd->descripcion = $obligaciones[$i]['obligacion'];
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->save();
                }
            }
            // Insert informes
            $affectedRows = SolicitudesInformes::where('id_solproc', '=', $solproc->id)->delete();
            if(count($infoprod) > 0) {
                for ($i=0; $i < count($infoprod); $i++) {
                    $dataAdd = new SolicitudesInformes();        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->titulo = $infoprod[$i]['titulo'];
                    $dataAdd->tipo = $infoprod[$i]['tipo'];
                    $dataAdd->descripcion = $infoprod[$i]['descripcion'];
                    $dataAdd->save();
                }
            }

            // Insert ciudades
            $affectedRows = SolicitudesCiudades::where('id_solproc', '=', $solproc->id)->delete();
            if(count($ciudades) > 0) {
                for ($i=0; $i < count($ciudades); $i++) {
                    $dataAdd = new SolicitudesCiudades();        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->codigo = $ciudades[$i];
                    $dataAdd->save();
                }
            }
    
            // Insert supervisores
            $affectedRows = SolicitudesSupervisores::where('id_solproc', '=', $solproc->id)->delete();
            if(count($supervisores) > 0) {
                for ($i=0; $i < count($supervisores); $i++) {
                    $dataAdd = new SolicitudesSupervisores();        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->codigo = $supervisores[$i];
                    $dataAdd->save();
                }
            }
            // Insert pagos
            $affectedRows = SolProcPagos::where('id_solproc', '=', $solproc->id)->delete();
            $affectedRows = PagosInfoProd::where('id_solproc', '=', $solproc->id)->delete();
            if(count($pagos) > 0) {
                $solinfpro = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM solproc_infoprod WHERE id_solproc = '.$solproc->id));
                for ($i=0; $i < count($pagos); $i++) {
                    $dataAdd = new SolProcPagos;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tiempo_ind = $pagos[$i]['tiempoInd'];
                    $dataAdd->tiempo_und = $pagos[$i]['tiempoUnd'];
                    $dataAdd->dias = $pagos[$i]['dias'];
                    $dataAdd->porc_pago = $pagos[$i]['porcPago'];
                    $dataAdd->save();
                    $idPago = $dataAdd->id;
                    $pagosIP = $pagos[$i]['infoProd']; // es una array
                    for ($j=0; $j <count($pagosIP) ; $j++) {
                        for($k=0; $k <count($solinfpro) ; $k++) {
                            if ($solinfpro[$k]->titulo === $pagosIP[$j]['titulo'] && $solinfpro[$k]->tipo === $pagosIP[$j]['tipo']) {
                                $dataChild = new PagosInfoProd;
                                $dataChild->id_solpagos = $idPago;
                                $dataChild->id_infoprod = $solinfpro[$k]->id;
                                $dataChild->id_solproc = $solproc->id;
                                $dataChild->save();
                            }
                        }
                    }
                }
            }
            // Ingresa educacion en solproc
            $cadena = getIds($educacion, 'id');
            DB::reconnect('mysql')->delete(DB::raw("delete from solproc_educacion where id NOT IN ($cadena) AND id_solproc = $solproc->id"));
            // $affectedRows = SolProcEducacion::where('id_solproc', '=', $solproc->id)->delete();
            if(count($educacion) > 0) {
                for ($i=0; $i < count($educacion); $i++) {
                    if (isset($educacion[$i]['id'])) {
                        $dataAdd = SolProcEducacion::find($educacion[$i]['id']);  
                    } else {
                        $dataAdd = new SolProcEducacion;  
                    }  
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tipo_educacion = $educacion[$i]['tipoEd'];
                    $dataAdd->tipo = $educacion[$i]['tipo'];
                    $dataAdd->titulo = $educacion[$i]['titulo'];
                    $dataAdd->cumple = isset($educacion[$i]['cumple']) ? ($educacion[$i]['cumple'] == true ? 1 : 0) : 0;
                    $dataAdd->no_aplica = isset($educacion[$i]['noAplica']) ? ($educacion[$i]['noAplica'] == true ? 1 : 0) : 0;
                    $dataAdd->observacion = isset($educacion[$i]['observacion']) ? $educacion[$i]['observacion'] : '';
                    $dataAdd->save();
                }
            }
            // Ingresa experiencia en solproc
            $cadena = getIds($experiencia, 'id');
            DB::reconnect('mysql')->delete(DB::raw("delete from solproc_experiencia where id NOT IN ($cadena) AND id_solproc = $solproc->id"));
            // $affectedRows = SolProcExperiencia::where('id_solproc', '=', $solproc->id)->delete();
            if(count($experiencia) > 0) {
                for ($i=0; $i < count($experiencia); $i++) {
                    if (isset($experiencia[$i]['id'])) {
                        $dataAdd = SolProcExperiencia::find($experiencia[$i]['id']);  
                    } else {
                        $dataAdd = new SolProcExperiencia; 
                    }        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tipo_exp = $experiencia[$i]['tipoExp'];
                    $dataAdd->descripcion = $experiencia[$i]['descripcion'];
                    $dataAdd->tiempo = $experiencia[$i]['tiempo'];
                    $dataAdd->cumple = isset($experiencia[$i]['cumple']) ? ($experiencia[$i]['cumple'] == true ? 1 : 0) : 0;
                    $dataAdd->no_aplica = isset($experiencia[$i]['noAplica']) ? ($experiencia[$i]['noAplica'] == true ? 1 : 0) : 0;
                    $dataAdd->observacion = isset($experiencia[$i]['observacion']) ? $experiencia[$i]['observacion'] : '';
                    $dataAdd->save();
                }
            }
            // guardando adjuntos de solproc
            $affectedRows = SolProcAdjuntos::where('id_solproc', '=', $solproc->id)->delete();
            if(count($adjuntos) > 0) {
                for ($i=0; $i < count($adjuntos); $i++) {
                    $dataAdd = new SolProcAdjuntos;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->doc_solicitado = isset($adjuntos[$i]['docSolicitado']) ? $adjuntos[$i]['docSolicitado'] : '';
                    $dataAdd->tipo = isset($adjuntos[$i]['tipo']) ? $adjuntos[$i]['tipo'] : '';
                    $dataAdd->nombre = isset($adjuntos[$i]['nombre']) ? $adjuntos[$i]['nombre'] : '';
                    $dataAdd->path = isset($adjuntos[$i]['path']) ? $adjuntos[$i]['path'] : '';
                    $dataAdd->aprobado = isset($adjuntos[$i]['aprobado']) ? ($adjuntos[$i]['aprobado'] == true ? 1 : 0) : 0;
                    $dataAdd->no_aplica = isset($adjuntos[$i]['noAplica']) ? ($adjuntos[$i]['noAplica'] == true ? 1 : 0) : 0;
                    $dataAdd->observacion = isset($adjuntos[$i]['observacion']) ? $adjuntos[$i]['observacion'] : '';
                    $dataAdd->save();
                }
            }
            // guardando proponentes
            // $affectedRows = SolProcProponentes::where('id_solproc', '=', $solproc->id)->delete();
            $cadena = getIds($proponentes, 'id');
            DB::reconnect('mysql')->delete(DB::raw("delete from solproc_proponentes where id NOT IN ($cadena) AND id_solproc = $solproc->id"));
            if(count($proponentes) > 0) {
                for ($i=0; $i < count($proponentes); $i++) {
                    if (isset($proponentes[$i]['id'])) {
                        $dataAdd = SolProcProponentes::find($proponentes[$i]['id']);
                    } else {
                        $dataAdd = new SolProcProponentes; 
                    }       
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->nombre = isset($proponentes[$i]['nombre']) ? $proponentes[$i]['nombre'] : '';
                    $dataAdd->tel_fijo = isset($proponentes[$i]['telFijo']) ? $proponentes[$i]['telFijo'] : '';
                    $dataAdd->tel_celular = isset($proponentes[$i]['telCelular']) ? $proponentes[$i]['telCelular'] : '';
                    $dataAdd->correo = isset($proponentes[$i]['correo']) ? $proponentes[$i]['correo'] : '';
                    $dataAdd->direccion = isset($proponentes[$i]['direccion']) ? $proponentes[$i]['direccion'] : '';
                    $dataAdd->cert_tyc = isset($proponentes[$i]['certTYC']) ? ($proponentes[$i]['certTYC'] == true ? 1 : 0) : 0;
                    $dataAdd->save();
                }
            }
            $cadena = getIds($expProp, 'id');
            DB::reconnect('mysql')->delete(DB::raw("delete from solproc_expprop where id NOT IN ($cadena) AND id_solproc = $solproc->id"));
            // $affectedRows = SolProcExpProp::where('id_solproc', '=', $solproc->id)->delete();
            if(count($expProp) > 0) {
                for ($i=0; $i < count($expProp); $i++) {
                    if (isset($expProp[$i]['id'])) {
                        $dataAdd = SolProcExpProp::find($expProp[$i]['id']);
                    } else {
                        $dataAdd = new SolProcExpProp;
                    }        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tipo = isset($expProp[$i]['tipo']) ? $expProp[$i]['tipo'] : '';
                    $dataAdd->tipo_exp = isset($expProp[$i]['tipoExp']) ? $expProp[$i]['tipoExp'] : '';
                    $dataAdd->determinante = isset($expProp[$i]['determinante']) ? $expProp[$i]['determinante'] : '';
                    $dataAdd->tipo_det = isset($expProp[$i]['tipoDet']) ? $expProp[$i]['tipoDet'] : '';
                    $dataAdd->unidad = isset($expProp[$i]['unidad']) ? $expProp[$i]['unidad'] : '';
                    $dataAdd->cantidad = isset($expProp[$i]['cantidad']) ? $expProp[$i]['cantidad'] : '';
                    $dataAdd->descripcion = isset($expProp[$i]['descripcion']) ? $expProp[$i]['descripcion'] : '';
                    $dataAdd->cumple = isset($expProp[$i]['cumple']) ? ($expProp[$i]['cumple'] == true ? 1 : 0) : 0;
                    $dataAdd->puntaje_und = isset($expProp[$i]['puntajeUnd']) ? $expProp[$i]['puntajeUnd'] : '';
                    $dataAdd->puntaje_max = isset($expProp[$i]['puntajeMax']) ? $expProp[$i]['puntajeMax'] : '';
                    $dataAdd->save();
                }
            }
            $cadena = getIds($roles, 'id');
            DB::reconnect('mysql')->delete(DB::raw("delete from solproc_roles where id NOT IN ($cadena) AND id_solproc = $solproc->id"));
            /* $affectedRows = SolProcRolEdu::where('id_solproc', '=', $solproc->id)->delete();
            $affectedRows = SolProcRolExp::where('id_solproc', '=', $solproc->id)->delete();
            $affectedRows = SolProcRoles::where('id_solproc', '=', $solproc->id)->delete(); */
            if(count($roles) > 0) {
                for ($i=0; $i < count($roles); $i++) {
                    if (isset($roles[$i]['id'])) {
                        $dataAdd = SolProcRoles::find($roles[$i]['id']);  
                    } else {
                        $dataAdd = new SolProcRoles; 
                    }
                    // $dataAdd = new SolProcRoles;        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->tipo = isset($roles[$i]['tipo']) ? $roles[$i]['tipo'] : '';
                    $dataAdd->rol = isset($roles[$i]['rol']) ? $roles[$i]['rol'] : '';
                    $dataAdd->porc_rol = isset($roles[$i]['porcRol']) ? $roles[$i]['porcRol'] : '';
                    $dataAdd->porc_edu = isset($roles[$i]['porcEdu']) ? $roles[$i]['porcEdu'] : '';
                    $dataAdd->porc_exp = isset($roles[$i]['porcExp']) ? $roles[$i]['porcExp'] : '';
                    $dataAdd->save();
                    if (isset($dataAdd->id)) {
                        $idRol = $dataAdd->id;
                        // ------ borrando edu del rol que no lleguen
                        $cadena = getIds($roles[$i]['educacion'], 'id');
                        DB::reconnect('mysql')->delete(DB::raw("delete from solproc_rol_edu where id NOT IN ($cadena) AND id_solproc_roles = $idRol"));
                        // ------ borrando exp del rol que no lleguen
                        $cadena = getIds($roles[$i]['experiencia'], 'id');
                        DB::reconnect('mysql')->delete(DB::raw("delete from solproc_rol_exp where id NOT IN ($cadena) AND id_solproc_roles = $idRol"));
                        
                        $vecedu = isset($roles[$i]['educacion']) ? $roles[$i]['educacion'] : [];
                        for ($j=0; $j < count($vecedu); $j++) {
                            if (isset($vecedu[$j]['id'])) {
                                $dataEdu = SolProcRolEdu::find($vecedu[$j]['id']);  
                            } else {
                                $dataEdu = new SolProcRolEdu; 
                            }
                            // $dataEdu = new SolProcRolEdu;        
                            $dataEdu->id_solproc = $solproc->id;
                            $dataEdu->id_solproc_roles = $dataAdd->id;
                            $dataEdu->tipo_educacion = isset($vecedu[$j]['tipoEd']) ? $vecedu[$j]['tipoEd'] : '';
                            $dataEdu->tipo = isset($vecedu[$j]['tipo']) ? $vecedu[$j]['tipo'] : '';
                            $dataEdu->titulo = isset($vecedu[$j]['titulo']) ? $vecedu[$j]['titulo'] : '';
                            $dataEdu->cumple = isset($vecedu[$j]['cumple']) ? ($vecedu[$j]['cumple'] == true ? 1 : 0) : 0;
                            $dataEdu->no_aplica = isset($vecedu[$j]['noAplica']) ? ($vecedu[$j]['noAplica'] == true ? 1 : 0) : 0;
                            $dataEdu->observacion = isset($vecedu[$j]['observacion']) ? $vecedu[$j]['observacion'] : '';
                            $dataEdu->puntajeUnd = isset($vecedu[$j]['puntajeUnd']) ? $vecedu[$j]['puntajeUnd'] : '';
                            $dataEdu->puntajeMax = isset($vecedu[$j]['puntajeMax']) ? $vecedu[$j]['puntajeMax'] : '';
                            $dataEdu->save();
                        }
                        $vecexp = isset($roles[$i]['experiencia']) ? $roles[$i]['experiencia'] : [];
                        for ($k=0; $k < count($vecexp); $k++) {
                            if (isset($vecexp[$k]['id'])) {
                                $dataExp = SolProcRolExp::find($vecexp[$k]['id']);  
                            } else {
                                $dataExp = new SolProcRolExp; 
                            }
                            // $dataExp = new SolProcRolExp;        
                            $dataExp->id_solproc = $solproc->id;
                            $dataExp->id_solproc_roles = $dataAdd->id;
                            $dataExp->tipo_exp = isset($vecexp[$k]['tipoExp']) ? $vecexp[$k]['tipoExp'] : '';
                            $dataExp->descripcion = isset($vecexp[$k]['descripcion']) ? $vecexp[$k]['descripcion'] : '';
                            $dataExp->tiempo = isset($vecexp[$k]['tiempo']) ? $vecexp[$k]['tiempo'] : '';
                            $dataExp->cantidad = isset($vecexp[$k]['cantidad']) ? $vecexp[$k]['cantidad'] : '';
                            $dataExp->cumple = isset($vecexp[$k]['cumple']) ? ($vecexp[$k]['cumple'] == true ? 1 : 0) : 0;
                            $dataExp->no_aplica = isset($vecexp[$k]['noAplica']) ? ($vecexp[$k]['noAplica'] == true ? 1 : 0) : 0;
                            $dataExp->observacion = isset($vecexp[$k]['observacion']) ? $vecexp[$k]['observacion'] : '';
                            $dataExp->puntajeUnd = isset($vecexp[$k]['puntajeUnd']) ? $vecexp[$k]['puntajeUnd'] : '';
                            $dataExp->puntajeMax = isset($vecexp[$k]['puntajeMax']) ? $vecexp[$k]['puntajeMax'] : '';
                            $dataExp->save();
                        }
                    }
                }
            }
            $cadena = getIds($expFin, 'id');
            DB::reconnect('mysql')->delete(DB::raw("delete from solproc_expfinanciera where id NOT IN ($cadena) AND id_solproc = $solproc->id"));
            // $affectedRows = SolProcExpFin::where('id_solproc', '=', $solproc->id)->delete();
            if(count($expFin) > 0) {
                for ($i=0; $i < count($expFin); $i++) {
                    if (isset($expFin[$i]['id'])) {
                        $dataAdd = SolProcExpFin::find($expFin[$i]['id']);  
                    } else {
                        $dataAdd = new SolProcExpFin; 
                    }        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->indicador = isset($expFin[$i]['indicador']) ? $expFin[$i]['indicador'] : '';
                    $dataAdd->operador = isset($expFin[$i]['operador']) ? $expFin[$i]['operador'] : '';
                    $dataAdd->numero = isset($expFin[$i]['numero']) ? $expFin[$i]['numero'] : '';
                    $dataAdd->inhabilita = isset($expFin[$i]['inhabilita']) ? ($expFin[$i]['inhabilita'] == true ? 1 : 0) : 0;
                    $dataAdd->save();
                }
            }
            $cadena = getIds($expOrg, 'id');
            DB::reconnect('mysql')->delete(DB::raw("delete from solproc_exporg where id NOT IN ($cadena) AND id_solproc = $solproc->id"));
            // $affectedRows = SolProcExpOrg::where('id_solproc', '=', $solproc->id)->delete();
            if(count($expOrg) > 0) {
                for ($i=0; $i < count($expOrg); $i++) {
                    if (isset($expOrg[$i]['id'])) {
                        $dataAdd = SolProcExpOrg::find($expOrg[$i]['id']);  
                    } else {
                        $dataAdd = new SolProcExpOrg; 
                    }      
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->indicador = isset($expOrg[$i]['indicador']) ? $expOrg[$i]['indicador'] : '';
                    $dataAdd->operador = isset($expOrg[$i]['operador']) ? $expOrg[$i]['operador'] : '';
                    $dataAdd->numero = isset($expOrg[$i]['numero']) ? $expOrg[$i]['numero'] : '';
                    // $dataAdd->inhabilita = isset($expOrg[$i]['inhabilita']) ? ($expOrg[$i]['inhabilita'] == true ? 1 : 0) : 0;
                    $dataAdd->save();
                }
            }
            $cadena = getIds($expMet, 'id');
            DB::reconnect('mysql')->delete(DB::raw("delete from solproc_expmet where id NOT IN ($cadena) AND id_solproc = $solproc->id"));
            // $affectedRows = SolProcExpMet::where('id_solproc', '=', $solproc->id)->delete();
            if(count($expMet) > 0) {
                for ($i=0; $i < count($expMet); $i++) {
                    if (isset($expMet[$i]['id'])) {
                        $dataAdd = SolProcExpMet::find($expMet[$i]['id']);  
                    } else {
                        $dataAdd = new SolProcExpMet; 
                    }         
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->aspecto = isset($expMet[$i]['aspecto']) ? $expMet[$i]['aspecto'] : '';
                    $dataAdd->puntajemax = isset($expMet[$i]['puntajemax']) ? $expMet[$i]['puntajemax'] : '';
                    $dataAdd->save();
                }
            }
            $cadena = getIds($criDes, 'id');
            DB::reconnect('mysql')->delete(DB::raw("delete from solproc_desempate where id NOT IN ($cadena) AND id_solproc = $solproc->id"));
            // $affectedRows = SolProcCriDes::where('id_solproc', '=', $solproc->id)->delete();
            if(count($criDes) > 0) {
                for ($i=0; $i < count($criDes); $i++) {
                    if (isset($criDes[$i]['id'])) {
                        $dataAdd = SolProcCriDes::find($criDes[$i]['id']);  
                    } else {
                        $dataAdd = new SolProcCriDes; 
                    }        
                    $dataAdd->id_solproc = $solproc->id;
                    $dataAdd->criterio = isset($criDes[$i]['criterio']) ? $criDes[$i]['criterio'] : '';
                    $dataAdd->save();
                }
            }
            return response()->json(['message' => 'actualizado'], 202);
        } else {
            return response()->json(['error' => 'No selogro crear proceso'], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    // -------------------------------------------------- funciones de ayuda ---------------------------------------
    
}

function getIds ($vector, $id) {
    $cadena = '';
    for ($i=0; $i < count($vector); $i++) {
        if (isset($vector[$i][$id])) {
            $cadena = $cadena == '' ? $cadena : $cadena . ',';
            $cadena = $cadena . $vector[$i][$id];
        }
    }
    return $cadena == '' ? "''" : $cadena;
}