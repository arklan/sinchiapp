<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\PresupuestosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Presupuestos;

use App\PresupuestosCatGas;
use App\Transformers\PresupuestosCatGasTransformer;

use DB;

class PresupuestosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Presupuestos = Presupuestos::select('presupuestos.prescod','presupuestos.convcod','convenios.convnum','convenios.convnom',
                                                 'presupuestos.presfeccre','presupuestos.presfuefinpgn',
                                                 'presupuestos.presfuefinsgr','presupuestos.presfuefincof',
                                                 'presupuestos.presfuefinfun','presupuestos.prestopact',
                                                 'presupuestos.prestiptopcatgas','monedas.monsim','convenios.convvaltot',
                                                 DB::raw("(CASE WHEN presupuestos.presfuefinpgn='1' THEN 'SI' ELSE 'NO' END) as txtpresfuefinpgn"),
                                                 DB::raw("(CASE WHEN presupuestos.presfuefinsgr='1' THEN 'SI' ELSE 'NO' END) as txtpresfuefinsgr"),
                                                 DB::raw("(CASE WHEN presupuestos.presfuefincof='1' THEN 'SI' ELSE 'NO' END) as txtpresfuefincof"),
                                                 DB::raw("(CASE WHEN presupuestos.presfuefinfun='1' THEN 'SI' ELSE 'NO' END) as txtpresfuefinfun"),
                                                 DB::raw("(CASE WHEN presupuestos.prestopact='1' THEN 'SI' ELSE 'NO' END) as txtprestopact"),
                                                 DB::raw("(CASE WHEN presupuestos.prestiptopcatgas='M' THEN 'DINERO' WHEN presupuestos.prestiptopcatgas='P' THEN 'PORCENTAJE' ELSE 'NINGUNO' END) as txtprestiptopcatgas"))
                     ->join('convenios','presupuestos.convcod','=','convenios.convcod')  
                     ->join('monedas','convenios.moncod','=','monedas.moncod')                   
                     ->orderby('presupuestos.presfeccre')    
                     //->limit(20)                  
                     ->get();
                     
            if ($Presupuestos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Presupuestos, new PresupuestosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function filtroXConvenio(Request $request)
    {       
       try{

            $convenio = $request->convenio;

            $Presupuestos = Presupuestos::select('presupuestos.prescod','presupuestos.convcod','convenios.convnum','convenios.convnom',
                                                 'presupuestos.presfeccre','presupuestos.presfuefinpgn',
                                                 'presupuestos.presfuefinsgr','presupuestos.presfuefincof',
                                                 'presupuestos.presfuefinfun','presupuestos.prestopact',
                                                 'presupuestos.prestiptopcatgas','monedas.monsim','convenios.convvaltot',
                                                 'convenios.perscod')
                     ->join('convenios','presupuestos.convcod','=','convenios.convcod')
                     ->join('monedas','convenios.moncod','=','monedas.moncod')
                     ->where('presupuestos.convcod',$convenio) 
                     ->get();
                     
            if ($Presupuestos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Presupuestos, new PresupuestosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function PresupuestosCatGasXPresupuesto(Request $request)
    {       
       try{

            $presupuesto = $request->presupuesto;

            $Presupuestos = PresupuestosCatGas::select('prescod','prescatgascod','prescatgasdes',
                                                        'prescatgastma')                     
                                ->where('prescod',$presupuesto) 
                                ->get();
                     
            if ($Presupuestos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Presupuestos, new PresupuestosCatGasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = Presupuestos::max('prescod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($convenio, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Presupuestos::select(DB::raw('count(prescod) as total'))                      
                      ->where('convcod', $convenio)
                      ->where('prescod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Convenio ya existe para otro Presupuesto';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    private function buscarCategoriaGasto($presupuesto, $codigocategoria){

        try{
          
            $Duplica = PresupuestosCatGas::select(DB::raw('count(prescod) as total'))
                      ->where('prescod',$presupuesto)
                      ->where('prescatgascod',$codigocategoria)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;
            }            

            return $total;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DataAddUpdate(Request $request)
    {
        date_default_timezone_set('America/Bogota');

        $data = $request->data;
        $categoriasgasto = $request->categoriasgasto;
        
        $codigoactual = 0;

        $codigo              = $data['codigo'];
        $convenio            = $data['convenio'];
        $fecha               = date('Y\-m\-d');
        $fuefinanciacionpgn  = $data['fuefinanciacionpgn']; 
        $fuefinanciacionsgr  = $data['fuefinanciacionsgr'];
        $fuefinanciacioncof  = $data['fuefinanciacioncof'];   
        $fuefinanciacionfun  = $data['fuefinanciacionfun'];
        $topeactividades     = $data['topeactividades'];
        $tipotopecatgasto    = $data['tipotopecatgasto'];        

        $mensaje = $this->buscarDuplicado($convenio, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();
            $codigoactual = $consecutivo;

            $PresupuestosAdd = new Presupuestos();
            
            $PresupuestosAdd->prescod           = $consecutivo;           
            $PresupuestosAdd->convcod           = $convenio; 
            $PresupuestosAdd->presfeccre        = $fecha; 
            $PresupuestosAdd->presfuefinpgn     = $fuefinanciacionpgn;
            $PresupuestosAdd->presfuefinsgr     = $fuefinanciacionsgr;
            $PresupuestosAdd->presfuefincof     = $fuefinanciacioncof;
            $PresupuestosAdd->presfuefinfun     = $fuefinanciacionfun;
            $PresupuestosAdd->prestopact        = $topeactividades;
            $PresupuestosAdd->prestiptopcatgas  = $tipotopecatgasto;           
            
            $result = $PresupuestosAdd->save();
        
        } else {

            $codigoactual = $codigo;
            $PresupuestosUpd = Presupuestos::where('prescod', $codigo)->first();
          
            $PresupuestosUpd->convcod           = $convenio; 
            $PresupuestosUpd->presfeccre        = $fecha; 
            $PresupuestosUpd->presfuefinpgn     = $fuefinanciacionpgn;
            $PresupuestosUpd->presfuefinsgr     = $fuefinanciacionsgr;
            $PresupuestosUpd->presfuefincof     = $fuefinanciacioncof;
            $PresupuestosUpd->presfuefinfun     = $fuefinanciacionfun;
            $PresupuestosUpd->prestopact        = $topeactividades;
            $PresupuestosUpd->prestiptopcatgas  = $tipotopecatgasto;          

            // Guardamos en base de datos
            $result = $PresupuestosUpd->save();
           
        }        

        //categorias del gasto
        if(count($categoriasgasto) > 0){

            $datadd = [];

            for ($i=0; $i < count($categoriasgasto); $i++) {

                $total = $this->buscarCategoriaGasto($codigoactual, $categoriasgasto[$i]['codigo']);
                
                if($total == 0){

                    $DataAdd = new PresupuestosCatGas();
            
                    $DataAdd->prescod        = $codigoactual;           
                    $DataAdd->prescatgascod  = $categoriasgasto[$i]['codigo']; 
                    $DataAdd->prescatgasdes  = $categoriasgasto[$i]['nombre']; 
                    $DataAdd->prescatgastma  = $categoriasgasto[$i]['topemaximo'];
                    
                    $DataAdd->save();

                } else {

                    $sql = "UPDATE presupuestos_categorias_gasto 
                            SET prescatgasdes = '".$categoriasgasto[$i]['nombre']."',
                                prescatgastma = '".$categoriasgasto[$i]['topemaximo']."'
                            WHERE prescod = ".$codigoactual." AND 
                                    prescatgascod = ".$categoriasgasto[$i]['codigo'];
                   
                    DB::update($sql);

                }                                               
            }            
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Presupuestos::where('prescod', $Id)->delete(); 
        return $result;

    }

    public function delCategoria(Request $request)
    {
        $presupuesto = $request->presupuesto;
        $categoria = $request->categoria;

        try {
            
            $result = PresupuestosCatGas::where('prescod',$presupuesto)
                      ->where('prescatgascod',$categoria)
                      ->delete();

            return $result;          

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }
    
}

?>