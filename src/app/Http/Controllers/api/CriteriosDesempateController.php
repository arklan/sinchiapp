<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\CriteriosDesempateTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CriteriosDesempate;
use DB;

class CriteriosDesempateController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $CriteriosDesempate = CriteriosDesempate::select('cridescod','cridesdes')
                     
                     ->orderby('cridesdes')                     
                     ->get();
                     
            if ($CriteriosDesempate->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($CriteriosDesempate, new CriteriosDesempateTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = CriteriosDesempate::max('cridescod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = CriteriosDesempate::select(DB::raw('count(cridescod) as total'))                      
                      ->where('cridesdes', $Descripcion)
                      ->where('cridescod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Descripcion ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CriteriosDesempateAddUpdate(Request $request)
    {
        $CriteriosDesempate = $request->data;

        $codigo = $CriteriosDesempate['codigo'];
        $Descripcion = strtoupper(trim($CriteriosDesempate['Descripcion']));       

        $mensaje = $this->buscarDuplicado($Descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $CriteriosDesempateAdd = new CriteriosDesempate();
            
            $CriteriosDesempateAdd->cridescod = $consecutivo;           
            $CriteriosDesempateAdd->cridesdes = $Descripcion;           
            
            $result = $CriteriosDesempateAdd->save();
        
        } else {

            $CriteriosDesempateUpd = CriteriosDesempate::where('cridescod', $codigo)->first();
          
            $CriteriosDesempateUpd->cridesdes = $Descripcion;          

            // Guardamos en base de datos
            $result = $CriteriosDesempateUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = CriteriosDesempate::where('cridescod', $Id)->delete(); 
        return $result;

    }
}

?>