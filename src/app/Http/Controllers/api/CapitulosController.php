<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\CapitulosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Capitulos;
use DB;

class CapitulosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Capitulos = Capitulos::select('capcod','capdes')
                     ->orderby('capdes')                     
                     ->get();
                     
            if ($Capitulos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Capitulos, new CapitulosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Capitulos::max('capcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Capitulos::select(DB::raw('count(capcod) as total'))                      
                      ->where('capdes', $Descripcion)
                      ->where('capcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La Descripción ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CapitulosAddUpdate(Request $request)
    {
        $Capitulos = $request->data;

        $codigo = $Capitulos['codigo'];
        $descripcion = strtoupper(trim($Capitulos['descripcion']));       

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $CapitulosAdd = new Capitulos();
            
            $CapitulosAdd->capcod = $consecutivo;           
            $CapitulosAdd->capdes = $descripcion;           
            
            $result = $CapitulosAdd->save();
        
        } else {

            $CapitulosUpd = Capitulos::where('capcod', $codigo)->first();
          
            $CapitulosUpd->capdes = $descripcion;          

            // Guardamos en base de datos
            $result = $CapitulosUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Capitulos::where('capcod', $Id)->delete(); 
        return $result;

    }
}

?>