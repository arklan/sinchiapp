<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosPilaresPiciaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosPilaresPicia;
use DB;

class ProyectosPilaresPiciaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosPilaresPicia = ProyectosPilaresPicia::select('proycod','pilpiccod')                        
                        ->orderby('pilpiccod')                     
                        ->get();
                     
            if ($ProyectosPilaresPicia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosPilaresPicia, new ProyectosPilaresPiciaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = '';

        for ($i=0; $i < count($data); $i++) {             
        
            $codigo = $data[$i]['codigo'];
            $proyecto = $data[$i]['proyecto'];
            
            if($i==0){

                $result = ProyectosPilaresPicia::where('proycod', $proyecto)->delete();
            }

            $dataAdd = new ProyectosPilaresPicia();
            
            $dataAdd->pilpiccod  = $codigo;
            $dataAdd->proycod    = $proyecto;

            $result = $dataAdd->save();            
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
                     
            $ProyectosPilaresPicia = ProyectosPilaresPicia::select('pilpiccod','proycod')                        
                        ->where('proycod', $proyecto)                        
                        ->orderby('pilpiccod')                     
                        ->get();
            
            return $this->response->collection($ProyectosPilaresPicia, new ProyectosPilaresPiciaTransformer);

            /*if ($ProyectosPilaresPicia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosPilaresPicia, new ProyectosPilaresPiciaTransformer);                
            }*/

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function seleccionadosProyectos(Request $request)
    {
        try{
            
            $proyecto = $request['proyecto'];
                     
            $sql = "SELECT l.pilpiccod codigo,l.pilpicdes descripcion, IF(p.proycod IS NULL,0,1) Activo 
                    FROM pilares_picia l LEFT JOIN 
                    proyectos_pilares_picia p ON l.pilpiccod = p.pilpiccod AND p.proycod=".$proyecto." 
                    WHERE l.pilpiccod <> 0";
            
            $result = DB::select($sql);
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
}

?>