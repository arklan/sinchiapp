<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\BitacoraTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Bitacora;
use DB;

class BitacoraController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $bitacora = Bitacora::select('bitacora.BitSec','bitacora.iduser','bitacora.BitFecReg','bitacora.BitModDes',
                                    'bitacora.BitAccDes','bitacora.BitEquIp','bitacora.BitMem','bitacora.BitHos','users.nombre')
                    ->join('users','bitacora.iduser','=','users.iduser') 
                    ->orderBy('bitacora.BitFecReg','desc') 
                    ->limit(50)
                    ->get();
                    
        if (!$bitacora->isEmpty()) {                

            return $this->response->collection($bitacora, new BitacoraTransformer);
        }             
    }               

    public function consecutivo()
    {      

        $maxVal  = Bitacora::max('BitSec');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    //Obtiene la IP del cliente
    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    public function Agregar(Request $request)
    {
        date_default_timezone_set('America/Bogota');

        $data = $request->data;
        
        $ip = $this->get_client_ip();

        $iduser  = $data['iduser'];
        $fecha   = date('Y\-m\-d H:i:s');
        $modulo  = trim($data['modulo']);        
        $accion  = trim($data['accion']);
        $detalle = trim($data['detalle']);
        $host    = gethostbyaddr($ip);
        
        $consecutivo =$this->consecutivo();

        $bitacoraAdd = new Bitacora();
        
        $bitacoraAdd->BitSec = $consecutivo;
        $bitacoraAdd->iduser = $iduser;
        $bitacoraAdd->BitFecReg = $fecha;
        $bitacoraAdd->BitModDes = $modulo;
        $bitacoraAdd->BitAccDes = $accion;
        $bitacoraAdd->BitEquIp = $ip;
        $bitacoraAdd->BitMem = $detalle;
        $bitacoraAdd->BitHos = $host;

        $result = $bitacoraAdd->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}