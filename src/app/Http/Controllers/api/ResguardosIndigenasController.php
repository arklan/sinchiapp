<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ResguardosIndigenasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ResguardosIndigenas;
use App\Ciudades;
use App\ResguardosIndMunicipios;
use App\Transformers\CiudadesTransformer;
use App\Transformers\ResguardosIndMunicipiosTransformer;
use DB;

class ResguardosIndigenasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            
            $ResguardosIndigenas = ResguardosIndigenas::select('resindcod','resindnom')
                     ->orderby('resindnom')                     
                     ->get();
                     
            if ($ResguardosIndigenas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ResguardosIndigenas, new ResguardosIndigenasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function resguardosCiudades(Request $request)
    {       
       try{

            $codigo = $request['codigo'];
            $data = ResguardosIndMunicipios::select('resguardos_ciudades.resindcod','resguardos_ciudades.ciucod','ciudades.ciunom',
                                                        'departamentos.depnom','resguardos_indigenas.resindnom')
                     ->join('resguardos_indigenas','resguardos_ciudades.resindcod','=','resguardos_indigenas.resindcod')                                   
                     ->join('ciudades','resguardos_ciudades.ciucod','=','ciudades.ciucod')
                     ->join('departamentos','ciudades.depcod','=','departamentos.depcod')
                     ->where('resguardos_ciudades.resindcod', $codigo)
                     ->orderby('ciudades.ciunom')
                     ->orderby('departamentos.depnom')                     
                     ->get();
                     
            if ($data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($data, new ResguardosIndMunicipiosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    
    
    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';            

            if($mensaje == ''){

                $Duplica = ResguardosIndigenas::select(DB::raw('count(resindcod) as total'))                      
                        ->where('resindnom', $Nombre)
                        ->where('resindcod','!=', $Codigo)
                        ->get();

                $total = 0;          
                if (!$Duplica->isEmpty()) {
                    
                    $total = $Duplica[0]->total;

                    if($total != '0'){

                        $mensaje = 'El Nombre del Resguardo Indígena ya existe';

                    }
                }            
            }

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    public function consecutivo()
    {      

        $maxVal  = ResguardosIndigenas::max('resindcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    public function DataAddUpdate(Request $request)
    {
        $data     = $request->data;
        $ciudades = $request->ciudades;

        $codigo       = $data['codigo'];
        $nombre       = strtoupper(trim($data['nombre']));       
        $edicion      = $data['edicion'];

        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        $codigoactual = 0;
        if($edicion=='0'){            

            $consecutivo = $this->consecutivo();
            $codigoactual = $consecutivo;

            $dataAdd = new ResguardosIndigenas();
            
            $dataAdd->resindcod = $consecutivo;           
            $dataAdd->resindnom = $nombre;
            
            $result = $dataAdd->save();
        
        } else {

            $codigoactual = $codigo;
            $dataUpd = ResguardosIndigenas::where('resindcod', $codigo)->first();
          
            $dataUpd->resindnom = $nombre;                   

            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        }

        if(count($ciudades) > 0){

            $arrDataActual = [];            

            if($codigo != '0'){

                ResguardosIndMunicipios::where('resindcod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($ciudades); $i++) {

                $arrDataActual[] = 
                    [                        
                        'resindcod' => $codigoactual,
                        'ciucod'    => $ciudades[$i]['codigociudad']                        
                    ];                                                
            }

            $result = ResguardosIndMunicipios::insert($arrDataActual);
        }

        return array( 'respuesta' => $result);     
    }

    public function destroy(Request $request)
    {
        $Id = $request->id;        

        try {
            
            $result = ResguardosIndMunicipios::where('resindcod', $Id)->delete();
            $result = ResguardosIndigenas::where('resindcod', $Id)->delete(); 
            return $result;          

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }

    public function deleteCiudadResguardo(Request $request)
    {
        $codigo = $request->codigo;
        $ciudad = $request->ciudad;

        try {
            
            $result = ResguardosIndMunicipios::where('resindcod', $codigo)->where('ciucod', $ciudad)->delete();            
            return $result;          

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }
}

?>