<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\PoasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Poas;
use DB;

class PoasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Poas = Poas::select('poas.poacod','poas.poades','poas.poaestr','poas.poaind','poas.poaunimed','poas.poametsec',
                                 'poas.poametins','poas.poaobs','poas.capcod','poas.linestcod','poas.objcod','poas.odscod',
                                 'poas.metcod','poas.proinvpencod','poas.lininvpencod','poas.aretemcod',
                                 'poas.proinvpeicod','poas.lininvpeicod','capitulos.capdes',
                                 'lineas_estrategicas.linestdesc','objetivos.objdesc','ods.odsdesc','metas.metnom',
                                 'penia.proinvdes as proinvpendes','lineapenia.lininvdes as lininvpendes',
                                 'areas_tematicas.aretemnom','pei.proinvdes as proinvpeides',
                                 'lineapei.lininvdes as lininvpeides')
                        ->join('capitulos','poas.capcod','=','capitulos.capcod')
                        ->join('lineas_estrategicas','poas.linestcod','=','lineas_estrategicas.linestcod')
                        ->join('objetivos','poas.objcod','=','objetivos.objcod')
                        ->join('ods','poas.odscod','=','ods.odscod')
                        ->join('metas','poas.metcod','=','metas.metcod')
                        ->join('programas_investigacion as penia','poas.proinvpencod','=','penia.proinvcod')
                        ->join('lineas_investigacion as lineapenia','poas.lininvpencod','=','lineapenia.lininvcod')
                        ->join('areas_tematicas','poas.aretemcod','=','areas_tematicas.aretemcod')
                        ->join('programas_investigacion as pei','poas.proinvpeicod','=','pei.proinvcod')
                        ->join('lineas_investigacion as lineapei','poas.lininvpeicod','=','lineapei.lininvcod')
                        ->orderby('poas.poacod')                     
                        ->get();
                     
            if ($Poas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Poas, new PoasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Poas::max('poacod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Poas::select(DB::raw('count(poacod) as total'))                      
                      ->where('poades', $Nombre)
                      ->where('poacod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo = $data['codigo'];       
        $descripcion = strtoupper(trim($data['descripcion']));
        $estrategia = trim($data['estrategia']);
        $indicador = trim($data['indicador']);
        $unidadmedida = trim($data['unidadmedida']);
        $metasectorial = trim($data['metasectorial']);
        $metainstitucional = trim($data['metainstitucional']);
        $observacion = trim($data['observacion']);        
        $capitulo = trim($data['capitulo']);
        $linea = trim($data['linea']);
        $objetivo = trim($data['objetivo']);
        $ods = trim($data['ods']);
        $meta = trim($data['meta']);
        $proyectopenia = trim($data['proyectopenia']);
        $lineainvpenia = trim($data['lineainvpenia']);
        $areatematica = trim($data['areatematica']);
        $proyectopei = trim($data['proyectopei']);
        $lineainvpei = trim($data['lineainvpei']);

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new Poas();
            
            $dataAdd->poacod       = $consecutivo;           
            $dataAdd->poades       = $descripcion;
            $dataAdd->poaestr      = $estrategia;
            $dataAdd->poaind       = $indicador; 
            $dataAdd->poaunimed    = $unidadmedida;
            $dataAdd->poametsec    = $metasectorial;
            $dataAdd->poametins    = $metainstitucional;
            $dataAdd->poaobs       = $observacion;            
            $dataAdd->capcod       = $capitulo;
            $dataAdd->linestcod    = $linea;
            $dataAdd->objcod       = $objetivo;            
            $dataAdd->odscod       = $ods;
            $dataAdd->metcod       = $meta;
            $dataAdd->proinvpencod = $proyectopenia;
            $dataAdd->lininvpencod = $lineainvpenia;
            $dataAdd->aretemcod    = $areatematica;
            $dataAdd->proinvpeicod = $proyectopei;
            $dataAdd->lininvpeicod = $lineainvpei;

            $result = $dataAdd->save();
        
        } else {

            $dataUpd = Poas::where('poacod', $codigo)->first();
          
            $dataUpd->poades       = $descripcion;
            $dataUpd->poaestr      = $estrategia;
            $dataUpd->poaind       = $indicador; 
            $dataUpd->poaunimed    = $unidadmedida;
            $dataUpd->poametsec    = $metasectorial;
            $dataUpd->poametins    = $metainstitucional;
            $dataUpd->poaobs       = $observacion;
            $dataUpd->capcod       = $capitulo;
            $dataUpd->linestcod    = $linea;
            $dataUpd->objcod       = $objetivo;            
            $dataUpd->odscod       = $ods;
            $dataUpd->metcod       = $meta;
            $dataUpd->proinvpencod = $proyectopenia;
            $dataUpd->lininvpencod = $lineainvpenia;
            $dataUpd->aretemcod    = $areatematica;
            $dataUpd->proinvpeicod = $proyectopei;
            $dataUpd->lininvpeicod = $lineainvpei;
            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Poas::where('poacod', $Id)->delete(); 
        return $result;

    }
}

?>