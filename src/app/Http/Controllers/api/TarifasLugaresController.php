<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\TarifasLugaresTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TarifasLugares;
use DB;

use App\TarifasLugaresDetalle;
use App\Transformers\TarifasLugaresDetalleTransformer;

use App\TarifasLugaresRangos;
use App\Transformers\TarifasLugaresRangosTransformer;

class TarifasLugaresController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $TarifasLugares = TarifasLugares::select('tarifas_lugares.tarlugcod','tarifas_lugares.moncod',
                                    'tarifas_lugares.tarlugdes','monedas.monsim','monedas.montas')
                        ->join('monedas','tarifas_lugares.moncod','=','monedas.moncod')     
                        ->orderby('tarifas_lugares.tarlugcod')                     
                        ->get();
                     
            if ($TarifasLugares->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($TarifasLugares, new TarifasLugaresTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function detalle(Request $request)
    {       
       try{

            $codigo = $request->codigo;
            $TarifasLugares = TarifasLugaresDetalle::select('tarifas_lugares_detalle.tarlugcod',
                                    'tarifas_lugares_detalle.paiscod','paises.paisnom')
                        ->join('paises','tarifas_lugares_detalle.paiscod','=','paises.paiscod')  
                        ->where('tarifas_lugares_detalle.tarlugcod',$codigo)   
                        ->orderby('paises.paisnom')                     
                        ->get();
                     
            if ($TarifasLugares->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($TarifasLugares, new TarifasLugaresDetalleTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function rangos(Request $request) 
    {       
       try{
            
            $codigo = $request->codigo;

            $TarifasLugares = TarifasLugaresRangos::select('tarlugcod','tarrancod','tarranini',
                                'tarranfin','tarranvaldia')
                        ->where('tarlugcod',$codigo)        
                        ->orderby('tarrancod')                     
                        ->get();
                     
            if ($TarifasLugares->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($TarifasLugares, new TarifasLugaresRangosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function rangosNacionales() //sirviria para nacionales
    {       
       try{

            //$codigo = $request->codigo;
            $TarifasLugares = TarifasLugaresRangos::select('tarlugcod','tarrancod','tarranini',
                                'tarranfin','tarranvaldia')                         
                        ->where('tarlugcod','<=',2)   
                        ->orderby('tarrancod')                     
                        ->get();
                     
            if ($TarifasLugares->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($TarifasLugares, new TarifasLugaresRangosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function rangosinternacionales(Request $request)
    {       
       try{

            $codigopais = $request->codigopais;
            $TarifasLugares = TarifasLugaresRangos::select('tarifas_lugares_rangos.tarlugcod',
                                'tarifas_lugares_rangos.tarrancod',
                                'tarifas_lugares_rangos.tarranini',
                                'tarifas_lugares_rangos.tarranfin',
                                'tarifas_lugares_rangos.tarranvaldia')
                        ->join('tarifas_lugares_detalle','tarifas_lugares_rangos.tarlugcod','=','tarifas_lugares_detalle.tarlugcod')                                 
                        ->where('tarifas_lugares_detalle.paiscod',$codigopais)   
                        ->orderby('tarrancod')                     
                        ->get();
                     
            if ($TarifasLugares->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($TarifasLugares, new TarifasLugaresRangosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function paisesTarifas()
    {

        try{            

            $sql = "SELECT p.paiscod codigo,p.paisnom nombrecompleto,0 tarifa,
                           m.moncod moneda,m.monsim simbolomoneda,m.montas tasa,
                           0 tarifapesos
                    FROM tarifas_lugares tl 
                    INNER JOIN tarifas_lugares_detalle tld ON tl.tarlugcod = tld.tarlugcod
                    INNER JOIN paises p ON tld.paiscod = p.paiscod  
                    INNER JOIN monedas m ON tl.moncod = m.moncod 
                    ORDER BY p.paisnom";
                     
            $result = $result =DB::select($sql);

            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }  

    }

    public function consecutivo()
    {      

        $maxVal  = TarifasLugares::max('tarlugcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = TarifasLugares::select(DB::raw('count(tarlugcod) as total'))                      
                      ->where('tarlugdes', $Descripcion)
                      ->where('tarlugcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La Descripción ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        $detalle = $request->detalle;
        $rango = $request->rangos;

        $codigo = $data['codigo'];
        $moneda = $data['moneda'];        
        $descripcion = $data['descripcion'];
       
        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new TarifasLugares();
            
            $dataAdd->tarlugcod = $consecutivo;           
            $dataAdd->moncod = $moneda;           
            $dataAdd->tarlugdes = $descripcion;           
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = TarifasLugares::where('tarlugcod', $codigo)->first();
          
            $dataUpd->moncod = $moneda;            
            $dataUpd->tarlugdes = $descripcion;
            
            $result = $dataUpd->save();
           
        } 

        $codigoactual = 0;

        if($codigo=='0'){

            $codigoactual = $consecutivo;                    

        } else {

            $codigoactual = $codigo;

        }
        
        if(count($detalle) > 0){

            $arrDataActual = [];

            if($codigo != '0'){

                $result = TarifasLugaresDetalle::where('tarlugcod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($detalle); $i++) {
                
                $arrDataActual[] = 
                    [                        
                        'tarlugcod' => $codigoactual,
                        'paiscod'   => $detalle[$i]['pais']                        
                    ];                                                
            }

            $result = TarifasLugaresDetalle::insert($arrDataActual);
        }

        //rangos
        if(count($rango) > 0){

            $arrDataActual = [];

            if($codigo != '0'){

                $result = TarifasLugaresRangos::where('tarlugcod', $codigo)->delete(); 

            }

            for ($i=0; $i < count($rango); $i++) {
                
                $arrDataActual[] = 
                    [                        
                        'tarlugcod' => $codigoactual,
                        'tarrancod' => $rango[$i]['codigo'],
                        'tarranini' => $rango[$i]['rangoinicial'],
                        'tarranfin' => $rango[$i]['rangofinal'], 
                        'tarranvaldia' => $rango[$i]['valordiario'],                        
                    ];                                                
            }

            $result = TarifasLugaresRangos::insert($arrDataActual);
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        try{

            $result = TarifasLugares::where('tarlugcod', $Id)->delete(); 
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }
}

?>