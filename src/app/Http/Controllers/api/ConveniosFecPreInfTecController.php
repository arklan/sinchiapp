<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ConveniosFecPreInfTecTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ConveniosFecPreInfTec;
use DB;

class ConveniosFecPreInfTecController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ConveniosFecPreInfTec = ConveniosFecPreInfTec::select('convcod','convfpit')                        
                        ->orderby('convfpit')                     
                        ->get();
                     
            if ($ConveniosFecPreInfTec->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ConveniosFecPreInfTec, new ConveniosFecPreInfTecTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function PorConvenio(Request $request)
    {
        try{
           
            $codigo = $request['codigo'];
                      
            $ConveniosFecPreInfTec = ConveniosFecPreInfTec::select('convcod','convfpit')                        
                        ->where('convcod', $codigo)                     
                        ->orderby('convfpit')                     
                        ->get();
                     
            if ($ConveniosFecPreInfTec->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ConveniosFecPreInfTec, new ConveniosFecPreInfTecTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }  
    
    public function destroy(Request $request)
    {
        $fecha = $request->fecha;
        $codigo = $request->codigo;
     
        try{

            $result = ConveniosFecPreInfTec::where('convcod', $codigo)
                                            ->where('convfpit', $fecha)
                                            ->delete(); 
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }
    
}

?>