<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\AreasTematicasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AreasTematicas;
use DB;

class AreasTematicasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $AreasTematica = AreasTematicas::select('aretemcod','aretemnom')
                     ->orderby('aretemnom')                     
                     ->get();
                     
            if ($AreasTematica->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($AreasTematica, new AreasTematicasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = AreasTematicas::max('aretemcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = AreasTematicas::select(DB::raw('count(aretemcod) as total'))                      
                      ->where('aretemnom', $Nombre)
                      ->where('aretemcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $AreasTematica = $request->data;

        $codigo = $AreasTematica['codigo'];
        $nombre = strtoupper(trim($AreasTematica['nombre']));       

        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $AreasTematicaAdd = new AreasTematicas();
            
            $AreasTematicaAdd->aretemcod = $consecutivo;           
            $AreasTematicaAdd->aretemnom = $nombre;           
            
            $result = $AreasTematicaAdd->save();
        
        } else {

            $AreasTematicaUpd = AreasTematicas::where('aretemcod', $codigo)->first();
          
            $AreasTematicaUpd->aretemnom = $nombre;          

            // Guardamos en base de datos
            $result = $AreasTematicaUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = AreasTematica::where('aretemcod', $Id)->delete(); 
        return $result;

    }
}

?>