<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PropAdjuntos;
use App\PropRolEdu;
use App\PropRolExp;
use App\PropExpProp;
use App\PropCapOrg;
use App\PropCapFin;
use App\PropMet;
use App\SolProcProponentes;
use App\PropSoportes;
use DB;

class EvaluacionProponenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $adjuntos = DB::reconnect('mysql')->select(DB::raw('SELECT id, id_prop, doc_solicitado as docSolicitado, nombre, path, aprobado, no_aplica as noAplica FROM prop_adjuntos WHERE id_prop = '.$id));
        return $adjuntos;
    }
    public function critProp($id)
    {
        // experiencia del proponente
        $expProp = DB::reconnect('mysql')->select(DB::raw('SELECT se.id as id_expprop, se.tipo, se.descripcion, se.cantidad, se.determinante, se.tipo_det, se.puntaje_und, se.puntaje_max, pp.cantSoportada, pp.cumple, pp.file_name, pp.file_path, pp.observacion FROM solproc_expprop se INNER JOIN solproc_proponentes sp ON sp.id_solproc = se.id_solproc LEFT JOIN prop_expprop pp ON pp.id_expprop = se.id AND pp.id_prop = sp.id WHERE sp.id = '.$id));
        // capacidad organizacional
        $capOrg = DB::reconnect('mysql')->select(DB::raw('SELECT se.id as id_exporg, se.indicador, se.operador, se.numero, po.cumple, po.inhabilita, po.file_name, po.file_path, observacion FROM solproc_exporg se INNER JOIN solproc_proponentes sp ON sp.id_solproc = se.id_solproc LEFT JOIN prop_org po ON po.id_exporg = se.id AND po.id_prop = sp.id WHERE sp.id = '.$id));
        // capacidad financiera
        $capFin = DB::reconnect('mysql')->select(DB::raw('SELECT se.id as id_expfin, se.indicador, se.operador, se.numero, pf.cumple, pf.inhabilita, pf.file_name, pf.file_path, pf.observacion FROM solproc_expfinanciera se INNER JOIN solproc_proponentes sp ON sp.id_solproc = se.id_solproc LEFT JOIN prop_fin pf ON pf.id_expfin = se.id AND pf.id_prop = sp.id WHERE sp.id = '.$id));
        // roles con su educacion y experiencia
        $roles = DB::reconnect('mysql')->select(DB::raw('SELECT sr.id as id_rol, sr.tipo as tipoRol, sr.rol, sr.porc_rol, sr.porc_edu, sr.porc_exp  FROM solproc_roles sr INNER JOIN solproc_proponentes sp ON sp.id_solproc = sr.id_solproc WHERE sp.id = '.$id));
        foreach ($roles as $rol) {
            $educacion = DB::reconnect('mysql')->select(DB::raw("SELECT pre.id as id, sre.id as id_roledu, sre.tipo_educacion, sre.tipo, sre.titulo, sre.puntajeUnd, sre.puntajeMax, sr.tipo as tipoCri, pre.cumple, pre.file_name, pre.file_path, pre.observacion FROM solproc_proponentes sp INNER JOIN solproc_rol_edu sre ON sre.id_solproc = sp.id_solproc INNER JOIN solproc_roles sr ON sr.id = sre.id_solproc_roles LEFT JOIN prop_rol_edu pre ON pre.id_prop = sp.id AND pre.id_solroledu = sre.id WHERE sp.id = " . $id . " AND sr.id = " . $rol->id_rol));
            foreach ($educacion as $edu) {
                $edu->cumple = ($edu->cumple === 0 || $edu->cumple === null) ? false : true;
                //$edu->cumple = ($edu->cumple === 0 || $edu->cumple === null) ? false : true;                
            }
            $experiencia =  DB::reconnect('mysql')->select(DB::raw("SELECT pre.id as id, sre.id as id_rolexp, sre.tipo_exp, sre.descripcion, sre.tiempo, sre.cantidad, sre.puntajeUnd, sre.puntajeMax, sr.tipo as tipoCri, pre.cumple, pre.file_name, pre.file_path, pre.observacion, pre.cantSoportada FROM solproc_proponentes sp INNER JOIN solproc_rol_exp sre ON sre.id_solproc = sp.id_solproc INNER JOIN solproc_roles sr ON sr.id = sre.id_solproc_roles LEFT JOIN prop_rol_exp pre ON pre.id_prop = sp.id AND pre.id_solrolexp = sre.id WHERE sp.id = " . $id . " AND sr.id = " . $rol->id_rol));
            foreach ($experiencia as $exp) {
                $exp->cumple = ($exp->cumple === 0 || $exp->cumple === null) ? false : true;
                if ($exp->id != null) {
                    $exp->soportes = DB::reconnect('mysql')->select(DB::raw("SELECT id, id_exp as idExp, tipo, fecha_ini as dateIni, fecha_fin as dateEnd, file_name, file_path, descripcion FROM prop_soportes WHERE id_exp = " . $exp->id . " AND tipo = 'proprolexp'"));
                    foreach ($exp->soportes as $sop) {
                        $sop->adjunto = ['file_name' => $sop->file_name, 'file_path' => $sop->file_path];
                    } 
                }    
            }
            $rol->educacion = $educacion;
            $rol->experiencia = $experiencia;
        }
        // metodologias
        $expMet = DB::reconnect('mysql')->select(DB::raw('SELECT se.id as id_expmet, se.aspecto, se.puntajemax, actor1, actor2, actor3, act1email, act2email, act3email, puntajeact1, puntajeact2, puntajeact3, pm.file_name, pm.file_path FROM solproc_proponentes sp INNER JOIN solproc_expmet se ON se.id_solproc = sp.id_solproc LEFT JOIN prop_met pm ON pm.id_solproc_met = se.id AND pm.id_prop = sp.id WHERE sp.id = ' . $id));
        $ofertaEco = DB::reconnect('mysql')->select(DB::raw('SELECT sp.ofertaEco as valor, sp.file_path, sp.file_name, of_economica  FROM solproc_proponentes sp INNER JOIN solicitudes_procesos s ON s.id = sp.id_solproc WHERE sp.id = ' . $id));
        $proponente = SolProcProponentes::find($id);
        $totalNec = DB::reconnect('mysql')->select(DB::raw('SELECT SUM(valor) as valor FROM solproc_necesidades WHERE id_solproc = ' . $proponente->id_solproc));
        return response()->json(['expProp' => $expProp, 'capOrg' => $capOrg, 'roles' => $roles, 'capFin' => $capFin, 'expMet' => $expMet, 'ofertaEco' => $ofertaEco, 'totalNec' => $totalNec[0]->valor], 200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function actOfEco(Request $request)
    {
        // return $request->data['idProp']; 
        $id = $request->data['idProp'];
        $dataAdd = SolProcProponentes::find($id);
        $dataAdd->ofertaEco = $request->data['valor'];
        $dataAdd->file_name = $request->data['file_name'];
        $dataAdd->file_path = $request->data['file_path'];
        $dataAdd->save();
        return response()->json(['msessage' => 'success',], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->data['id'];
        $adjuntos = isset($request->data['adjuntos']) ? $request->data['adjuntos'] : [];
        $roles = isset($request->data['roles']) ? $request->data['roles'] : [];
        $expProp = isset($request->data['expProp']) ? $request->data['expProp'] : [];
        $capOrg = isset($request->data['capOrg']) ? $request->data['capOrg'] : [];
        $capFin = isset($request->data['capFin']) ? $request->data['capFin'] : [];
        $expMet = isset($request->data['expMet']) ? $request->data['expMet'] : [];
        $affectedRows = PropAdjuntos::where('id_prop', '=', $id)->delete();
        foreach ($adjuntos as $adj ) {
            // $insertAdj = DB::reconnect('mysql')->insert(DB::raw("INSERT INTO prop_adjuntos (id_prop, doc_solicitado, nombre, path, aprobado, no_aplica) VALUES ($id, $adj['docSolicitado'], $adj['nombre'], $adj['path'])"));
            $propAdj = new PropAdjuntos;
            $propAdj->id_prop = $id;
            $propAdj->doc_solicitado = $adj['docSolicitado'];
            $propAdj->nombre = isset($adj['nombre']) ? $adj['nombre'] : '';
            $propAdj->path = isset($adj['path']) ? $adj['path'] : '';
            $propAdj->aprobado = isset($adj['aprobado']) ? ($adj['aprobado'] == true ? 1 : 0) : 0;
            $propAdj->no_aplica = isset($adj['noAplica']) ? ($adj['noAplica'] == true ? 1 : 0) : 0;
            $propAdj->save();
        }
        // experiencia proponente
        $affectedRows = PropExpProp::where('id_prop', '=', $id)->delete();
        foreach ($expProp as $ep) {
            $propExpProp = new PropExpProp;
            $propExpProp->id_prop = $id;
            $propExpProp->id_expprop = $ep['id_expprop'];
            $propExpProp->cumple = isset($ep['cumple']) ? ($ep['cumple'] == true ? 1 : 0) : 0;
            $propExpProp->file_name = isset($ep['file_name']) ? $ep['file_name'] : '';
            $propExpProp->file_path = isset($ep['file_path']) ? $ep['file_path'] : '';
            $propExpProp->observacion = isset($ep['observacion']) ? $ep['observacion'] : '';
            $propExpProp->cantSoportada = isset($ep['cantSoportada']) ? $ep['cantSoportada'] : '';
            $propExpProp->save();
        }
        // entrando a rows para exp, edu 
        $affectedRows = PropRolEdu::where('id_prop', '=', $id)->delete();
        $affectedRows = PropRolExp::where('id_prop', '=', $id)->delete();
        foreach ($roles as $rol ) {
            foreach ($rol['educacion'] as $rolEdu) {
                $pRolEdu = new PropRolEdu;
                $pRolEdu->id_prop = $id;
                $pRolEdu->id_solroledu = $rolEdu['id_roledu'];
                $pRolEdu->cumple = isset($rolEdu['cumple']) ? ($rolEdu['cumple'] == true ? 1 : 0) : 0;
                $pRolEdu->file_name = isset($rolEdu['file_name']) ? $rolEdu['file_name'] : '';
                $pRolEdu->file_path = isset($rolEdu['file_path']) ? $rolEdu['file_path'] : '';
                $pRolEdu->observacion = $rolEdu['observacion'];
                $pRolEdu->save();
            }
            foreach ($rol['experiencia'] as $rolExp) {
                $pRolExp = new PropRolExp;
                $pRolExp->id_prop = $id;
                $pRolExp->id_solrolexp = $rolExp['id_rolexp'];
                $pRolExp->cumple = isset($rolExp['cumple']) ? ($rolExp['cumple'] == true ? 1 : 0) : 0;
                $pRolExp->file_name = isset($rolExp['file_name']) ? $rolExp['file_name'] : '';
                $pRolExp->file_path = isset($rolExp['file_path']) ? $rolExp['file_path'] : '';
                $pRolExp->observacion = $rolExp['observacion'];
                $pRolExp->cantSoportada = isset($rolExp['cantSoportada']) ? $rolExp['cantSoportada'] : '';
                $pRolExp->save();

                // return $request;
                $items = isset($rolExp['soportes']) ? $rolExp['soportes'] : [];
                $idExp = isset($rolExp['id']) ? $rolExp['id'] : 0;
                DB::reconnect('mysql')->delete(DB::raw("DELETE FROM prop_soportes WHERE id_exp = $idExp AND tipo = 'proprolexp'" ));
                foreach ($items as $item) {
                    $pSoporte = new PropSoportes;
                    $pSoporte->id_exp = $pRolExp->id;
                    $pSoporte->tipo = $item['tipo'];
                    $pSoporte->fecha_ini = $item['dateIni'];
                    $pSoporte->fecha_fin = $item['dateEnd'];
                    $pSoporte->file_name = $item['adjunto']['file_name'];
                    $pSoporte->file_path = $item['adjunto']['file_path'];
                    $pSoporte->descripcion = $item['descripcion'];
                    $pSoporte->save();
                }

            }
        }
        // capacidad organizacional
        $affectedRows = PropCapOrg::where('id_prop', '=', $id)->delete();
        foreach ($capOrg as $co) {
            $propCapOrg = new PropCapOrg;
            $propCapOrg->id_prop = $id;
            $propCapOrg->id_exporg = $co['id_exporg'];
            $propCapOrg->cumple = isset($co['cumple']) ? ($co['cumple'] == true ? 1 : 0) : 0;
            $propCapOrg->inhabilita = isset($co['inhabilita']) ? ($co['inhabilita'] == true ? 1 : 0) : 0;
            $propCapOrg->file_name = isset($co['file_name']) ? $co['file_name'] : '';
            $propCapOrg->file_path = isset($co['file_path']) ? $co['file_path'] : '';
            $propCapOrg->observacion = isset($co['observacion']) ? $co['observacion'] : '';
            $propCapOrg->save();
        }
        // capacidad financiera
        $affectedRows = PropCapFin::where('id_prop', '=', $id)->delete();
        foreach ($capFin as $cf) {
            $propCapFin = new PropCapFin;
            $propCapFin->id_prop = $id;
            $propCapFin->id_expfin = $cf['id_expfin'];
            $propCapFin->cumple = isset($cf['cumple']) ? ($cf['cumple'] == true ? 1 : 0) : 0;
            $propCapFin->inhabilita = isset($cf['inhabilita']) ? ($cf['inhabilita'] == true ? 1 : 0) : 0;
            $propCapFin->file_name = isset($cf['file_name']) ? $cf['file_name'] : '';
            $propCapFin->file_path = isset($cf['file_path']) ? $cf['file_path'] : '';
            $propCapFin->observacion = isset($cf['observacion']) ? $cf['observacion'] : '';
            $propCapFin->save();
        }
        $affectedRows = PropMet::where('id_prop', '=', $id)->delete();
        foreach ($expMet as $em) {
            $propMet = new PropMet;
            $propMet->id_prop = $id;
            $propMet->id_solproc_met = $em['id_expmet'];
            $propMet->actor1 = isset($em['actor1']) ? $em['actor1'] : '';
            $propMet->actor2 = isset($em['actor2']) ? $em['actor2'] : '';
            $propMet->actor3 = isset($em['actor3']) ? $em['actor3'] : '';
            $propMet->file_name = isset($em['file_name']) ? $em['file_name'] : '';
            $propMet->file_path = isset($em['file_path']) ? $em['file_path'] : '';
            $propMet->save();
        }
        return $adjuntos;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
