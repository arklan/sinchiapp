<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\LineasInvestigacionPeniaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LineasInvestigacionPenia;
use DB;

class LinInvestigacionPeniaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{
            
            $Programas = LineasInvestigacionPenia::select('lineas_investigacion_penia.lininvpencod',
                                    'lineas_investigacion_penia.lininvpendes','lineas_investigacion_penia.proinvpencod',
                                    'programas_investigacion_penia.proinvpendes')  
                        ->join('programas_investigacion_penia','lineas_investigacion_penia.proinvpencod','=','programas_investigacion_penia.proinvpencod')                                  
                        ->orderby('lineas_investigacion_penia.lininvpendes')                     
                        ->get();
                     
            if ($Programas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Programas, new LineasInvestigacionPeniaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function FiltroXPrograma(Request $request)
    {       
       try{

            $programa = $request->programa;

            $data = LineasInvestigacionPenia::select('lineas_investigacion_penia.lininvpencod',
                                                'lineas_investigacion_penia.lininvpendes','lineas_investigacion_penia.proinvpencod',
                                                'programas_investigacion_penia.proinvpendes') 
                                            ->join('programas_investigacion_penia','lineas_investigacion_penia.proinvpencod','=','programas_investigacion_penia.proinvpencod')  
                                            ->where('lineas_investigacion_penia.proinvpencod',$programa)
                                            ->orderby('lineas_investigacion_penia.lininvpendes')                     
                                            ->get();
                     
            if ($data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($data, new LineasInvestigacionPeniaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = LineasInvestigacionPenia::max('lininvpencod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = LineasInvestigacionPenia::select(DB::raw('count(lininvpencod) as total'))                      
                      ->where('lininvpendes', $Nombre)
                      ->where('lininvpencod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo = $data['codigo'];       
        $descripcion = strtoupper(trim($data['descripcion']));
        $codigoproinvpenia = $data['codigoproinvpenia'];
              
        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new LineasInvestigacionPenia();
            
            $dataAdd->lininvpencod = $consecutivo;
            $dataAdd->lininvpendes = $descripcion;
            $dataAdd->proinvpencod = $codigoproinvpenia;            
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = LineasInvestigacionPenia::where('lininvpencod', $codigo)->first();
          
            $dataUpd->lininvpendes = $descripcion;
            $dataUpd->proinvpencod = $codigoproinvpenia;

            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = LineasInvestigacionPenia::where('lininvpencod', $Id)->delete(); 
        return $result;

    }
}

?>