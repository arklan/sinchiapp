<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\TiposContratosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TiposContratos;
use DB;

class TiposContratosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $TiposContratos = TiposContratos::select('tipcontcod','tipcontdes')
                     ->orderby('tipcontdes')                     
                     ->get();
                     
            if ($TiposContratos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($TiposContratos, new TiposContratosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = TiposContratos::max('tipcontcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($descripcion, $codigo){

        try{
           
            $mensaje = '';

            $Duplica = TiposContratos::select(DB::raw('count(tipcontcod) as total'))                      
                      ->where('tipcontdes', $descripcion)
                      ->where('tipcontcod','!=', $codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El descripcion ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function TiposContratosAddUpdate(Request $request)
    {
        $TiposContratos = $request->data;

        $codigo = $TiposContratos['codigo'];
        $descripcion = strtoupper(trim($TiposContratos['descripcion']));       

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $TiposContratosAdd = new TiposContratos();
            
            $TiposContratosAdd->tipcontcod = $consecutivo;           
            $TiposContratosAdd->tipcontdes = $descripcion;           
            
            $result = $TiposContratosAdd->save();
        
        } else {

            $TiposContratosUpd = TiposContratos::where('tipcontcod', $codigo)->first();
          
            $TiposContratosUpd->tipcontdes = $descripcion;          

            // Guardamos en base de datos
            $result = $TiposContratosUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = TiposContratos::where('tipcontcod', $Id)->delete(); 
        return $result;

    }
}

?>