<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ConveniosMarcoTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ConveniosMarco;

use DB;

class ConveniosMarcoController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ConveniosMarco = ConveniosMarco::select('convenios_marco.conmarcod', 'convenios_marco.conmarnomaso','convenios_marco.conmarfecsus',
                                            'convenios_marco.conmarfecter','convenios_marco.conmarfecini','convenios_marco.estcod',
                                            'convenios_marco.conmarobj','convenios_marco.conmarobs','convenios_marco.conmararccon',
                                            'convenios_marco.conmararcoda','estados.estdes')
                     ->join('estados','convenios_marco.estcod','=','estados.estcod')                   
                     ->orderby('convenios_marco.conmarcod')    
                     ->limit(20)                  
                     ->get();
                     
            if ($ConveniosMarco->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ConveniosMarco, new ConveniosMarcoTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function filtroXConvenio(Request $request)
    {       
       try{

            $convenio = $request->convenio;

            $ConveniosMarco = ConveniosMarco::select('convenios_marco.conmarcod', 'convenios_marco.conmarnomaso','convenios_marco.conmarfecsus',
                                            'convenios_marco.conmarfecter','convenios_marco.conmarfecini','convenios_marco.estcod',
                                            'convenios_marco.conmarobj','convenios_marco.conmarobs',
                                            'estados.estdes','convenios_marco.conmararccon','convenios_marco.conmararcoda')
                     ->join('estados','convenios_marco.estcod','=','estados.estcod')                    
                     ->where('convenios_marco.conmarcod',$convenio)
                     ->orderby('conmarcod')                                         
                     ->get();
                     
            if ($ConveniosMarco->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ConveniosMarco, new ConveniosMarcoTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = ConveniosMarco::max('conmarcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = ConveniosMarco::select(DB::raw('count(conmarcod) as total'))                      
                      ->where('conmarnomaso', $nombre)
                      ->where('conmarcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Nombre Asociado ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        print_r($data);

        $codigo             = $data['codigo'];
        $nombreasociado     = trim($data['nombreasociado']);
        $fechasuscripcion   = $data['fechasuscripcion'];
        $fechaterminacion   = $data['fechaterminacion']; 
        $fechainicio        = $data['fechainicio'];       
        $codigoestado       = $data['codigoestado'];
        $objeto             = $data['objeto'];
        $observacion        = $data['observacion'];
        $archivogeneral     = $data['archivogeneral'];
        $archivootda        = $data['archivootda'];
        
        $mensaje = $this->buscarDuplicado($nombreasociado, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $ConveniosMarcoAdd = new ConveniosMarco();
            
            $ConveniosMarcoAdd->conmarcod       = $consecutivo;           
            $ConveniosMarcoAdd->conmarnomaso    = $nombreasociado; 
            $ConveniosMarcoAdd->conmarfecsus    = $fechasuscripcion;
            $ConveniosMarcoAdd->conmarfecter    = $fechaterminacion;
            $ConveniosMarcoAdd->conmarfecini    = $fechainicio;            
            $ConveniosMarcoAdd->estcod          = $codigoestado;
            $ConveniosMarcoAdd->conmarobj       = $objeto; 
            $ConveniosMarcoAdd->conmarobs       = $observacion;                     
            $ConveniosMarcoAdd->conmararccon    = $archivogeneral;
            $ConveniosMarcoAdd->conmararcoda    = $archivootda;
            
            $result = $ConveniosMarcoAdd->save();
        
        } else {

            $ConveniosMarcoUpd = ConveniosMarco::where('conmarcod', $codigo)->first();
          
            $ConveniosMarcoUpd->conmarnomaso    = $nombreasociado; 
            $ConveniosMarcoUpd->conmarfecsus    = $fechasuscripcion;
            $ConveniosMarcoUpd->conmarfecter    = $fechaterminacion;
            $ConveniosMarcoUpd->conmarfecini    = $fechainicio;            
            $ConveniosMarcoUpd->estcod          = $codigoestado;
            $ConveniosMarcoUpd->conmarobj       = $objeto; 
            $ConveniosMarcoUpd->conmarobs       = $observacion;            
            $ConveniosMarcoUpd->conmararccon    = $archivogeneral;
            $ConveniosMarcoUpd->conmararcoda    = $archivootda;         

            // Guardamos en base de datos
            $result = $ConveniosMarcoUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = ConveniosMarco::where('conmarcod', $Id)->delete(); 
        return $result;

    }
}

?>