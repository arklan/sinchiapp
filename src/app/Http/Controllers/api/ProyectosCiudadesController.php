<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosCiudadesTransformer;
use App\Transformers\CiudadesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosCiudades;
use App\Ciudades;
use DB;
class ProyectosCiudadesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosCiudades = ProyectosCiudades::select('proyectos_ciudades.proycod','proyectos_ciudades.ciucod',
                                'ciudades.ciunom','departamentos.depnom')
                        ->join('ciudades','ciudades.ciucod','=','proyectos_ciudades.ciucod')
                        ->join('departamentos','departamentos.depcod','=','ciudades.depcod')
                        ->get();
                     
            if ($ProyectosCiudades->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosCiudades, new ProyectosCiudadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = '';

        for ($i=0; $i < count($data); $i++) {

            $codigo = $data[$i]['codigo'];
            $proyecto = $data[$i]['proyecto'];
            $departamento = $data[$i]['departamento'];

            if($i==0){

                $query = "DELETE pc FROM proyectos_ciudades pc INNER JOIN ciudades c ON pc.ciucod = c.ciucod  
                         WHERE pc.proycod=".$proyecto." AND c.depcod = '".$departamento."'";
                DB::delete($query);
            }                

            $dataAdd[] = array(
                'proycod'=>$proyecto, 'ciucod'=> $codigo               
            );
        }

        if(count($dataAdd) > 0)
            $result = ProyectosCiudades::insert($dataAdd);        

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];                       

            $data = ProyectosCiudades::select('proyectos_ciudades.proycod','proyectos_ciudades.ciucod','ciudades.ciunom',
                                                'departamentos.depcod','departamentos.depnom')
                        ->join('ciudades','ciudades.ciucod','=','proyectos_ciudades.ciucod')
                        ->join('departamentos','departamentos.depcod','=','ciudades.depcod')                       
                        ->where('proycod', $proyecto)                               
                        ->get();
                     
            if ($data->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($data, new ProyectosCiudadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function CiudadesSeleccionadasXDpto(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
            $departamento = $request['departamento'];                       

            $ciudades = Ciudades::select('ciudades.ciucod','ciudades.ciunom','departamentos.depnom','departamentos.depcod',
                                         DB::raw('IF(proyectos_ciudades.ciucod IS NULL, 0, 1) AS Seleccionar'))                        
                        ->join('departamentos','departamentos.depcod','=','ciudades.depcod')
                        ->leftJoin('proyectos_ciudades', function($join) use ($proyecto) {
                            $join->on('ciudades.ciucod', '=', 'proyectos_ciudades.ciucod')
                                ->where('proyectos_ciudades.proycod','=',$proyecto);
                          })
                        ->where('ciudades.depcod', $departamento)
                        ->orderby('ciudades.ciunom')                               
                        ->get();
                          
            if ($ciudades->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ciudades, new CiudadesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $codigo = $request->codigo;
        $proyecto = $request->proyecto;
        
        print_r('code'.$codigo);
        print_r('proyecto'.$proyecto);

        try{

            $result = ProyectosCiudades::where('proycod', $proyecto)
                                        ->where('ciucod', $codigo)
                                        ->delete(); 
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }

    public function FiltrarProyectoAll(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];                       

            $data = ProyectosCiudades::select('ciucod')                       
                        ->where('proycod', $proyecto)                               
                        ->get();
                     
            return $this->response->collection($data, new ProyectosCiudadesTransformer);

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    
}

?>