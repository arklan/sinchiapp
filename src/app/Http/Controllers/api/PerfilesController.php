<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\PerfilesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Perfiles;
use DB;

class PerfilesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Perfiles = Perfiles::select('perfcod','perfnom')
                     ->orderby('perfnom')                     
                     ->get();
                     
            if ($Perfiles->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Perfiles, new PerfilesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = Perfiles::max('perfcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Valor, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = Perfiles::select(DB::raw('count(perfcod) as total'))                      
                      ->where('perfnom', $Valor)
                      ->where('perfcod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El Perfil ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function perfilAddUpdate(Request $request)
    {
        $perfil = $request->data;

        $codigo = $perfil['codigo'];
        $nombre = strtoupper($perfil['nombre']);       

        $mensaje = $this->buscarDuplicado($nombre, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $perfilAdd = new Perfiles();
            
            $perfilAdd->perfcod = $consecutivo;           
            $perfilAdd->perfnom = $nombre;
                       
            $result = $perfilAdd->save();
        
        } else {

            $perfilUpd = Perfiles::where('perfcod', $codigo)->first();          
           
            $perfilUpd->perfnom = $nombre;          

            // Guardamos en base de datos
            $result = $perfilUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    public function actualizarPermisosPerfil(Request $request)
    {        

        $perfilid = $request->perfilid;
        $permisos = $request->data;
        
        //se elimina los permisos actuales del usaurio
        $result = DB::table('opciones_perfiles')->where(['perfcod' => $perfilid])->delete();
        
        if(count($permisos) > 0){
            foreach ($permisos as $value) {
                    
                $opciones[] = 
                    [
                        'perfcod' => $value['perfcod'],
                        'opccod'  => $value['opccod'],                    
                    ];           
            }
            //inserts
            $result = DB::table('opciones_perfiles')->insert($opciones);
        }

        return array( 'respuesta' => $result);          
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = Perfiles::where('perfcod', $Id)->delete(); 
        return $result;

    }
}

?>