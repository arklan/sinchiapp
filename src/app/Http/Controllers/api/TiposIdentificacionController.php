<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\TiposIdentificacionTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TiposIdentificacion;
use DB;

class TiposIdentificacionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $TiposIdentificacion = TiposIdentificacion::select('tipidecod','tipidedes','tipidelet')
                     ->orderby('tipidedes')                     
                     ->get();
                     
            if ($TiposIdentificacion->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($TiposIdentificacion, new TiposIdentificacionTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = TiposIdentificacion::max('tipidecod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $letra, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = TiposIdentificacion::select(DB::raw('count(tipidecod) as total'))                      
                      ->where('tipidedes', $Nombre)
                      ->where('tipidecod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                } else {

                    $Duplica = TiposIdentificacion::select(DB::raw('count(tipidecod) as total'))                      
                          ->where('tipidelet', $letra)
                          ->where('tipidecod','!=', $Codigo)
                          ->get();
    
                    if (!$Duplica->isEmpty()) {
                    
                        $total = $Duplica[0]->total;
        
                        if($total != '0'){
        
                            $mensaje = 'La Abreviatura ya existe';
        
                        }
                    }      
    
                }
                
            } else {

                $Duplica = TiposIdentificacion::select(DB::raw('count(tipidecod) as total'))                      
                      ->where('tipidelet', $letra)
                      ->where('tipidecod','!=', $Codigo)
                      ->get();

                if (!$Duplica->isEmpty()) {
                
                    $total = $Duplica[0]->total;
    
                    if($total != '0'){
    
                        $mensaje = 'La Abreviatura ya existe';
    
                    }
                }      

            }           

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo = $data['codigo'];
        $descripcion = strtoupper(trim($data['descripcion']));
        $letra = strtoupper(trim($data['letra']));       

        $mensaje = $this->buscarDuplicado($descripcion, $letra, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new TiposIdentificacion();
            
            $dataAdd->tipidecod = $consecutivo;           
            $dataAdd->tipidedes = $descripcion;
            $dataAdd->tipidelet = $letra;           
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = TiposIdentificacion::where('tipidecod', $codigo)->first();
          
            $dataUpd->tipidedes = $descripcion;
            $dataUpd->tipidelet = $letra;          

            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = TiposIdentificacion::where('tipidecod', $Id)->delete(); 
        return $result;

    }
}

?>