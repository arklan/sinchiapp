<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\OrganismosVivosTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\OrganismosVivos;
use DB;

class OrganismosVivosController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $OrganismosVivos = OrganismosVivos::select('orgvivcod','orgvivdes')
                     ->orderby('orgvivdes')                     
                     ->get();
                     
            if ($OrganismosVivos->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($OrganismosVivos, new OrganismosVivosTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = OrganismosVivos::max('orgvivcod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($descripcion, $codigo){

        try{
           
            $mensaje = '';

            $Duplica = OrganismosVivos::select(DB::raw('count(orgvivcod) as total'))                      
                      ->where('orgvivdes', $descripcion)
                      ->where('orgvivcod','!=', $codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'El descripcion ya existe';

                }
            }            

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $OrganismosVivos = $request->data;

        $codigo = $OrganismosVivos['codigo'];
        $descripcion = strtoupper(trim($OrganismosVivos['descripcion']));       

        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $OrganismosVivosAdd = new OrganismosVivos();
            
            $OrganismosVivosAdd->orgvivcod = $consecutivo;           
            $OrganismosVivosAdd->orgvivdes = $descripcion;           
            
            $result = $OrganismosVivosAdd->save();
        
        } else {

            $OrganismosVivosUpd = OrganismosVivos::where('orgvivcod', $codigo)->first();
          
            $OrganismosVivosUpd->orgvivdes = $descripcion;          

            // Guardamos en base de datos
            $result = $OrganismosVivosUpd->save();
           
        } 

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = OrganismosVivos::where('orgvivcod', $Id)->delete(); 
        return $result;

    }
}

?>