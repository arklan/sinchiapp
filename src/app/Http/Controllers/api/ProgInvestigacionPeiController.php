<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProgInvestigacionPeiTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProgramasInvestigacionPei;
use DB;

class ProgInvestigacionPeiController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{
            
            $Programas = ProgramasInvestigacionPei::select('proinvpeicod','proinvpeides')                       
                        ->orderby('proinvpeides')                     
                        ->get();
                     
            if ($Programas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Programas, new ProgInvestigacionPeiTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function consecutivo()
    {      

        $maxVal  = ProgramasInvestigacionPei::max('proinvpeicod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    private function buscarDuplicado($Nombre, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = ProgramasInvestigacionPei::select(DB::raw('count(proinvpeicod) as total'))                      
                      ->where('proinvpeides', $Nombre)
                      ->where('proinvpeicod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $codigo = $data['codigo'];        
        $descripcion = strtoupper(trim($data['descripcion']));        
              
        $mensaje = $this->buscarDuplicado($descripcion, $codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new ProgramasInvestigacionPei();
            
            $dataAdd->proinvpeicod = $consecutivo;
            $dataAdd->proinvpeides = $descripcion;            
                       
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = ProgramasInvestigacionPei::where('proinvpeicod', $codigo)->first();
          
            $dataUpd->proinvpeides = $descripcion;
            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = ProgramasInvestigacionPei::where('proinvpeicod', $Id)->delete(); 
        return $result;

    }
}

?>