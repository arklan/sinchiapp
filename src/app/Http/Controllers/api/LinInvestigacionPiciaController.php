<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\LineasInvestigacionPiciaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LineasInvestigacionPicia;
use DB;

class LinInvestigacionPiciaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{
            
            $Programas = LineasInvestigacionPicia::select('lininvpiccod','lininvpicdes')                        
                        ->orderby('lininvpicdes')                     
                        ->get();
                     
            if ($Programas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Programas, new LineasInvestigacionPiciaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = LineasInvestigacionPicia::max('lininvpiccod');

        if (is_numeric($maxVal)) {

            $Codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $Codigo = 1;
        }
            
        return $Codigo;
    }

    private function buscarDuplicado($Descripcion, $Codigo){

        try{
           
            $mensaje = '';

            $Duplica = LineasInvestigacionPicia::select(DB::raw('count(lininvpiccod) as total'))                      
                      ->where('lininvpicdes', $Descripcion)
                      ->where('lininvpiccod','!=', $Codigo)
                      ->get();

            $total = 0;          
            if (!$Duplica->isEmpty()) {
                
                $total = $Duplica[0]->total;

                if($total != '0'){

                    $mensaje = 'La descripción ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;

        $Codigo = $data['codigo'];       
        $Descripcion = strtoupper(trim($data['descripcion']));
              
        $mensaje = $this->buscarDuplicado($Descripcion, $Codigo);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($Codigo=='0'){

            $consecutivo =$this->consecutivo();

            $dataAdd = new LineasInvestigacionPicia();
            
            $dataAdd->lininvpiccod = $consecutivo;
            $dataAdd->lininvpicdes = $Descripcion;            
            
            $result = $dataAdd->save();
        
        } else {

            $dataUpd = LineasInvestigacionPicia::where('lininvpiccod', $Codigo)->first();
          
            $dataUpd->lininvpicdes = $Descripcion;            
            // Guardamos en base de datos
            $result = $dataUpd->save();
           
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;

        $result = LineasInvestigacionPicia::where('lininvpiccod', $Id)->delete(); 
        return $result;

    }
}

?>