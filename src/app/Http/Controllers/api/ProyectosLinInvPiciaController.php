<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosLinInvPiciaTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosLinInvPicia;
use DB;

class ProyectosLinInvPiciaController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosLinInvPicia = ProyectosLinInvPicia::select('proycod','lininvpiccod')                        
                        ->orderby('lininvpiccod')                     
                        ->get();
                     
            if ($ProyectosLinInvPicia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosLinInvPicia, new ProyectosLinInvPiciaTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = '';

        for ($i=0; $i < count($data); $i++) {             
        
            $codigo = $data[$i]['codigo'];
            $proyecto = $data[$i]['proyecto'];
            
            if($i==0){

                $result = ProyectosLinInvPicia::where('proycod', $proyecto)->delete();
            }

            $dataAdd = new ProyectosLinInvPicia();
            
            $dataAdd->lininvpiccod  = $codigo;
            $dataAdd->proycod       = $proyecto;

            $result = $dataAdd->save();            
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
                     
            $ProyectosLinInvPicia = ProyectosLinInvPicia::select('lininvpiccod','proycod')                        
                        ->where('proycod', $proyecto)                        
                        ->orderby('lininvpiccod')                     
                        ->get();
            
            return $this->response->collection($ProyectosLinInvPicia, new ProyectosLinInvPiciaTransformer);            
            
            /*if ($ProyectosLinInvPicia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosLinInvPicia, new ProyectosLinInvPiciaTransformer);                
            }*/

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function seleccionadosProyectos(Request $request)
    {
        try{
            
            $proyecto = $request['proyecto'];
                     
            $sql = "SELECT l.lininvpiccod codigo,l.lininvpicdes descripcion, IF(p.proycod IS NULL,0,1) Activo 
                    FROM lineas_investigacion_picia l LEFT JOIN 
                    proyectos_lininv_picia p ON l.lininvpiccod = p.lininvpiccod AND p.proycod=".$proyecto;
            
            $result = DB::select($sql);
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
}

?>