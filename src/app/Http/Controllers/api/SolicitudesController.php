<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\SolicitudesTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Solicitudes;
// use App\SolicitudesPlanesadquisicionNecesidades;
use App\SolNecesidades;
use App\SolicitudesObligaciones;
use App\SolicitudesProductos;
use App\SolicitudesInformes;
use App\SolicitudesCiudades;
use App\SolicitudesSupervisores;
use App\SolicitudesPasante;
use App\SolicitudesConvenio;
use App\SolicitudesAdjuntos;
use App\SolicitudesFormapagoEncabezado;
use App\SolicitudesFormapagoDetalle;
use App\PlanesAdquisicionNecesidades;

use DB;

class SolicitudesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Solicitudes = Solicitudes::select('id','estado', 'fecha_creacion')                               
                ->orderby('id')                     
                ->get();
                     
            if ($Solicitudes->isEmpty()) {

                return $this->response->errorNotFound('No hay datos para Mostrar');
                
            }
            else {

                return $this->response->collection($Solicitudes, new SolicitudesTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $necesidades = $request->data['necesidades'];
        $codTipo = $request->data['codTipo'];
        if (count($necesidades) > 0) {
            $solicitud = new Solicitudes;
            $solicitud->estado = 'creado';
            $solicitud->codtipo = $codTipo;
            $solicitud->fecha_creacion = date('Y-m-d H:i:s');
            $solicitud->save();
            if (isset($solicitud->id)) {
                foreach ($necesidades as $necesidad) {
                    $nec = new SolNecesidades;
                    $nec->id_solcontratacion = $solicitud->id;
                    $nec->cod_necesidad = $necesidad['codigo'];
                    $nec->cod_plan = $necesidad['planadquisicion'];
                    $nec->descripcion = $necesidad['descripcion'];
                    $nec->save();
                }
                return response()->json(['id' => $solicitud->id], 201);
            }
        } else {
            return response()->json(['error' => 'No hay necesidades asociadas a la solicitud'], 401);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Solicitudes = Solicitudes::where('id', '=', $id)->get();
                    
            if ($Solicitudes->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {
                $solicitud = $Solicitudes[0];

                $Necesidades = DB::reconnect('mysql')->select(DB::raw('SELECT sn.cod_plan as planadquisicion, sn.cod_necesidad as cod_necesidad, pn.plaadqnecdes as descripcion, pn.plaadqnecval as valor FROM solicitudes_necesidades sn INNER JOIN planesadquisiciones_necesidades pn ON sn.cod_plan = pn.plaadqcod AND sn.cod_necesidad = pn.plaadqneccod WHERE sn.id_solcontratacion = '.$id));
                $Procesos = DB::reconnect('mysql')->select(DB::raw('SELECT id, obj_contractual as objContractual, fecha_limite as fechaLimite, fecha_creacion as fechaCreacion, tipo_contratacion as tipoContratacion, tipo_proc as tipoProceso, tiempo_asignado as tiempoAsignado FROM solicitudes_procesos WHERE id_solcontratacion = '.$id));
                
                $solicitud->necesidades = $Necesidades;
                $solicitud->Procesos = $Procesos;

                return json_decode(json_encode($solicitud), true);    
            }
            
        }catch(Exception $e){

            return $e->getMessage();
        }
    }

    public function edit($id)
    {
        return 'not set yet';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $solicitud = Solicitudes::find($id);
        $solicitud->delete();
        Solicitudes::destroy($id);
        return('registro eliminado');
    }
}

?>