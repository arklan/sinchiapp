<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ContratistasTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contratistas;
use DB;

class ContratistasController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $Contratistas = Contratistas::select('contratistas.contacod','contratistas.tipidecod','contratistas.contide',
                                    'contratistas.contape','contratistas.contnom','contratistas.contfecnac',
                                    'contratistas.contdir','contratistas.conttel','contratistas.contema',
                                    'contratistas.contmes','contratistas.contval','tipos_identificacion.tipidedes')
                     ->join('tipos_identificacion','contratistas.tipidecod','=','tipos_identificacion.tipidecod')                    
                     ->orderby('contnom','contape')
                     ->get();
                     
            if ($Contratistas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Contratistas, new ContratistasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function FiltroXCodigo(Request $request)
    {       
       $codigo = $request['codigo'];

       try{

            $Contratistas = Contratistas::select('contratistas.contacod','contratistas.tipidecod','contratistas.contide',
                                'contratistas.contape','contratistas.contnom','contratistas.contfecnac',
                                'contratistas.contdir','contratistas.conttel','contratistas.contema',
                                'contratistas.contmes','contratistas.contval','tipos_identificacion.tipidedes')
                     ->join('tipos_identificacion','contratistas.tipidecod','=','tipos_identificacion.tipidecod')                     
                     ->where('contratistas.contacod', $codigo)
                     ->orderby('contnom','contape')                     
                     ->get();
                     
            if ($Contratistas->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($Contratistas, new ContratistasTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }
}

?>