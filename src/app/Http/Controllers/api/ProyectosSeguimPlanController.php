<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosSeguimPlanTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosSeguimPlan;
use DB;

class ProyectosSeguimPlanController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosSeguimPlan = ProyectosSeguimPlan::select('proycod',                                                
                                                                'proyobjcod',
                                                                'proyactsec',
                                                                'proyspaano',
                                                                'proyspames1',
                                                                'proyspames2',
                                                                'proyspames3',
                                                                'proyspames4',
                                                                'proyspames5',
                                                                'proyspames6',
                                                                'proyspames7',
                                                                'proyspames8',
                                                                'proyspames9',
                                                                'proyspames10',
                                                                'proyspames11',
                                                                'proyspames12',
                                                                'proyspakmes1',
                                                                'proyspakmes2',
                                                                'proyspakmes3',
                                                                'proyspakmes4',
                                                                'proyspakmes5',
                                                                'proyspakmes6',
                                                                'proyspakmes7',
                                                                'proyspakmes8',
                                                                'proyspakmes9',
                                                                'proyspakmes10',
                                                                'proyspakmes11',
                                                                'proyspakmes12')
                                                    ->orderby('proyactsec')
                                                    ->orderby('proyspaano')                     
                                                    ->get();
                     
            if ($ProyectosSeguimPlan->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosSeguimPlan, new ProyectosSeguimPlanTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {        
        $proyecto = $request->proyecto;
        $objetivo = $request->objetivo;
        $actividad = $request->actividad;
        $anno = $request->anno;

        try{

            $result = ProyectosSeguimPlan::where('proycod', $proyecto)->where('proyobjcod', $objetivo)
                                            ->where('proyactcod', $actividad)
                                            ->where('proyspaano', $anno)->delete();
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }

    }

    private function buscarDataExiste($proyecto, $objetivo, $actividad, $anno){

        try{
           
            $ok = false;

            $Encontrado = ProyectosSeguimPlan::select(DB::raw('count(proyobjcod) as total'))
                      ->where('proycod', $proyecto)
                      ->where('proyobjcod',$objetivo)
                      ->where('proyactcod',$actividad)
                      ->where('proyspaano',$anno)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    private function buscarDataExisteSec($actividad, $anno){

        try{
           
            $ok = false;

            $Encontrado = ProyectosSeguimPlan::select(DB::raw('count(proyobjcod) as total'))
                      ->where('proyactsec', $actividad)                     
                      ->where('proyspaano',$anno)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        print_r($data);

        if (count($data) > 0 ){

            for ($i=0; $i < count($data); $i++) {

                //$Encontrado = $this->buscarDataExiste($data[$i]['proyecto'] , $data[$i]['codigoobjetivo'], $data[$i]['codigoactividad'], $data[$i]['anno']);

                $Encontrado = $this->buscarDataExisteSec($data[$i]['codigoactividad'], $data[$i]['anno']); 

                if($Encontrado){

                    $query = "UPDATE proyectos_seguimiento_planaccion SET proyspames1 = ".$data[$i]['mes1'].",
                                proyspames2 = ".$data[$i]['mes2'].",proyspames3 = ".$data[$i]['mes3'].",
                                proyspames4 = ".$data[$i]['mes4'].",proyspames5 = ".$data[$i]['mes5'].",
                                proyspames6 = ".$data[$i]['mes6'].",proyspames7 = ".$data[$i]['mes7'].",
                                proyspames8 = ".$data[$i]['mes8'].",proyspames9 = ".$data[$i]['mes9'].",
                                proyspames10 = ".$data[$i]['mes10'].",proyspames11 = ".$data[$i]['mes11'].",
                                proyspames12 = ".$data[$i]['mes12'].",proyspakmes1 = ".$data[$i]['mesk1'].",
                                proyspakmes2 = ".$data[$i]['mesk2'].",proyspakmes3 = ".$data[$i]['mesk3'].",
                                proyspakmes4 = ".$data[$i]['mesk4'].",proyspakmes5 = ".$data[$i]['mesk5'].",
                                proyspakmes6 = ".$data[$i]['mesk6'].",proyspakmes7 = ".$data[$i]['mesk7'].",
                                proyspakmes8 = ".$data[$i]['mesk8'].",proyspakmes9 = ".$data[$i]['mesk9'].",
                                proyspakmes10 = ".$data[$i]['mesk10'].",proyspakmes11 = ".$data[$i]['mesk11'].",
                                proyspakmes12 = ".$data[$i]['mesk12']."
                                WHERE proyactsec = ".$data[$i]['codigoactividad']." AND proyspaano = ".$data[$i]['anno'] ;
                    $result = DB::update($query); 

                } else {

                    $dataSave = new ProyectosSeguimPlan();

                    $dataSave->proycod    = $data[$i]['proyecto'];
                    $dataSave->proyobjcod = $data[$i]['codigoobjetivo'];
                    $dataSave->proyactsec = $data[$i]['codigoactividad'];
                    $dataSave->proyspaano = $data[$i]['anno'];
                    $dataSave->proyspames1 = $data[$i]['mes1'];
                    $dataSave->proyspames2 = $data[$i]['mes2'];
                    $dataSave->proyspames3 = $data[$i]['mes3'];
                    $dataSave->proyspames4 = $data[$i]['mes4'];
                    $dataSave->proyspames5 = $data[$i]['mes5'];
                    $dataSave->proyspames6 = $data[$i]['mes6'];
                    $dataSave->proyspames7 = $data[$i]['mes7'];
                    $dataSave->proyspames8 = $data[$i]['mes8'];
                    $dataSave->proyspames9 = $data[$i]['mes9'];
                    $dataSave->proyspames10 = $data[$i]['mes10'];
                    $dataSave->proyspames11 = $data[$i]['mes11'];
                    $dataSave->proyspames12 = $data[$i]['mes12'];
                    $dataSave->proyspakmes1 = $data[$i]['mesk1'];
                    $dataSave->proyspakmes2 = $data[$i]['mesk2'];
                    $dataSave->proyspakmes3 = $data[$i]['mesk3'];
                    $dataSave->proyspakmes4 = $data[$i]['mesk4'];
                    $dataSave->proyspakmes5 = $data[$i]['mesk5'];
                    $dataSave->proyspakmes6 = $data[$i]['mesk6'];
                    $dataSave->proyspakmes7 = $data[$i]['mesk7'];
                    $dataSave->proyspakmes8 = $data[$i]['mesk8'];
                    $dataSave->proyspakmes9 = $data[$i]['mesk9'];
                    $dataSave->proyspakmes10 = $data[$i]['mesk10'];
                    $dataSave->proyspakmes11 = $data[$i]['mesk11'];
                    $dataSave->proyspakmes12 = $data[$i]['mesk12'];

                    $result = $dataSave->save();

                }
            }
        }

        return array( 'respuesta' => $result);        

    }

    public function SegxObjetivoActividad(Request $request)
    {
        try{           
            
            $actividad = $request['actividad'];

            $sql = "select ps.proycod as proyecto,ps.proyobjcod as codigoobjetivo,ps.proyactsec as codigoactividad,ps.proyspaano as anno,
                    ps.proyspames1 as mes1,ps.proyspames2 as mes2,ps.proyspames3 as mes3,ps.proyspames4 as mes4,ps.proyspames5 as mes5,
                    ps.proyspames6 as mes6,ps.proyspames7 as mes7,ps.proyspames8 as mes8,ps.proyspames9 as mes9,ps.proyspames10 as mes10,
                    ps.proyspames11 as mes11,ps.proyspames12 as mes12,ps.proyspakmes1 as mesk1,ps.proyspakmes2 as mesk2,ps.proyspakmes3 as mesk3,
                    ps.proyspakmes4 as mesk4,ps.proyspakmes5 as mesk5,ps.proyspakmes6 as mesk6,ps.proyspakmes7 as mesk7,ps.proyspakmes8 as mesk8,
                    ps.proyspakmes9 as mesk9,ps.proyspakmes10 as mesk10,ps.proyspakmes11 as mesk11,ps.proyspakmes12 as mesk12,
                    pa.proyactdes as txtactividad,pa.proyactpes as peso,pa.proyactkpi as kpi,pa.proyactdeskpi as descripcionkpi,                    
                    pp.proyppames1 as mesesp1,pp.proyppames2 as mesesp2,pp.proyppames3 as mesesp3,pp.proyppames4 as mesesp4,pp.proyppames5 as mesesp5,
                    pp.proyppames6 as mesesp6,pp.proyppames7 as mesesp7,pp.proyppames8 as mesesp8,pp.proyppames9 as mesesp9,pp.proyppames10 as mesesp10,
                    pp.proyppames11 as mesesp11,pp.proyppames12 as mesesp12,pp.proyppakmes1 as mesespk1,pp.proyppakmes2 as mesespk2,
                    pp.proyppakmes3 as mesespk3,pp.proyppakmes4 as mesespk4,pp.proyppakmes5 as mesespk5,pp.proyppakmes6 as mesespk6,
                    pp.proyppakmes7 as mesespk7,pp.proyppakmes8 as mesespk8,
                    pp.proyppakmes9 as mesespk9,pp.proyppakmes10 as mesespk10,pp.proyppakmes11 as mesespk11,pp.proyppakmes12 as mesespk12
                    FROM proyectos_seguimiento_planaccion ps inner join proyectos_actividades pa ON ps.proyactsec = pa.proyactsec
                    INNER JOIN proyectos_programacion_planaccion pp ON ps.proyactsec = pp.proyactsec AND ps.proyspaano = pp.proyppaano 
                    where ps.proyactsec = ".$actividad." order by ps.proyactcod,ps.proyspaano;"; 

            $ProyectosProgrPlan = DB::select($sql); 
            
            return $ProyectosProgrPlan;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    public function AddBasico(Request $request)
    {
        $data = $request->data;
        
        $proyecto = $data['proyecto'];
        $objetivo = $data['objetivo'];
        $actividad = $data['actividad'];
        $annoini = $data['annoini'];
        $annofin = $data['annofin'];

        $result = true;

        for ($i=$annoini; $i <= $annofin; $i++) {
            
            $Encontrado = $this->buscarDataExisteSec($actividad, $i);

            if(!$Encontrado){

                $dataSave = new ProyectosSeguimPlan();

                $dataSave->proycod    = $proyecto;
                $dataSave->proyobjcod = $objetivo;
                $dataSave->proyactsec = $actividad;
                $dataSave->proyspaano = $i;

                $result = $dataSave->save();
            }    

        }

        return array( 'respuesta' => $result);        

    }
    
}

?>