<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosProgInvPeiTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosProgInvPei;
use DB;

class ProyectosProgInvPeiController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosProgInvPei = ProyectosProgInvPei::select('proycod','proinvpeicod')                        
                        ->orderby('proinvpeicod')                     
                        ->get();
                     
            if ($ProyectosProgInvPei->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosProgInvPei, new ProyectosProgInvPeiTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
        
        $result = '';

        for ($i=0; $i < count($data); $i++) {             
        
            $codigo = $data[$i]['codigo'];
            $proyecto = $data[$i]['proyecto'];
            
            if($i==0){

                $result = ProyectosProgInvPei::where('proycod', $proyecto)->delete();
            }

            $dataAdd = new ProyectosProgInvPei();
            
            $dataAdd->proinvpeicod  = $codigo;
            $dataAdd->proycod    = $proyecto;

            $result = $dataAdd->save();            
        }

        return array( 'respuesta' => $result);     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];
                     
            $ProyectosProgInvPei = ProyectosProgInvPei::select('proinvpeicod','proycod')                        
                        ->where('proycod', $proyecto)                        
                        ->orderby('proinvpeicod')                     
                        ->get();
                     
            return $this->response->collection($ProyectosProgInvPei, new ProyectosProgInvPeiTransformer);                
            
        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    public function seleccionadosProyectos(Request $request)
    {
        try{
            
            $proyecto = $request['proyecto'];
                     
            $sql = "SELECT l.proinvpeicod codigo,l.proinvpeides descripcion, IF(p.proycod IS NULL,0,1) Activo 
                    FROM programas_investigacion_pei l LEFT JOIN 
                    proyectos_proginv_pei p ON l.proinvpeicod = p.proinvpeicod AND p.proycod=".$proyecto;
            
            $result = DB::select($sql);
            
            return $result;

        }catch(Exception $e){

            return $e->getMessage();

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
}

?>