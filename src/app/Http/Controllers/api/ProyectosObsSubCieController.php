<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Transformers\ProyectosObsSubCieTransformer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProyectosObsSubCie;
use DB;

class ProyectosObsSubCieController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
       try{

            $ProyectosObsSubCie = ProyectosObsSubCie::select('proysdccod','proycod', 'proysdcdes')                        
                        ->orderby('proysdccod')                     
                        ->get();
                     
            if ($ProyectosObsSubCie->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosObsSubCie, new ProyectosObsSubCieTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }    

    public function consecutivo()
    {      

        $maxVal  = ProyectosObsSubCie::max('proysdccod');

        if (is_numeric($maxVal)) {

            $codigo = $maxVal + 1;

        } else {
            $maxVal = 0;
            $codigo = 1;
        }
            
        return $codigo;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try{
           
            $proyecto = $request['proyecto'];

            $ProyectosObsSubCie = ProyectosObsSubCie::select('proysdccod','proycod', 'proysdcdes')                        
                        ->where('proycod', $proyecto)
                        ->orderby('proysdccod')                     
                        ->get();
                     
            if ($ProyectosObsSubCie->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $this->response->collection($ProyectosObsSubCie, new ProyectosObsSubCieTransformer);                
            }

        }catch(Exception $e){

            return $e->getMessage();

        }
    }    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $Id = $request->id;
        $proyecto = $request->proyecto;

        try{

            $result = ProyectosObsSubCie::where('proysdccod', $Id)->where('proycod', $proyecto)->delete();
            return $result;

        } catch(\Illuminate\Database\QueryException $ex) {

            if($ex->getCode() == 23000){

                return $this->response->errorInternal('No se puede eliminar la información, los datos se relacionan con otros módulos');

            } else {

                return $this->response->errorInternal($ex->getMessage());
            }

        }
    }    

    private function buscarCodigoProyecto($Proyecto, $Codigo){

        try{
           
            $ok = false;

            $Encontrado = ProyectosObsSubCie::select(DB::raw('count(proysdccod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('proysdccod',$Codigo)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $ok = true;

                }
                
            }        

            return $ok;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    private function buscarDuplicado($Proyecto,$codigo, $descripcion){

        try{
           
            $mensaje = '';

            $Encontrado = ProyectosObsSubCie::select(DB::raw('count(proysdccod) as total'))
                      ->where('proycod', $Proyecto)
                      ->where('proysdcdes',$descripcion)
                      ->where('proysdccod','!=',$codigo)
                      ->get();

            $total = 0;          
            if (!$Encontrado->isEmpty()) {
                
                $total = $Encontrado[0]->total;

                if($total != '0'){

                    $mensaje = 'La Observación ya existe';

                }
                
            }        

            return $mensaje;

        }catch(Exception $e){

            return $e->getMessage();

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAddUpdate(Request $request)
    {
        $data = $request->data;
                
        $codigo = $data['codigo'];
        $proyecto = $data['proyecto'];
        $observacion = $data['observacion'];
        
        $mensaje = $this->buscarDuplicado($proyecto,$codigo,$observacion);

        if($mensaje != ''){

            return $this->response->errorNotFound($mensaje);

        }

        if($codigo == '0'){

            $consecutivo = $this->consecutivo();
            $dataSave = new ProyectosObsSubCie();

            $dataSave->proycod = $data['proyecto'];
            $dataSave->proysdccod = $consecutivo;
            $dataSave->proysdcdes = $data['observacion'];                    

            $result = $dataSave->save();

        } else {

            $query = "UPDATE proyectos_obssubdcientifica SET proysdcdes = '".$data['observacion']."'   
                            WHERE proycod=".$data['proyecto']." AND proysdccod = ".$data['codigo'];
            $result = DB::update($query);

        }

        return array( 'respuesta' => $result);       

    }
}

?>