<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pqr;
use App\Municipios;
use App\Areas;
use App\Personal;
use App\TipoComunicacion;
use App\Asignacion;
use App\Comentarios;
use App\Respuesta;
use App\Adjunto;
use DB;

class PqrsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{

            $pqr = Pqr::select('consecutivo', 'radicado_vu', 'pqrs_reclamos.id', 'fecha_creacion', 'id_estado', 'pqrs_reclamos.dias_respuesta', 'responsable', 'descripcion')
                    ->join('pqr_tipo_comunicacion','pqrs_reclamos.tipo_comunicacion','=','pqr_tipo_comunicacion.id')
                    ->get();
                     
            if ($pqr->isEmpty()) {
                
                return 'No hay datos para Mostrar';
                // return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $pqr;

            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function getMunicipios()
    {
        try{

            $municipio = Municipios::select('idmunicipio', 'municipio')                                     
                     ->get();
                     
            if ($municipio->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $municipio;

            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function getUsers()
    {
        try{

            $users = DB::reconnect('mysql')->select("SELECT 
                LTRIM(perside) as documento,p.arecod as areacod,arenom as area,
                CONCAT(p.persnom,' ',p.persape) as nombre
            FROM personal p
            INNER JOIN areas a ON p.arecod = a.arecod");
                     
            if (!$users) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $users;

            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function getDependencias()
    {
        try{

            $dependencia = Areas::select('arecod', 'arenom')                                     
                     ->get();
                     
            if ($dependencia->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $dependencia;

            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function getTipoComunicacion()
    {
        try{

            $tipo = TipoComunicacion::select('id', 'descripcion', 'dias_respuesta')                                     
                     ->get();
                     
            if ($tipo->isEmpty()) {
                
                return 'No hay datos para Mostrar';
            }
            else {

                return $tipo;

            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function getResponsables(Request $request)
    {

        $id = $request->id;

        $data = DB::reconnect('mysql')->select("SELECT 
                    documento,id_area as arecod,arenom as area,
                    CONCAT(p.persnom,' ',p.persape) as nombre,
                    a.responsable_salida, soporte_fisico
                FROM pqr_asignacion a
                INNER JOIN areas ar ON a.id_area = ar.arecod
                INNER JOIN personal p ON a.documento = p.perside
                WHERE id_pqr = ? ",[$id]);

        return $data;

    }
    
    public function getDataResponsable(Request $request)
    {

        $documento = $request->documento;

        $data =  DB::reconnect('mysql')->select("SELECT 
            perside as documento,p.arecod,arenom as area,
            CONCAT(p.persnom,' ',p.persape) as nombre, '' as soporte_fisico 
        FROM personal p
        INNER JOIN areas a ON p.arecod = a.arecod
        WHERE perside = ? ",[$documento]);

        return $data;

    }

    public function getInfoResponsable(Request $request)
    {

        $area = $request->idarea;

        $data =  DB::reconnect('mysql')->select("SELECT 
            documento,areacod,arenom as area,
            CONCAT(p.persnom,' ',p.persape) as nombre, '' as soporte_fisico
        FROM pqr_encargados e
        INNER JOIN personal p ON e.documento = p.perside
        INNER JOIN areas a ON e.areacod = a.arecod
        WHERE areacod = ? ",[$area]);

        return $data;

    }

    public function savePqr(Request $request)
    {

        $info = $request->data;
        $responsables = $info['responsables'];

        DB::connection('mysql')->beginTransaction();
        try {

            $maxVal  = Pqr::max('consecutivo');
            if (trim($maxVal) === '') { 
                $consecutivo = '00001';
                $final = 'R'.date('Y').date('m').date('d').$consecutivo;
            } else {
                $year = substr($maxVal,1,4);
                $today_year = date('Y');
                if ($year == $today_year) {
                    $consecutivo = (intval(substr($maxVal,9))+1);
                    $zeros = str_pad($consecutivo, 5, "0", STR_PAD_LEFT);
                    $final = 'R'.date('Y').date('m').date('d').$zeros;
                } else {
                    $consecutivo = '00001';
                    $final = 'R'.date('Y').date('m').date('d').$consecutivo;
                }
            }

            $pqr = new Pqr();
         
            $pqr->consecutivo = $final;
            $pqr->remitente = $info['nombre'];        
            $pqr->cedula = $info['cedula'];        
            $pqr->direccion = $info['direccion'];
            $pqr->telefono = $info['telefono'];
            $pqr->email = $info['email'];
            $pqr->fecha_radicado = $info['fecha_radicado'];
            $pqr->codmunicipio = $info['municipio'];
            $pqr->tipo_comunicacion = $info['tipo_comunicacion'];
            $pqr->medio_solicitante = $info['medio_solicita'];
            $pqr->prioridad = $info['prioridad'];
            $pqr->confidencialidad = $info['confidencial'];
            $pqr->asunto = $info['asunto'];
            $pqr->detalle = $info['detalle'];
            $pqr->fecha_asignacion = $info['fecha_asignacion'];
            $pqr->dias_respuesta = $info['dias'];
            $pqr->observacion = $info['observaciones'];
            $pqr->radicado_vu = $info['radicado'];
            $pqr->fecha_radicado_vu = $info['fecha_radicado_vu'];
            $pqr->num_folios = $info['num_folios'];
                       
            $result = $pqr->save(); 
            $pqr_id = $pqr->id;

            if (count($responsables) > 0) {

                for ($i=0; $i < count($responsables); $i++) {

                    $resp_salida = 0;
                    $soporte = trim($responsables[$i]['soporte_fisico']) === '' ? 0 : 1;

                    if (trim($info['responsable_salida']) != '') {
                        $user_salida = intval($info['responsable_salida']);
                        if (intval($responsables[$i]['documento']) === $user_salida) {
                            $resp_salida = 1;
                        }
                    }

                    $asignacion = new Asignacion();
                
                    $asignacion->documento = $responsables[$i]['documento'];
                    $asignacion->id_pqr = $pqr_id;
                    $asignacion->id_area = $responsables[$i]['areacod'];
                    $asignacion->responsable_salida = $resp_salida;
                    $asignacion->soporte_fisico = $soporte;
        
                    $result = $asignacion->save(); 
                    
                }

            }

            $data_head = Pqr::select('fecha_creacion','consecutivo')->where('id', $pqr_id)->get();

            $datos['headers'] = $data_head;

            $pdf = \PDF::loadView('pqr.sticker', $datos);
            $pdf->setOption('no-outline', true);
            $pdf->setOption('disable-smart-shrinking', true);
            $pdf->setOption('dpi', 600);
            $pdf->setOption('image-quality', 100);
            $pdf->setOption('copies', 1);
            $pdf->setOption('page-height', 80);
            $pdf->setOption('page-width', 120);
            $pdf->setOption('margin-top', 0);
            $pdf->setOption('margin-bottom', 0);
            // $pdf->setOption('orientation','Landscape');
            // $pdf->setOption('orientation','Portrait');
            $pdf->setOption('header-spacing', 0);
            $pdf->setOption('footer-spacing', 0);

            $path = storage_path('tmp/STICKER_'.$final.'.pdf');
            $printer = 'Holi';

            if ($pdf->save($path)) {

                $printcmd = '"C:\Program Files (x86)\Foxit Software\Foxit Reader\Foxit Reader.exe" /t "'.$path.'" "'.$printer.'"';
                exec($printcmd);  	
                // unlink($path);
                // $message = "El sticker se almacenó correctamente";
    
            }

            // all good
            DB::connection('mysql')->commit();

            $result = true;

        } catch (\Exception $e) {

            // something went wrong
            DB::connection('mysql')->rollback();
            // $result = $e->getMessage();
            $result = false;
            
        }

        return array('respuesta' => $result);

    }

    public function listPqr(Request $request)
    {
        try{

            $id = $request->id;

            $pqr = Pqr::select('pqrs_reclamos.id', 'fecha_creacion', 'remitente', 'email', 'direccion', 'telefono', 
                                'fecha_radicado', 'codmunicipio', 'tipo_comunicacion', 'medio_solicitante',
                                'prioridad', 'confidencialidad', 'asunto', 'detalle',
                                'nombre_oficio', 'ruta_oficio', 'fecha_asignacion',
                                'pqrs_reclamos.dias_respuesta', 'observacion', 'radicado_vu', 'fecha_radicado_vu', 
                                'num_folios', 'id_estado', 'descripcion', 'municipio', 'consecutivo', 'cedula')                                     
                    ->join('pqr_tipo_comunicacion','pqrs_reclamos.tipo_comunicacion','=','pqr_tipo_comunicacion.id')
                    ->join('municipios','pqrs_reclamos.codmunicipio','=','municipios.idmunicipio')
                    ->where('pqrs_reclamos.id', $id)
                    ->get();
                     
            if ($pqr->isEmpty()) {
                
                return $this->response->errorNotFound('No hay datos para Mostrar');
            }
            else {

                return $pqr;

            }

        }catch(Exception $e){

            return $e->getMessage();

        }     
    }

    public function deleteAsignacion(Request $request)
    {

        $id = $request->id;

        try {
            
            Asignacion::where('id',$id)->delete();

            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se eliminó el registro con éxito'],200);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);

        }
        
        return $respuesta;

    }

    public function saveAsignacion(Request $request)
    {

        $tipo = $request->tipo;
        $datos = $request->data;
        $id_pqr = $request->id;

        try {

            if (count($datos) > 0) {

                for ($i=0; $i < count($datos); $i++) {

                    $resp_salida = 0;
                    $soporte = 0;

                    if ( $tipo === 'individual' ) {

                        $data =  DB::reconnect('mysql')->select("SELECT 
                            perside as documento,p.arecod,arenom as area,
                            CONCAT(p.persnom,' ',p.persape) as nombre, '' as soporte_fisico 
                        FROM personal p
                        INNER JOIN areas a ON p.arecod = a.arecod
                        WHERE perside = ? ",[$datos[$i]]);

                        $area = $data[0]->arecod;

                        $asignacion = new Asignacion();
                    
                        $asignacion->documento = $datos[$i];
                        $asignacion->id_pqr = $id_pqr;
                        $asignacion->id_area = $area;
                        $asignacion->responsable_salida = $resp_salida;
                        $asignacion->soporte_fisico = $soporte;

                        $asignacion->save(); 

                    }

                    if ( $tipo === 'areas' ) {

                        $data = DB::reconnect('mysql')->select("SELECT 
                            documento,areacod
                        FROM pqr_encargados 
                        WHERE areacod = ? ",[$datos[$i]]);

                        for ($j=0; $j < count($data) ; $j++) {

                            $asignacion = new Asignacion();
                        
                            $asignacion->documento = $data[$j]->documento;
                            $asignacion->id_pqr = $id_pqr;
                            $asignacion->id_area = $data[$j]->areacod;
                            $asignacion->responsable_salida = $resp_salida;
                            $asignacion->soporte_fisico = $soporte;

                            $asignacion->save(); 
                            
                        }

                    }
                    
                }

            }

            $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se guardó el registro con éxito'],200);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);

        }
        
        return $respuesta;

    }

    public function getAgentes(Request $request)
    {

        $id = $request->id;

        $data =  DB::reconnect('mysql')->select("SELECT 
            a.id,a.id_area,ar.arenom,a.documento,
            CONCAT(p.persnom,' ',p.persape) as nombre
        FROM pqr_asignacion a
        INNER JOIN personal p ON a.documento = p.perside
        INNER JOIN areas ar ON a.id_area = ar.arecod
        WHERE id_pqr = ? ",[$id]);

        return $data;

    }

    public function saveRespuesta(Request $request)
    {

        try {

            $obj = $request->obj;
            $obj = json_decode($obj,true);

            $respuesta = new Respuesta();
                        
            $respuesta->id_reclamo = $obj['id_reclamo'];
            $respuesta->comentario = $obj['comentario'];
            $respuesta->id_usuario = $obj['id_usuario'];

            $respuesta->save();

            if (isset($respuesta->id)) {
                if ($request->hasFile('archivos')) {
                    $files = $request->file('archivos');
                    foreach ($files as $file) {
                        $random = str_random(10);
                        $extension = $file->getClientOriginalExtension();
                        $fileOriginal = $file->getClientOriginalName();
                        $filename = $random.'.'.$extension;
                        $path = $file->move(public_path('pqr_adjuntos/'),$filename);
                        $fileURL = url('/pqr_adjuntos/'.$filename);

                        $adjunto = new Adjunto();
                        $adjunto->id_pqr = $obj['id_reclamo'];           
                        $adjunto->ruta_adjunto = $fileURL;
                        $adjunto->nombre = $fileOriginal;
                        $adjunto->id_comentario = $respuesta->id;
                        $adjunto->respuesta = 1;
                        $adjunto->save(); 
                    }   
                }
                $respuesta = response()->json(['tipo' => 'success','respuesta' => 'Se guardó el registro con éxito'],200);
            }
            
        } catch (\Throwable $th) {
            $respuesta = response()->json(['tipo' => 'error','respuesta' => 'Ocurrió un error en la base de datos.'],200);
        }

        return $respuesta;

    }

    public function getRespuesta(Request $request)
    {

        $id = $request->id;

        $comentarios =  DB::reconnect('mysql')->select("SELECT 
            c.id,comentario,c.fecha,c.id_reclamo,
            CONCAT(u.usuario,' - ',u.nombre) as nombre
        FROM pqr_respuesta c
        INNER JOIN users u ON c.id_usuario = u.iduser
        WHERE id_reclamo = ? ",[$id]);

        foreach ($comentarios as $comentario) {
            $comentario->adjuntos = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM pqr_adjuntos WHERE id_comentario = ' .$comentario->id. ' AND respuesta = 1'));
        }
        
        return $comentarios;

    }

    public function saveComentario(Request $request)
    {

        $info = $request->data;

        try {
            
            $comentario = new Comentarios();
                        
            $comentario->id_reclamo = $info['id_reclamo'];
            $comentario->comentario = $info['comentario'];
            $comentario->id_usuario = $info['id_usuario'];

            $comentario->save();
            $id = $comentario->id;

            $respuesta = response()->json(['id' => $id, 'tipo' => 'success','respuesta' => 'Se guardó el registro con éxito'],200);

        } catch (Exception $e) {
            
            $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);

        }
        
        return $respuesta;

    }

    public function getResponsablepqr(Request $request)
    {

        $id = $request->id;
        $id_pqr = $request->id_pqr;

        $data_doc =  DB::reconnect('mysql')->select("SELECT 
            perside
        FROM personal
        WHERE perscod = ? ",[$id]);

        $documento = $data_doc[0]->perside;

        $data =  DB::reconnect('mysql')->select("SELECT 
            documento
        FROM pqr_asignacion
        WHERE id_pqr = ? AND responsable_salida = 1 ",[$id_pqr]);

        $responsable_doc = $data[0]->documento;

        if ($documento === $responsable_doc) {
            $result = false;
        } else {
            $result = true;
        }
        
        return array('respuesta' => $result);

    }

    public function getComentario(Request $request)
    {

        $id = $request->id;

        $comentarios =  DB::reconnect('mysql')->select("SELECT 
            c.id,comentario,c.fecha,c.id_reclamo,
            CONCAT(u.usuario,' - ',u.nombre) as nombre
        FROM pqr_comentarios c
        INNER JOIN users u ON c.id_usuario = u.iduser
        WHERE id_reclamo = ? ",[$id]);

        foreach ($comentarios as $comentario) {
            $comentario->adjuntos = DB::reconnect('mysql')->select(DB::raw('SELECT * FROM pqr_adjuntos WHERE id_comentario = ' .$comentario->id. ' AND respuesta = 0'));
        }
        
        return $comentarios;

    }

    public function getAsignados(Request $request)
    {

        $id = $request->id;

        $asignacion = Asignacion::select('documento')->where('id_pqr',$id)->get();

        return $asignacion;

    }

    public function updatePqr(Request $request)
    {

        $info = $request->data;

        $responsables_add = $info['responsables_add'];
        $responsables_drop = $info['responsables_drop'];

        DB::connection('mysql')->beginTransaction();
        try {

            $pqr = Pqr::find($info['id']);

            $pqr->cedula = $info['cedula'];  
            $pqr->remitente = $info['nombre'];           
            $pqr->direccion = $info['direccion'];
            $pqr->telefono = $info['telefono'];
            $pqr->email = $info['email'];
            $pqr->fecha_radicado = $info['fecha_radicado'];
            $pqr->codmunicipio = $info['municipio'];
            $pqr->tipo_comunicacion = $info['tipo_comunicacion'];
            $pqr->medio_solicitante = $info['medio_solicita'];
            $pqr->prioridad = $info['prioridad'];
            $pqr->confidencialidad = $info['confidencial'];
            $pqr->asunto = $info['asunto'];
            $pqr->detalle = $info['detalle'];
            $pqr->fecha_asignacion = $info['fecha_asignacion'];
            $pqr->dias_respuesta = $info['dias'];
            $pqr->observacion = $info['observaciones'];
            $pqr->radicado_vu = $info['radicado'];
            $pqr->fecha_radicado_vu = $info['fecha_radicado_vu'];
            $pqr->num_folios = $info['num_folios'];
                       
            $pqr->save();

            if (count($responsables_add) > 0) {

                for ($i=0; $i < count($responsables_add); $i++) { 
                   
                    $asignacion = new Asignacion();
                        
                    $asignacion->documento = $responsables_add[$i]['documento'];
                    $asignacion->id_pqr = $info['id'];
                    $asignacion->id_area = $responsables_add[$i]['areacod'];
                    $asignacion->soporte_fisico = $responsables_add[$i]['soporte_fisico'];

                    $asignacion->save(); 

                }

            }

            if (count($responsables_drop) > 0) {

                for ($i=0; $i < count($responsables_drop); $i++) { 
                    Asignacion::where('documento',$responsables_drop[$i])->where('id_pqr',$info['id'])->delete();
                }

            }

            if (trim($info['responsable_salida']) != '') {

                Asignacion::where('id_pqr',$info['id'])->update(['responsable_salida' => 0]);
                Asignacion::where('id_pqr',$info['id'])->where('documento',$info['responsable_salida'])->update(['responsable_salida' => 1]);

            }

            // all good
            DB::connection('mysql')->commit();

            $result = true;

        } catch (\Exception $e) {

            // something went wrong
            DB::connection('mysql')->rollback();
            // $respuesta = response()->json(['tipo' => 'error','respuesta' => $e->getMessage()],200);
            // $result = $e->getMessage();
            $result = false;

        }

        return array('respuesta' => $result);

    }

}
