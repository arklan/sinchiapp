<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization, enctype');

Route::get('/', function () {
    return view('welcome');
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1',['namespace' => 'App\Http\Controllers\api'], function ($api) {
	
    $api->post('login', 'LoginController@search');

    // API Mail
    $api->get('email', 'EmailController@recovery');
    $api->post('email', 'EmailController@send');
    $api->post('updatePassword', 'LoginController@updatePassword');    

    $api->post('Usuarios','LoginController@index');
    $api->post('DelUser','LoginController@destroy');
    $api->post('personalFaltanteUser','LoginController@personalFaltanteUser');

    $api->post('guardarUser','LoginController@guardarUsuario');
    $api->post('getPermisos','OpcionesController@Permisos');
    $api->post('getPermisosFal','OpcionesController@PermisosFaltantes');
    $api->post('PermisosActuales','OpcionesController@PermisosActuales');
    $api->post('getOpcion','OpcionesController@buscarOpcion');

    $api->get('getPqrs', 'PqrsController@index');
    $api->get('getUsers', 'PqrsController@getUsers');
    $api->post('getDataResponsable', 'PqrsController@getDataResponsable');
    $api->post('getInfoResponsable', 'PqrsController@getInfoResponsable');
    $api->post('listPqr', 'PqrsController@listPqr');
    $api->post('updatePqr', 'PqrsController@updatePqr');
    $api->get('getMunicipios', 'PqrsController@getMunicipios');
    $api->get('getDependencias', 'PqrsController@getDependencias');
    $api->get('getTipoComunicacion', 'PqrsController@getTipoComunicacion');
    $api->post('getResponsables', 'PqrsController@getResponsables');
    $api->post('savePqr', 'PqrsController@savePqr');
    $api->post('getAgentes', 'PqrsController@getAgentes');
    $api->post('deleteAsignacion', 'PqrsController@deleteAsignacion');
    $api->post('saveAsignacion', 'PqrsController@saveAsignacion');
    $api->post('saveComentario', 'PqrsController@saveComentario');
    $api->post('getComentario', 'PqrsController@getComentario');
    $api->post('getAsignados', 'PqrsController@getAsignados');
    $api->post('saveRespuesta', 'PqrsController@saveRespuesta');
    $api->post('getRespuesta', 'PqrsController@getRespuesta');
    $api->post('getResponsablepqr', 'PqrsController@getResponsablepqr');

    $api->post('saveAdjunto', 'AdjuntoController@saveAdjunto');
    $api->post('savefromH', 'AdjuntoController@saveAdjuntofromH');
    $api->post('updateAdjunto', 'AdjuntoController@updateAdjunto');
    $api->post('listAdjuntos', 'AdjuntoController@listAdjuntos');

    $api->post('uploadFilepqr', 'FilespqrController@upload');
    $api->get('downloadFilepqr', 'FilespqrController@download');
    $api->post('deleteFilepqr', 'FilespqrController@delete');
    $api->post('deleteOficio', 'FilespqrController@deleteOficio');
    $api->post('updateOficio', 'FilespqrController@updateOficio');

    $api->post('resetPassword','LoginController@resetearPassword');

    $api->post('actualizaPermisos','PerfilesController@actualizarPermisosPerfil');

    $api->post('Bitacora', 'BitacoraController@index');
    $api->post('AddBitacora', 'BitacoraController@Agregar');

    $api->post('Perfiles', 'PerfilesController@index');
    $api->post('guardarPerfil', 'PerfilesController@perfilAddUpdate');
    $api->post('delPerfil', 'PerfilesController@destroy');

    $api->post('Areas', 'AreasController@index');
    $api->post('guardarArea', 'AreasController@areaAddUpdate');
    $api->post('delArea', 'AreasController@destroy');
    $api->post('AreaXCodigo', 'AreasController@dataXArea');
    $api->post('userXArea', 'AreasController@userXArea');

    $api->post('Cargos', 'CargosController@index');
    $api->post('guardarCargo', 'CargosController@cargoAddUpdate');
    $api->post('delCargo', 'CargosController@destroy');

    $api->post('Categorias', 'CategoriasController@index');
    $api->post('guardarCategoria', 'CategoriasController@categoriaAddUpdate');
    $api->post('delCategoria', 'CategoriasController@destroy');

    $api->post('TiposIde', 'TiposIdentificacionController@index');
    $api->post('guardarTiposIde', 'TiposIdentificacionController@dataAddUpdate');
    $api->post('delTiposIde', 'TiposIdentificacionController@destroy');

    $api->post('TiposEstados', 'TiposEstadoController@index');
    $api->post('guardarTipoEstado', 'TiposEstadoController@dataAddUpdate');
    $api->post('delTipoEstado', 'TiposEstadoController@destroy');

    $api->post('Estados', 'EstadosController@index');
    $api->post('EstadosXTipo', 'EstadosController@EstadosXTipo');
    $api->post('guardarEstado', 'EstadosController@dataAddUpdate');
    $api->post('delEstado', 'EstadosController@destroy');

    $api->post('ProgramasInvPenia', 'ProgInvestigacionPeniaController@index');    
    $api->post('guardarProInvPenia', 'ProgInvestigacionPeniaController@dataAddUpdate');
    $api->post('delProInvPenia', 'ProgInvestigacionPeniaController@destroy');

    $api->post('ProgramasInvPei', 'ProgInvestigacionPeiController@index');    
    $api->post('guardarProInvPei', 'ProgInvestigacionPeiController@dataAddUpdate');
    $api->post('delProInvPei', 'ProgInvestigacionPeiController@destroy');

    $api->post('Poas', 'PoasController@index');
    $api->post('guardarPoa', 'PoasController@dataAddUpdate');
    $api->post('delPoa', 'PoasController@destroy');

    $api->post('Proyectos', 'ProyectosController@index');
    $api->post('ProyectosGrilla', 'ProyectosController@listGrilla');
    $api->post('ProyectoXId', 'ProyectosController@FiltroXId');
    $api->post('guardarProyecto', 'ProyectosController@dataAddUpdate');
    $api->post('delProyecto', 'ProyectosController@destroy');
    $api->post('actualizaTasa', 'ProyectosController@actualizaTasa');
    $api->post('proyectosXVoBo', 'ProyectosController@FiltroXVoBo');
    $api->post('buscarPlanAdqXproyecto', 'ProyectosController@buscarproyectoPlanAdq');
    
    $api->post('ProyectosActividades', 'ProyectosActividadesController@index');
    $api->post('FiltroXProyecto', 'ProyectosActividadesController@show');
    $api->post('FiltroXProyectoObj', 'ProyectosActividadesController@BuscarXProyectoObjetivo');
    $api->post('guardarProyectoActividad', 'ProyectosActividadesController@dataAddUpdate');
    $api->post('delProyectoActividad', 'ProyectosActividadesController@destroy');
    $api->post('ActividadXCodigo', 'ProyectosActividadesController@FiltrarxActividad');
    
    $api->post('ProyectoEquipo', 'ProyectosEquipoController@show');
    $api->post('DelProyectoEquipo', 'ProyectosEquipoController@destroy');
    $api->post('guardarProyectoEquipo', 'ProyectosEquipoController@dataAddUpdate');
    $api->post('horasProyectosPersonal', 'ProyectosEquipoController@TotalHorasIntegrante');
    
    $api->post('Ods', 'OdsController@index');
    $api->post('guardarOds', 'OdsController@OdsAddUpdate');
    $api->post('delOds', 'OdsController@destroy');
    $api->post('OdsXFiltro', 'OdsController@FiltroXObjetivo');

    $api->post('Metas', 'MetasController@index');
    $api->post('guardarMetas', 'MetasController@MetasAddUpdate');
    $api->post('delMetas', 'MetasController@destroy');
    $api->post('MetasXLinvIngPenia', 'MetasController@FiltroXLinea');

    $api->post('AreasTematicas', 'AreasTematicasController@index');

    $api->post('Personal', 'PersonalController@index');
    $api->post('PersonalXCargo', 'PersonalController@FiltroXCargo');
    $api->post('PersonalXEstado', 'PersonalController@FiltroXEstado');
    $api->post('guardarPersonal', 'PersonalController@dataAddUpdate');    
    $api->post('delPersonal', 'PersonalController@destroy');   

    $api->post('Departamentos', 'DepartamentosController@index');

    $api->post('Ciudades', 'CiudadesController@index');
    $api->post('CiudadesxDepartamentos', 'CiudadesController@FiltroXDepartamento');

    $api->get ('getFTPDF', 'ProyectosController@getFTPDF');
    $api->get ('getActaPI', 'ProyectosController@getActaPI');

    $api->post('AlertasXUsuario', 'UsersAlertasController@UserAlertaXId');
    $api->post('AlertasXPersonal', 'UsersAlertasController@UserAlertaXPersonal');
    $api->post('guardarUsuariosAlertas', 'UsersAlertasController@dataAddUpdate');

    $api->post('Notificaciones', 'NotificacionesController@ByUser');
    $api->post('NotificacionesAdd', 'NotificacionesController@DataAdd');
    $api->post('NotificacionesOcultar', 'NotificacionesController@OcultarNotificaciones');

    $api->post('Monedas', 'MonedasController@index');
    $api->post('guardarMoneda', 'MonedasController@DataAddUpdate');
    $api->post('delMoneda', 'MonedasController@destroy');

    $api->post('ProyectosObjetivos', 'ProyectosObjetivosController@index');
    $api->post('ObjetivosXProyecto', 'ProyectosObjetivosController@show');    
    $api->post('delProyectoObjetivo', 'ProyectosObjetivosController@destroy');
    $api->post('addupdProyectoObjetivo', 'ProyectosObjetivosController@dataAddUpdate');    

    $api->post('Productos', 'ProductosController@index');
    $api->post('guardarProducto', 'ProductosController@dataAddUpdate');
    $api->post('delProducto', 'ProductosController@destroy');

    /*$api->post('ProyectosObjetivos', 'ProyectosObjetivosController@index');
    $api->post('ObjetivosXProyecto', 'ProyectosObjetivosController@show');    
    $api->post('delProyectoObjetivo', 'ProyectosObjetivosController@destroy');*/

    $api->post('ProyectosProductos', 'ProyectosProductosController@index');
    $api->post('ProductosXProyecto', 'ProyectosProductosController@show'); 
    $api->post('ProductosXObjetivo', 'ProyectosProductosController@ProductosxObjetivos');   
    $api->post('delProyectoProducto', 'ProyectosProductosController@destroy');
    $api->post('addupdProyectoProducto', 'ProyectosProductosController@dataAddUpdate');
    
    $api->post('ProyectosCiudades', 'ProyectosCiudadesController@index');
    $api->post('CiudadesXProyecto', 'ProyectosCiudadesController@show');    
    $api->post('delProyectoCiudad', 'ProyectosCiudadesController@destroy');
    $api->post('addupdProyectoCiudad', 'ProyectosCiudadesController@dataAddUpdate');
    $api->post('CiudadesSelXDepto', 'ProyectosCiudadesController@CiudadesSeleccionadasXDpto');

    $api->post('ProyectosOtrosi', 'ProyectosOtrosiController@index');
    $api->post('OtroSiXProyecto', 'ProyectosOtrosiController@show');    
    $api->post('delProyectoOtrosi', 'ProyectosOtrosiController@destroy');
    $api->post('addupdProyectoOtrosi', 'ProyectosOtrosiController@dataAddUpdate');   
    $api->post('MaxFecTerminacion', 'ProyectosOtrosiController@MaxFecTerminacion');
    $api->post('ProyectoOtrosiMaxFechas', 'ProyectosOtrosiController@FechasMaxOtroSi');

    $api->post('ProyectosEquipoComp', 'ProyectosEquipoCompController@index');
    $api->post('ProyectosEquipoCompXProyecto', 'ProyectosEquipoCompController@show');    
    $api->post('delProyectoEquipoComp', 'ProyectosEquipoCompController@destroy');
    $api->post('addupdProyectoEquipoComp', 'ProyectosEquipoCompController@dataAddUpdate');

    $api->post('ProyectosSegPlan', 'ProyectosSeguimPlanController@index');
    $api->post('delProyectoSeguimPlan', 'ProyectosSeguimPlanController@destroy');
    $api->post('addupdProyectoSeguimPlan', 'ProyectosSeguimPlanController@dataAddUpdate');
    $api->post('SegXObjetivoActividad', 'ProyectosSeguimPlanController@SegxObjetivoActividad');
    $api->post('AddSegActividadBase', 'ProyectosSeguimPlanController@AddBasico');

    $api->post('Roles', 'RolesController@index');
    $api->post('guardarRol', 'RolesController@dataAddUpdate');
    $api->post('delRol', 'RolesController@destroy');

    $api->post('ProyectosProgPlan', 'ProyectosProgrPlanController@index');     
    $api->post('ProgXObjetivo', 'ProyectosProgrPlanController@SeguimientoxObjetivos');   
    $api->post('delProyectoProgPlan', 'ProyectosProgrPlanController@destroy');
    $api->post('addupdProyectoProgPlan', 'ProyectosProgrPlanController@dataAddUpdate');
    $api->post('ProgXObjetivoActividad', 'ProyectosProgrPlanController@ProgxObjetivoActividad');
    $api->post('AddActividadBase', 'ProyectosProgrPlanController@AddBasico');

    $api->post('ProyectosSegDet', 'ProyectosSegDetontroller@index');     
    $api->post('SegDetXItem', 'ProyectosSegDetController@show');
    $api->post('addupdProyectoSegDet', 'ProyectosSegDetController@dataAddUpdate'); 
    $api->post('deleteFileSegDet', 'ProyectosSegDetController@eliminarArchivo');  
    $api->post('obsSubDirCientifica', 'ProyectosSegDetController@actualizarObsSubDirCientifica');  

    $api->post('EstadosActualProyecto', 'ProyectosController@EstadoActual');    

    $api->post('LineasInvPicia', 'LinInvestigacionPiciaController@index');
    $api->post('addupdLineaInvPicia', 'LinInvestigacionPiciaController@dataAddUpdate');
    $api->post('delLineaInvPicia', 'LinInvestigacionPiciaController@destroy');

    $api->post('SelLineaInvPiciaProy', 'ProyectosLinInvPiciaController@seleccionadosProyectos');

    $api->post('AccionesPicia', 'AccionesPiciaController@index');
    $api->post('addupdAccionPicia', 'AccionesPiciaController@dataAddUpdate');
    $api->post('delAccionPicia', 'AccionesPiciaController@destroy');
    
    $api->post('SelAccionesPiciaProy', 'ProyectosAccionesPiciaController@seleccionadosProyectos');

    $api->post('IndicadoresPicia', 'IndicadoresPiciaController@index');
    $api->post('addupdIndicadorPicia', 'IndicadoresPiciaController@dataAddUpdate');
    $api->post('delIndicadorPicia', 'IndicadoresPiciaController@destroy');
   
    $api->post('SelIndicadoresPiciaProy', 'ProyectosIndicadoresPiciaController@seleccionadosProyectos');

    $api->post('SelProgInvPeiProy', 'ProyectosProgInvPeiController@seleccionadosProyectos');

    $api->post('SelLinInvPeiProy', 'ProyectosLinInvPeiController@seleccionadosProyectos');

    $api->post('FechasPreInfTecXConvenio', 'ConveniosFecPreInfTecController@PorConvenio');
    $api->post('delConvFechaPIT', 'ConveniosFecPreInfTecController@destroy');

    $api->post('FechasPreInfFinXConvenio', 'ConveniosFecPreInfFinController@PorConvenio');
    $api->post('delConvFechaPIF', 'ConveniosFecPreInfFinController@destroy');

    $api->post('Convenios', 'ConveniosController@index');
    $api->post('convenioXCodigo', 'ConveniosController@filtroXConvenio');
    $api->post('guardarConvenio', 'ConveniosController@DataAddUpdate');
    $api->post('delConvenio', 'ConveniosController@destroy');
    $api->post('eliminarArchivoConvenio', 'ConveniosController@eliminarArchivo');    
    
    $api->post('ConveniosMarco', 'ConveniosMarcoController@index');
    $api->post('conveniomarcoXCodigo', 'ConveniosMarcoController@filtroXConvenio');
    $api->post('guardarConvenioMarco', 'ConveniosMarcoController@DataAddUpdate');
    $api->post('delConvenioMarco', 'ConveniosMarcoController@destroy');

    $api->post('ProyectosObsPla', 'ProyectosObsPlaController@index');
    $api->post('proyectoobsplaXproyecto', 'ProyectosObsPlaController@show');
    $api->post('guardarProyectoObsPla', 'ProyectosObsPlaController@dataAddUpdate');
    $api->post('delProyectoObsPla', 'ProyectosObsPlaController@destroy');

    $api->post('ProyectosObsSubCie', 'ProyectosObsSubCieController@index');
    $api->post('proyectoobssubcieXproyecto', 'ProyectosObsSubCieController@show');
    $api->post('guardarProyectoObsSubCie', 'ProyectosObsSubCieController@dataAddUpdate');
    $api->post('delProyectoObsSubCie', 'ProyectosObsSubCieController@destroy');

    $api->post('ProyectosFormulacion', 'ProyectosFormulacionController@index');
    $api->post('proyectoformulacionXproyecto', 'ProyectosFormulacionController@show');
    $api->post('guardarproyectoformulacion', 'ProyectosFormulacionController@dataAddUpdate');
    $api->post('addPreguntasproyectoformulacion', 'ProyectosFormulacionController@agregaPreguntasProyectos');

    $api->post('CausalesContratacion', 'CausalesContratacionController@index');
    $api->post('guardarCausalContracion', 'CausalesContratacionController@CausalesContratacionAddUpdate');
    $api->post('delCausalContracion', 'CausalesContratacionController@destroy');

    $api->post('CriteriosEvaluacion', 'CriteriosEvaluacionController@index');
    $api->post('guardarCriteriosEvaluacion', 'CriteriosEvaluacionController@CriteriosEvaluacionAddUpdate');
    $api->post('delCriteriosEvaluacion', 'CriteriosEvaluacionController@destroy');

    $api->post('FormasPago', 'FormasPagoController@index');
    $api->post('guardarFormasPago', 'FormasPagoController@FormasPagoAddUpdate');
    $api->post('delFormasPago', 'FormasPagoController@destroy');

    $api->post('LineasInvPei', 'LinInvestigacionPeiController@index');
    $api->post('guardarLineaInvPei', 'LinInvestigacionPeiController@dataAddUpdate');
    $api->post('delLineaInvPei', 'LinInvestigacionPeiController@destroy');

    $api->post('LineasInvPenia', 'LinInvestigacionPeniaController@index');
    $api->post('guardarLinInvPenia', 'LinInvestigacionPeniaController@dataAddUpdate');
    $api->post('delLinInvPenia', 'LinInvestigacionPeniaController@destroy');
    $api->post('LineasInvPeniaXProgInv', 'LinInvestigacionPeniaController@FiltroXPrograma');

    $api->post('LineasEstrategicas', 'LineasEstrategicasController@index');
    $api->post('guardarLineaEstrategica', 'LineasEstrategicasController@LineasEstrategicasAddUpdate');
    $api->post('delLineaEstrategica', 'LineasEstrategicasController@destroy');
    $api->post('LinEstrXCapitulos', 'LineasEstrategicasController@FiltroXCapitulo');

    $api->post('Capitulos', 'CapitulosController@index');
    $api->post('guardarCapitulo', 'CapitulosController@CapitulosAddUpdate');
    $api->post('delCapitulos', 'CapitulosController@destroy');

    $api->post('MetodosSeleccion', 'MetodosSeleccionController@index');
    $api->post('guardarMetodoSeleccion', 'MetodosSeleccionController@MetodosSeleccionAddUpdate');
    $api->post('delMetodoSeleccion', 'MetodosSeleccionController@destroy');

    $api->post('Objetivos', 'ObjetivosController@index');
    $api->post('guardarObjetivo', 'ObjetivosController@ObjetivosAddUpdate');
    $api->post('delObjetivo', 'ObjetivosController@destroy');
    $api->post('ObjetivoxLineaEst', 'ObjetivosController@FiltroXLineaEst');

    $api->post('Proveedores', 'ProveedoresController@index');
    $api->post('guardarProveedor', 'ProveedoresController@ProveedoresAddUpdate');
    $api->post('delProveedor', 'ProveedoresController@destroy');

    $api->post('TiposContratacion', 'TiposContratacionController@index');
    $api->post('guardarTipoContratacion', 'TiposContratacionController@dataAddUpdate');
    $api->post('delTipoContratacion', 'TiposContratacionController@destroy');
    
    $api->post('TiposContrato', 'TiposContratosController@index');
    $api->post('guardarTipoContrato', 'TiposContratosController@TiposContratosAddUpdate');
    $api->post('delTipoContrato', 'TiposContratosController@destroy');

    $api->post('TiposNovedad', 'TiposNovedadController@index');
    $api->post('guardarTipoNovedad', 'TiposNovedadController@dataAddUpdate');
    $api->post('delTipoNovedad', 'TiposNovedadController@destroy');   

    $api->post('Unspsc', 'UnspscController@index');
    $api->post('guardarUnspsc', 'UnspscController@dataAddUpdate');
    $api->post('delUnspsc', 'UnspscController@destroy');
    $api->post('UnspscXFiltro', 'UnspscController@dataXFiltro');

    $api->post('IndicadoresPicia', 'IndicadoresPiciaController@index');
    $api->post('addupdIndicadorPicia', 'IndicadoresPiciaController@dataAddUpdate');
    $api->post('delIndicadorPicia', 'IndicadoresPiciaController@destroy');

    $api->post('OrganismosVivos', 'OrganismosVivosController@index');
    $api->post('guardarOrganismoVivo', 'OrganismosVivosController@dataAddUpdate');
    $api->post('delOrganismoVivo', 'OrganismosVivosController@destroy');

    $api->post('ProdDerAutor', 'ProductosDerechosAutorController@index');
    $api->post('guardarProdDerAutor', 'ProductosDerechosAutorController@DataAddUpdate');
    $api->post('delProdDerAutor', 'ProductosDerechosAutorController@destroy');

    $api->post('ProdPropIndustrial', 'ProductosPropiedadIndustrialController@index');
    $api->post('guardarProdPropIndustrial', 'ProductosPropiedadIndustrialController@dataAddUpdate');
    $api->post('delProdPropIndustrial', 'ProductosPropiedadIndustrialController@destroy');

    $api->post('ProyectosOrgVivos', 'ProyectosOrgVivosController@index');
    $api->post('ProyectosOrgVivosXproyecto', 'ProyectosOrgVivosController@show');    
    $api->post('selProyectosOrgVivos', 'ProyectosOrgVivosController@seleccionadosProyectos');
    $api->post('addProyectoOrgVivos', 'ProyectosOrgVivosController@dataAddUpdate');

    $api->post('ProyectosProdDerAut', 'ProyectosProdDerAutController@index');
    $api->post('ProyectosProdDerAutXproyecto', 'ProyectosProdDerAutController@show');    
    $api->post('selProyectosProdDerAut', 'ProyectosProdDerAutController@seleccionadosProyectos');
    $api->post('addProyectoProdDerAut', 'ProyectosProdDerAutController@dataAddUpdate');

    $api->post('ProyectosProdPropInd', 'ProyectosProdPropIndController@index');
    $api->post('ProyectosProdPropIndXproyecto', 'ProyectosProdPropIndController@show');    
    $api->post('selProyectosProdPropInd', 'ProyectosProdPropIndController@seleccionadosProyectos');
    $api->post('addProyectoProdPropInd', 'ProyectosProdPropIndController@dataAddUpdate');

    $api->post('selPilaresPicia', 'ProyectosPilaresPiciaController@seleccionadosProyectos');

    $api->post('personalXId', 'PersonalController@FiltroXId');

    $api->post('PilaresPiciaXProyecto', 'ProyectosPilaresPiciaController@show');

    $api->post('LineaInvPiciaXProyecto', 'ProyectosLinInvPiciaController@show');

    $api->post('AccionesPiciaXProyecto', 'ProyectosAccionesPiciaController@show');

    $api->post('IndicadoresPiciaXProyecto', 'ProyectosIndicadoresPiciaController@show');

    $api->post('ProgramasInvPeiXProyecto', 'ProyectosProgInvPeiController@show'); 

    $api->post('LineasInvPeiXProyecto', 'ProyectosLinInvPeiController@show');

    $api->post('updateVoBoProyecto', 'ProyectosController@actualizaVoBo');

    $api->post('ObjetivosXProyectoAll', 'ProyectosObjetivosController@FiltroProyectoAll'); 

    $api->post('ProductosXProyectoAll', 'ProyectosProductosController@FiltroProyectoAll');

    $api->post('ActividadesXProyectoAll', 'ProyectosActividadesController@FiltrarProyectoAll');

    $api->post('CiudadesXProyectoAll', 'ProyectosCiudadesController@FiltrarProyectoAll');

    $api->post('EquipoXProyectoAll', 'ProyectosEquipoController@FiltroProyectoAll');

    $api->post('ConveniosSocios', 'ConveniosController@ConvenioSociosXConvenio');

    $api->post('DelConveniosSocios', 'ConveniosController@DelConvenioSocio');

    $api->post('Presupuestos', 'PresupuestosController@index');
    $api->post('presupuestoXConvenio', 'PresupuestosController@filtroXConvenio');
    $api->post('guardarPresupuesto', 'PresupuestosController@DataAddUpdate');
    $api->post('delPresupuesto', 'PresupuestosController@destroy');
    $api->post('catgastoXPresupuesto', 'PresupuestosController@PresupuestosCatGasXPresupuesto');
    $api->post('delCategoriaPresupuesto', 'PresupuestosController@delCategoria'); 

    $api->post('CategoriasGasto', 'CategoriasGastoController@index');
    $api->post('guardarCategoriaGasto', 'CategoriasGastoController@categoriagastoAddUpdate');
    $api->post('delCategoriaGasto', 'CategoriasGastoController@destroy');	
	
	$api->post('PilaresPicia', 'PilaresPiciaController@index');
    $api->post('guardarPilarPicia', 'PilaresPiciaController@PilaresPiciaAddUpdate');
    $api->post('delPilaresPicia', 'PilaresPiciaController@destroy');

    $api->post('PlanesAdquisicion', 'PlanesAdquicisionController@index');
    $api->post('planesadquisicionXConvenio', 'PlanesAdquicisionController@filtroXConvenio');
    $api->post('guardarPlanAdquisicion', 'PlanesAdquicisionController@DataAddUpdate');  
    $api->post('NecesidadesXPlanAdq', 'PlanesAdquicisionController@NecesidadesXPlanAdq');
    $api->get('NecesidadesXSolicitud', 'PlanesAdquicisionController@NecesidadesXSolicitud');
    $api->get('solProcNecesidades', 'PlanesAdquicisionController@solProcNecesidades');
    $api->post('UnspscXNecesidad', 'PlanesAdquicisionController@UnspscXNecesidad');
    $api->post('UnspscXNecesidadAddUpd', 'PlanesAdquicisionController@DataUnspscAddUpdate');
    $api->post('DelUnspscXNecesidad', 'PlanesAdquicisionController@DelUnspsc');
    $api->post('ActividadXNecesidad', 'PlanesAdquicisionController@ActividadXNecesidad');
    $api->post('ActividadXNecesidadAddUpd', 'PlanesAdquicisionController@DataActividadAddUpdate');
    $api->post('DelActividadXNecesidad', 'PlanesAdquicisionController@DelActividad');
    $api->post('ActividadXPlanCategoria', 'PlanesAdquicisionController@ActividadXPlaAdqCatGasto');
    $api->post('DelNecesidad', 'PlanesAdquicisionController@DelNecesidad');
    $api->post('UnspscXPlanAdq', 'PlanesAdquicisionController@UnspscXPlanAdq');
    $api->post('ActividadXPlanAdq', 'PlanesAdquicisionController@ActividadXPlanAdq');
    $api->post('planAdquisicionXProyecto', 'PlanesAdquicisionController@filtroXProyecto');
    
    $api->post('DataAddTmp', 'PlanesAdquicisionController@DataAddTmp');

    $api->post('TmpNecesidadesXTmpPlanAdq', 'PlanesAdquicisionController@TmpNecesidadesXTmpPlanAdq');
    $api->post('TmpUnspscXTmpPlanAdq', 'PlanesAdquicisionController@TmpUnspscXTmpPlanAdq');
    $api->post('TmpActividadXTmpPlanAdq', 'PlanesAdquicisionController@TmpActividadXTmpPlanAdq');

    $api->post('PlanAdqVersiones', 'PlanesAdquicisionController@HisPlanAdquisicionBase');
    $api->post('PlanAdqVersionActual', 'PlanesAdquicisionController@HisPlanAdquisicionVersion');    
    $api->post('PlanAdqNecVersiones', 'PlanesAdquicisionController@HisNecesidadesXHisPlanAdq');
    $api->post('HisUnspscXHisPlanAdq', 'PlanesAdquicisionController@HisUnspscXHisPlanAdq');
    $api->post('HisActividadXHisPlanAdq', 'PlanesAdquicisionController@HisActividadXHisPlanAdq'); 
    $api->post('RubrosPresupuestales', 'PlanesAdquicisionController@RubrosPresupuestales');        
    
    $api->post('catgastoXConvenio', 'PlanesAdquicisionController@CatGasXConvenio');
    $api->post('ActualizarVoBoNecesidad', 'PlanesAdquicisionController@ActualizarVoBoNecesidad');    
    
    $api->post('ValidaNecesidades', 'PlanesAdquicisionController@ValidaNecesidades');
    $api->post('ValidaUnspsc', 'PlanesAdquicisionController@ValidaUnspsc');
    $api->post('ValidaActividadNecesidad', 'PlanesAdquicisionController@ValidaActividadNecesidad');
    $api->post('ValidaActividadCategoria', 'PlanesAdquicisionController@ValidaActividadCategoria');    
    $api->post('enviarParaVoBo', 'PlanesAdquicisionController@enviarParaVoBo');

    $api->post('VistaFinNecesidades', 'PlanesAdquicisionController@VistaFinNecesidades');
    $api->post('VistaFinActNec', 'PlanesAdquicisionController@VistaFinActNec');
    $api->post('VistaFinActCat', 'PlanesAdquicisionController@VistaFinActCat');
    $api->post('VistaFinResActividades', 'PlanesAdquicisionController@VistaFinResActividades');

    $api->post('VistaFinTmpNecesidades', 'PlanesAdquicisionController@VistaFinTmpNecesidades');
    $api->post('VistaFinTmpActNec', 'PlanesAdquicisionController@VistaFinTmpActNec');
    $api->post('VistaFinTmpActCat', 'PlanesAdquicisionController@VistaFinTmpActCat');
    $api->post('VistaFinResTmpActividades', 'PlanesAdquicisionController@VistaFinResTmpActividades');    
    
    $api->post('ActualizarTmpVoBo', 'PlanesAdquicisionController@ActualizarTmpVoBo');

    $api->post('BuscarPlanAdquisicion', 'PlanesAdquicisionController@BuscarPlanAdquisicion');

    $api->post('delTemporalRechazo', 'PlanesAdquicisionController@delTemporalRechazo');
    
    $api->post('creaVersion', 'PlanesAdquicisionController@creaVersion');

    $api->get('downloadFile', 'FilesController@download');
    $api->post('uploadFile', 'FilesController@upload');
    $api->post('uploadFileGen', 'FilesController@uploadGen');
    $api->post('deleteFile', 'FilesController@deleteFile');

    $api->get('solicitudes', 'SolicitudesController@index');
    $api->get('solicitudes/{id}', 'SolicitudesController@show');
    $api->post('saveSolicitud', 'SolicitudesController@store');
    $api->post('updateSolicitud/{id}', 'SolicitudesController@edit');
    $api->delete('solicitudes/{id}', 'SolicitudesController@destroy');
    $api->post('solproceso', 'SolProcController@store');
    $api->put('solproceso', 'SolProcController@update');
    $api->get('solproceso/{id}', 'SolProcController@show');
    $api->get('evaluacionProponente/{id}', 'EvaluacionProponenteController@index');
    $api->post('evaluacionProponente', 'EvaluacionProponenteController@store');
    $api->get('criteriosProponente/{id}', 'EvaluacionProponenteController@critProp');
    $api->post('actOfertaEco', 'EvaluacionProponenteController@actOfEco');

    $api->get('InfoViews', 'InformesDataController@infoViews');
    $api->get('InfoCampos', 'InformesDataController@infoCampos');
    $api->post('launchConsulta', 'InformesDataController@launchConsulta');
    
    $api->post('Desplazamientos', 'DesplazamientosController@index');    
    $api->post('addupdDesplazamientos', 'DesplazamientosController@dataAddUpdate');
    $api->post('DesplazamientosDestinos', 'DesplazamientosController@desplazamientoDestinos');
    $api->post('DesplazamientosDestinosXCodigo', 'DesplazamientosController@desplazamientoDestinoXCodigo');
    $api->post('DesplazamientosRubrosXCodigo', 'DesplazamientosController@desplazamientoRubrosXCodigo');
    $api->post('modificarVBDesplazamientos', 'DesplazamientosController@cambiarestadosVB');
    $api->post('DesplazamientosDestinosOrdenFecha', 'DesplazamientosController@desplazamientoDestinosOrdenFecha');
    $api->post('desplazamientoseliminarArchivo', 'DesplazamientosController@eliminarArchivo');
    $api->post('liquidaViaticos', 'DesplazamientosController@liquidarViaticos');
    $api->post('updDatosPresupuestoViaticos', 'DesplazamientosController@updDatosPresupuesto');    
    $api->post('pagoTesoreriaDesplazamiento', 'DesplazamientosController@pagoPresupuesto');    

    $api->post('Paises','PaisesController@index');
    $api->post('PaisesXFiltro','PaisesController@paisXFiltro');

    $api->post('TarifasLugares', 'TarifasLugaresController@index'); 
    $api->post('detalleTarifasLugares', 'TarifasLugaresController@detalle');    
    $api->post('addupdTarifas', 'TarifasLugaresController@dataAddUpdate');
    $api->post('delTarifasLugares', 'TarifasLugaresController@destroy');
    $api->post('paisesTarifas', 'TarifasLugaresController@paisesTarifas');
    $api->post('rangosNacionales', 'TarifasLugaresController@rangosNacionales');
    $api->post('rangosXPais', 'TarifasLugaresController@rangosinternacionales');
    $api->post('tarifasRangos', 'TarifasLugaresController@rangos');    

    $api->post('Reuniones', 'ReunionesController@index');
    $api->post('guardarReunion', 'ReunionesController@reunionAddUpdate');
    $api->post('delReunion', 'ReunionesController@destroy');

    $api->post('Sede', 'SedeController@index');
    $api->post('guardarSedeInformes', 'SedeController@dataAddUpdate');
    $api->post('delSede', 'SedeController@destroy');

    $api->post('guardarReunionAsistentes', 'ReunionesPersonalAsistentesController@ReunionesPersonalAsistentesAdd');
    $api->post('ReunionesAsistentesXReunion', 'ReunionesPersonalAsistentesController@AsistentesxCodReunion');

    $api->post('guardarReunionVisualiza', 'ReunionesPersonalVisualizacionController@ReunionesPersonalVisualizacionAdd');
    $api->post('ReunionesVisualizaXReunion', 'ReunionesPersonalVisualizacionController@VisualizacionxCodReunion');

    $api->post('guardarReunionExterno', 'ReunionesExternoController@ReunionesExternoAdd');
    $api->post('ReunionesExternoXReunion', 'ReunionesExternoController@ReunionesExternoxCodReunion');

    $api->post('ReunionesTarea', 'ReunionesTareaController@index');
    $api->post('ReunionesTareaXCodReunion', 'ReunionesTareaController@ReunionesTareaXCodReunion');
    $api->post('guardarReunionTarea', 'ReunionesTareaController@reuniontareaAddUpdate');
    $api->post('delReunionTarea', 'ReunionesTareaController@destroy');
    $api->post('PersonalXCodTarea', 'ReunionesTareaController@PersonasXCodTareas');
    $api->post('ReunionesTareasImportar', 'ReunionesTareaController@ReunionesTareasImportar');
    $api->post('updatereunionesresumen', 'ReunionesController@updResumen');

    $api->post('Contratistas', 'ContratistasController@index');
    $api->post('contratistasXCodigo', 'ContratistasController@FiltroXCodigo');

    $api->post('usuariosXPerfil', 'LoginController@usuariosPorPerfil');   

    $api->post('ResguardosIndigenas', 'ResguardosIndigenasController@index');
    $api->post('resguardosCiudades', 'ResguardosIndigenasController@resguardosCiudades');
    $api->post('guardarResguardoIndigena', 'ResguardosIndigenasController@DataAddUpdate');    
    $api->post('delResguardoIndigena', 'ResguardosIndigenasController@destroy');
    $api->post('delCiudadResguardo', 'ResguardosIndigenasController@deleteCiudadResguardo');

    $api->post('Comunidades', 'ComunidadesController@index');
    $api->post('guardarComunidad', 'ComunidadesController@dataAddUpdate');    
    $api->post('delComunidad', 'ComunidadesController@destroy');

    $api->post('Asociaciones', 'AsociacionesController@index');
    $api->post('asociacionesComunidades', 'AsociacionesController@asociacionesComunidades');
    $api->post('asociacionesResguardos', 'AsociacionesController@asociacionesResguardos');
    $api->post('guardarAsociacion', 'AsociacionesController@DataAddUpdate');    
    $api->post('delAsociacion', 'AsociacionesController@destroy');
    $api->post('delAsociacionResguardo', 'AsociacionesController@deleteAsociacionResguardo');
    $api->post('delAsociacionComunidad', 'AsociacionesController@deleteAsociacionComunidad');
    
});