<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropRolEdu extends Model
{
    protected $connection = 'mysql';
    protected $table = 'prop_rol_edu';
    protected $fillable = ['id', 'id_proprol', 'id_solroledu', 'cumple', 'file_name', 'file_path'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
