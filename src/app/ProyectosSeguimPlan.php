<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;
    class ProyectosSeguimPlan extends Model
    {
        protected $connection = 'mysql';
        protected $table = 'proyectos_seguimiento_planaccion';
        protected $fillable = ['proycod','proyobjcod', 'proyactcod','proyspaano','proyspames1','proyspames2','proyspames3',
                                'proyspames4','proyspames5','proyspames6','proyspames7','proyspames8','proyspames9',
                                'proyspames10','proyspames11','proyspames12','proyspakmes1','proyspakmes2','proyspakmes3',
                                'proyspakmes4','proyspakmes5','proyspakmes6','proyspakmes7','proyspakmes8','proyspakmes9',
                                'proyspakmes10','proyspakmes11','proyspakmes12','proyactsec'];
        protected $primaryKey = 'proyspaano';
        public $timestamps = false;
    }

?>