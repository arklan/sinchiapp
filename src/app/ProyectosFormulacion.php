<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosFormulacion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_preguntas_formulacion';
    protected $fillable = ['proycod','formnum', 'proyfprsino','proyfprobs'];
    protected $primaryKey = 'formnum';
    public $timestamps = false;
}