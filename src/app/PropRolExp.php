<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropRolExp extends Model
{
    protected $connection = 'mysql';
    protected $table = 'prop_rol_exp';
    protected $fillable = ['id', 'id_prop', 'id_solrolexp', 'cumple', 'file_name', 'file_path', 'observacion', 'cantSoportada'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
