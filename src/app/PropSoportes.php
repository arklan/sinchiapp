<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropSoportes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'prop_soportes';
    protected $fillable = ['id', 'id_exp', 'tipo', 'fecha_ini', 'fecha_fin', 'file_name', 'file_path', 'descripcion'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
