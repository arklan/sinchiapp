<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HisPlaAdqNecActividades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'hisplanesadquisiciones_necesidadesactividades';
    protected $fillable = ['hisplaadqcod', 'plaadqneccod', 'proyactsec','catgascod',
                            'plaadqnecacttag','plaadqnecactval'];
    protected $primaryKey = 'proyactsec';
    public $timestamps = false;
}