<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planesadquisicion_solicitudes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'planesadquisicion_solicitudes';
    protected $fillable = ['plaadqcod'];
    protected $primaryKey = 'plaadqcod';
    public $timestamps = false;
}