<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class Proyectos extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'proyectos';
        protected $fillable = ['proycod', 'proyant','estcod','poacod','proyobj','proyjusimp','proyjusper','proyjusipa', 'proyobjgen',
								'proyact','proyben','proydesc','proynomcor','proytip','convcod','conmarcod','proyvbpla','proyvbdir'];
	    protected $primaryKey = 'proycod';
	    public $timestamps = false;	
	}

?>