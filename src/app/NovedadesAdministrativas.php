<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Novedades_administrativas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'novedades_administrativas';
    protected $fillable = ['novadmcod', 'novadmfec'];
    protected $primaryKey = 'novadmcod';
    public $timestamps = false;
}