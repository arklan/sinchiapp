<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paises extends Model
{
    protected $connection = 'mysql';
    protected $table = 'paises';
    protected $fillable = ['paiscod', 'paisnom','paisreg','paiscon'];
    protected $primaryKey = 'paiscod';
    public $timestamps = false;
}