<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    protected $connection = 'mysql';
    protected $table = 'estados';
    protected $fillable = ['estcod', 'estdes', 'tipestcod','tipestdes'];
    protected $primaryKey = 'estcod';
    public $timestamps = false;
}