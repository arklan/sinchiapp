<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Objetivos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'objetivos';
    protected $fillable = ['objcod', 'objdesc','linestcod'];
    protected $primaryKey = 'objcod';
    public $timestamps = false;
}