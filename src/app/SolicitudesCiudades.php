<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesCiudades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solproc_ciudades';
    protected $fillable = ['id', 'id_solproc','codigo'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
