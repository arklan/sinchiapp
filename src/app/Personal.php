<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    protected $connection = 'mysql';
    protected $table = 'personal';
    protected $fillable = ['perscod','tipidecod', 'perside', 'persape', 'persnom','cargcod','arecod', 'persdir', 'perstel','ciucod','persema',
                            'persest','tipcontcod','pershorsem','perssex'];
    protected $primaryKey = 'perscod';
    public $timestamps = false;
}

?>