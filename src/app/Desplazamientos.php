<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desplazamientos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'desplazamientos';
    protected $fillable = ['descod', 'desfec','destipben','desbentipide','desbenide',
                            'desbencar','desbendir','desbentel','desbenema','desbenfecnac',
                            'desbenvalcon','desbenmescon','desreqtiq','desobjgen','desvia',
                            'desviapagant','desviaree','desvaltiq','desgasvia','desvalgasvia',
                            'desobs','desvalvia','desenvvb','desestpla','userid','desobspla',
                            'destipliq','desarccerarl','desvalliq','desestfin','desobsfin',
                            'desestdir','desobsdir','desnumregpre','desarcregpre','desestpre',
                            'desobspre','despagtes'];
    protected $primaryKey = 'descod';
    public $timestamps = false;
}