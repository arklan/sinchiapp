<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentarios extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pqr_comentarios';
    protected $fillable = ['id_reclamo', 'comentario', 'id_usuario'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
