<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;
    class ProyectosSegDet extends Model
    {
        protected $connection = 'mysql';
        protected $table = 'proyectos_seguimiento_detalle';
        protected $fillable = ['proyactsec','proyspaano','proyseddetmes','proyseddetlog','proysegdetnec',
                                'proysegdetale','proysegdetain','proysegdetsop'];
        protected $primaryKey = 'proyspaano';
        public $timestamps = false;
    }

?>