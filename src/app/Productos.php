<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'productos';
    protected $fillable = ['prodcod', 'proddes'];
    protected $primaryKey = 'prodcod';
    public $timestamps = false;
}