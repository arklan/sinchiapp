<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolNecesidades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_necesidades';
    protected $fillable = ['id', 'id_solcontratacion', 'cod_necesidad', 'cod_plan', 'descripcion'];
    // protected $fillable = ['id', 'objetocontractual', 'tiempo', 'fecha_limite', 'incluye_gastos', 'tarifa_sinchi', 'vlrtarifa_diaria', 'tipo_solicitud', 'anticipo', 'porcentaje_anticipo', 'usuario', 'abAsignado', 'fecha_creacion', 'id_estado', 'tipo_contrato'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
