<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolProcProponentes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solproc_proponentes';
    protected $fillable = ['id', 'id_solproc', 'nombre', 'tel_fijo', 'tel_celular', 'correo', 'direccion', 'cert_tyc', 'ofertaEco', 'file_name', 'file_path'];
    // protected $fillable = ['id', 'objetocontractual', 'tiempo', 'fecha_limite', 'incluye_gastos', 'tarifa_sinchi', 'vlrtarifa_diaria', 'tipo_solicitud', 'anticipo', 'porcentaje_anticipo', 'usuario', 'abAsignado', 'fecha_creacion', 'id_estado', 'tipo_contrato'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
