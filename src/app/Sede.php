<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    protected $connection = 'mysql';
    protected $table = 'sede';
    protected $fillable = ['sedcod', 'sednom'];
    protected $primaryKey = 'sedcod';
    public $timestamps = false;
}