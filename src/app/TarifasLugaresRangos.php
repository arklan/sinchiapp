<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TarifasLugaresRangos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tarifas_lugares_rangos';
    protected $fillable = ['tarlugcod','tarrancod','tarranini','tarranfin','tarranvaldia'];
    protected $primaryKey = 'tarrancod';
    public $timestamps = false;
}