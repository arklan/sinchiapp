<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capitulos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'capitulos';
    protected $fillable = ['capcod', 'capdes'];
    protected $primaryKey = 'capcod';
    public $timestamps = false;
}