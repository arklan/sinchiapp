<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionesActaAprobaciones extends Model
{
    protected $connection = 'mysql';
    protected $table = 'reuniones_acta_aprobaciones';
    protected $fillable = ['reuactaprcod', 'reuactaprfec', 'reuactaprest', 'reuactaprcom', 'reuactcod','reupercodasi'];
    protected $primaryKey = 'reuactaprcod';
    public $timestamps = false;
}