<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;
    class ProyectosProductos extends Model
    {
        protected $connection = 'mysql';
        protected $table = 'proyectos_productos';
        protected $fillable = ['proycod','proyobjcod', 'prodcod', 'proyproddes','proyprodpes'];
        protected $primaryKey = 'prodcod';
        public $timestamps = false;
    }

?>