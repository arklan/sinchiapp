<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposIdentificacion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tipos_identificacion';
    protected $fillable = ['tipidecod','tipidelet', 'tipidedes'];
    protected $primaryKey = 'tipidecod';
    public $timestamps = false;
}