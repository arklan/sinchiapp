<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'departamentos';
    protected $fillable = ['depcod', 'depnom'];
    protected $primaryKey = 'depcod';
    public $timestamps = false;
}