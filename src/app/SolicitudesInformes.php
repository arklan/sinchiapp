<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesInformes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solproc_infoprod';
    protected $fillable = ['id', 'id_solproc','tipo','titulo', 'descripcion'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}