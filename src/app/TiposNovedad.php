<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposNovedad extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tipos_novedad';
    protected $fillable = ['tipnovcod', 'tipnovdesc'];
    protected $primaryKey = 'tipnovcod';
    public $timestamps = false;
}