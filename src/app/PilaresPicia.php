<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PilaresPicia extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pilares_picia';
    protected $fillable = ['pilpiccod', 'pilpicdes'];
    protected $primaryKey = 'pilpiccod';
    public $timestamps = false;
}