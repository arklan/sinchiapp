<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionesExterno extends Model
{
    protected $connection = 'mysql';
    protected $table = 'reuniones_externo';
    protected $fillable = ['reuextcod', 'reuextnom','reuextema','reucod'];
    protected $primaryKey = 'reuextcod';
    public $timestamps = false;
}