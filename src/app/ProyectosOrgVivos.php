<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosOrgVivos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_organismos_vivos';
    protected $fillable = ['proycod', 'orgvivcod'];
    protected $primaryKey = 'orgvivcod';
    public $timestamps = false;
}

?>