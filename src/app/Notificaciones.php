<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
    protected $connection = 'mysql';
    protected $table = 'notificaciones';
    protected $fillable = ['notcod','iduser', 'notfec','notmen','notvis','opccod','notnumint'];
    protected $primaryKey = 'notcod';
    public $timestamps = false;
}