<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndicadoresPicia extends Model
{
    protected $connection = 'mysql';
    protected $table = 'indicadores_picia';
    protected $fillable = ['indpiccod', 'indpicdes'];
    protected $primaryKey = 'indpiccod';
    public $timestamps = false;
}