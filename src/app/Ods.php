<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ods extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ods';
    protected $fillable = ['odscod', 'odsdesc' ,'objcod'];
    protected $primaryKey = 'odscod';
    public $timestamps = false;
}