<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opciones extends Model
{
    protected $connection = 'mysql';
    protected $table = 'opciones';
    protected $fillable = ['opccod', 'opccodpad', 'opcico','opclin','opcnom'];
    protected $primaryKey = 'opccod';
    public $timestamps = false;
}

?>