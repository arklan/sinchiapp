<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesProductos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_productos';
    protected $fillable = ['id', 'id_solicitud','producto','descripcion'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}