<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ParametrosGenerales extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'parametrosgenerales';
	    protected $fillable = ['pargencod', 'pargennom', 'pargenval'];
	    protected $primaryKey = 'pargencod';
	    public $timestamps = false;	
	}

?>