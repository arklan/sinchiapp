<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolProcExpMet extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solproc_expmet';
    protected $fillable = ['id', 'id_solproc', 'aspecto', 'puntajemax'];
    // protected $fillable = ['id', 'objetocontractual', 'tiempo', 'fecha_limite', 'incluye_gastos', 'tarifa_sinchi', 'vlrtarifa_diaria', 'tipo_solicitud', 'anticipo', 'porcentaje_anticipo', 'usuario', 'abAsignado', 'fecha_creacion', 'id_estado', 'tipo_contrato'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
