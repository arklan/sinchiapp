<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asociaciones extends Model
{
    protected $connection = 'mysql';
    protected $table = 'asociaciones';
    protected $fillable = ['asocod', 'asonom'];
    protected $primaryKey = 'asocod';
    public $timestamps = false;
}