<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaAdqNecActividades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'planesadquisiciones_necesidades_actividades';
    protected $fillable = ['plaadqcod', 'plaadqneccod', 'proyactsec','catgascod',
                            'plaadqnecacttag','plaadqnecactval'];
    protected $primaryKey = 'proyactsec';
    public $timestamps = false;
}