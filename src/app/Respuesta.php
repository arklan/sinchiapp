<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pqr_respuesta';
    protected $fillable = ['id_reclamo', 'comentario', 'id_usuario'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
