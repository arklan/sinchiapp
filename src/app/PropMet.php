<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropMet extends Model
{
    protected $connection = 'mysql';
    protected $table = 'prop_met';
    protected $fillable = ['id', 'id_prop', 'id_solproc_met', 'actor1', 'actor2', 'actor3', 'file_name', 'file_path'];
    // protected $fillable = ['id', 'objetocontractual', 'tiempo', 'fecha_limite', 'incluye_gastos', 'tarifa_sinchi', 'vlrtarifa_diaria', 'tipo_solicitud', 'anticipo', 'porcentaje_anticipo', 'usuario', 'abAsignado', 'fecha_creacion', 'id_estado', 'tipo_contrato'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
