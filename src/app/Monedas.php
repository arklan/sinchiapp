<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class Monedas extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'monedas';
        protected $fillable = ['moncod', 'mondes', 'monsim','montas'];
	    protected $primaryKey = 'moncod';
	    public $timestamps = false;	
	}

?>