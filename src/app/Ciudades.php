<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ciudades';
    protected $fillable = ['ciucod', 'ciunom'];
    protected $primaryKey = 'ciucod';
    public $timestamps = false;
}