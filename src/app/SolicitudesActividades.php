<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitudes_actividades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_actividades';
    protected $fillable = ['solactcod', 'solactdes'];
    protected $primaryKey = 'solactcod';
    public $timestamps = false;
}