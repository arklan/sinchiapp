<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropExpProp extends Model
{
    protected $connection = 'mysql';
    protected $table = 'prop_expprop';
    protected $fillable = ['id', 'id_prop', 'id_expprop', 'cumple', 'file_name', 'file_path', 'observacion', 'cantSoportada'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
