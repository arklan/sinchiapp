<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminos_referencia extends Model
{
    protected $connection = 'mysql';
    protected $table = 'terminos_referencia';
    protected $fillable = ['terrefcod', 'terreffec', 'terrefjthcod', 'terrefest'];
    protected $primaryKey = 'terrefcod';
    public $timestamps = false;
}