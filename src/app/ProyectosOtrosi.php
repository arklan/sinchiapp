<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosOtrosi extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_otrosi';
    protected $fillable = ['proycod','proyotscod', 'proyfecsus', 'proyfectereje', 'proyfecinc',
                            'proyvaltot','proyvalcof','proyvalctp','proyvaldin','proytotesp',
                            'proytieeje'];
    protected $primaryKey = 'proyotscod';
    public $timestamps = false;
}