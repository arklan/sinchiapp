<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposEstados extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tipos_estados';
    protected $fillable = ['tipestcod','tipestdes'];
    protected $primaryKey = 'tipestcod';
    public $timestamps = false;
}