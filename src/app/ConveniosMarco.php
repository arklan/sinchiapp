<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConveniosMarco extends Model
{
    protected $connection = 'mysql';
    protected $table = 'convenios_marco';
    protected $fillable = ['conmarcod', 'conmarnomaso','conmarfecsus','conmarfecter','conmarfecini','estcod','conmarobj','conmarobs',
                            'conmararccon','conmararcoda'];
    protected $primaryKey = 'conmarcod';
    public $timestamps = false;
}