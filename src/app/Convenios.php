<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convenios extends Model
{
    protected $connection = 'mysql';
    protected $table = 'convenios';
    protected $fillable = ['convcod', 'convnum','convnom', 'convfecsus','convfectereje','convfecinc','convtieeje','moncod',
                            'convtas','convvalcof','convvalctp','convvaltot','convvaldin','convtotesp','estcod',
                            'perscod','convenc','convobj','convcaf','convaff','convtff','convfuefin','convarcgen',
                            'convarcoda','convarccon','convtecfuefin','convcontaff'];
    protected $primaryKey = 'convcod';
    public $timestamps = false;
}