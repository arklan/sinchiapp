<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolProcRoles extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solproc_roles';
    protected $fillable = ['id', 'id_solproc', 'tipo', 'rol', 'porc_rol', 'porc_edu', 'porc_exp'];
    // protected $fillable = ['id', 'objetocontractual', 'tiempo', 'fecha_limite', 'incluye_gastos', 'tarifa_sinchi', 'vlrtarifa_diaria', 'tipo_solicitud', 'anticipo', 'porcentaje_anticipo', 'usuario', 'abAsignado', 'fecha_creacion', 'id_estado', 'tipo_contrato'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
