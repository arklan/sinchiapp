<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesAdjuntos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_adjuntos';
    protected $fillable = ['id','id_solicitud','docSolicitado','path','nombre','aprobado','noAplica','observaciones'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
