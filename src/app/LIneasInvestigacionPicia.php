<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineasInvestigacionPicia extends Model
{
    protected $connection = 'mysql';
    protected $table = 'lineas_investigacion_picia';
    protected $fillable = ['lininvpiccod', 'liniinvpicdes'];
    protected $primaryKey = 'lininvpiccod';
    public $timestamps = false;
}