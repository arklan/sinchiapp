<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosObsSubCie extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_obssubdcientifica';
    protected $fillable = ['proycod','proysdccod', 'proysdcdes'];
    protected $primaryKey = 'proysdccod';
    public $timestamps = false;
}