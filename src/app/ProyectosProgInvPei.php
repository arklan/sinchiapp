<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ProyectosProgInvPei extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'proyectos_proginv_pei';
	    protected $fillable = ['proycod', 'proinvpeicod'];
	    protected $primaryKey = 'proinvpeicod';
	    public $timestamps = false;	
	}

?>