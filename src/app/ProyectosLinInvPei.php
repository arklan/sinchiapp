<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ProyectosLinInvPei extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'proyectos_lininv_pei';
	    protected $fillable = ['proycod', 'lininvpeicod'];
	    protected $primaryKey = 'lininvpeicod';
	    public $timestamps = false;	
	}

?>