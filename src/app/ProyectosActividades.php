<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosActividades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_actividades';
    protected $fillable = ['proyactcod', 'proycod','proyobjcod','prodcod', 'proyactdes','proyactfecini','proyactfecfin','proyactpes',
                            'proyactkpi','proyactdeskpi','proyactsec','proyacttip','proyactval'];
    protected $primaryKey = 'proyactsec';
    public $timestamps = false;
}

?>