<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unspsc extends Model
{
    protected $connection = 'mysql';
    protected $table = 'unspsc';
    protected $fillable = ['unspsccod', 'unspscdes'];
    protected $primaryKey = 'unspsccod';
    public $timestamps = false;
}