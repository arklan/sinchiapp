<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosFormulacion;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosFormulacionTransformer extends TransformerAbstract
{
    public function transform(ProyectosFormulacion $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $Data->formnum,
            'proyecto'    => $Data->proycod,   
            'pregunta'    => $Data->formpreg,      
            'sino'        => $Data->proyfprsino,
            'observacion' => $Data->proyfprobs
    	];
    }
}

?>