<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosIndicadoresPicia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosIndicadoresPiciaTransformer extends TransformerAbstract
{
    public function transform(ProyectosIndicadoresPicia $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $Data->indpiccod,            
            'proyecto'    => $Data->proycod            
    	];
    }
}

?>