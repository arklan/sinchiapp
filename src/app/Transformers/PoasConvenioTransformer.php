<?php

namespace App\Transformers;

// We need to reference the Model
use App\Convenios;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ConveniosTransformer extends TransformerAbstract
{
    public function transform(Convenios $Convenios)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $Convenios->poacod,           
            'codigoconvenios'       => $Convenios->convcod
            
    	];
    }
}

?>