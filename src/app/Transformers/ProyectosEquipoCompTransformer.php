<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosEquipoComp;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosEquipoCompTransformer extends TransformerAbstract
{
    public function transform(ProyectosEquipoComp $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Data->proyetccod,
            'codigoreal'      => $Data->proyetccod,   
            'proyecto'        => $Data->proycod, 
            'personal'        => $Data->perscod,         
            'descripcion'     => $Data->proyetcdes
    	];
    }
}

?>