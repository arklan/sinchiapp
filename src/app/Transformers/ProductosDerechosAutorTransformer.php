<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProductosDerechosAutor;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProductosDerechosAutorTransformer extends TransformerAbstract
{
    public function transform(ProductosDerechosAutor $ProductosDerechosAutor)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $ProductosDerechosAutor->prodderautcod,           
            'descripcion' => $ProductosDerechosAutor->prodderautdes
    	];
    }
}

?>