<?php

namespace App\Transformers;

// We need to reference the Model
use App\Ods;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class OdsTransformer extends TransformerAbstract
{
    public function transform(Ods $Ods)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Ods->odscod,            
            'descripcion'     => $Ods->odsdesc,
            'codigoobjetivo'  => $Ods->objcod,
            'nombreobjetivo'  => $Ods->objdesc             
    	];
    }
}

?>