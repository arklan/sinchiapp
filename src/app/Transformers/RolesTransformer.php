<?php

namespace App\Transformers;

// We need to reference the Model
use App\Roles;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class RolesTransformer extends TransformerAbstract
{
    public function transform(Roles $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Data->rolcod,            
            'descripcion'     => $Data->roldes
    	];
    }
}

?>