<?php

namespace App\Transformers;

// We need to reference the Model
use App\Unspsc;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class UnspscTransformer extends TransformerAbstract
{
    public function transform(Unspsc $Unspsc)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'  => $Unspsc->unspsccod,           
            'nombre'  => $Unspsc->unspscdes	           
    	];
    }
}

?>