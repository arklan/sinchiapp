<?php

namespace App\Transformers;

// We need to reference the Model
use App\ConveniosFecPreInfTec;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ConveniosFecPreInfTecTransformer extends TransformerAbstract
{
    public function transform(ConveniosFecPreInfTec $Data)
    {
        // Specify what elements are going to be visible to the API
        return [            
            'codigo'            => $Data->convcod,         
            'fechapresentacion' => $Data->convfpit           
    	];
    }
}

?>