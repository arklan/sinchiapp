<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosOtrosi;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;
class ProyectosOtrosiTransformer extends TransformerAbstract
{
    public function transform(ProyectosOtrosi $Proyectos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'proyecto'            => $Proyectos->proycod,            
            'codigo'              => $Proyectos->proyotscod,            
            'fechasuscripcion'    => $Proyectos->proyfecsus,
            'fechaterejecucion'   => $Proyectos->proyfectereje,
            'fechaincorporacion'  => $Proyectos->proyfecinc,
            'valortotal'          => $Proyectos->proyvaltot,
            'cofinanciado'        => $Proyectos->proyvalcof,
            'contrapartida'       => $Proyectos->proyvalctp,
            'totaldinero'         => $Proyectos->proyvaldin,
            'totalespecie'        => $Proyectos->proytotesp,
            'tiempo'              => $Proyectos->proytieeje,
            'codigomoneda'        => $Proyectos->moncod,
            'moneda'              => $Proyectos->monsim,
            'tasa'                => $Proyectos->proytas,
            'convenioproyecto'    => $Proyectos->proynumcon,
            'nombreproyecto'      => $Proyectos->proynomcor, 
            'codigocoordinador'   => $Proyectos->perscod            
    	];
    }
}

?>