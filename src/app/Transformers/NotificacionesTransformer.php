<?php

namespace App\Transformers;

// We need to reference the Model
use App\Notificaciones;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class NotificacionesTransformer extends TransformerAbstract
{
    public function transform(Notificaciones $Notificaciones)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'        => $Notificaciones->notcod,
            'codigousuario' => $Notificaciones->iduser,           
            'nombre'        => $Notificaciones->nombre,
            'fecha'         => $Notificaciones->notfec,
            'mensaje'       => $Notificaciones->notmen,
            'visto'         => $Notificaciones->notvis,
            'codigoopcion'  => $Notificaciones->opccod,
            'opcion'        => $Notificaciones->opcnom,
            'numerointerno' => $Notificaciones->notnumint,
    	];
    }
}

?>