<?php

namespace App\Transformers;

// We need to reference the Model
use App\Capitulos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class CapitulosTransformer extends TransformerAbstract
{
    public function transform(Capitulos $Capitulos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $Capitulos->capcod,           
            'descripcion' => $Capitulos->capdes
    	];
    }
}

?> 	