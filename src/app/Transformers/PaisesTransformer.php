<?php

namespace App\Transformers;

// We need to reference the Model
use App\Paises;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PaisesTransformer extends TransformerAbstract
{
    public function transform(Paises $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'         => $Data->paiscod,           
            'nombrecompleto' => $Data->paisnom,
            'region'         => $Data->paisreg,
            'continente'     => $Data->paiscon,            
    	];
    }
}

?>