<?php

namespace App\Transformers;

// We need to reference the Model
use App\Contratos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ContratosTransformer extends TransformerAbstract
{
    public function transform(Contratos $Contratos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'           => $Contratos->contcod,           
            'tipocontrato'     => $Contratos->conttip,
            'nombre'           => $Contratos->contnom,
            'objetivo'         => $Contratos->conobj,
            'fechafirma'       => $Contratos->confecfir,
            'fecha'            => $Contratos->confecapr,
            'fechainicial'     => $Contratos->confecini,
            'fechaterminacion' => $Contratos->confecter,
            'ejecuciondia'     => $Contratos->conejedia,
            'fechacdp'         => $Contratos->confeccdp,
            'numerocdp'        => $Contratos->connumcdp,
            'fecharp'          => $Contratos->confecrp,
            'numerorp'         => $Contratos->connumrp,
            'predial'          => $Contratos->conrubpre
    	];
    }
}

?> 	