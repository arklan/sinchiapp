<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosSegDet;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosSegDetTransformer extends TransformerAbstract
{
    public function transform(ProyectosSegDet $Data)
    {
        // Specify what elements are going to be visible to the API
        return [           
            'codigo'              => $Data->proyactsec,
            'anno'                => $Data->proyspaano,
            'mes'                 => $Data->proyseddetmes,
            'logros'              => $Data->proyseddetlog,
            'necesidades'         => $Data->proysegdetnec,
            'alertas'             => $Data->proysegdetale,
            'ampliacioninfo'      => $Data->proysegdetain,
            'obssubdircientifica' => $Data->proysegdetosc,
            'archivo'             => $Data->proysegdetsop,
            'rutasarchivogen'     => url('/proyectos/S'.$Data->proyactsec.$Data->proyspaano.$Data->proyseddetmes.'/'.$Data->proysegdetsop),           
    	];
    }
}

?>