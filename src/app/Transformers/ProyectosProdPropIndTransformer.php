<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosProdPropInd;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosProdPropIndTransformer extends TransformerAbstract
{
    public function transform(ProyectosProdPropInd $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Data->prodproindcod,
            'proyecto'        => $Data->proycod,         
            'descripcion'     => $Data->prodproinddes
    	];
    }
}

?>