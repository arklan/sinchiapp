<?php

namespace App\Transformers;

// We need to reference the Model
use App\LineasInvestigacionPicia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class LineasInvestigacionPiciaTransformer extends TransformerAbstract
{
    public function transform(LineasInvestigacionPicia $LineasInvestigacion)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $LineasInvestigacion->lininvpiccod,           
            'descripcion' => $LineasInvestigacion->lininvpicdes
    	];
    }
}

?>