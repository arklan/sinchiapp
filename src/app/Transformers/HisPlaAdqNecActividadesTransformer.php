<?php

namespace App\Transformers;

// We need to reference the Model
use App\HisPlaAdqNecActividades;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class HisPlaAdqNecActividadesTransformer extends TransformerAbstract
{
    public function transform(HisPlaAdqNecActividades $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'planadquisicion'    => $Data->hisplaadqcod,            
            'necesidad'          => $Data->plaadqneccod,
            'secuenciaactividad' => $Data->proyactsec,
            'actividadtxt'       => $Data->proyactdes,
            'categoriagasto'     => $Data->prescatgascod,
            'categoriagastotxt'  => $Data->prescatgasdes,
            'topecategoriagasto' => $Data->plaadqnecacttag,           
            'valor'              => $Data->plaadqnecactval,
            'rubro'              => $Data->plaadqnecactrpr,
            'presupuesto'        => $Data->prescod,            
    	];
    }
}

?>