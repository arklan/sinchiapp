<?php

namespace App\Transformers;

// We need to reference the Model
use App\Productos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProductosTransformer extends TransformerAbstract
{
    public function transform(Productos $Productos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $Productos->prodcod,           
            'descripcion' => $Productos->proddes
            
    	];
    }
}

?>