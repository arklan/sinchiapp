<?php

namespace App\Transformers;

// We need to reference the Model
use App\Login;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class LoginTransformer extends TransformerAbstract
{
    public function transform(Login $login)
    {
        // Specify what elements are going to be visible to the API
        return [
                'usuarioid'      => $login->iduser,
                'nombreusuario'  => $login->nombre,
                'usuario'        => $login->usuario,
                'sexo'           => $login->sexo,
                'email'          => $login->email,
                'estado'         => $login->estado,
                'nomestado'      => $login->estnom,
                'codigoperfil'   => $login->perfcod,
                'nombreperfil'   => $login->perfnom,
                'codigopersonal' => $login->perscod,
                'nombrepersonal' => $login->persnom.' '.$login->persape
        ];
    }
}