<?php

namespace App\Transformers;

// We need to reference the Model
use App\Comunidades;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ComunidadesTransformer extends TransformerAbstract
{
    public function transform(Comunidades $data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'           => $data->comcod,           
            'nombre'           => $data->comnom,
            'codigoasociacion' => $data->asocod,
            'nombreasociacion' => $data->asonom
    	];
    }
}

?>