<?php

namespace App\Transformers;

// We need to reference the Model
use App\TiposContratacion;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class TiposContratacionTransformer extends TransformerAbstract
{
    public function transform(TiposContratacion $TiposContratacion)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $TiposContratacion->tipconcod,           
            'nombre'       => $TiposContratacion->tipconnom           
    	];
    }
}

?>