<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosCiudades;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosCiudadesTransformer extends TransformerAbstract
{
    public function transform(ProyectosCiudades $Data)
    {
        // Specify what elements are going to be visible to the API
        return [            
            'proyecto'     => $Data->proycod,         
            'ciudad'       => $Data->ciucod,
            'nombreciudad' => $Data->ciunom.' - '.$Data->depnom,
            'codigodepto'  => $Data->depcod,
            'nombredepto'  => $Data->depnom,
            'nomciudad'    => $Data->ciunom            
     	];
    }
}

?>