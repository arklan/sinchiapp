<?php

namespace App\Transformers;

// We need to reference the Model
use App\TarifasLugaresRangos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class TarifasLugaresRangosTransformer extends TransformerAbstract
{
    public function transform(TarifasLugaresRangos $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigolugar'  => $Data->tarlugcod,           
            'codigo'       => $Data->tarrancod,
            'rangoinicial' => $Data->tarranini,
            'rangofinal'   => $Data->tarranfin,
            'valordiario'  => $Data->tarranvaldia,                     
    	];
    }
}

?>