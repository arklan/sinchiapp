<?php

namespace App\Transformers;

// We need to reference the Model
use App\Bitacora;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class BitacoraTransformer extends TransformerAbstract
{
    public function transform(Bitacora $Bitacora)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'  => $Bitacora->BitSec,           
            'userid'  => $Bitacora->iduser,
            'fecha'   => $Bitacora->BitFecReg,
            'modulo'  => $Bitacora->BitModDes,
            'accion'  => $Bitacora->BitAccDes,
            'ip'      => utf8_encode($Bitacora->BitEquIp),
            'detalle' => $Bitacora->BitMem,
            'host'    => utf8_encode($Bitacora->BitHos),
            'usuario' => utf8_encode($Bitacora->nombre),
    	];
    }
}

?>