<?php

namespace App\Transformers;

// We need to reference the Model
use App\TmpPlaAdqNecesidadesUnspsc;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class TmpPlaAdqNecesidadesUnspscTransformer extends TransformerAbstract
{
    public function transform(TmpPlaAdqNecesidadesUnspsc $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'planadquisicion'  => $Data->tmpplaadqcod,            
            'necesidad'        => $Data->plaadqneccod,
            'unspsc'           => $Data->unspsccod,
            'unspsctxt'        => $Data->unspscdes,
            'tipomod'          => $Data->plaadqnecunstmod           
    	];
    }
}

?>