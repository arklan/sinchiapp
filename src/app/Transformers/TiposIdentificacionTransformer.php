<?php

namespace App\Transformers;

// We need to reference the Model
use App\TiposIdentificacion;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class TiposIdentificacionTransformer extends TransformerAbstract
{
    public function transform(TiposIdentificacion $TiposIdentificacion)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $TiposIdentificacion->tipidecod,
            'letra'       => $TiposIdentificacion->tipidelet,            
            'descripcion' => $TiposIdentificacion->tipidedes
    	];
    }
}

?>