<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProgramasInvestigacionPei;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProgInvestigacionPeiTransformer extends TransformerAbstract
{
    public function transform(ProgramasInvestigacionPei $ProgInvest)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $ProgInvest->proinvpeicod,            
            'descripcion'     => $ProgInvest->proinvpeides           
    	];
    }
}

?>