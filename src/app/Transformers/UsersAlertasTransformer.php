<?php

namespace App\Transformers;

// We need to reference the Model
use App\UsersAlertas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class UsersAlertasTransformer extends TransformerAbstract
{
    public function transform(UsersAlertas $Data)
    {
        return [
            'codigo'   => $Data->iduser,           
            'nombre'   => $Data->nombre,
            'issystem' => $Data->useconfnsi,
            'isemail'  => $Data->useconfnco,
            'correo'   => $Data->email           
    	];
    }
}

?>