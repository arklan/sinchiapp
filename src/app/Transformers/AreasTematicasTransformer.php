<?php

namespace App\Transformers;

// We need to reference the Model
use App\AreasTematicas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class AreasTematicasTransformer extends TransformerAbstract
{
    public function transform(AreasTematicas $AreasTematica)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $AreasTematica->aretemcod,           
            'nombre' => $AreasTematica->aretemnom
    	];
    }
}

?>