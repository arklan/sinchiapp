<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosProductos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosProductosTransformer extends TransformerAbstract
{
    public function transform(ProyectosProductos $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Data->prodcod,
            'descripcion'     => $Data->proyproddes,    
            'proyecto'        => $Data->proycod,
            'codigoobjetivo'  => $Data->proyobjcod,
            'txtobjetivo'     => $Data->proyobjdes,
            'codigoreal'      => $Data->codigoreal,
            'peso'            => $Data->proyprodpes
    	];
    }
}

?>