<?php

namespace App\Transformers;

// We need to reference the Model
use App\ConveniosSocios;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ConveniosSociosTransformer extends TransformerAbstract
{
    public function transform(ConveniosSocios $ConveniosSocios)
    {
        // Specify what elements are going to be visible to the API
        return [
            'convenio'           => $ConveniosSocios->convcod, 
            'codigo'             => $ConveniosSocios->convsoccod,
            'codigomoneda'       => $ConveniosSocios->moncod,
            'moneda'             => $ConveniosSocios->monsim,
            'tasa'               => $ConveniosSocios->convtas, 
            'cofinanciado'       => $ConveniosSocios->convsocvalcof,
            'contrapartida'      => $ConveniosSocios->convsocvalctp,
            'totaldinero'        => $ConveniosSocios->convsocvaldin,
            'totalespecie'       => $ConveniosSocios->convsoctotesp,
            'valortotal'         => $ConveniosSocios->convsocvaltot,
            'fuentefinanciacion' => $ConveniosSocios->convsocfuefin,
            
    	];
    }
}

?>