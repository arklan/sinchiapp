<?php

namespace App\Transformers;

// We need to reference the Model
use App\TiposNovedad;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class TiposNovedadTransformer extends TransformerAbstract
{
    public function transform(TiposNovedad $Tipos_Novedad)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $Tipos_Novedad->tipnovcod,           
            'descripcion' => $Tipos_Novedad->tipnovdesc            
    	];
    }
}

?>