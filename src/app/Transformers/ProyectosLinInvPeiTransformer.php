<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosLinInvPei;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosLinInvPeiTransformer extends TransformerAbstract
{
    public function transform(ProyectosLinInvPei $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'   => $Data->lininvpeicod,            
            'proyecto' => $Data->proycod
    	];
    }
}

?>