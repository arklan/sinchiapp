<?php

namespace App\Transformers;

// We need to reference the Model
use App\HisPlanesAdquisicion;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class HisPlanesAdquisicionTransformer extends TransformerAbstract
{
    public function transform(HisPlanesAdquisicion $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'hiscod'               => $Data->hisplaadqcod,
            'codigo'               => $Data->plaadqcod,
            'fecha'                => $Data->plaadqfec,
            'proyecto'             => $Data->proycod,
            'proyectonombre'       => $Data->proynomcor,
            'convenio'             => $Data->convcod,
            'numeroconvenio'       => $Data->convnum,
            'nombreconvenio'       => $Data->convnom,
            'coordinador'          => $Data->perscod,
            'moneda'               => $Data->monsim,
            'valortotalconvenio'   => $Data->convvaltot,
            'estadojurudico'       => $Data->estjurcod,
            'estadojurudicodesc'   => $Data->estjurdes,    
            'aclaracionjurudica'   => $Data->plaadqacj,
            'estadoplaneacion'     => $Data->estplacod,
            'estadoplaneaciondesc' => $Data->estplades,    
            'aclaracionplaneacion' => $Data->plaadqacp,
            'enviovobo'            => $Data->plaadqevb,
            'basemoneda'           => $Data->plaadqbasmon,
            'tasa'                 => $Data->convtas,
            'valorpesos'           => $Data->convtas != 0 ? $Data->convtas * $Data->convvaltot : 
                                            $Data->convvaltot,
            'version'              => $Data->plaadqver,                                 
    	];
    }
    
}

?>