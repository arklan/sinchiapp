<?php

namespace App\Transformers;

// We need to reference the Model
use App\Solicitudes;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class SolicitudesTransformer extends TransformerAbstract
{
    public function transform(Solicitudes $data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'id' => $data->id,           
            'estado' => $data->estado,
            'fecha_creacion' => $data->fecha_creacion,           
            'ab_asignado' => $data->ab_asignado
    	];
    }
}

?>