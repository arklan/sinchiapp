<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosLinInvPicia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosLinInvPiciaTransformer extends TransformerAbstract
{
    public function transform(ProyectosLinInvPicia $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'   => $Data->lininvpiccod,            
            'proyecto' => $Data->proycod
    	];
    }
}

?>