<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosEquipo;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosEquipoTransformer extends TransformerAbstract
{
    public function transform(ProyectosEquipo $Data)
    {
        // Specify what elements are going to be visible to the API
        return [            
            'proyecto'        => $Data->proycod,         
            'persona'         => $Data->perscod,
            'nombrecompleto'  => $Data->persnom.' '.$Data->persape,
            'rol'             => $Data->rolcod,
            'rolnombre'       => $Data->roldes,
            'dedicacion'      => $Data->proyeteded,
            'porcentaje'      => $Data->proyetepor,
            'titular'         => $Data->proyetetit,
            'titulartxt'      => $Data->proyetetit == 1 ? 'SI' : 'NO',
            'codigoreal'      => $Data->perscod,            
    	];
    }
}

?>