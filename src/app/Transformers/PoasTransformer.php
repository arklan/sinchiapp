<?php

namespace App\Transformers;

// We need to reference the Model
use App\Poas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PoasTransformer extends TransformerAbstract
{
    public function transform(Poas $Poas)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                  => $Poas->poacod,
            'descripcion'             => utf8_encode($Poas->poades),
            'estrategia'              => utf8_encode($Poas->poaestr),            
            'indicador'               => utf8_encode($Poas->poaind),
            'unidadmedida'            => utf8_encode($Poas->poaunimed),            
            'metasectorial'           => $Poas->poametsec,
            'metainstitucional'       => $Poas->poametins,
            'observacion'             => utf8_encode($Poas->poaobs),
            'capitulocodigo'          => $Poas->capcod,
            'capitulodescripcion'     => utf8_encode($Poas->capdes),
            'lineacodigo'             => $Poas->linestcod,
            'lineadescripcion'        => utf8_encode($Poas->linestdesc),
            'objetivocodigo'          => $Poas->objcod,
            'objetivodescripcion'     => utf8_encode($Poas->objdesc),
            'odscodigo'               => $Poas->odscod,
            'odsdescripcion'          => utf8_encode($Poas->odsdesc),
            'metacodigo'              => $Poas->metcod,
            'metadescripcion'         => utf8_encode($Poas->metnom),
            'proginvpeniacodigo'      => $Poas->proinvpencod,
            'proginvpeniadescripcion' => utf8_encode($Poas->proinvpendes),
            'lininvpeniacodigo'       => $Poas->lininvpencod,
            'lininvpeniadescripcion'  => utf8_encode($Poas->lininvpendes),
            'areatematcodigo'         => $Poas->aretemcod,
            'areatematdescripcion'    => utf8_encode($Poas->aretemnom),
            'proginvpeicodigo'        => $Poas->proinvpeicod,
            'proginvpeidescripcion'   => utf8_encode($Poas->proinvpeides),
            'lininvpeicodigo'         => $Poas->lininvpeicod,
            'lininvpeidescripcion'    => utf8_encode($Poas->lininvpeides),
    	];
    }
}

?>