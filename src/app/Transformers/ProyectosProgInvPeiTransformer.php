<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosProgInvPei;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosProgInvPeiTransformer extends TransformerAbstract
{
    public function transform(ProyectosProgInvPei $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'   => $Data->proinvpeicod,            
            'proyecto' => $Data->proycod
    	];
    }
}

?>