<?php

namespace App\Transformers;

// We need to reference the Model
use App\Categorias;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class CategoriasTransformer extends TransformerAbstract
{
    public function transform(Categorias $Categorias)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $Categorias->catcod,           
            'descripcion' => $Categorias->catdes
    	];
    }
}

?>