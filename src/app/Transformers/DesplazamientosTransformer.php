<?php

namespace App\Transformers;

// We need to reference the Model
use App\Desplazamientos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class DesplazamientosTransformer extends TransformerAbstract
{
    public function transform(Desplazamientos $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'              => $Data->descod,           
            'fecha'               => $Data->desfec,
            'tipobeneficiario'    => $Data->destipben,
            'tipobeneficiariotxt' => $Data->destipben == 'T' ? 'TRABAJADOR' : 
                                        ($Data->destipben == 'C' ? 'CONTRATISTA' : 'INVITADO'),
            'tipoidentcodigo'     => $Data->desbentipide,
            'tipoidentificacion'  => $Data->tipidelet,
            'identificacion'      => $Data->desbenide,
            'beneficiario'        => $Data->desbennom,
            'cargocodigo'         => $Data->desbencar,
            'cargo'               => $Data->cargnom,
            'direccion'           => $Data->desbendir,
            'telefono'            => $Data->desbentel,
            'email'               => $Data->desbenema,
            'fechanacimiento'     => $Data->desbenfecnac,
            'valorcontrato'       => $Data->desbenvalcon,
            'mesescontrato'       => $Data->desbenmescon,
            'requieretiquetes'    => $Data->desreqtiq,
            'requieretiquetestxt' => $Data->desreqtiq == 'S' ? 'SI' : 'NO',
            'objetivogeneral'     => $Data->desobjgen,
            'viaticos'            => $Data->desvia,
            'viaticostxt'         => $Data->desvia == 'S' ? 'SI' : 'NO',
            'valorviaticos'       => $Data->desvalvia,
            'pagoanticipado'      => $Data->desviapagant,
            'reembolso'           => $Data->desviaree,
            'valortiquetes'       => $Data->desvaltiq,
            'gastosviaje'         => $Data->desgasvia,
            'gastosviajetxt'      => $Data->desgasvia == 'S' ? 'SI' : 'NO',
            'valorgastosviaje'    => $Data->desvalgasvia,
            'observacion'         => $Data->desobs,
            'valorbaseben'        => $Data->desbenvalbas,
            'enviovobo'           => $Data->desenvvb, 
            'usuario'             => $Data->userid,
            'voboplaneacion'      => $Data->desestpla,
            'obsplaneacion'       => $Data->desobspla,
            'tipoliquidacion'     => $Data->destipliq,
            'archivoarl'          => $Data->desarccerarl,
            'valorliquidado'      => $Data->desvalliq,
            'vobofinanciera'      => $Data->desestfin,
            'obsfinanciera'       => $Data->desobsfin,
            'vobodireccion'       => $Data->desestdir,
            'obsdireccion'        => $Data->desobsdir,
            'vobopresupuesto'     => $Data->desestpre,
            'obspresupuesto'      => $Data->desobspre,    
            'numerorp'            => $Data->desnumregpre,
            'archivorp'           => $Data->desarcregpre,
            'pagotesoreria'       => $Data->despagtes,

            'rutaarchivoarl'      => url('/desplazamientos/D'.$Data->descod.'/'.$Data->desarccerarl),
            'rutaarchivorp'       => url('/desplazamientos/D'.$Data->descod.'/'.$Data->desarcregpre),
    	];
    }
}

?>