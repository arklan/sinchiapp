<?php

namespace App\Transformers;

// We need to reference the Model
use App\ResguardosIndigenas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ResguardosIndigenasTransformer extends TransformerAbstract
{
    public function transform(ResguardosIndigenas $data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'         => $data->resindcod,           
            'nombre'         => $data->resindnom,
            'seleccionar'    => $data->Seleccionar
    	];
    }
}

?>