<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProgramasInvestigacionPenia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProgInvestigacionPeniaTransformer extends TransformerAbstract
{
    public function transform(ProgramasInvestigacionPenia $ProgInvest)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $ProgInvest->proinvpencod,            
            'descripcion'     => $ProgInvest->proinvpendes           
    	];
    }
}

?>