<?php

namespace App\Transformers;

// We need to refereance the Model
use App\Solicitudes_Antecedentes;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class Solicitudes_AntecedentesTransformer extends TransformerAbstract
{
    public function transform(Solicitudes_Antecedentes $Solicitudes_Antecedentes)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $Solicitudes_Antecedentes->solantcod,           
            'codigosolicitud' => $Solicitudes_Antecedentes->solcod,
            'antecedentes' => $Solicitudes_Antecedentes->solantdes
    	];
    }
}

?>