<?php

namespace App\Transformers;

// We need to reference the Model
use App\HisPlanesAdquisicionNec;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class HisPlanesAdquisicionNecTransformer extends TransformerAbstract
{
    public function transform(HisPlanesAdquisicionNec $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'planadquisicion'  => $Data->hisplaadqcod,            
            'codigo'           => $Data->plaadqneccod,
            'descripcion'      => $Data->plaadqnecdes,
            'valor'            => $Data->plaadqnecval,
            'metodoleccion'    => $Data->metselcod,
            'metodolecciontxt' => $Data->metseldes,
            'fechasolcontrata' => $Data->plaadqnecfeccont,
            'estadocodigo'     => $Data->estcod,
            'estadotxt'        => $Data->estdes,
            'vobofinanciador'  => $Data->plaadqnecvbf,
            'obsfinanciador'   => $Data->plaadqnecobf,
            'vobojuridica'     => $Data->plaadqnecvbj,
            'obsjuridica'      => $Data->plaadqnecobj,
            'voboplaneacion'   => $Data->plaadqnecvbp,
            'obsplaneacion'    => $Data->plaadqnecobp,
            'enviovobo'        => $Data->plaadqevb,
            'modificada'       => 'N',                      
    	];
    }
}

?>