<?php

namespace App\Transformers;

// We need to reference the Model
use App\AccionesPicia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class AccionesPiciaTransformer extends TransformerAbstract
{
    public function transform(AccionesPicia $data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $data->accpiccod,           
            'descripcion' => $data->accpicdes,
            'codigolininvpicia' => $data->lininvpiccod
    	];
    }
}

?>