<?php

namespace App\Transformers;

// We need to reference the Model
use App\ReunionesActaAprobaciones;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ReunionesActaAprobacionesTransformer extends TransformerAbstract
{
    public function transform(ReunionesActaAprobaciones $ReunionesActaAprobaciones)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'            => $ReunionesActaAprobaciones->reuactaprcod,           
            'fecha'             => $ReunionesActaAprobaciones->reuactaprfec,
            'comentario'        => $ReunionesActaAprobaciones->reuactaprcom,
            'estado'            => $ReunionesActaAprobaciones->reuactaprest,
            'codigoacta'        => $ReunionesActaAprobaciones->reuactcod,
            'codigoasistente'   => $ReunionesActaAprobaciones->reupercodasi,
            'codigopersonal'    => $ReunionesActaAprobaciones->perscod
    	];
    }
}

?>