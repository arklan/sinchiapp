<?php

namespace App\Transformers;

// We need to reference the Model
use App\Perfiles;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PerfilesTransformer extends TransformerAbstract
{
    public function transform(Perfiles $Perfiles)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $Perfiles->perfcod,           
            'nombre' => $Perfiles->perfnom
    	];
    }
}

?>