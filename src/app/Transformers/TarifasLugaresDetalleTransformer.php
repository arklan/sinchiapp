<?php

namespace App\Transformers;

// We need to reference the Model
use App\TarifasLugaresDetalle;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class TarifasLugaresDetalleTransformer extends TransformerAbstract
{
    public function transform(TarifasLugaresDetalle $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'     => $Data->tarlugcod,           
            'pais'       => $Data->paiscod,
            'paisnombre' => $Data->paisnom,                     
    	];
    }
}

?>