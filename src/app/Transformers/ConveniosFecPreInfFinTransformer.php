<?php

namespace App\Transformers;

// We need to reference the Model
use App\ConveniosFecPreInfFin;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ConveniosFecPreInfFinTransformer extends TransformerAbstract
{
    public function transform(ConveniosFecPreInfFin $Data)
    {
        // Specify what elements are going to be visible to the API
        return [            
            'codigo'            => $Data->convcod,         
            'fechapresentacion' => $Data->convfpif           
    	];
    }
}

?>