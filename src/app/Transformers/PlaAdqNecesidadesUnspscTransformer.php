<?php

namespace App\Transformers;

// We need to reference the Model
use App\PlaAdqNecesidadesUnspsc;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PlaAdqNecesidadesUnspscTransformer extends TransformerAbstract
{
    public function transform(PlaAdqNecesidadesUnspsc $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'planadquisicion'  => $Data->plaadqcod,            
            'necesidad'        => $Data->plaadqneccod,
            'unspsc'           => $Data->unspsccod,
            'unspsctxt'        => $Data->unspscdes,
            'tipomod'          => '' //para la temporal           
    	];
    }
}

?>