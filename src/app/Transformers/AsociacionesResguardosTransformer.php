<?php

namespace App\Transformers;

// We need to reference the Model
use App\AsociacionesResguardos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class AsociacionesResguardosTransformer extends TransformerAbstract
{
    public function transform(AsociacionesResguardos $data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigoasociacion' => $data->asocod,
            'nombreasociacion' => $data->asonom,
            'codigoresguardo'  => $data->resindcod,           
            'nombreresguardo'  => $data->resindnom           
    	];
    }
}

?>