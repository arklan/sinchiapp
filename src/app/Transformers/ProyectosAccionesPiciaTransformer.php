<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosAccionesPicia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosAccionesPiciaTransformer extends TransformerAbstract
{
    public function transform(ProyectosAccionesPicia $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $Data->accpiccod,            
            'proyecto'    => $Data->proycod
    	];
    }
}

?>