<?php

namespace App\Transformers;

// We need to reference the Model
use App\Cargos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class CargosTransformer extends TransformerAbstract
{
    public function transform(Cargos $Cargos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $Cargos->cargcod,           
            'nombre' => $Cargos->cargnom
    	];
    }
}

?>