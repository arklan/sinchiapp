<?php

namespace App\Transformers;

// We need to reference the Model
use App\TiposContratos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class TiposContratosTransformer extends TransformerAbstract
{
    public function transform(TiposContratos $TiposContratos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $TiposContratos->tipcontcod,           
            'descripcion'       => $TiposContratos->tipcontdes           
    	];
    }
}

?>