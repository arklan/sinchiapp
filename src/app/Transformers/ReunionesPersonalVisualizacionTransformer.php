<?php

namespace App\Transformers;

// We need to reference the Model
use App\ReunionesPersonalVisualizacion;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ReunionesPersonalVisualizacionTransformer extends TransformerAbstract
{
    public function transform(ReunionesPersonalVisualizacion $ReunionesPersonalVisualizacion)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                => $ReunionesPersonalVisualizacion->reupercodvis,           
            'Codigopersonal'        => $ReunionesPersonalVisualizacion->perscod,
            'codigoreunion'         => $ReunionesPersonalVisualizacion->reucod,
            'nombrecompletopersonal'       => $ReunionesPersonalVisualizacion->persnom . ' ' . $ReunionesPersonalVisualizacion->persape
    	];
    }
}

?>