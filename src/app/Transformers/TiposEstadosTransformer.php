<?php

namespace App\Transformers;

// We need to reference the Model
use App\TiposEstados;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class TiposEstadosTransformer extends TransformerAbstract
{
    public function transform(TiposEstados $TiposEstados)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $TiposEstados->tipestcod,            
            'descripcion' => $TiposEstados->tipestdes
    	];
    }
}

?>