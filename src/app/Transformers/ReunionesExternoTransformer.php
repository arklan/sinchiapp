<?php

namespace App\Transformers;

// We need to reference the Model
use App\ReunionesExterno;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ReunionesExternoTransformer extends TransformerAbstract
{
    public function transform(ReunionesExterno $ReunionesExterno)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                => $ReunionesExterno->reuextcod,           
            'nombre'        => $ReunionesExterno->reuextnom,
            'email'         => $ReunionesExterno->reuextema,
            'codigoreunion'       => $ReunionesExterno->reucod
    	];
    }
}

?>