<?php

namespace App\Transformers;

// We need to reference the Model
use App\Opciones;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class OpcionesTransformer extends TransformerAbstract
{
    public function transform(Opciones $Opciones)
    {
            // Specify what elements are going to be visible to the API
            return [
                'codigo' => $Opciones->opccod == null ? 0 : $Opciones->opccod,
                'nombre' => $Opciones->opcnom,
                'padre'  => $Opciones->opccodpad == null ? 0 : $Opciones->opccodpad,
                'icono'  => $Opciones->opcico,
                'link'   => $Opciones->opclin,                
        ];
    }
}