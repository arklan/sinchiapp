<?php

namespace App\Transformers;

// We need to reference the Model
use App\OrganismosVivos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class OrganismosVivosTransformer extends TransformerAbstract
{
    public function transform(OrganismosVivos $OrganismosVivos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $OrganismosVivos->orgvivcod,           
            'descripcion' => $OrganismosVivos->orgvivdes
            
        ];
    }
}

?>