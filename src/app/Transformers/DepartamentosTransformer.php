<?php

namespace App\Transformers;

// We need to reference the Model
use App\Departamentos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class DepartamentosTransformer extends TransformerAbstract
{
    public function transform(Departamentos $Departamentos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $Departamentos->depcod,           
            'descripcion' => $Departamentos->depnom
    	];
    }
}

?>