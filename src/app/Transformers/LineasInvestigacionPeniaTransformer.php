<?php

namespace App\Transformers;

// We need to reference the Model
use App\LineasInvestigacionPenia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class LineasInvestigacionPeniaTransformer extends TransformerAbstract
{
    public function transform(LineasInvestigacionPenia $LineasInvestigacion)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                 => $LineasInvestigacion->lininvpencod,           
            'descripcion'            => $LineasInvestigacion->lininvpendes,
            'codigoproinvpenia'      => $LineasInvestigacion->proinvpencod,
            'descripcionproinvpenia' => $LineasInvestigacion->proinvpendes
    	];
    }
}

?>