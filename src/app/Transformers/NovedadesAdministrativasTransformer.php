<?php

namespace App\Transformers;

// We need to reference the Model
use App\Novedades_Administrativas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class Novedades_AdministrativasTransformer extends TransformerAbstract
{
    public function transform(Novedades_Administrativas $Novedades_Administrativas)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $Novedades_Administrativasas->metcod,           
            'nombre' => $Novedades_Administrativas->metnom
            
    	];
    }
}

?>