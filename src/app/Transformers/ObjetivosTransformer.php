<?php

namespace App\Transformers;

// We need to reference the Model
use App\Objetivos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ObjetivosTransformer extends TransformerAbstract
{
    public function transform(Objetivos $Objetivos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'            => $Objetivos->objcod,            
            'descripcion'       => $Objetivos->objdesc,
            'codigolinest'      => $Objetivos->linestcod,
            'descripcionlinest' => $Objetivos->linestdesc
    	];
    }
}

?>