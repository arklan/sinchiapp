<?php

namespace App\Transformers;

// We need to reference the Model
use App\HisPlaAdqNecesidadesUnspsc;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class HisPlaAdqNecesidadesUnspscTransformer extends TransformerAbstract
{
    public function transform(HisPlaAdqNecesidadesUnspsc $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'planadquisicion'  => $Data->hisplaadqcod,            
            'necesidad'        => $Data->plaadqneccod,
            'unspsc'           => $Data->unspsccod,
            'unspsctxt'        => $Data->unspscdes
    	];
    }
}

?>