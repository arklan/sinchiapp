<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosPilaresPicia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosPilaresPiciaTransformer extends TransformerAbstract
{
    public function transform(ProyectosPilaresPicia $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'   => $Data->pilpiccod,            
            'proyecto' => $Data->proycod
    	];
    }
}

?>