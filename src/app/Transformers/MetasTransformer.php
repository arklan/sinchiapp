<?php

namespace App\Transformers;

// We need to reference the Model
use App\Metas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class MetasTransformer extends TransformerAbstract
{
    public function transform(Metas $Metas)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                 => $Metas->metcod,           
            'nombre'                 => $Metas->metnom,
            'codigolininvpenia'      => $Metas->lininvpencod,
            'descripcionlininvpenia' => $Metas->lininvpendes
    	];
    }
}

?>