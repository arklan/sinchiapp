<?php

namespace App\Transformers;

// We need to reference the Model
use App\Proveedores;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProveedoresTransformer extends TransformerAbstract
{
    public function transform(Proveedores $Proveedores)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $Proveedores->provcod,           
            'nombre' => $Proveedores->provnom,
            'nit' => $Proveedores->provnit,
            'codigociudad' => $Proveedores->ciucod,
            'direccion' => $Proveedores->provdir,
            'correo' => $Proveedores->provema
    	];
    }
}

?>