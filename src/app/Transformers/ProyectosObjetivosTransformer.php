<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosObjetivos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosObjetivosTransformer extends TransformerAbstract
{
    public function transform(ProyectosObjetivos $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Data->proyobjcod,
            'codigoreal'      => $Data->codigoreal,   
            'proyecto'        => $Data->proycod,         
            'descripcion'     => $Data->proyobjdes,
            'peso'            => $Data->proyobjpes
    	];
    }
}

?>