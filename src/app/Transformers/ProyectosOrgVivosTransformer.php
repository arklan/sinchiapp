<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosOrgVivos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosOrgVivosTransformer extends TransformerAbstract
{
    public function transform(ProyectosOrgVivos $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Data->orgvivcod,
            'proyecto'        => $Data->proycod,         
            'descripcion'     => $Data->orgvivdes
    	];
    }
}

?>