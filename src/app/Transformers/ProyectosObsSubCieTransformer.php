<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosObsSubCie;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosObsSubCieTransformer extends TransformerAbstract
{
    public function transform(ProyectosObsSubCie $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Data->proysdccod,
            'proyecto'        => $Data->proycod,         
            'observacion'     => $Data->proysdcdes
    	];
    }
}

?>