<?php

namespace App\Transformers;

// We need to reference the Model
use App\Monedas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class MonedasTransformer extends TransformerAbstract
{
    public function transform(Monedas $Monedas)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'       => $Monedas->moncod,            
            'descripcion'  => $Monedas->mondes,
            'simbolo'      => $Monedas->monsim,
            'tasa'         => $Monedas->montas             
    	];
    }
}

?>