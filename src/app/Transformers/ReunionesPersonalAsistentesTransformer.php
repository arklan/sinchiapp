<?php

namespace App\Transformers;

// We need to reference the Model
use App\ReunionesPersonalAsistentes;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ReunionesPersonalAsistentesTransformer extends TransformerAbstract
{
    public function transform(ReunionesPersonalAsistentes $ReunionesPersonalAsistentes)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                => $ReunionesPersonalAsistentes->reupercodasi,           
            'Codigopersonal'        => $ReunionesPersonalAsistentes->perscod,
            'codigoreunion'         => $ReunionesPersonalAsistentes->reucod,
            'asistio'               => $ReunionesPersonalAsistentes->reuperasi,
            'solicitudaprobacion'   => $ReunionesPersonalAsistentes->reupersolapro,
            'nombrecompletopersonal'       => $ReunionesPersonalAsistentes->persnom . ' ' . $ReunionesPersonalAsistentes->persape
    	];
    }
}

?>