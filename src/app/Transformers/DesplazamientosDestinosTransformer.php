<?php

namespace App\Transformers;

// We need to reference the Model
use App\DesplazamientosDestinos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class DesplazamientosDestinosTransformer extends TransformerAbstract
{
    public function transform(DesplazamientosDestinos $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'desplazamiento'       => $Data->descod,           
            'codigo'               => $Data->desdescod,
            'origennacional'       => $Data->desdesnacori,
            'origeninternacional'  => $Data->desdesintori,
            'destinonacional'      => $Data->desdesnacdes,
            'destinointernacional' => $Data->desdesintdes,
            'txtdestinoorigen'     => $Data->desdesnomdor,
            'txtdestinofinal'      => $Data->desdesnomdfi,
            'fechaida'             => $Data->desdesnacfid,
            'fecharegreso'         => $Data->desdesnacfre,
            'tipotransporte'       => $Data->desdesnactip,
            'txttipotransporte'    => $Data->desdesnactip == 'F' ? 'FLUVIAL' : 
                                        ($Data->desdesnactip == 'T' ? 'TERRRESTRE' : 'AÉREO'),
            'tarifa'               => $Data->desdestar,
            'moneda'               => $Data->moncod,
            'simbolomoneda'        => $Data->monsim,
            'tasa'                 => $Data->desdestas,
            'tarifapesos'          => $Data->desdestarpes,
            'totaltarifa'          => $Data->desdestottar,
            'diasviaje'            => $Data->desdesdvj,                   
    	];
    }
}

?>