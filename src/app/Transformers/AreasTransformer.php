<?php

namespace App\Transformers;

// We need to reference the Model
use App\Areas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class AreasTransformer extends TransformerAbstract
{
    public function transform(Areas $Areas)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Areas->arecod,           
            'nombre'          => $Areas->arenom,
            'encargadocodigo' => $Areas->areenccod,
            'encargadonombre' => $Areas->persnom.' '.$Areas->persape,
            'userid'          => $Areas->iduser
    	];
    }
}

?>