<?php

namespace App\Transformers;

// We need to reference the Model
use App\Estados;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class EstadosTransformer extends TransformerAbstract
{
    public function transform(Estados $Estados)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Estados->estcod,            
            'descripcion'     => $Estados->estdes,
            'tipocodigo'      => $Estados->tipestcod,            
            'tipodescripcion' => $Estados->tipestdes 
    	];
    }
}

?>