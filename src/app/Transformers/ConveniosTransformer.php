<?php

namespace App\Transformers;

// We need to reference the Model
use App\Convenios;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ConveniosTransformer extends TransformerAbstract
{
    public function transform(Convenios $Convenios)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'             => $Convenios->convcod, 
            'nombre'             => $Convenios->convnom,           
            'numero'             => $Convenios->convnum,
            'fechasuscripcion'   => $Convenios->convfecsus,
            'fechaterejecucion'  => $Convenios->convfectereje,
            'fechaincorporacion' => $Convenios->convfecinc,
            'tiempo'             => $Convenios->convtieeje,
            'codigomoneda'       => $Convenios->moncod,
            'moneda'             => $Convenios->monsim,
            'tasa'               => $Convenios->convtas, 
            'cofinanciado'       => $Convenios->convvalcof,
            'contrapartida'      => $Convenios->convvalctp,
            'totaldinero'        => $Convenios->convvaldin,
            'totalespecie'       => $Convenios->convtotesp,
            'valortotal'         => $Convenios->convvaltot,
            'codigoestado'       => $Convenios->estcod,
            'descripcionestado'  => $Convenios->estdes,
            'codigocoordinador'  => $Convenios->perscod,
            'nombrecoordinador'  => $Convenios->persnom . ' ' . $Convenios->persape,
            'codigoencargado'    => $Convenios->convenc,
            'nombreencaragado'   => $Convenios->convencnom . ' ' . $Convenios->convencape,
            'objeto'             => $Convenios->convobj,
            'fuentefinanciacion' => $Convenios->convfuefin,
            'codigocontactoafn'  => $Convenios->convcaf,
            'contactoafn'        => $Convenios->perscaf,
            'codigocontactoaff'  => $Convenios->convaff,
            'contactoaff'        => $Convenios->persaff,
            'codigocontactotff'  => $Convenios->convtff,
            'contactotff'        => $Convenios->persttf,
            'archivogeneral'     => $Convenios->convarcgen,
            'archivootda'        => $Convenios->convarcoda,
            'archivocont'        => $Convenios->convarccon,
            'contactotecfuefin'  => $Convenios->convtecfuefin,
            'contactoadmfuefin'  => $Convenios->convcontaff,
            'valorpesos'         => $Convenios->convtas != 0 ? $Convenios->convtas * $Convenios->convvaltot : $Convenios->convvaltot,           
            
            'rutasarchivogen'   => url('/convenios/C'.$Convenios->convcod.'/'.$Convenios->convarcgen),
            'rutasarchivotda'   => url('/convenios/C'.$Convenios->convcod.'/'.$Convenios->convarcoda),
            'rutasarchivocon'   => url('/convenios/C'.$Convenios->convcod.'/'.$Convenios->convarccon),

    	];
    }
}

?>