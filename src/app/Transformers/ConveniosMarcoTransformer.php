<?php

namespace App\Transformers;

// We need to reference the Model
use App\ConveniosMarco;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ConveniosMarcoTransformer extends TransformerAbstract
{
    public function transform(ConveniosMarco $ConveniosMarco)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'             => $ConveniosMarco->conmarcod,           
            'nombreasociado'     => $ConveniosMarco->conmarnomaso,
            'fechasuscripcion'   => $ConveniosMarco->conmarfecsus,
            'fechaterminacion'   => $ConveniosMarco->conmarfecter,
            'fechainicio'        => $ConveniosMarco->conmarfecini,            
            'codigoestado'       => $ConveniosMarco->estcod,
            'descripcionestado'  => $ConveniosMarco->estdes,
            'objeto'             => $ConveniosMarco->conmarobj,
            'observacion'        => $ConveniosMarco->conmarobs,
            'archivogeneral'     => $ConveniosMarco->conmararccon,
            'archivootda'        => $ConveniosMarco->conmararcoda
    	];
    }
}

?>