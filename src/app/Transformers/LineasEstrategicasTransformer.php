<?php

namespace App\Transformers;

// We need to reference the Model
use App\LineasEstrategicas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class LineasEstrategicasTransformer extends TransformerAbstract
{
    public function transform(LineasEstrategicas $LineasEstrategicas)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'              => $LineasEstrategicas->linestcod,           
            'descripcion'         => $LineasEstrategicas->linestdesc,
            'codigocapitulo'      => $LineasEstrategicas->capcod,
            'descripcioncapitulo' => $LineasEstrategicas->capdes            
    	];
    }
}

?>