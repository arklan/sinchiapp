<?php

namespace App\Transformers;

// We need to reference the Model
use App\ReunionesOrden;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ReunionesOrdenTransformer extends TransformerAbstract
{
    public function transform(ReunionesOrden $ReunionesOrden)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                => $ReunionesOrden->reuordcod,           
            'numorden'              => $ReunionesOrden->reuordnum,
            'tematica'              => $ReunionesOrden->reuordtem,
            'codigopersonal'        => $ReunionesOrden->perscod,
            'observaciones'         => $ReunionesOrden->reuordobs,
            'codigoreunion'         => $ReunionesOrden->reucod,
            'responsable'     	    => $ReunionesOrden->persnom . ' ' . $ReunionesOrden->persape,
            
               
    	];
    }
}

?>