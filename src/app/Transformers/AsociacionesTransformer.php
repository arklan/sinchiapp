<?php

namespace App\Transformers;

// We need to reference the Model
use App\Asociaciones;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class AsociacionesTransformer extends TransformerAbstract
{
    public function transform(Asociaciones $data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'           => $data->asocod,           
            'nombre'           => $data->asonom
    	];
    }
}

?>