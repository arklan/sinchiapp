<?php

namespace App\Transformers;

// We need to reference the Model
use App\ReunionesActa;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ReunionesActaTransformer extends TransformerAbstract
{
    public function transform(ReunionesActa $ReunionesActa)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'        => $ReunionesActa->reuactcod,           
            'fecha'         => $ReunionesActa->reuactfec,
            'pdf'           => $ReunionesActa->reuactpdf,
            'estado'        => $ReunionesActa->reuactest,
            'codigoreunion' => $ReunionesActa->reucod,
            'estadodescripcion'  => $ReunionesActa->reuactestdes,                           
    	];
    }
}

?>