<?php

namespace App\Transformers;

// We need to refereance the Model
use App\Solicitudes_Actividades;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class Solicitudes_ActividadesTransformer extends TransformerAbstract
{
    public function transform(Solicitudes_Actividades $Solicitudes_Actividades)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $Solicitudes_Actividades->solactcod,           
            'codigosolicitud' => $Solicitudes_Actividades->solcod,
            'actividades' => $Solicitudes_Actividades->solactdes
    	];
    }
}

?>