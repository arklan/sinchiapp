<?php

namespace App\Transformers;

// We need to reference the Model
use App\Proyectos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosTransformer extends TransformerAbstract
{
    public function transform(Proyectos $Proyectos)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                  => $Proyectos->proycod,
            'descripcion'             => $Proyectos->proydesc,
            'nombre'                  => $Proyectos->proynomcor,
            'codigoconvenio'          => $Proyectos->convcod,  
            'convenio'                => $Proyectos->convnum,
            'fechasuscripcion'        => $Proyectos->convfecsus,
            'fechaterejecucion'       => $Proyectos->convfectereje,
            'fechaincorporacion'      => $Proyectos->convfecinc,
            'antecedentes'            => $Proyectos->proyant,
            'valortotal'              => $Proyectos->convvaltot,
            'codigoestado'            => $Proyectos->estcod,
            'descripcionestado'       => $Proyectos->estdes,
            'codigocoordinador'       => $Proyectos->perscod,
            'nombrecoordinador'       => $Proyectos->persnom . ' ' . $Proyectos->persape,

            'codigoencargado'         => $Proyectos->convenc,
            //'nombreencaragado'        => $Proyectos->convencnom . ' ' . $Proyectos->convencape,

            'codigopoa'               => $Proyectos->poacod,
            'descripcionpoa'          => $Proyectos->poades,
            'objeto'                  => $Proyectos->proyobj,
            //'fuentefinanciacion'      => $Proyectos->proyfuefin,
            //'cofinanciado'            => $Proyectos->proyvalcof,
            //'contrapartida'           => $Proyectos->proyvalctp,
            //'totaldinero'             => $Proyectos->proyvaldin,
            //'totalespecie'            => $Proyectos->proytotesp,
            'justificaimportancia'    => $Proyectos->proyjusimp,
            'justificapertenencia'    => $Proyectos->proyjusper,
            'justificaimpacto'        => $Proyectos->proyjusipa,            
            'objetivogeneral'         => $Proyectos->proyobjgen,            
            //'tiempo'                  => $Proyectos->proytieeje,
            'actores'                 => $Proyectos->proyact,
            'beneficiarios'           => $Proyectos->proyben,           
            'codigomoneda'            => $Proyectos->moncod,
            'moneda'                  => $Proyectos->monsim,
            'tasa'                    => $Proyectos->convtas, 
            'tipo'                    => $Proyectos->proytip,
            
            'estrategia'              => $Proyectos->poaestr,            
            'indicador'               => $Proyectos->poaind,
            'unidadmedida'            => $Proyectos->poaunimed,            
            'metasectorial'           => $Proyectos->poametsec,
            'metainstitucional'       => $Proyectos->poametins,
            'observacion'             => $Proyectos->poaobs,
            'capitulocodigo'          => $Proyectos->capcod,
            'capitulodescripcion'     => $Proyectos->capdes,
            'lineacodigo'             => $Proyectos->linestcod,
            'lineadescripcion'        => $Proyectos->linestdesc,
            'objetivocodigo'          => $Proyectos->objcod,
            'objetivodescripcion'     => $Proyectos->objdesc,
            'odscodigo'               => $Proyectos->odscod,
            'odsdescripcion'          => $Proyectos->odsdesc,
            'metacodigo'              => $Proyectos->metcod,
            'metadescripcion'         => $Proyectos->metnom,
            'proginvpeniacodigo'      => $Proyectos->proinvpencod,
            'proginvpeniadescripcion' => $Proyectos->proinvpendes,
            'lininvpeniacodigo'       => $Proyectos->lininvpencod,
            'lininvpeniadescripcion'  => $Proyectos->lininvpendes,
            'conveniomarco'           => $Proyectos->conmarcod,
            'vbplaneacion'            => $Proyectos->proyvbpla,
            'vbdireccion'             => $Proyectos->proyvbdir,
            'nombreconvenio'          => $Proyectos->convnom,
            'valorpesos'              => $Proyectos->convtas != 0 ? $Proyectos->convtas * $Proyectos->convvaltot : $Proyectos->convvaltot,
            'version'                 => $Proyectos->proyver,
            'voboactualizacion'       => $Proyectos->proyapract,

           
    	];
    }
}

?>