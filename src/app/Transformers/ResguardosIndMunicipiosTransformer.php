<?php

namespace App\Transformers;

// We need to reference the Model
use App\ResguardosIndMunicipios;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ResguardosIndMunicipiosTransformer extends TransformerAbstract
{
    public function transform(ResguardosIndMunicipios $data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigoresguardo' => $data->resindcod,           
            'nombreresguardo' => $data->resindnom,
            'codigociudad'    => $data->ciucod,
            'nombreciudad'    => $data->ciunom,
            'ciudadcompleto'  => $data->ciunom . ' - ' . $data->depnom            
    	];
    }
}

?>