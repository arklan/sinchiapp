<?php

namespace App\Transformers;

// We need to reference the Model
use App\Reuniones;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ReunionesTransformer extends TransformerAbstract
{
    public function transform(Reuniones $Reuniones)
    {
        // Specify what elements are going to be visible to the API

        
        return [
            'codigo'                => $Reuniones->reucod,           
            'titulo'	            => $Reuniones->reutit,
            'codigoproyecto'        => $Reuniones->proycod,
            'codigoplanaquisicion'  => $Reuniones->plaadqcod,
            'codigoplanecesidades'  => $Reuniones->plaadqneccod,
            'codigosolicitud'		=> $Reuniones->solcod,
            'fecha'     			=> $Reuniones->reufec,
            'horainicial'			=> $Reuniones->reuhorini,
            'horafinal'			    => $Reuniones->reuhorfin,
            'lugar'					=> $Reuniones->reulug,
            'proceso'               => $Reuniones->proceso,
            'SubProceso'            => $Reuniones->SubProceso,
            'linkreunion'           => $Reuniones->reuenl,
            'respcitar'             => $Reuniones->perscodcit,
            'respacta'              => $Reuniones->perscodact,
            'sede'                  => $Reuniones->sedcod,
            'sedenombre'            => $Reuniones->sednom,
            'citador'     	        => $Reuniones->nomcitador . ' ' . $Reuniones->apecitador,
            'elaboracta'     	    => $Reuniones->nomacta . ' ' . $Reuniones->apeacta,
            'resumen'     	        => $Reuniones->reures,
            'codigoacta'   	        => $Reuniones->codacta,
            'estadoacta'   	        => $Reuniones->estadoacta,
            'adjunto'   	        => $Reuniones->reuadj
    	];
    }
}

?>