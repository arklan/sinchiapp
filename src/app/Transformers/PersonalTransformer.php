<?php

namespace App\Transformers;

// We need to reference the Model
use App\Personal;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PersonalTransformer extends TransformerAbstract
{
    public function transform(Personal $Personal)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Personal->perscod,            
            'tipoid'          => $Personal->tipidecod,
            'nombretipoid'    => $Personal->tipidedes, 
            'identificacion'  => $Personal->perside,
            'apellido'        => $Personal->persape,
            'nombre'          => $Personal->persnom,
            'nombrecompleto'  => $Personal->persnom . ' ' . $Personal->persape,
            'codigocargo'     => $Personal->cargcod,
            'nombrecargo'     => $Personal->cargnom,
            'codigoarea'      => $Personal->arecod,
            'nombrearea'      => $Personal->arenom,
            'direccion'       => $Personal->persdir,
            'telefono'        => $Personal->perstel,
            'codigociudad'    => $Personal->ciucod,
            'nombreciudad'    => $Personal->ciunom . ' - ' .$Personal->depnom,    
            'correo'          => $Personal->persema,
            'codigoestado'    => $Personal->persest,
            'nombreestado'        => $Personal->estdes,
            'codigotipocont'      => $Personal->tipcontcod,
            'descripciontipocont' => $Personal->tipcontdes,
            'horasemanales'       => $Personal->pershorsem,
            'sexo'                => $Personal->perssex,
            'fechanacimiento'     => $Personal->persfecnac,
            'salariobasico'       => $Personal->perssalbas,             
    	];
    }
}

?>