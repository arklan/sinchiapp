<?php

namespace App\Transformers;

// We need to reference the Model
use App\PlanesAdq_Solicitud;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PlanesAdqSolicitudTransformer extends TransformerAbstract
{
    public function transform(PlanesAdq_Solicitud $PlanesAdq_Solicitud)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $PlanesAdq_Solicitud->plaadqcod,            
            'codigosolicitud'     => $PlanesAdq_Solicitud->solccod
            
             
    	];
    }
}

?>