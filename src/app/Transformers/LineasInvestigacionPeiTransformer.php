<?php

namespace App\Transformers;

// We need to reference the Model
use App\LineasInvestigacionPei;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class LineasInvestigacionPeiTransformer extends TransformerAbstract
{
    public function transform(LineasInvestigacionPei $LineasInvestigacion)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $LineasInvestigacion->lininvpeicod,           
            'descripcion' => $LineasInvestigacion->lininvpeides,
            'codigoproginv' => $LineasInvestigacion->proinvpeicod
    	];
    }
}

?>