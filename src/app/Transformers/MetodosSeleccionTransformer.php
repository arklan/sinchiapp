<?php

namespace App\Transformers;

// We need to reference the Model
use App\MetodosSeleccion;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class MetodosSeleccionTransformer extends TransformerAbstract
{
    public function transform(MetodosSeleccion $Metodos_Seleccion)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Metodos_Seleccion->metselcod,            
            'descripcion'     => $Metodos_Seleccion->metseldes             
    	];
    }
}

?>