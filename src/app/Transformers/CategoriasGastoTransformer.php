<?php

namespace App\Transformers;

// We need to reference the Model
use App\CategoriasGasto;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class CategoriasGastoTransformer extends TransformerAbstract
{
    public function transform(CategoriasGasto $CategoriasGasto)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $CategoriasGasto->catgascod,           
            'nombre' => $CategoriasGasto->catgasnom
    	];
    }
}

?>