<?php

namespace App\Transformers;

// We need to reference the Model
use App\FormasPago;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class FormasPagoTransformer extends TransformerAbstract
{
    public function transform(FormasPago $FormasPago)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $FormasPago->forpagcod,           
            'descripcion' => $FormasPago->forpagdes
            
    	];
    }
}

?>