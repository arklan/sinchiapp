<?php

namespace App\Transformers;

// We need to reference the Model
use App\Users;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class UsersTransformer extends TransformerAbstract
{
    public function transform(Users $Users)
    {
        // Specify what elements are going to be visible to the API
        return [
            'id'      => $Users->iduser,           
            'nombre'       => $Users->nombre,
            'usuario'       => $Users->usuario,
            'contrasña'       => $Users->password,
            'correo'       => $Users->email,
            'sexo'       => $Users->sexo,
            'estado'       => $Users->estado,
            'codigopersonal'       => $Users->perfcod
    	];
    }
}

?>