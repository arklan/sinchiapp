<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosActividades;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosActividadesTransformer extends TransformerAbstract
{
    public function transform(ProyectosActividades $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'         => $Data->proyactcod,   
            'proyecto'       => $Data->proycod,         
            'descripcion'    => $Data->proyactdes,
            'fechainicio'    => $Data->proyactfecini,
            'fechafin'       => $Data->proyactfecfin,
            'objetivo'       => $Data->proyobjcod,
            'producto'       => $Data->prodcod,
            'peso'           => $Data->proyactpes,
            'kpi'            => $Data->proyactkpi,
            'descripcionkpi' => $Data->proyactdeskpi,
            'secuencia'      => $Data->proyactsec,
            'secuenciareal'  => $Data->proyactsec,
            'tipo'           => $Data->proyacttip,
            'valor'          => $Data->proyactval,
            'txtvalor'       => '$ '. number_format($Data->proyactval,2,',','.')          
            
    	];
    }
}

?>