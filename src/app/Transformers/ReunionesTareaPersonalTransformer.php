<?php

namespace App\Transformers;

// We need to reference the Model
use App\ReunionesTareaPersonal;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ReunionesTareaPersonalTransformer extends TransformerAbstract
{
    public function transform(Reuniones $ReunionesTareaPersonal)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                      => $ReunionesTareaPersonal->reutarperscod,           
            'codigoreuniontarea'          => $ReunionesTareaPersonal->reutarcod,
            'codigopersonal'              => $ReunionesTareaPersonal->perscod,           
               
    	];
    }
}

?>