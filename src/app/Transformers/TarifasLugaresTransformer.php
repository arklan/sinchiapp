<?php

namespace App\Transformers;

// We need to reference the Model
use App\TarifasLugares;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class TarifasLugaresTransformer extends TransformerAbstract
{
    public function transform(TarifasLugares $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'        => $Data->tarlugcod,           
            'moneda'        => $Data->moncod,
            'simbolomoneda' => $Data->monsim,
            'tasa'          => $Data->montas,            
            'descripcion'   => $Data->tarlugdes,           
    	];
    }
}

?>