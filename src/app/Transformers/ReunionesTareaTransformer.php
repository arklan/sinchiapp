<?php

namespace App\Transformers;

// We need to reference the Model
use App\ReunionesTarea;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ReunionesTareaTransformer extends TransformerAbstract
{
    public function transform(ReunionesTarea $ReunionesTarea)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'                => $ReunionesTarea->reutarcod,           
            'tarea'	                => $ReunionesTarea->reutar,
            'recursos'              => $ReunionesTarea->reutarrecapr,
            'fechalimite'           => $ReunionesTarea->reutarfeclim,
            'tipo'                  => $ReunionesTarea->reutartip,
            'estado'     			=> $ReunionesTarea->reutarest,
            'codigoreunion'     	=> $ReunionesTarea->reucod,
            'codigopersonal'     	=> $ReunionesTarea->perscod,
            'Responsable'     	    => $ReunionesTarea->persnom . ' ' . $ReunionesTarea->persape,
               
    	];
    }
}

?>