<?php

namespace App\Transformers;

// We need to reference the Model
use App\PresupuestosCatGas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PresupuestosCatGasTransformer extends TransformerAbstract
{
    public function transform(PresupuestosCatGas $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'             => $Data->prescatgascod,
            'nombre'             => $Data->prescatgasdes,
            'presupuesto'        => $Data->prescod, 
            'topemaximo'         => $Data->prescatgastma,
            'tipotope'           => $Data->prestiptopcatgas,
    	];
    }
}

?>