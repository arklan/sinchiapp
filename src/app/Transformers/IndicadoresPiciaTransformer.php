<?php

namespace App\Transformers;

// We need to reference the Model
use App\IndicadoresPicia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class IndicadoresPiciaTransformer extends TransformerAbstract
{
    public function transform(IndicadoresPicia $data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $data->indpiccod,           
            'descripcion' => $data->indpicdes
    	];
    }
}

?>