<?php

namespace App\Transformers;

// We need to reference the Model
use App\PilaresPicia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PilaresPiciaTransformer extends TransformerAbstract
{
    public function transform(PilaresPicia $PilaresPicia)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $PilaresPicia->pilpiccod,           
            'descripcion' => $PilaresPicia->pilpicdes
    	];
    }
}

?> 	