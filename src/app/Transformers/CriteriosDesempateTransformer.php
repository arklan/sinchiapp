<?php

namespace App\Transformers;

// We need to reference the Model
use App\CriteriosDesempate;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class CriteriosDesempateTransformer extends TransformerAbstract
{
    public function transform(CriteriosDesempate $CriteriosDesempate)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $CriteriosDesempate->cridescod,           
            'descripcion' => $CriteriosDesempate->cridesdes
    	];
    }
}

?>