<?php

namespace App\Transformers;

// We need to refereance the Model
use App\Solicitudes_Bienes;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class Solicitudes_BienesTransformer extends TransformerAbstract
{
    public function transform(Solicitudes_Bienes $Solicitudes_Bienes)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $Solicitudes_Bienes->solcod,           
            'item' => $Solicitudes_Bienes->solbieite,
            'descripcion' => $Solicitudes_Bienes->solbiedes,
            'can' => $Solicitudes_Bienes->solbiecan
    	];
    }
}

?>