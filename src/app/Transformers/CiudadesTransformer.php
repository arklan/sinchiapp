<?php

namespace App\Transformers;

// We need to reference the Model
use App\Ciudades;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class CiudadesTransformer extends TransformerAbstract
{
    public function transform(Ciudades $Ciudades)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'         => $Ciudades->ciucod,           
            'descripcion'    => $Ciudades->ciunom,
            'nombrecompleto' => $Ciudades->ciunom . ' - ' . $Ciudades->depnom,
            'codigodep'      => $Ciudades->depcod,
            'nombredep'      => $Ciudades->depnom,
            'seleccionar'    => $Ciudades->Seleccionar
    	];
    }
}

?>