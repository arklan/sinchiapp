<?php

namespace App\Transformers;

// We need to reference the Model
use App\Presupuestos;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PresupuestosTransformer extends TransformerAbstract
{
    public function transform(Presupuestos $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'             => $Data->prescod,
            'convenio'           => $Data->convcod, 
            'nombre'             => $Data->convnom,           
            'numero'             => $Data->convnum,
            'moneda'             => $Data->monsim,
            'fecha'              => $Data->presfeccre,
            'fuefinanciacionpgn' => $Data->presfuefinpgn,
            'fuefinanciacionsgr' => $Data->presfuefinsgr,
            'fuefinanciacioncof' => $Data->presfuefincof,
            'fuefinanciacionfun' => $Data->presfuefinfun,
            'topeactividades'    => $Data->prestopact,
            'tipotopecatgasto'   => $Data->prestiptopcatgas,

            'txtfuefinpgn'       => $Data->txtpresfuefinpgn,
            'txtfuefinsgr'       => $Data->txtpresfuefinsgr,
            'txtfuefincof'       => $Data->txtpresfuefincof,
            'txtfuefinfun'       => $Data->txtpresfuefinfun,
            'txttopeactividad'   => $Data->txtprestopact,
            'txttiptopcatgas'    => $Data->txtprestiptopcatgas,

            'valortotal'         => $Data->convvaltot,
            'coordinador'        => $Data->perscod
    	];
    }
}

?>