<?php

namespace App\Transformers;

// We need to reference the Model
use App\Contratistas;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ContratistasTransformer extends TransformerAbstract
{
    public function transform(Contratistas $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'          => $Data->contacod,            
            'tipoid'          => $Data->tipidecod,
            'nombretipoid'    => $Data->tipidedes, 
            'identificacion'  => $Data->contide,
            'apellido'        => $Data->contape,
            'nombre'          => $Data->contnom,
            'nombrecompleto'  => $Data->contnom . ' ' . $Data->contape,            
            'fechanacimiento' => $Data->contfecnac,
            'valorcontrato'   => $Data->contval,
            'mesescontrato'   => $Data->contmes,
            'salariobasico'   => $Data->contmes > 0 ? ($Data->contval / $Data->contmes) : 0,
            'direccion'       => $Data->contdir,
            'telefono'        => $Data->conttel,            
            'correo'          => $Data->contema,
            'codigocargo'     => 9999,
            'nombrecargo'     => 'NA',

    	];
    }
}

?>