<?php

namespace App\Transformers;

// We need to refereance the Model
use App\Terminos_Referncia;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class Terminos_RefernciaTransformer extends TransformerAbstract
{
    public function transform(Terminos_Referncia $Terminos_Referncia)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $Terminos_Referncia->terrefcod,           
            'fecha' => $Terminos_Referncia->terreffec,
            'codigosolicitud' => $Terminos_Referncia->solcod,
            'codigoestado' => $Terminos_Referncia->estcod,
            'jth' => $Terminos_Referncia->terrefest,
            'codigocategoria' => $Terminos_Referncia->catcod           
    	];
    }
}

?>