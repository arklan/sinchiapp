<?php

namespace App\Transformers;

// We need to reference the Model
use App\CriteriosEvaluacion;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class CriteriosEvaluacionTransformer extends TransformerAbstract
{
    public function transform(CriteriosEvaluacion $CriteriosEvaluacion)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo'      => $CriteriosEvaluacion->crievacod,           
            'descripcion'       => $CriteriosEvaluacion->crievades
            
    	];
    }
}

?>