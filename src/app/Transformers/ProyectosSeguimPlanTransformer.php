<?php

namespace App\Transformers;

// We need to reference the Model
use App\ProyectosSeguimPlan;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class ProyectosSeguimPlanTransformer extends TransformerAbstract
{
    public function transform(ProyectosSeguimPlan $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'proyecto'        => $Data->proycod,
            'codigoobjetivo'  => $Data->proyobjcod,
            'txtobjetivo'     => $Data->proyobjdes,
            'codigoactividad' => $Data->proyactcod,
            'txtactividad'    => $Data->proyactdes,
            'anno'            => $Data->proyspaano,
            'mes1'            => $Data->proyspames1,
            'mes2'            => $Data->proyspames2,
            'mes3'            => $Data->proyspames3,
            'mes4'            => $Data->proyspames4,
            'mes5'            => $Data->proyspames5,
            'mes6'            => $Data->proyspames6,
            'mes7'            => $Data->proyspames7,
            'mes8'            => $Data->proyspames8,
            'mes9'            => $Data->proyspames9,
            'mes10'           => $Data->proyspames10,
            'mes11'           => $Data->proyspames11,
            'mes12'           => $Data->proyspames12,
            'mesk1'           => $Data->proyspakmes1,
            'mesk2'           => $Data->proyspakmes2,
            'mesk3'           => $Data->proyspakmes3,
            'mesk4'           => $Data->proyspakmes4,
            'mesk5'           => $Data->proyspakmes5,
            'mesk6'           => $Data->proyspakmes6,
            'mesk7'           => $Data->proyspakmes7,
            'mesk8'           => $Data->proyspakmes8,
            'mesk9'           => $Data->proyspakmes9,
            'mesk10'          => $Data->proyspakmes10,
            'mesk11'          => $Data->proyspakmes11,
            'mesk12'          => $Data->proyspakmes12,
            'secuencia'       => $Data->proyactsec
    	];
    }
}

?>