<?php

namespace App\Transformers;

// We need to refereance the Model
use App\Solicitudes_Proponentes;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class Solicitudes_ProponentesTransformer extends TransformerAbstract
{
    public function transform(Solicitudes_Proponentes $Solicitudes_Proponentes)
    {
        // Specify what elements are going to be visible to the API
        return [
            'codigo' => $Solicitudes_Proponentes->solcod,           
            'codigoproveedor' => $Solicitudes_Proponentes->provcod
            
    	];
    }
}

?>