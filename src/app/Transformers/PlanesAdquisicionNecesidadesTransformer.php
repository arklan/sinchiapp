<?php

namespace App\Transformers;

// We need to reference the Model
use App\PlanesAdquisicionNecesidades;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class PlanesAdquisicionNecesidadesTransformer extends TransformerAbstract
{
    public function transform(PlanesAdquisicionNecesidades $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'planadquisicion'  => $Data->plaadqcod,            
            'codigo'           => $Data->plaadqneccod,
            'descripcion'      => $Data->plaadqnecdes,
            'valor'            => $Data->plaadqnecval,
            'metodoleccion'    => $Data->metselcod,
            'metodolecciontxt' => $Data->metseldes,
            'fechasolcontrata' => $Data->plaadqnecfeccont,
            'estadocodigo'     => $Data->estcod,
            'estadotxt'        => $Data->estdes,
            'vobofinanciador'  => $Data->plaadqnecvbf,
            'obsfinanciador'   => $Data->plaadqnecobf,
            'vobojuridica'     => $Data->plaadqnecvbj,
            'obsjuridica'      => $Data->plaadqnecobj,
            'voboplaneacion'   => $Data->plaadqnecvbp,
            'obsplaneacion'    => $Data->plaadqnecobp,
            'enviovobo'        => $Data->plaadqevb,
            'modificada'       => 'N',
            'tipomod'          => '' //para la temporal            
    	];
    }
}

?>