<?php

namespace App\Transformers;

// We need to reference the Model
use App\DesplazamientosRubros;

// Dingo includes Fractal to help with transformations
use League\Fractal\TransformerAbstract;

class DesplazamientosRubrosTransformer extends TransformerAbstract
{
    public function transform(DesplazamientosRubros $Data)
    {
        // Specify what elements are going to be visible to the API
        return [
            'desplazamiento'      => $Data->descod,           
            'codigo'              => $Data->desrubcod,
            'plan'                => $Data->plaadqcod,
            'necesidad'           => $Data->plaadqneccod,
            'actividad'           => $Data->proyactsec,
            'presupuesto'         => $Data->prescod,
            'rubro'               => $Data->plaadqnecactrpr,
            'valor'               => $Data->plaadqnecactval,            
            'comprometido'        => $Data->desrubcom,
            'saldo'               => $Data->desrubsal,
            'valordistribuido'    => $Data->desrubvaldis,        
    	];
    }
}

?>