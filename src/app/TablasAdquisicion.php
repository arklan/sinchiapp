<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tablas_adquisicion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tablas_adquisicion';
    protected $fillable = ['plaadqcod'];
    protected $primaryKey = 'plaadqcod';
    public $timestamps = false;
}