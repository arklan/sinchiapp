<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaAdqNecesidadesUnspsc extends Model
{
    protected $connection = 'mysql';
    protected $table = 'planesadquisiciones_necesidades_unspsc';
    protected $fillable = ['plaadqcod', 'plaadqneccod', 'unspsccod'];
    protected $primaryKey = 'unspsccod';
    public $timestamps = false;
}