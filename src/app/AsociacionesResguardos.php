<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsociacionesResguardos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'asociaciones_resguardos';
    protected $fillable = ['asocod' ,'resindcod'];
    protected $primaryKey = 'asocod';
    public $timestamps = false;
}