<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionesOrden extends Model
{
    protected $connection = 'mysql';
    protected $table = 'reuniones_orden';
    protected $fillable = ['reuordcod', 'reuordnum', 'reuordtem', 'perscod', 'reuordobs', 'reucod'];
    protected $primaryKey = 'reuordcod';
    public $timestamps = false;
}