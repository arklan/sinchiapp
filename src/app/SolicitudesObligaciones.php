<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesObligaciones extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solproc_obligaciones';
    protected $fillable = ['id','id_solproc', 'descripcion'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
