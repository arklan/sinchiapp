<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpPlanesAdquisicionNec extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tmpplanesadquisiciones_necesidades';
    protected $fillable = ['tmpplaadqcod', 'plaadqneccod', 'plaadqnecdes','plaadqnecval','metselcod','plaadqnecfeccont','plaadqnectmo'];
    protected $primaryKey = 'tmpplaadqcod';
    public $timestamps = false;
}