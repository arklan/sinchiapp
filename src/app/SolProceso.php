<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolProceso extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_procesos';
    protected $fillable = ['id', 'id_solcontratacion', 'obj_contractual', 'fecha_limite', 'tiempo_asignado', 'fecha_terminacion', 'inc_gastos',
        'tar_sinchi', 'val_gastos_viaje', 'tipo_contratacion', 'tipo_proc', 'nombre', 'correo', 'rol', 'telefono', 'causal', 'justificacion',
        'caracteristicas_bs', 'acces_info_conf', 'of_economica', 'porc_of_eco', 'peso_prop', 'peso_equ', 'peso_fin', 'peso_org', 'peso_eco', 'peso.met',
        'antisipo', 'contrapartida', 'conv_valor', 'conv_especie', 'conv_dinero', 'convenio_marco', 'val_arl', 'fecha_creacion'
    ];
    // protected $fillable = ['id', 'objetocontractual', 'tiempo', 'fecha_limite', 'incluye_gastos', 'tarifa_sinchi', 'vlrtarifa_diaria', 'tipo_solicitud', 'anticipo', 'porcentaje_anticipo', 'usuario', 'abAsignado', 'fecha_creacion', 'id_estado', 'tipo_contrato'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
