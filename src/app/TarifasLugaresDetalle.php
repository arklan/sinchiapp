<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TarifasLugaresDetalle extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tarifas_lugares_detalle';
    protected $fillable = ['tarlugcod','paiscod'];
    protected $primaryKey = 'paiscod';
    public $timestamps = false;
}