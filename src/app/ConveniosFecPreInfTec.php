<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ConveniosFecPreInfTec extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'convenios_fecpresinftec';
	    protected $fillable = ['convcod', 'convfpit'];
	    protected $primaryKey = 'convfpit';
	    public $timestamps = false;	
	}

?>