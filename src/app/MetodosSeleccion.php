<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetodosSeleccion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'metodos_seleccion';
    protected $fillable = ['metselcod', 'metseldes'];
    protected $primaryKey = 'metselcod';
    public $timestamps = false;
}