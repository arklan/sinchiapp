<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class Poas extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'poas';
		protected $fillable = ['poacod','poades', 'poaestr', 'poaind','poaunimed','poametsec','poametins','poaobs',
								'capcod','linestcod','objcod','odscod','metcod','proinvpencod','lininvpencod','aretemcod',
								'proinvpeicod','lininvpeicod'];
	    protected $primaryKey = 'poacod';
	    public $timestamps = false;	
	}

?>