<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ProyectosIndicadoresPicia extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'proyectos_indicadores_picia';
	    protected $fillable = ['proycod', 'indpiccod'];
	    protected $primaryKey = 'indpiccod';
	    public $timestamps = false;	
	}

?>