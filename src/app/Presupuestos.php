<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presupuestos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'presupuestos';
    protected $fillable = ['prescod','convcod', 'presfeccre','presfuefinpgn', '	presfuefinsgr','presfuefincof',
                            'presfuefinfun','prestopact','prestiptopcatgas'];
    protected $primaryKey = 'prescod';
    public $timestamps = false;
}