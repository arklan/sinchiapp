<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitudes_proponentes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_proponentes';
    protected $fillable = ['solcod'];
    protected $primaryKey = 'solcod';
    public $timestamps = false;
}