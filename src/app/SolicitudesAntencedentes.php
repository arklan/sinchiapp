<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitudes_antecedentes extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_antecedentes';
    protected $fillable = ['solantcod', 'solcod'];
    protected $primaryKey = 'solantcod';
    public $timestamps = false;
}