<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolProcNecesidades extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solproc_necesidades';
    protected $fillable = ['id', 'id_solproc', 'id_sol_necesidades', 'valor'];
    // protected $fillable = ['id', 'objetocontractual', 'tiempo', 'fecha_limite', 'incluye_gastos', 'tarifa_sinchi', 'vlrtarifa_diaria', 'tipo_solicitud', 'anticipo', 'porcentaje_anticipo', 'usuario', 'abAsignado', 'fecha_creacion', 'id_estado', 'tipo_contrato'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
