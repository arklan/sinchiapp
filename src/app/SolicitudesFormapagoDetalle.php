<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesFormapagoDetalle extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_formapago_detalle';
    protected $fillable = ['id','id_formapago','id_inforprod','tipo'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
