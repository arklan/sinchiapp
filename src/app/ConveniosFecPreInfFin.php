<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ConveniosFecPreInfFin extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'convenios_fecpresinffin';
	    protected $fillable = ['convcod', 'convfpif'];
	    protected $primaryKey = 'convfpif';
	    public $timestamps = false;	
	}

?>