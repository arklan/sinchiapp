<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adjunto extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pqr_adjuntos';
    protected $fillable = ['id_pqr', 'ruta_adjunto', 'nombre'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
