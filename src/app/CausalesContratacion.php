<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CausalesContratacion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'causales_contratacion';
    protected $fillable = ['cauconcod', 'caucondes'];
    protected $primaryKey = 'cauconcod';
    public $timestamps = false;
}