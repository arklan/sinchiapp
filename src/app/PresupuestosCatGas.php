<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PresupuestosCatGas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'presupuestos_categorias_gasto';
    protected $fillable = ['prescod','prescatgascod','prescatgasdes', 'prescatgastma'];
    protected $primaryKey = 'prescatgascod';
    public $timestamps = false;
}