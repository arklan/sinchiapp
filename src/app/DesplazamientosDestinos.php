<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesplazamientosDestinos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'desplazamientos_destinos';
    protected $fillable = ['descod', 'desdescod','desdesnacori','desdesintori','desdesnacdes',
                            'desdesintdes','desdesnacfid','desdesnacfre','desdesnactip',
                            'desdesnomdor','desdesnomdfi','desdestar','moncod','desdestas',
                            'desdestarpes','desdesdvj'];
    protected $primaryKey = 'desdescod';
    public $timestamps = false;
}