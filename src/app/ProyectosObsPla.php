<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosObsPla extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_obsplaneacion';
    protected $fillable = ['proycod','proyobsplacod', 'proyobsplades'];
    protected $primaryKey = 'proyobsplacod';
    public $timestamps = false;
}