<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposContratos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tipos_contratos';
    protected $fillable = ['tipcontcod', 'tipcontdes'];
    protected $primaryKey = 'tipcontcod';
    public $timestamps = false;
}