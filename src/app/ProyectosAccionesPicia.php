<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ProyectosAccionesPicia extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'proyectos_acciones_picia';
	    protected $fillable = ['proycod', 'accpiccod'];
	    protected $primaryKey = 'accpiccod';
	    public $timestamps = false;	
	}

?>