<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'pedidos';
    protected $fillable = ['pedcod', 'pedfec', 'pedobs','pedest'];
    protected $primaryKey = 'pedcod';
    public $timestamps = false;
}