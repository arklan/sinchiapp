<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResguardosIndigenas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'resguardos_indigenas';
    protected $fillable = ['resindcod', 'resindnom'];
    protected $primaryKey = 'resindcod';
    public $timestamps = false;
}