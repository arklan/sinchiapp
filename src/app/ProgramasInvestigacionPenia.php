<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ProgramasInvestigacionPenia extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'programas_investigacion_penia';
	    protected $fillable = ['proinvpencod', 'proinvpendes'];
	    protected $primaryKey = 'proinvpencod';
	    public $timestamps = false;	
	}

?>