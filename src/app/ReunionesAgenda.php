<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionesAgenda extends Model
{
    protected $connection = 'mysql';
    protected $table = 'reuniones_agenda';
    protected $fillable = ['reuagecod', 'reuagenum', 'reuagedes', 'reucod'];
    protected $primaryKey = 'reuagecod';
    public $timestamps = false;
}