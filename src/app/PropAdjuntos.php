<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropAdjuntos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'prop_adjuntos';
    protected $fillable = ['id', 'id_prop', 'doc_solicitado', 'nombre', 'path', 'aprobado', 'no_aplica'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
