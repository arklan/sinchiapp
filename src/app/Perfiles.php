<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfiles extends Model
{
    protected $connection = 'mysql';
    protected $table = 'perfiles';
    protected $fillable = ['perfcod', 'perfnom'];
    protected $primaryKey = 'perfcod';
    public $timestamps = false;
}