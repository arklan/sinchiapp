<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CriteriosEvaluacion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'criterios_evaluacion';
    protected $fillable = ['crievacod', 'crievades'];
    protected $primaryKey = 'crievacod';
    public $timestamps = false;
}