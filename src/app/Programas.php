<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programas extends Model
{
    protected $connection = 'mysql';
    protected $table = 'programas';
    protected $fillable = ['progrcod', 'progrnom', 'progdesc'];
    protected $primaryKey = 'progrcod';
    public $timestamps = false;
}