<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReunionesTarea extends Model
{
    protected $connection = 'mysql';
    protected $table = 'reuniones_tarea';
    protected $fillable = ['reutarcod', 'reutar', 'reutarrecapr', 'reutarfeclim', 'reutartip', 'reucod', 'reutarest'];
    protected $primaryKey = 'reutarcod';
    public $timestamps = false;
}