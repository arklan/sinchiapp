<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proveedores';
    protected $fillable = ['provcod', 'provnom', 'provnit', 'provdir', 'provtel', 'provema'];
    protected $primaryKey = 'provcod';
    public $timestamps = false;
}