<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosProdPropInd extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_productos_proindustrial';
    protected $fillable = ['proycod', 'prodproindcod'];
    protected $primaryKey = 'prodproindcod';
    public $timestamps = false;
}

?>