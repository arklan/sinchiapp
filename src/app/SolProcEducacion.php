<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolProcEducacion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solproc_educacion';
    protected $fillable = ['id', 'id_solproc', 'tipo_educacion', 'tipo', 'titulo', 'cumple', 'no_aplica', 'observacion'];
    // protected $fillable = ['id', 'objetocontractual', 'tiempo', 'fecha_limite', 'incluye_gastos', 'tarifa_sinchi', 'vlrtarifa_diaria', 'tipo_solicitud', 'anticipo', 'porcentaje_anticipo', 'usuario', 'abAsignado', 'fecha_creacion', 'id_estado', 'tipo_contrato'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
