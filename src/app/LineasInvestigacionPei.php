<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineasInvestigacionPei extends Model
{
    protected $connection = 'mysql';
    protected $table = 'lineas_investigacion_pei';
    protected $fillable = ['lininvpeicod', 'liniinvpeidesc','proinvpeicod'];
    protected $primaryKey = 'lininvpeicod';
    public $timestamps = false;
}