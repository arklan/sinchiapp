<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criterios_desempate extends Model
{
    protected $connection = 'mysql';
    protected $table = 'criterios_desempate';
    protected $fillable = ['cridescod', 'cridesdes'];
    protected $primaryKey = 'cridescod';
    public $timestamps = false;
} 	