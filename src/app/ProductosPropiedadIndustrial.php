<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosPropiedadIndustrial extends Model
{
    protected $connection = 'mysql';
    protected $table = 'productos_propiedad_industrial';
    protected $fillable = ['prodproindcod', 'prodproinddes'];
    protected $primaryKey = 'prodproindcod';
    public $timestamps = false;
}