<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;
    class ProyectosProgrPlan extends Model
    {
        protected $connection = 'mysql';
        protected $table = 'proyectos_programacion_planaccion';
        protected $fillable = ['proycod','proyobjcod', 'proyactcod','proyppaano','proyppames1','proyppames2','proyppames3',
                                'proyppames4','proyppames5','proyppames6','proyppames7','proyppames8','proyppames9',
                                'proyppames10','proyppames11','proyppames12','proyppakmes1','proyppakmes2','proyppakmes3',
                                'proyppakmes4','proyppakmes5','proyppakmes6','proyppakmes7','proyppakmes8','proyppakmes9',
                                'proyppakmes10','proyppakmes11','proyppakmes12'];
        protected $primaryKey = 'proyppaano';
        public $timestamps = false;
    }

?>