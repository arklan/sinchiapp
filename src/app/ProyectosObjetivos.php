<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectosObjetivos extends Model
{
    protected $connection = 'mysql';
    protected $table = 'proyectos_objetivos';
    protected $fillable = ['proycod', 'proyobjcod', 'proyobjdes','proyobjpes'];
    protected $primaryKey = 'proyobjcod';
    public $timestamps = false;
}

?>