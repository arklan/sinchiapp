<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class ProgramasInvestigacionPei extends Model
	{	
	    protected $connection = 'mysql';
	    protected $table = 'programas_investigacion_pei';
	    protected $fillable = ['proinvpeicod', 'proinvpeides'];
	    protected $primaryKey = 'proinvpeicod';
	    public $timestamps = false;	
	}

?>