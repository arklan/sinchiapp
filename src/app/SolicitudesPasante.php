<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudesPasante extends Model
{
    protected $connection = 'mysql';
    protected $table = 'solicitudes_pasante';
    protected $fillable = ['id', 'id_solicitud','id_convenio','valorarl'];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
