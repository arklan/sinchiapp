<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposContratacion extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tipos_contratacion';
    protected $fillable = ['tipconcod', 'tipconnom'];
    protected $primaryKey = 'tipconcod';
    public $timestamps = false;
}