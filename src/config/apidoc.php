<?php
return [
    'apiVersion'     => '1.0.0',
    'apiTitle'       => 'api Taita',
    'apiDescription' => 'Api para los adiferentes aplicativos de Taita Cafe',
    'apiBasePath'    => '/api/v1',
    'authorization'  => 'jwt',
];
