import axios from 'axios'
import configService from './config'

const FileExchange = {}

  FileExchange.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'GET',
        url: configService.apiUrl + '/FileExchange',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  FileExchange.upload = function(file){
    let formData = new FormData();
    formData.append('archivo', file);

    return new Promise(function(resolve, reject) {
      
      axios.post(configService.apiUrl + '/uploadFile', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  FileExchange.uploadGen = function(file, ruta){
    let formData = new FormData();
    formData.append('archivo', file);

    return new Promise(function(resolve, reject) {
      
      axios.post(configService.apiUrl + '/uploadFileGen', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  FileExchange.deleteFile = function(ruta, file){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/deleteFile',
        dataType: "json",
        data: {
            ruta: ruta,
            archivo: file     
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

  export default FileExchange