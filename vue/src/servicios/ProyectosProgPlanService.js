import axios from 'axios'
import configService from './config'

const proyectoprogplanService = {}

  proyectoprogplanService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectosProgPlan',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoprogplanService.DelData = function(proyecto,objetivo,actividad,anno){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delProyectoProgPlan',
        dataType: "json",
        data: {
          proyecto: proyecto,
          objetivo: objetivo,
          actividad: actividad,
          anno:anno          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoprogplanService.BuscarXObjetivo = function(proyecto,objetivo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProgXObjetivo',
        dataType: "json",
        data: {
          proyecto: proyecto,
          objetivo: objetivo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoprogplanService.BuscarXObjetivoActividad = function(actividad){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProgXObjetivoActividad',
        dataType: "json",
        data: {         
          actividad:actividad          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoprogplanService.addupdData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/addupdProyectoProgPlan',
        dataType: "json",
        data: {
          data: data         
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoprogplanService.addBasico = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/AddActividadBase',
        dataType: "json",
        data: {
          data: data         
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default proyectoprogplanService