import axios from 'axios'
import configService from './config'

const proyectoequipoService = {}  

  proyectoequipoService.BuscarXProyecto = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectoEquipo',
        dataType: "json",
        data: {
          proyecto: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoequipoService.DelData = function(proyecto, personal){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/DelProyectoEquipo',
        dataType: "json",
        data: {
          proyecto: proyecto,
          personal: personal        
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoequipoService.guardarData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarProyectoEquipo',
        dataType: "json",
        data: {
          data: data       
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoequipoService.HorasXProyecto = function(proyecto, personal){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/horasProyectosPersonal',
        dataType: "json",
        data: {
          proyecto: proyecto,
          personal:personal          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoequipoService.FiltroXProyectoAll = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/EquipoXProyectoAll',
        dataType: "json",
        data: {
          proyecto: Codigo          
        }
      })
      .then(function(res) {

        resolve(res.data.data)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default proyectoequipoService