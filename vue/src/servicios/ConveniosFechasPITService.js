import axios from 'axios'
import configService from './config'

const conveniofechasPITService = {}  

conveniofechasPITService.BuscarXConvenio = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/FechasPreInfTecXConvenio',
        dataType: "json",
        data: {
          codigo: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  conveniofechasPITService.delData = function(codigo,fecha){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delConvFechaPIT',
        dataType: "json",
        data: {
          codigo: codigo,
          fecha:fecha          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default conveniofechasPITService