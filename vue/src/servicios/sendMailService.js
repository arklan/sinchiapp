import axios from 'axios'
import configService from './config'

const mailService = {}

mailService.sendMail = function(correo){

  return new Promise(function(resolve, reject) {axios({
      method:'get',
      url: configService.apiUrl + '/email',
      dataType: "json",
      params: {
        email: correo
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

mailService.sendMailData = function(correo,nombre,asunto,mensaje,adjunto){

  return new Promise(function(resolve, reject) {axios({
      method:'post',
      url: configService.apiUrl + '/email',
      dataType: "json",
      data: {
        email: correo,
        nombre:nombre,
        asunto:asunto,
        mensaje:mensaje,
        adjunto:adjunto
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

export default mailService
/*
export default function sendMail(correo){

  return new Promise(function(resolve, reject) {axios({
      method:'get',
      url: configService.apiUrl + '/email',
      dataType: "json",
      params: {
	     email: correo
	  }
    })
    .then(function(res) {

      resolve({
         title: 'success',
         data: res,         
       })    

    })
    .catch(function(err) {
      
      reject({
         title: 'error',
         data: err,         
      })
         
    })
  })  
  
}

export default function sendMailData(correo){

  return new Promise(function(resolve, reject) {axios({
      method:'get',
      url: configService.apiUrl + '/email',
      dataType: "json",
      params: {
	     email: correo
	  }
    })
    .then(function(res) {

      resolve({
         title: 'success',
         data: res,         
       })    

    })
    .catch(function(err) {
      
      reject({
         title: 'error',
         data: err,         
      })
         
    })
  })  
  
}*/