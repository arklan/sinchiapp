import axios from 'axios'
import configService from './config'
 
const permisosService = {}

  permisosService.getPermisos = function(UserId){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/getPermisos',
        dataType: "json",
        data: {
            usuarioid: UserId          
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }

  permisosService.getOpcion = function(UserId, codigoopcion){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/getOpcion',
        dataType: "json",
        data: {
            usuarioid: UserId,
            codigo:codigoopcion          
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }
  
  permisosService.getPermisosActuales = function(perfId){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/PermisosActuales',
        dataType: "json",
        data: {
          perfilid: perfId          
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }

  permisosService.getPermisosFal = function(perfId){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/getPermisosFal',
        dataType: "json",
        data: {
          perfilid: perfId          
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }  

  export default permisosService