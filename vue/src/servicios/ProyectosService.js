import axios from 'axios'
import configService from './config'

const proyectoService = {}

proyectoService.getData = function(tipo,convenio,nombreconvenio){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ProyectosGrilla',
      dataType: "json",
      data:{
          tipo:tipo,
          convenio:convenio,
          nombreconvenio:nombreconvenio
      }      
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

  proyectoService.getDataAll = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/Proyectos',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoService.getDataXId = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectoXId',
        dataType: "json",
        data:{
          proyecto:proyecto
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoService.guardarData = function(poa,data,LineasInvPicia,AccionesPicia,IndicadoresPicia,
                                        arrLIneasInvPei,arrProyectosPei,pilaresPicia){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarProyecto',
        dataType: "json",        
        data: {
            data: data ,
            poa:poa,
            LineasInvPicia:LineasInvPicia,
            AccionesPicia:AccionesPicia,
            IndicadoresPicia:IndicadoresPicia,
            LinInvPei:arrLIneasInvPei,
            ProgramasPei:arrProyectosPei,
            pilarespicia:pilaresPicia          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }  

  proyectoService.DelData = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delProyecto',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoService.FtToPDF = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'get',
        url: configService.apiUrl + '/getFTPDF',
        //dataType: "json",
        data: {
          proyecto: proyecto       
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoService.actualizarTasa = function(proyecto,tasa){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/actualizaTasa',
        dataType: "json",
        data: {
          proyecto: proyecto ,
          tasa:tasa
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoService.EstadoActual = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/EstadosActualProyecto',
        dataType: "json",
        data: {
          proyecto: proyecto         
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

  proyectoService.actualizaVoBo = function(proyecto,vistobueno,tipo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/updateVoBoProyecto',
        dataType: "json",        
        data: {
          proyecto: proyecto,
          vistobueno:vistobueno,
          tipo: tipo         
        }
      })
      .then(function(res) {

        resolve(res)      

      })
      .catch(function(err) {     

        resolve(err)

      })
    })

  }  

  proyectoService.getDataXVoBo = function(convenio='', nombreconvenio=''){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/proyectosXVoBo',
        dataType: "json",
        data: {
            convenio:convenio,
            nombreconvenio:nombreconvenio          
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoService.traePlanAdqXProyecto = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/buscarPlanAdqXproyecto',
        dataType: "json",        
        data: {
          proyecto: proyecto                  
        }
      })
      .then(function(res) {

        resolve(res)      

      })
      .catch(function(err) {     

        resolve(err)

      })
    })

  }  

  export default proyectoService