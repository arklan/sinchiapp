import axios from 'axios'
import configService from './config'

const rolesService = {}

  rolesService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/Roles',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  rolesService.guardarData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarRol',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  rolesService.DelData = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delRol',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default rolesService