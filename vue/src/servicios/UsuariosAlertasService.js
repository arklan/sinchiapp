import axios from 'axios'
import configService from './config'

const usuariosalertasService = {}

  usuariosalertasService.getDataXId = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/AlertasXUsuario',
        dataType: "json",
        data: {
            codigo: codigo          
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  usuariosalertasService.getDataXPersonal = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/AlertasXPersonal',
        dataType: "json",
        data: {
          codigopersonal: codigo          
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  usuariosalertasService.guardarData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarUsuariosAlertas',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default usuariosalertasService