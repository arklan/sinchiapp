import axios from 'axios'
import configService from './config'

const SolProceso = {}

  SolProceso.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'GET',
        url: configService.apiUrl + '/solproceso',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolProceso.getExpSupports = function(id, tipo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/solprocesoSupportAll',
        dataType: "json",
        data: { id: id, tipo: tipo }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolProceso.getSolProcDetail = function(id){

    return new Promise(function(resolve, reject) {axios({
        method:'GET',
        url: configService.apiUrl + '/solproceso/' + id,
        dataType: "json",
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolProceso.save = function(solproc){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/solproceso',
        dataType: "json",
        data: {
          solproc: solproc          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolProceso.saveSupport = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/solprocesoSupport',
        dataType: "json",
        data: data
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolProceso.updateProgres = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'PUT',
        url: configService.apiUrl + '/solproceso',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolProceso.DelData = function(Codigo){

    /* return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delPersonal',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    }) */

  }

  export default SolProceso