import axios from 'axios'
import configService from './config'

export default function updatePassword(ant_pass,new_pass, userid){

  return new Promise(function(resolve, reject) {axios({
        method:'post',
        url: configService.apiUrl + '/updatePassword',
        dataType: "json",      
        data: {
          passwordant : ant_pass,
          password: new_pass,
          usuario: userid
        }
    })
    .then(function(res) {

      resolve({
         title: 'success',
         data: res,         
      })

    })
    .catch(function(err) {     

      reject({
         title: 'error',
         data: err,         
      })
         
    })
  })  
  
}