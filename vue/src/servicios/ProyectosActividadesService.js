import axios from 'axios'
import configService from './config'

const proyectoactividadService = {}

  proyectoactividadService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectoActividades',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoactividadService.guardarData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarProyectoActividad',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoactividadService.DelData = function(proyecto, objetivo, producto, actividad){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delProyectoActividad',
        dataType: "json",
        data: {
          proyecto: proyecto,
          objetivo: objetivo,
          producto: producto,
          actividad: actividad          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoactividadService.BuscarXProyectoObjetivo = function(proyecto, objetivo, producto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/FiltroXProyecto',
        dataType: "json",
        data: {
          proyecto: proyecto,
          objetivo: objetivo,
          producto:producto
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoactividadService.BuscarXProyObjSimple = function(proyecto, objetivo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/FiltroXProyectoObj',
        dataType: "json",
        data: {
          proyecto: proyecto,
          objetivo: objetivo         
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoactividadService.FiltroxActividad = function(actividad){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ActividadXCodigo',
        dataType: "json",
        data: {
          //proyecto: proyecto,
          //objetivo: objetivo,
          actividad: actividad         
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoactividadService.FiltroXProyectoAll = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ActividadesXProyectoAll',
        dataType: "json",
        data: {
          proyecto: proyecto       
        }
      })
      .then(function(res) {

        resolve(res.data)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default proyectoactividadService