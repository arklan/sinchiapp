import axios from 'axios'
import configService from './config'

const proyectosprodproindService = {}

  proyectosprodproindService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectosProdPropInd',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosprodproindService.getDataXProyecto = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectosProdPropIndXproyecto',
        dataType: "json",
        data: {
            proyecto: proyecto          
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosprodproindService.seleccionadosxProyecto = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/selProyectosProdPropInd',
        dataType: "json",
        data: {
            proyecto: proyecto          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default proyectosprodproindService