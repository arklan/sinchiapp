import axios from 'axios'
import configService from './config'

const proyectosegplanService = {}

  proyectosegplanService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectosSegPlan',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegplanService.DelData = function(proyecto,objetivo,actividad,anno){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delProyectoSeguimPlan',
        dataType: "json",
        data: {
          proyecto: proyecto,
          objetivo: objetivo,
          actividad: actividad,
          anno:anno          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegplanService.BuscarXObjetivo = function(proyecto,objetivo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/SeguimientoXObjetivo',
        dataType: "json",
        data: {
          proyecto: proyecto,
          objetivo: objetivo           
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegplanService.addupdData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/addupdProyectoSeguimPlan',
        dataType: "json",
        data: {
          data: data         
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegplanService.BuscarXObjetivoActividad = function(actividad){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/SegXObjetivoActividad',
        dataType: "json",
        data: {         
          actividad:actividad          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegplanService.addBasico = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/AddSegActividadBase',
        dataType: "json",
        data: {
          data: data         
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default proyectosegplanService