import axios from 'axios'
import configService from './config'

/* const authHeader = {
    'Accept': 'application/json',
    'Content-Type': 'application/json,application/x-www-form-urlencoded; charset=UTF-8',
    'Access-Control-Allow-Headers':  'Content-Type, X-Auth-Token, Origin, Authorization, enctype',    
    'Access-Control-Allow-Methods':'GET,HEAD,OPTIONS,POST,PUT, DELETE',
    'Access-Control-Allow-Origin' : '*'    
} */

export default function getLogin(user){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/login', // 'http://localhost:81/ApisTaita/public/api/login',
      dataType: "json",
      data: {
        username: user.email,
        password: user.password
      }/*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
  
}