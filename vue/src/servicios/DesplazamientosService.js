import axios from 'axios'
import configService from './config'

const desplazamientosService = {}

  desplazamientosService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/Desplazamientos',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }  

  desplazamientosService.guardarData = function(data,destinos, rubros){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/addupdDesplazamientos',
        dataType: "json",
        data: {
          data: data,
          destinos:destinos,
          rubros:rubros          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  desplazamientosService.destinos = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/DesplazamientosDestinos',
        dataType: "json",        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  desplazamientosService.destinosXCodigo = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/DesplazamientosDestinosXCodigo',
        dataType: "json",
        data: {
          codigo: codigo              
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }

  desplazamientosService.destinosXCodigoOrdenFecha = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/DesplazamientosDestinosOrdenFecha',
        dataType: "json",
        data: {
          codigo: codigo              
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }

  desplazamientosService.rubrosXCodigo = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/DesplazamientosRubrosXCodigo',
        dataType: "json",
        data: {
          codigo: codigo              
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }
  
  desplazamientosService.cambiarEstadosVB = function(codigo,tipo,estado,observacion){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/modificarVBDesplazamientos',
        dataType: "json",
        data: {
          codigo: codigo,
          tipo: tipo,
          estado: estado,
          observacion: observacion              
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }

  desplazamientosService.liquidaViaticos = function(data,rubros){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/liquidaViaticos',
        dataType: "json",
        data: {
          data: data,          
          rubros:rubros          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  desplazamientosService.updDatosPresupuesto = function(codigo,vobopresupuesto,obspresupuesto,numerorp,archivorp){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/updDatosPresupuestoViaticos',
        dataType: "json",
        data: {
          codigo: codigo,
          vobopresupuesto:vobopresupuesto,
          obspresupuesto:obspresupuesto,       
          numerorp:numerorp,
          archivorp:archivorp          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  desplazamientosService.pagoTesoreria = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/pagoTesoreriaDesplazamiento',
        dataType: "json",
        data: {
          codigo: codigo                   
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  desplazamientosService.deleteFile = function(codigo,tiparchivo,ruta,archivo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/desplazamientoseliminarArchivo',
        dataType: "json",       
        data: {
          codigo: codigo  ,
          tiparchivo:tiparchivo,
          ruta:ruta,
          archivo:archivo     
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default desplazamientosService