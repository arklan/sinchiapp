import axios from 'axios'
import configService from './config'

const Proponente = {}

  Proponente.getInfo = function(id){

    return new Promise(function(resolve, reject) {axios({
        method:'GET',
        url: configService.apiUrl + '/evaluacionProponente/' + id,
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }

  Proponente.getCriterios = function(id){

    return new Promise(function(resolve, reject) {axios({
        method:'GET',
        url: configService.apiUrl + '/criteriosProponente/' + id,
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }
  

  Proponente.saveInfo = function(obj){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/evaluacionProponente',
        dataType: "json",
        data: {
          data: obj          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  Proponente.saveOfertaEco = function(obj){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/actOfertaEco',
        dataType: "json",
        data: {
          data: obj          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })
  }

  

  Proponente.save = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/saveSolicitud',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  Proponente.savePositions = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/savePositions',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  Proponente.updateProgres = function(data, id){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/updateSolicitud/' + id,
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  Proponente.DelData = function(Codigo){

    /* return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delPersonal',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    }) */

  }

  export default Proponente