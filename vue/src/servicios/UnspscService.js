import axios from 'axios'
import configService from './config'

const UnspscService = {}

  UnspscService.getUnspsc = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/Unspsc',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  UnspscService.getDataXFiltro = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/UnspscXFiltro',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  UnspscService.guardarUnspsc = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarUnspsc',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  UnspscService.delUnspsc = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delUnspsc',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default UnspscService