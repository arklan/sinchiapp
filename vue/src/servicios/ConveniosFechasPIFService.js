import axios from 'axios'
import configService from './config'

const conveniofechasPIFService = {}  

conveniofechasPIFService.BuscarXConvenio = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/FechasPreInfFinXConvenio',
        dataType: "json",
        data: {
          codigo: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  conveniofechasPIFService.delData = function(codigo,fecha){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delConvFechaPIF',
        dataType: "json",
        data: {
          codigo: codigo,
          fecha:fecha          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default conveniofechasPIFService