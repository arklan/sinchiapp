import axios from 'axios'
import configService from './config'

const accionesPiciaService = {}

  accionesPiciaService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/AccionesPicia',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  accionesPiciaService.getDataXProyecto = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/AccionesPiciaXProyecto',
        dataType: "json",
        data: {
            proyecto: proyecto            
        }        
      })
      .then(function(res) {

        resolve(res.data.data)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  accionesPiciaService.guardarData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/addupdAccionPicia',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  accionesPiciaService.DelData = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delAccionPicia',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  accionesPiciaService.getSeleccionados = function(proyecto,lininvpicia){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/SelAccionesPiciaProy',
        dataType: "json",
        data: {
            proyecto: proyecto,
            linvinv: lininvpicia         
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  accionesPiciaService.getXLinInvPicia = function(lininvpicia){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/AccionPiciaXLinInv',
        dataType: "json",
        data: {
          lininvpicia: lininvpicia          
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  accionesPiciaService.getXLinInvPiciaAray = function(lininvpicia){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/AccPicXLinInvArray',
        dataType: "json",
        data: {
          lininvpicia: lininvpicia          
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default accionesPiciaService