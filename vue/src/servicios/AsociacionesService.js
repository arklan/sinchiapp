import axios from 'axios'
import configService from './config'

const asociacionesService = {}

  asociacionesService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/Asociaciones',
        dataType: "json"        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  asociacionesService.asociacionesComunidades = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/asociacionesComunidades',
        dataType: "json",
        data: {
          codigo:codigo
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  } 
  
  asociacionesService.asociacionesResguardos = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/asociacionesResguardos',
        dataType: "json",
        data: {
          codigo:codigo
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }  

  asociacionesService.guardarData = function(data, comunidades, resguardos){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarAsociacion',
        dataType: "json",
        data: {
          data: data,
          comunidades: comunidades,
          resguardos: resguardos          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  asociacionesService.DelData = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delAsociacion',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  asociacionesService.delAsociacionResguardo = function(codigo, resguardo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delAsociacionResguardo',
        dataType: "json",
        data: {
          codigo: codigo,
          resguardo: resguardo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  asociacionesService.delAsociacionComunidad = function(codigo, comunidad){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delAsociacionComunidad',
        dataType: "json",
        data: {
          codigo: codigo,
          comunidad: comunidad          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default asociacionesService