import axios from 'axios'
import configService from './config'

const SolprocPagos = {}

  SolprocPagos.getPagos = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'GET',
        url: configService.apiUrl + '/solprocPagos',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  /* SolprocPagos.detailSol = function(id){

    return new Promise(function(resolve, reject) {axios({
        method:'GET',
        url: configService.apiUrl + '/solicitudes/' + id,
        dataType: "json",
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolprocPagos.save = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/saveSolicitud',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolprocPagos.updateProgres = function(data, id){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/updateSolicitud/' + id,
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolprocPagos.DelData = function(Codigo){


  } */

  export default SolprocPagos