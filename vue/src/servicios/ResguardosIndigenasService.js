import axios from 'axios'
import configService from './config'

const resguardosindigenasService = {}

  resguardosindigenasService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ResguardosIndigenas',
        dataType: "json"        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  resguardosindigenasService.resguardosCiudades = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/resguardosCiudades',
        dataType: "json",
        data: {
          codigo:codigo
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }  

  resguardosindigenasService.guardarData = function(data, items){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarResguardoIndigena',
        dataType: "json",
        data: {
          data: data,
          ciudades: items          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  resguardosindigenasService.DelData = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delResguardoIndigena',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  resguardosindigenasService.DelCiudadResguardo = function(codigo, ciudad){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delCiudadResguardo',
        dataType: "json",
        data: {
          codigo: codigo,
          ciudad: ciudad          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default resguardosindigenasService