import axios from 'axios'
import configService from './config'

const presupuestosService = {}

presupuestosService.getData = function(){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/Presupuestos',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

presupuestosService.getDataXConvenio = function(convenio){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/presupuestoXConvenio',
        dataType: "json",
        data: {
            convenio: convenio       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

presupuestosService.guardarData = function(data, categoriasgasto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarPresupuesto',
        dataType: "json",        
        data: {
          data: data ,
          categoriasgasto:categoriasgasto              
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }  

  presupuestosService.DelData = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delPresupuesto',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  presupuestosService.getCatGasXPresupuesto = function(presupuesto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/catgastoXPresupuesto',
        dataType: "json",
        data: {
          presupuesto: presupuesto       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

  presupuestosService.getCatGasXConvenio = function(convenio){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/catgastoXConvenio',
        dataType: "json",
        data: {
          convenio: convenio       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

  presupuestosService.delCategoriaPresupuesto = function(presupuesto,categoria){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delCategoriaPresupuesto',
        dataType: "json",
        data: {
            presupuesto: presupuesto,
            categoria:categoria
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

  export default presupuestosService