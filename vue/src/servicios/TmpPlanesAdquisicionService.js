import axios from 'axios'
import configService from './config'

const tmpplanesadquisicionService = {}

tmpplanesadquisicionService.BuscarPlanAdquisicion = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/BuscarPlanAdquisicion',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion       
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

export default tmpplanesadquisicionService