import axios from 'axios'
import configService from './config'

const ProyectosPilaresPiciaService = {}


  ProyectosPilaresPiciaService.getSeleccionados = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/selPilaresPicia',
        dataType: "json",
        data: {
            proyecto: proyecto          
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }
  
  ProyectosPilaresPiciaService.getDataXProyecto = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/PilaresPiciaXProyecto',
        dataType: "json",
        data: {
            proyecto: proyecto          
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res.data.data)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default ProyectosPilaresPiciaService