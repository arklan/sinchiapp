import axios from 'axios'
import configService from './config'

const ProductosPropiedadIndustrialService = {}

  ProductosPropiedadIndustrialService.getProductosPropiedadIndustrial = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProductosPropiedadIndustrial',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  ProductosPropiedadIndustrialService.guardarProductosPropiedadIndustrial = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarProductosPropiedadIndustrial',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  ProductosPropiedadIndustrialService.DelProductosPropiedadIndustrial = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delProductosPropiedadIndustrial',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default ProductosPropiedadIndustrialService