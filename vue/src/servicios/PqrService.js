import axios from 'axios'
import configService from './config'

const Pqrs = {}

Pqrs.getData = function() {

  return new Promise(function(resolve, reject) {axios({
      method:'GET',
      url: configService.apiUrl + '/getPqrs',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getMunicipios = function() {

  return new Promise(function(resolve, reject) {axios({
      method:'GET',
      url: configService.apiUrl + '/getMunicipios',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getDependencias = function() {

  return new Promise(function(resolve, reject) {axios({
      method:'GET',
      url: configService.apiUrl + '/getDependencias',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getTipoComunicacion = function() {

  return new Promise(function(resolve, reject) {axios({
      method:'GET',
      url: configService.apiUrl + '/getTipoComunicacion',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getUsers = function() {

  return new Promise(function(resolve, reject) {axios({
      method:'GET',
      url: configService.apiUrl + '/getUsers',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getDataResponsable = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/getDataResponsable',
      dataType: "json",
      data: {
        documento: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getAsignados = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/getAsignados',
      dataType: "json",
      data: {
        id: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getInfoResponsable = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/getInfoResponsable',
      dataType: "json",
      data: {
        idarea: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.savePqr = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/savePqr',
      dataType: "json",
      data: {
        data: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.uploadFile = function(file){
  let formData = new FormData();
  formData.append('archivo', file);

  return new Promise(function(resolve, reject) {
    
    axios.post(configService.apiUrl + '/uploadFilepqr', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

Pqrs.savefromH = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/savefromH',
      dataType: "json",
      data: {
        data: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.updateOficio = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/updateOficio',
      dataType: "json",
      data: {
        data: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.saveAdjunto = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/saveAdjunto',
      dataType: "json",
      data: {
        data: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.updateAdjunto = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/updateAdjunto',
      dataType: "json",
      data: {
        data: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.listPqr = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/listPqr',
      dataType: "json",
      data: {
        id: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.updatePqr = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/updatePqr',
      dataType: "json",
      data: {
        data: data
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getAgentes = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/getAgentes',
      dataType: "json",
      data: {
        id: data
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.deleteAsignacion = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/deleteAsignacion',
      dataType: "json",
      data: {
        id: data
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getComentario = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/getComentario',
      dataType: "json",
      data: {
        id: data,
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.saveRespuesta = function(info,adjuntos){
  const formData = new FormData()
  for (var i = 0; i < adjuntos.length; i++) {
    const file = adjuntos[i]
    formData.append(`archivos[${i}]`, file)
  }
  formData.append('obj', JSON.stringify(info))

  return new Promise(function(resolve, reject) {
    
    axios.post(configService.apiUrl + '/saveRespuesta', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

Pqrs.getRespuesta = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/getRespuesta',
      dataType: "json",
      data: {
        id: data,
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getResponsablepqr = function(data,pqr) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/getResponsablepqr',
      dataType: "json",
      data: {
        id: data,
        id_pqr: pqr
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.saveComentario = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/saveComentario',
      dataType: "json",
      data: {
        data: data,
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.saveAsignacion = function(tipo,data,id) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/saveAsignacion',
      dataType: "json",
      data: {
        tipo: tipo,
        data: data,
        id: id
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.getResponsables = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/getResponsables',
      dataType: "json",
      data: {
        id: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.listAdjuntos = function(data) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/listAdjuntos',
      dataType: "json",
      data: {
        id: data          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.deleteFile = function(nombre, id) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/deleteFilepqr',
      dataType: "json",
      data: {
        archivo: nombre,
        id: id          
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

Pqrs.deleteOficio = function(nombre) {

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/deleteOficio',
      dataType: "json",
      data: {
        archivo: nombre        
      }
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
          
    })
  })

}

export default Pqrs