import axios from 'axios'
import configService from './config'

const proyectosegdetService = {}

  proyectosegdetService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectosSegDet',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegdetService.BuscarXItem = function(codigo,anno,mes){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/SegDetXItem',
        dataType: "json",
        data: {
            codigo: codigo,
            anno: anno,
            mes:mes          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegdetService.addupdData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/addupdProyectoSegDet',
        dataType: "json",       
        data: {
          data: data                 
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegdetService.uploadFile = function(codigo, File){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/addupdProyectoSegDet',
        dataType: "json",
        data: {
          codigo: codigo,
          anno:anno,
          mes:mes,
          file:File

        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegdetService.deleteFile = function(codigo,anno,mes,ruta,archivo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/deleteFileSegDet',
        dataType: "json",       
        data: {
          codigo: codigo  ,
          anno:anno,  
          mes:mes,
          ruta:ruta,
          archivo:archivo     
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectosegdetService.obsSubDirCientifica = function(codigo,anno,mes,observacion){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/obsSubDirCientifica',
        dataType: "json",       
        data: {
          codigo:codigo,
          anno:anno,  
          mes:mes,
          obssubdircientifica:observacion
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }
  

  export default proyectosegdetService