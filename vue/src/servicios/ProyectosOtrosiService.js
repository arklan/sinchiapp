import axios from 'axios'
import configService from './config'

const proyectootrosiService = {}

proyectootrosiService.getData = function(){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ProyectosOtrosi',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

proyectootrosiService.getDataXProyecto = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/OtroSiXProyecto',
        dataType: "json",
        data:{
          proyecto:proyecto
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectootrosiService.getFecTerminaMax = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/MaxFecTerminacion',
        dataType: "json",
        data:{
          proyecto:proyecto
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectootrosiService.getDataMaxFechas = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectoOtrosiMaxFechas',
        dataType: "json",
        data:{
          proyecto:proyecto
        }        
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectootrosiService.guardarData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/addupdProyectoOtrosi',
        dataType: "json",
        data: {
          data: data         
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }  

  proyectootrosiService.DelData = function(Codigo,Proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delProyectoOtrosi',
        dataType: "json",
        data: {
            codigo: Codigo,
            proyecto:proyecto          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default proyectootrosiService