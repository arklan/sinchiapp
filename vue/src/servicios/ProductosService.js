import axios from 'axios'
import configService from './config'

const productosService = {}

  productosService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/Productos',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  productosService.guardarData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarProducto',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  productosService.DelData = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delProducto',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default productosService