import axios from 'axios'
import configService from './config'

const LineaInvPeiService = {}

  LineaInvPeiService.getLineasInvPei = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/LineasInvPei',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  LineaInvPeiService.guardarLineaInvPei = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarLineaInvPei',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  LineaInvPeiService.delLineaInvPei = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delLineaInvPei',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  LineaInvPeiService.getSeleccionados = function(proyecto,programas){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/SelLinInvPeiProy',
        dataType: "json",
        data: {
            proyecto: proyecto,
            programas:programas          
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  LineaInvPeiService.getDataXProyecto = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/LineasInvPeiXProyecto',
        dataType: "json",
        data: {
            proyecto: proyecto       
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res.data.data)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default LineaInvPeiService