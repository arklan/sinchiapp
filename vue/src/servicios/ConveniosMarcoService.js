import axios from 'axios'
import configService from './config'

const conveniosmarcoService = {}

conveniosmarcoService.getData = function(){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ConveniosMarco',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

conveniosmarcoService.getDataXId = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/conveniomarcoXCodigo',
        dataType: "json",
        data: {
            convenio: codigo       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

conveniosmarcoService.guardarData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarConvenioMarco',
        dataType: "json",        
        data: {
          data: data                 
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }  

  conveniosmarcoService.DelData = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delConvenioMarco',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default conveniosmarcoService