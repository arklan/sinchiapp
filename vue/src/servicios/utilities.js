import axios from 'axios'
import configService from './config'

export function calcPuntaje (cantSop, cantRec, pUnd, pMax) {
  if (cantSop > cantRec) {
    if (pMax > pUnd) {
      return pMax
    } else {
      return pUnd
    }
  } else if (cantSop === cantRec) {
    return pUnd
  } else {
    return 0
  }
}

export function getCarreras () {
  return new Promise((resolve, reject) => {
    axios.get('https://www.datos.gov.co/resource/shx5-k4uu.json')
      .then(response => {
        resolve (response)
      })
      .catch(err => {
        reject(err)
      })
  })
}