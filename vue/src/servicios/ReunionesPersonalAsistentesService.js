import axios from 'axios'
import configService from './config'

const ReunionesPersonalAsistentesService = {}

ReunionesPersonalAsistentesService.getReunionesAsistentes = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ReunionesAsistentes',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  ReunionesPersonalAsistentesService.guardarReunionAsistentes = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarReunionAsistentes',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  ReunionesPersonalAsistentesService.getReunionesAsistentesXReunion = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ReunionesAsistentesXReunion',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })   

    }

    ReunionesPersonalAsistentesService.getReunionesAsistentesAprobarXReunion = function(Codigo){

      return new Promise(function(resolve, reject) {axios({
          method:'POST',
          url: configService.apiUrl + '/ReunionesAsistentesAprobarXReunion',
          dataType: "json",
          data: {
            id: Codigo          
          }
        })
        .then(function(res) {
  
          resolve(res)      
          
        })
        .catch(function(err) {     
  
          resolve(err)
             
        })
      })
    }

  export default ReunionesPersonalAsistentesService