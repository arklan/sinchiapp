import axios from 'axios'
import configService from './config'

const paisesService = {}

  paisesService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/Paises',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }
  
  paisesService.getDataXFiltro = function(tipofiltro,valorfiltro){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/PaisesXFiltro',
        dataType: "json",
        data: {
            tipofiltro: tipofiltro,
            valorfiltro:valorfiltro          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default paisesService