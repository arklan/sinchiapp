import axios from 'axios'
import configService from './config'

const reunionesactaaprobacionesService = {}

reunionesactaaprobacionesService.getReunionesActaAprobaciones = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ReunionesActaAprobaciones',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  
  reunionesactaaprobacionesService.getReunionesActaAprobacionesxActa = function(CodigoReunion, CodigoActa){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ReunionesActaAprobacionesXActa',
        dataType: "json",
        data: {
          CodReunion: CodigoReunion,      
          CodActa: CodigoActa    
        }
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  
  reunionesactaaprobacionesService.guardarReunionActaAprovaciones = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarReunionActaAprobaciones',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }
  
  reunionesactaaprobacionesService.DelReunionActaAprovaciones = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delReunionActaAprovaciones',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }  

  export default reunionesactaaprobacionesService