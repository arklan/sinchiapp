import axios from 'axios'
import configService from './config'

const proyectoproductosService = {}

  proyectoproductosService.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProyectosProductos',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoproductosService.DelData = function(proyecto,objetivo,producto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delProyectoProducto',
        dataType: "json",
        data: {
          proyecto: proyecto,
          objetivo: objetivo,
          producto: producto          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoproductosService.BuscarXProyecto = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProductosXProyecto',
        dataType: "json",
        data: {
          proyecto: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoproductosService.BuscarXObjetivo = function(proyecto,objetivo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProductosXObjetivo',
        dataType: "json",
        data: {
          proyecto: proyecto,
          objetivo: objetivo           
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoproductosService.addupdData = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/addupdProyectoProducto',
        dataType: "json",
        data: {
          data: data         
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  proyectoproductosService.FiltroXProyectoAll = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ProductosXProyectoAll',
        dataType: "json",
        data: {
          proyecto: Codigo          
        }
      })
      .then(function(res) {

        resolve(res.data.data)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default proyectoproductosService