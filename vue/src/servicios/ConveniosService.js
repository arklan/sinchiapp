import axios from 'axios'
import configService from './config'

const conveniosService = {}

conveniosService.getData = function(){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/Convenios',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

conveniosService.getDataXId = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/convenioXCodigo',
        dataType: "json",
        data: {
            convenio: codigo       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

conveniosService.guardarData = function(data,arrFechasPIT,arrFechasPIF,ArrConvenios){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarConvenio',
        dataType: "json", 
        /*headers: {
          'Content-Type': 'multipart/form-data'
        },*/       
        data: {
          data: data,
          FechasPIT:arrFechasPIT,
          FechasPIF:arrFechasPIF,
          Socios: ArrConvenios         
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }  

  conveniosService.DelData = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delConvenio',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  conveniosService.getSocios = function(codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/ConveniosSocios',
        dataType: "json",
        data: {
            convenio: codigo                 
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

  conveniosService.delSocio = function(convenio,fuentefinanciacion){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/DelConveniosSocios',
        dataType: "json",
        data: {
          convenio: convenio,
          fuentefinanciacion:fuentefinanciacion                
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

  conveniosService.deleteFile = function(codigo,tiparchivo,ruta,archivo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/eliminarArchivoConvenio',
        dataType: "json",       
        data: {
          codigo: codigo  ,
          tiparchivo:tiparchivo,
          ruta:ruta,
          archivo:archivo     
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default conveniosService