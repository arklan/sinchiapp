import axios from 'axios'
import configService from './config'

 const authHeader = {
    'Accept': 'application/json',
    'Content-Type': 'application/json,application/x-www-form-urlencoded; charset=UTF-8',
    'Access-Control-Allow-Headers':  'Content-Type, X-Auth-Token, Origin, Authorization, enctype',    
    'Access-Control-Allow-Methods':'GET,HEAD,OPTIONS,POST,PUT, DELETE',
    'Access-Control-Allow-Origin' : '*'    
} 

const usuariosService = {}

  usuariosService.getUsuarios = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/Usuarios',
        dataType: "json"/*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  usuariosService.DelUsuario = function(Codigo){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/DelUser',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  usuariosService.guardarUsuario = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarUser',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  } 

  usuariosService.resetPassword = function(userid, usuario){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/resetPassword',
        dataType: "json",
        data: {
          userid: userid,
          nomusuario: usuario,          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  usuariosService.personalFaltante = function(perscod){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/personalFaltanteUser',
        dataType: "json",
        data: {
          codigo: perscod                   
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  usuariosService.getUsuariosXPerfil = function(codigoperfil){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/usuariosXPerfil',
        dataType: "json",
        data: {
          codigoperfil: codigoperfil                   
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  export default usuariosService