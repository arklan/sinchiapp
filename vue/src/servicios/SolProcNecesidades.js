import axios from 'axios'
import configService from './config'

const SolProcNecesidades = {}

  SolProcNecesidades.getData = function(){

    return new Promise(function(resolve, reject) {axios({
        method:'GET',
        url: configService.apiUrl + '/SolProcNecesidades',
        dataType: "json"
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  SolProcNecesidades.detailSol = function(id){

    return new Promise(function(resolve, reject) {axios({
        method:'GET',
        url: configService.apiUrl + '/solicitudes/' + id,
        dataType: "json",
        /*,
        headers:authHeader*/
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  Solicitudes.save = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/saveSolicitud',
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  Solicitudes.updateProgres = function(data, id){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/updateSolicitud/' + id,
        dataType: "json",
        data: {
          data: data          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  Solicitudes.DelData = function(Codigo){

    /* return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/delPersonal',
        dataType: "json",
        data: {
          id: Codigo          
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    }) */

  }

  export default Solicitudes