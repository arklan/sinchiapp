import axios from 'axios'
import configService from './config'

const planesadquisicionService = {}

planesadquisicionService.getData = function(){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/PlanesAdquisicion',
      dataType: "json"
      /*,
      headers:authHeader*/
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

planesadquisicionService.getDataXConvenio = function(convenio){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/planesadquisicionXConvenio',
        dataType: "json",
        data: {
            convenio: convenio       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

  planesadquisicionService.getDataXProyecto = function(proyecto){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/planAdquisicionXProyecto',
        dataType: "json",
        data: {
          proyecto: proyecto       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }

  planesadquisicionService.guardarData = function(data,necesidades){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/guardarPlanAdquisicion',
        dataType: "json",        
        data: {
          data: data,
          necesidades: necesidades                     
        }
      })
      .then(function(res) {

        resolve(res)      
        
      })
      .catch(function(err) {     

        resolve(err)
           
      })
    })

  }

  /****************************** Necesidades ***************************************
  **********************************************************************************/
  planesadquisicionService.getNecesidadXPlanAdq = function(planadquisicion){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/NecesidadesXPlanAdq',
        dataType: "json",
        data: {
          planadquisicion: planadquisicion       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })
  
  }
  planesadquisicionService.getNecesidadesXSolicitud = function(id){

    return new Promise(function(resolve, reject) {
      axios.get(configService.apiUrl + '/NecesidadesXSolicitud', {
        params: {
          idSolicitud: id
        }
      })
        .then(function(res) {
          resolve(res)
        })
        .catch(function(err) {
          resolve(err)
        })
    })
  }

  planesadquisicionService.getSolProcNecesidades = function(id){

    return new Promise(function(resolve, reject) {
      axios.get(configService.apiUrl + '/solProcNecesidades', {
        params: {
          idSolProc: id
        }
      })
        .then(function(res) {
          resolve(res)
        })
        .catch(function(err) {
          resolve(err)
        })
    })
  }

  /******************************** Unspsc *****************************************
  **********************************************************************************/
 planesadquisicionService.UnspscXPlanAdq = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/UnspscXPlanAdq',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion               
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

  planesadquisicionService.getUnspscXNecesidad = function(planadquisicion,necesidad){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/UnspscXNecesidad',
        dataType: "json",
        data: {
          planadquisicion: planadquisicion,
          necesidad:necesidad       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })  
  }

  planesadquisicionService.addupdUnspsc = function(data){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/UnspscXNecesidadAddUpd',
        dataType: "json",
        data: {
          data: data      
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })  
  }

  planesadquisicionService.delUnspsc = function(planadquisicion,necesidad,unspsc){

    return new Promise(function(resolve, reject) {axios({
        method:'POST',
        url: configService.apiUrl + '/DelUnspscXNecesidad',
        dataType: "json",
        data: {
          planadquisicion: planadquisicion,
          necesidad:necesidad,
          unspsc:unspsc       
        }
      })
      .then(function(res) {
  
        resolve(res)      
        
      })
      .catch(function(err) {     
  
        resolve(err)
           
      })
    })  
  }

  /******************************** Actividades ************************************
  **********************************************************************************/
 planesadquisicionService.ActividadXPlanAdq = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ActividadXPlanAdq',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion               
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

 planesadquisicionService.getActividadXNecesidad = function(planadquisicion,necesidad){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ActividadXNecesidad',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion,
        necesidad:necesidad       
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.getActividadXPlanCategoria = function(planadquisicion,categoria){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ActividadXPlanCategoria',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion,
        categoria:categoria       
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })
}

planesadquisicionService.addupdActividad = function(data){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ActividadXNecesidadAddUpd',
      dataType: "json",
      data: {
        data: data      
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.delActividad = function(planadquisicion,necesidad,secuenciaactividad){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/DelActividadXNecesidad',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion,
        necesidad:necesidad,
        secuenciaactividad:secuenciaactividad       
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.updVoBoNecesidad = function(planadquisicion,codigo,tipo,vobo,observacion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ActualizarVoBoNecesidad',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion,
        codigo:codigo, 
        tipo:tipo,
        vobo:vobo,
        observacion:observacion     
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

//validaciones
planesadquisicionService.validaNecesidades = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ValidaNecesidades',
      dataType: "json",
      data: {        
        codigo:planadquisicion           
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.ValidaUnspsc = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ValidaUnspsc',
      dataType: "json",
      data: {        
        codigo:planadquisicion           
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.ValidaActividadNecesidad = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ValidaActividadNecesidad',
      dataType: "json",
      data: {        
        codigo:planadquisicion           
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.ValidaActividadCategoria = function(planadquisicion, basemoneda, tasa){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ValidaActividadCategoria',
      dataType: "json",
      data: {        
          codigo:planadquisicion,
          basemoneda:basemoneda,
          tasa:tasa             
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.enviarParaVoBo = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/enviarParaVoBo',
      dataType: "json",
      data: {        
        codigo:planadquisicion           
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

//vista financiera
planesadquisicionService.VistaFinNecesidades = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/VistaFinNecesidades',
      dataType: "json",
      data: {        
        codigo:planadquisicion           
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.VistaFinActNec = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/VistaFinActNec',
      dataType: "json",
      data: {        
        codigo:planadquisicion           
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.VistaFinActCat = function(planadquisicion, basemoneda, tasa){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/VistaFinActCat',
      dataType: "json",
      data: {        
          codigo:planadquisicion,
          basemoneda:basemoneda,
          tasa:tasa             
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.VistaFinResActividades = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/VistaFinResActividades',
      dataType: "json",
      data: {        
          codigo:planadquisicion                    
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.DelNecesidad = function(planadquisicion,necesidad){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/DelNecesidad',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion,
        necesidad:necesidad   
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.guardarDataTmp = function(data,necesidades,dataunspsc,dataactividades){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/DataAddTmp',
      dataType: "json",        
      data: {
        data: data,
        necesidades: necesidades,
        dataunspsc:dataunspsc,
        dataactividades:dataactividades                     
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })

}

planesadquisicionService.getNecesidadesXTmpPlanCategoria = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/TmpNecesidadesXTmpPlanAdq',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion        
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })
}

planesadquisicionService.getActividadXTmpPlanCategoria = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/TmpActividadXTmpPlanAdq',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion        
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })
}

planesadquisicionService.getUnspscXTmpPlanCategoria = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/TmpUnspscXTmpPlanAdq',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion        
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })
}

//versiones
planesadquisicionService.getVersiones = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/PlanAdqVersiones',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion        
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })
}

planesadquisicionService.getVersionActual = function(planadquisicion,version){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/PlanAdqVersionActual',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion,
        version: version         
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })
}

planesadquisicionService.getVersionesNecesidades = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/PlanAdqNecVersiones',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion        
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })
}

planesadquisicionService.getActividadXHisPlanCategoria = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/HisActividadXHisPlanAdq',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion        
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })
}

planesadquisicionService.getUnspscXHisPlanCategoria = function(planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/HisUnspscXHisPlanAdq',
      dataType: "json",
      data: {
        planadquisicion: planadquisicion        
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })
}

//vista TMP financiera
planesadquisicionService.VistaFinTmpNecesidades = function(codigo){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/VistaFinTmpNecesidades',
      dataType: "json",
      data: {        
        codigo:codigo           
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.VistaFinTmpActNec = function(codigo){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/VistaFinTmpActNec',
      dataType: "json",
      data: {        
        codigo:codigo           
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.VistaFinTmpActCat = function(codigo, basemoneda, tasa){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/VistaFinTmpActCat',
      dataType: "json",
      data: {        
          codigo:codigo,
          basemoneda:basemoneda,
          tasa:tasa             
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.VistaFinResTmpActividades = function(codigo){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/VistaFinResTmpActividades',
      dataType: "json",
      data: {        
          codigo:codigo                    
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.ActualizarTmpVoBo = function(codigo,tipo,vobo,observacion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/ActualizarTmpVoBo',
      dataType: "json",
      data: {
        planadquisicion: codigo,        
        tipo:tipo,
        vobo:vobo,
        observacion:observacion     
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

planesadquisicionService.creaVersion = function(tmpplanadquisicion,planadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/creaVersion',
      dataType: "json",
      data: {
        tmpplanadquisicion:tmpplanadquisicion,
        planadquisicion:planadquisicion     
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      reject(err)
         
    })
  })  
}

planesadquisicionService.delTemporalRechazo = function(tmpplanadquisicion){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/delTemporalRechazo',
      dataType: "json",
      data: {
        planadquisicion:tmpplanadquisicion
      }
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

//************************* rubro presupuestal *************************
planesadquisicionService.rubrosPresupuestales = function(convenio,nombreproyecto,codigoplan,codigonecesidad){

  return new Promise(function(resolve, reject) {axios({
      method:'POST',
      url: configService.apiUrl + '/RubrosPresupuestales',
      dataType: "json",    
      data: {
        convenio:convenio,
        nombreproyecto:nombreproyecto,
        codigoplan:codigoplan,
        codigonecesidad:codigonecesidad
      }  
    })
    .then(function(res) {

      resolve(res)      
      
    })
    .catch(function(err) {     

      resolve(err)
         
    })
  })  
}

export default planesadquisicionService