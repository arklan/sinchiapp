import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

	  avatar:"ImgH_blanco.png",
	  imgnotificacion:"notifications_none",
	  proyobjetivos:'0',
	  arrPlanAdqNecActividades:[],
	  arrPlanAdqNecUnspsc:[],
	  carreras: [],
	  solproc: {},
	  tipoOfertaEconomica: ['Media aritmética', 'Media aritmética alta', 'Media geométrica con presupuesto oficial', 'Menor valor'],
	  adjPersonales: [
        { docSolicitado: 'Visto bueno de la Directora General', tipo: 'solproc' },
        { docSolicitado: 'Cédula de ciudadanía', tipo: 'solproc' },
        { docSolicitado: 'Hoja de vida actualizada', tipo: 'solproc' },
        { docSolicitado: 'Soportes Académicos / Diplomas - Cursosl', tipo: 'solproc' },
        { docSolicitado: 'Soportes Laborales', tipo: 'solproc' },
        { docSolicitado: 'Tarjeta Profesional', tipo: 'solproc' },
        { docSolicitado: 'Libreta militar', tipo: 'solproc' },
        { docSolicitado: 'RUT actualizado', tipo: 'solproc' },
        { docSolicitado: 'Certificación Bancaria actualizada', tipo: 'solproc' },
        { docSolicitado: 'Examen de Medicina Ocupacional', tipo: 'solproc' },
        { docSolicitado: 'Certificado de Categoría Tributaria', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes Disciplinarios actualizados', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes Fiscales actualizados', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes Judiciales actualizados', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes del Registro Nacional de Medidas Correctivas', tipo: 'solproc' }
	  ],
	  adjBienes: [
        { docSolicitado: 'Cédula de ciudadanía', tipo: 'solproc' },
        { docSolicitado: 'Certificado de matrícula mercantil', tipo: 'solproc' },
        { docSolicitado: 'Propuesta económica y técnica', tipo: 'solproc' },
        { docSolicitado: 'Certificación de experiencia', tipo: 'solproc' },
        { docSolicitado: 'RUT actualizado', tipo: 'solproc' },
        { docSolicitado: 'Certificación Bancaria actualizada', tipo: 'solproc' },
        { docSolicitado: 'Certificado expedido por el contador o revisor fiscal, en el que se indique que se ha cumplido con el pago de Parafiscales de los últimos seis (6) meses', tipo: 'solproc' },
        { docSolicitado: 'Certificado de Categoría Tributaria', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes Disciplinarios actualizados', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes Fiscales actualizados', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes Judiciales actualizados', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes del Registro Nacional de Medidas Correctivas', tipo: 'solproc' }
	  ],
	  adjJuridica: [
        { docSolicitado: 'Certificado de Existencia y Representación', tipo: 'solproc' },
        { docSolicitado: 'Cédula de ciudadanía del Representante Legal', tipo: 'solproc' },
		{ docSolicitado: 'Autorización Representante Legal en caso de no tener la facultad para firmar (Cuando aplica)', tipo: 'solproc' },
		{ docSolicitado: 'Propuesta económica y técnica', tipo: 'solproc' },
        { docSolicitado: 'Certificación de experiencia (Cuando aplica)', tipo: 'solproc' },
        { docSolicitado: 'RUT actualizado', tipo: 'solproc' },
        { docSolicitado: 'Certificación Bancaria actualizada', tipo: 'solproc' },
        { docSolicitado: 'Certificado expedido por el contador o revisor fiscal, en el que se indique que se ha cumplido con el pago de Parafiscales de los últimos seis (6) meses', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes Disciplinarios actualizados', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes Fiscales actualizados', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes Judiciales actualizados', tipo: 'solproc' },
        { docSolicitado: 'Antecedentes del Registro Nacional de Medidas Correctivas del representante legal', tipo: 'solproc' }							
	  ],
	  tipoLista: { text: 'PERSONA NATURAL – PRESTACIÓN DE SERVICIOS / CONSULTORÍAS INDIVIDUALES', valor: 1 }
	  // -----------------------------------

  },

  getters: {

  	getAvatar:(state) => {

  		return state.avatar	

	},
	getNotificacion:(state) => {

		return state.imgnotificacion	

	},
	getProyObjetivo:(state) => {

		return state.proyobjetivos

	},
	getArrPlanAdqNecAct:(state) => {

		return state.arrPlanAdqNecActividades	

	},
	getArrPlanAdqNecUnspsc:(state) => {

		return state.arrPlanAdqNecUnspsc	

	  }

  },
  mutations: {
	// -------------------------------- mutaciones de proceso contratacion -----------------------
	setSolProc: (state, obj) => {
		state.solproc = obj
	},
	removeAdjCausales: (state, val) => {
		if (val === 'personales') {
			state.solproc.adjuntos.map(item => {
				const encontrado = state.adjPersonales.find(causal => causal.docSolicitado === item.docSolicitado)
				if (!encontrado) {
					state.solproc.adjuntos.splice(state.solproc.adjuntos.indexOf(item), 1)	
				}
			})
		} else if (val === 'bienes') {
			state.solproc.adjuntos.map(item => {
				const encontrado = state.adjBienes.find(causal => causal.docSolicitado === item.docSolicitado)
				if (!encontrado) state.solproc.adjuntos.splice(state.solproc.adjuntos.indexOf(item), 1)
			})
		} else if (val === 'juridica') {
			state.solproc.adjuntos.map(item => {
				const encontrado = state.adjJuridica.find(causal => causal.docSolicitado === item.docSolicitado)
				if (!encontrado) state.solproc.adjuntos.splice(state.solproc.adjuntos.indexOf(item), 1)
			})
		}
	},
	updatePropAdjuntos: (state, obj) => {
		const index = state.solproc.proponentes.findIndex(item => item.nombre === obj.proponente.nombre)
		Vue.set(state.solproc.proponentes[index].adjuntos, obj.index, obj.adjunto)
	},
	updateAdjuntos: (state, obj) => {
		Vue.set(state.solproc.adjuntos, obj.index, obj.adjunto)
	},

  	setAvatar:(state, sexo) => {

  		if(sexo == 'M')
  			state.avatar = "ImgH_blanco.png" //"img/ImgH_blanco.png"
  		else if (sexo == 'F')	 
  			state.avatar = "ImgM_blanco.png"
	},
	setNotificacion:(state, total) => {

		if(total > 0)
			state.imgnotificacion = "notifications_active" //"img/ImgH_blanco.png"
		else	 
			state.imgnotificacion = "notifications_none"
	},
	setProyObjetivo:(state, nuevoval) => {

		state.proyobjetivos = nuevoval

	},
	setArrPlanAdqNecAct:(state, nuevoval) => {

		state.arrPlanAdqNecActividades = nuevoval	

  	},
	AddArrPlanAdqNecAct:(state, nuevoval) => {
		
		console.log(nuevoval)

		state.arrPlanAdqNecActividades.push(nuevoval)	

	},
	ModArrPlanAdqNecAct:(state, item) => {
		
		state.arrPlanAdqNecActividades.forEach((itemactual, index) => {
			if (itemactual.necesidad === item.necesidad && itemactual.secuenciaactividad === item.secuenciaactividad) {
				Vue.set(state.arrPlanAdqNecActividades, index, item);
			}
		});

	},
	DelArrPlanAdqNecAct:(state, index) => {

		state.arrPlanAdqNecActividades.splice( index, 1 )	

	},
	setArrPlanAdqNecUnspsc:(state, nuevoval) => {

		state.arrPlanAdqNecUnspsc = nuevoval	

  	},
	AddArrPlanAdqNecUnspsc:(state, nuevoval) => {

		state.arrPlanAdqNecUnspsc.push(nuevoval)

	},
	ModArrPlanAdqNecUnspsc:(state, item) => {
		
		state.arrPlanAdqNecUnspsc.forEach((itemactual, index) => {
			if (itemactual.necesidad === item.necesidad && itemactual.unspsc === item.unspsc) {
				Vue.set(state.arrPlanAdqNecUnspsc, index, item);
			}
		});

	},
	DelArrPlanAdqNecUnspsc:(state, index) => {

		state.arrPlanAdqNecUnspsc.splice( index, 1 )	

	},

  },
  actions: {

  }
})