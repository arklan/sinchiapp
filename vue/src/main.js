import '@babel/polyfill'
import Vue from 'vue'
import vuetify from './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import VueSession from 'vue-session'
import Vuelidate from 'vuelidate'
import Toasted from 'vue-toasted'
import VueCurrencyFilter from 'vue-currency-filter'
import money from 'v-money'
import JsonExcel from 'vue-json-excel'
import './plugins/chartist'

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'

import VueBarcode from 'vue-barcode'

Vue.use(Vuelidate)
Vue.use(VueBarcode)
Vue.use(Toasted)
Vue.use(VueCurrencyFilter)
Vue.use(money)
Vue.use(require('vue-chartist'))
Vue.component('downloadExcel', JsonExcel)
Vue.component('barcode', VueBarcode)

var options = {
  persist: true
}

Vue.use(VueSession, options)

Vue.config.productionTip = false

new Vue({
  router,
  store,  
  vuetify,
  render: h => h(App)
}).$mount('#app')
