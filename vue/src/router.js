import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import NotFoundView from './components/NotFoundView.vue'
import Usuarios from './components/CUsuarios.vue'
import Perfiles from './components/CPerfiles.vue'
import Bitacora from './components/CBitacora.vue'
import Areas from './components/CAreas.vue'
import Cargos from './components/CCargos.vue'
import Categorias from './components/CCategorias.vue'
import TiposIde from './components/CTiposIde.vue'
import TiposEstado from './components/CTiposEstado.vue'
import Estados from './components/CEstados.vue'
import ProgInvesPenia from './components/CProyInvesPenia.vue'
import ProgInvesPei from './components/CProgInvesPei.vue'
import Poas from './components/CPoas.vue'
import Proyectos from './components/CProyectos.vue'
import Convenios from './components/CConvenios.vue'
import ConveniosMarco from './components/CConveniosMarco.vue'

import Capitulos from './components/CCapitulos.vue'
import CausalesContratacion from './components/CCausalesContratacion.vue'
import CriteriosEvaluacion from './components/CCriteriosEvaluacion.vue'
import FormasPago from './components/CFormasPago.vue'
import LineasEstrategicas from './components/CLineasEstrategicas.vue'
import LineaInvPei from './components/CLineaInvPei.vue'
import LineaInvPenia from './components/CLineaInvPenia.vue'
import LineasInvPicia from './components/CLineasInvPicia.vue'
import Metas from './components/CMetas.vue' 
import MetodosSeleccion from './components/CMetodosSeleccion.vue'
import Objetivos from './components/CObjetivos.vue'
import Ods from './components/COds.vue'
import Personal from './components/CPersonal.vue'
import Roles from './components/CRoles.vue'
import TiposContratacion from './components/CTiposContratacion.vue'
import TiposContrato from './components/CTiposContrato.vue'
import TiposNovedad from './components/CTiposNovedad.vue'
import Unspsc from './components/CUnspsc.vue'
import IndicadoresPicia from './components/CIndicadoresPicia.vue'
import AccionesPicia from './components/CAccionesPicia.vue'
import Presupuestos from './components/CPresupuestos.vue'

import PilaresPicia from './components/CPilaresPicia.vue'
import CategoriasGasto from './components/CCategoriasGasto.vue'
import PlanesAdquisicion from './components/CPlanesAdquisicion.vue'

import Desplazamientos from './components/CDesplazamientos.vue'
import vSolContrataciones from './components/solProcContrata/vSolContrataciones.vue'
import TarifasLugares from './components/CTarifasLugares.vue'
import vSolicitud from './components/solProcContrata/vSolicitud.vue'
import vistaInformes from './components/informes/vistaInformes.vue'
import VueRouter from 'vue-router'


import Reuniones from './components/CReuniones.vue'
import Sede from './components/CSede.vue'

import vpqr from './components/pqrs/vpqr.vue'
import detailpqr from './components/pqrs/detailpqr.vue'

import ResguardosIndigenas from './components/CResguardosIndigenas.vue'
import Asociaciones from './components/CAsociaciones.vue'

Vue.use(VueRouter)

const routes = [
    {
      path: '/myapp',
      name: 'home',
      component: Home
    },
    {
      path: '/',
      name: 'Login',
      component: Login
    },    
    {
      path: '/Usuarios', 
      component: Usuarios,
      name: 'Usuarios',      
    },
    {
      path: '/Bitacora', 
      component: Bitacora,
      name: 'Bitacora',      
    },
    {
      path: '/Perfiles', 
      component: Perfiles,
      name: 'Perfiles',      
    },
    {
      path: '/Areas', 
      component: Areas,
      name: 'Areas',      
    },
    {
      path: '/Cargos', 
      component: Cargos,
      name: 'Cargos',      
    },
    {
      path: '/Categorias', 
      component: Categorias,
      name: 'Categorias',      
    },
    {
      path: '/TiposIde', 
      component: TiposIde,
      name: 'TiposIde',      
    },
    {
      path: '/TiposEstado', 
      component: TiposEstado,
      name: 'TiposEstado',      
    },
    {
      path: '/Estados', 
      component: Estados,
      name: 'Estados',      
    },
    {
      path: '/ProgInvesPenia', 
      component: ProgInvesPenia,
      name: 'ProgInvesPenia',      
    },
    {
      path: '/ProgInvesPei', 
      component: ProgInvesPei,
      name: 'ProgInvesPei',      
    },
    {
      path: '/Poas', 
      component: Poas,
      name: 'Poas',      
    },
    {
      path: '/Proyectos', 
      component: Proyectos,
      name: 'Proyectos',
      props: true      
    },
    {
      path: '/Convenios', 
      component: Convenios,
      name: 'Convenios',      
    },
    {
      path: '/ConveniosMarco', 
      component: ConveniosMarco,
      name: 'ConveniosMarco',      
    },
    
    {
      path:'/Capitulos', 
        component: Capitulos,
        name: 'Capitulos',
    },
    {
      path:'/CausalesContratacion', 
        component: CausalesContratacion,
        name: 'CausalesContratacion',
    },
    {
      path:'/FormasPago', 
        component: FormasPago,
        name: 'FormasPago',
    },
    
    {
      path:'/LineasEstrategicas', 
        component: LineasEstrategicas,
        name: 'LineasEstrategicas',
    },
    {
      path:'/Metas', 
        component: Metas,
        name: 'Metas',
    },
    {
      path:'/MetodosSeleccion', 
        component: MetodosSeleccion,
        name: 'MetodosSeleccion',
    },
    {
      path:'/Objetivos', 
        component: Objetivos,
        name: 'Objetivos',
    },    
    {
      path:'/Ods', 
        component: Ods,
        name: 'Ods',
    },
    {
      path:'/Personal', 
        component: Personal,
        name: 'Personal',
    },    
    {
      path:'/Roles', 
        component: Roles,
        name: 'Roles',
    },
    {
      path:'/Unspsc', 
        component: Unspsc,
        name: 'Unspsc',
    },
    {
      path:'/CriteriosEvaluacion', 
        component: CriteriosEvaluacion,
        name: 'CriteriosEvaluacion',
    },  
    {
      path:'/LineaInvPei', 
        component: LineaInvPei,
        name: 'LineaInvPei',
    },
    {
      path:'/LineaInvPenia', 
        component: LineaInvPenia,
        name: 'LineaInvPenia',
    },  
    {
      path:'/LineasInvPicia', 
        component: LineasInvPicia,
        name: 'LineasInvPicia',
    },  
    {
      path:'/TiposContratacion', 
        component: TiposContratacion,
        name: 'TiposContratacion',
    },
    {
      path:'/TiposContrato', 
        component: TiposContrato,
        name: 'TiposContrato',
    },
    {
      path:'/TiposNovedad', 
        component: TiposNovedad,
        name: 'TiposNovedad',
    },  
    {
      path:'/IndicadoresPicia', 
        component: IndicadoresPicia,
        name: 'IndicadoresPicia',
    },
    {
      path:'/AccionesPicia', 
        component: AccionesPicia,
        name: 'AccionesPicia',
    },
    {
      path:'/Presupuestos', 
        component: Presupuestos,
        name: 'Presupuestos',
    },
    {
      path:'/PilaresPicia', 
        component: PilaresPicia,
        name: 'PilaresPicia',
    },
    {
      path:'/CategoriasGasto', 
        component: CategoriasGasto,
        name: 'CategoriasGasto',
    },        
    {   
        path:'/PlanesAdquisicion', 
        component: PlanesAdquisicion,
        name: 'PlanesAdquisicion',
    },
    {   
      path:'/Desplazamientos', 
      component: Desplazamientos,
      name: 'Desplazamientos',
    },
    {
      path: '/SolicitudesContratacion',
      component: vSolContrataciones,
      name: 'vSolContrataciones'
    },
    {
      path: '/SolicitudesContratacion/:id',
      component: vSolicitud,
      name: 'vSolicitud'
    },
    {
      path: '/informes',
      component: vistaInformes,
      name: 'vistaInformes'
    },
    {
      path: '/TarifasLugares',
      component: TarifasLugares,
      name: 'TarifasLugares'     
    },
    {
      path:'/Reuniones', 
        component: Reuniones,
        name: 'Reuniones',
    },
    {
      path:'/Sede', 
        component: Sede,
        name: 'Sede',
    },
    {
      path:'/pqrs', 
        component: vpqr,
        name: 'vpqr',
    },
    {
      path:'/pqrs/:id', 
        component: detailpqr,
        name: 'detailpqr',
    },
    {
      path: '/ResguardosIndigenas',
      component: ResguardosIndigenas,
      name: 'ResguardosIndigenas'     
    },
    {
      path: '/Asociaciones',
      component: Asociaciones,
      name: 'Asociaciones'     
    },
    /*
    { 
        path: '/Proyectos/:tipo', 
        component: SolicitaModProyectos,
        name: 'SolicitaModProyectos',
        props: true    
        //path:'/SolicitaModProyectos', 
        //component: SolicitaModProyectos,
        //name: 'SolicitaModProyectos',
    },*/
    {
      path: '*',
      redirect: '/myapp'
    }
]

const router = new VueRouter({
  // mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/', '/register'];
  const authRequired = !publicPages.includes(to.path);
  const self = Vue.prototype
  const loggedIn = self.$session.exists()
  
  if (authRequired && !loggedIn) {
    return next('/') 
  }
  next()

})

export default router