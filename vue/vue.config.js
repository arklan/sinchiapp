module.exports = {
  lintOnSave: false,
  publicPath: process.env.NODE_ENV === 'production'  ? '/' : '/',
  pwa: {
      themeColor: '#943A4A'
  }
};
